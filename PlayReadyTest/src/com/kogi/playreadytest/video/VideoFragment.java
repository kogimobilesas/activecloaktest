package com.kogi.playreadytest.video;

import java.io.IOException;
import java.util.List;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import com.irdeto.media.ActiveCloakAgent;
import com.irdeto.media.ActiveCloakDeviceIdChangedListener;
import com.irdeto.media.ActiveCloakEventListener;
import com.irdeto.media.ActiveCloakEventType;
import com.irdeto.media.ActiveCloakException;
import com.irdeto.media.ActiveCloakLocaleOption;
import com.irdeto.media.ActiveCloakMediaPlayer;
import com.irdeto.media.ActiveCloakOutputRestrictions;
import com.irdeto.media.ActiveCloakUrlType;
import com.kogi.playreadytest.R;
import com.kogi.playreadytest.data.ACMS_ContentDescriptor;

public class VideoFragment extends Fragment {

	public static final boolean DEBUG = true;
	// UI components specifying the location where the video will play. These
	// are defined externally in the layout XML file but we keep a local
	// reference here in order to associate them with the media player.
	private SurfaceView mVideoSurface;
	private FrameLayout mVideoFrame;

	// Local handles on the agent and media player. These are instantiated
	// each time this activity is created, e.g. every time a video is played.
	private ActiveCloakAgent mActiveCloakAgent = null;
	private ActiveCloakMediaPlayer mActiveCloakMediaPlayer = null;

	// Content manager for license acquisition.
//	private ActiveCloakContentManager mActiveCloakContentManager;

	private ACMS_ContentDescriptor mContentDescriptor;
	private int mPositionToRestore = 0;

	// Increment for each seek, decrement when POSITION_CHANGED event arrives.
	private int mSeeking = 0; 

    // Whether the video is downloadable; indicates potential progressive download.
    private boolean mDownloadableVideo = false;
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initVars();
	}

	private void initVars() {
//		Uri path = Uri.parse("android.resource://com.kogi.playreadytest/raw/test");
		
//		mContentDescriptor = new ACMS_ContentDescriptor("Big Buck Bunny", "http://iis7test.entriq.net/ActiveCloak/Encrypted/Bunny.m3u8.prdy", ActiveCloakUrlType.HLS, false);
		mContentDescriptor = new ACMS_ContentDescriptor("Biplop", "http://demo.irdeto.com/Test/bipbop/hls/clear/bipbopall.m3u8", ActiveCloakUrlType.HLS, true);
//		mContentDescriptor = new ACMS_ContentDescriptor("uri", path.toString(), ActiveCloakUrlType.HLS, true);
		mContentDescriptor = new ACMS_ContentDescriptor("Nasa", "http://www.nasa.gov/multimedia/nasatv/NTV-Public-IPS.m3u8", ActiveCloakUrlType.HLS, false);
		
		
		// Our exception handling extends to catching ActiveCloakExceptions
		// thrown by our classes, and then any general exceptions. In both
		// cases this application simply throws up a dialog reporting the
		// exception, but other handling can be put in as appropriate.
		try {
			// Create the agent.
			mActiveCloakAgent = new ActiveCloakAgent(getActivity(),
					new ActiveCloakDeviceIdChangedListener() {

						@Override
						public void deviceIdChanged(String previousDeviceId,
								String currentDeviceId) {
						}
					});

//			mActiveCloakContentManager = new ActiveCloakContentManager(mActiveCloakAgent);
//			
//			String contentHeader = mActiveCloakContentManager.getContentHeader(
//					mContentDescriptor.getUrl(), mContentDescriptor.getType());
//			mActiveCloakContentManager.acquireLicense(contentHeader,
//					new ACMS_SendUrlRequestListener(null, mActiveCloakAgent,
//							getActivity()), new ActiveCloakEventListener() {
//						@Override
//						public void onEvent(ActiveCloakEventType eventType,
//								long result1, long result2, long result3,
//								String resultString) {
//							Log.d(getTag(), "LicenseManager event: "
//									+ eventType.toString() + "\n");
////							ACMS_LogService.writeToLog(getActivity(), "LicenseManager event: "
////									+ eventType.toString() + "\n");
//						}
//
//					}, "TestCustomData" // simple custom data (ignored
//										// by test servers, but real
//										// servers may use)
//			);
			
			// Create the player
			mActiveCloakMediaPlayer = new ActiveCloakMediaPlayer(
					mActiveCloakAgent);
			
			// Reset performance capturing information.  
			mActiveCloakAgent.resetPerfReport();
		} catch (ActiveCloakException e) {
			Log.e(getTag(), 
					"An ActiveCloakException was thrown by the PlayerActivity.\n"
							+ "Result = " + e.getResult() + "\n"
							+ "Message = " + e.getMessage());
		}		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.video_fragment, container, false);
		initViews(v);
		initListeners();
		initData();
		return v;
	}

	private void initViews(View v) {
		mVideoSurface = (SurfaceView) v
				.findViewById(R.id.VideoViewOutput_Surface);
		mVideoFrame = (FrameLayout) v.findViewById(R.id.VideoViewOutput_Frame);

	}

	private void initListeners() {
		// TODO Auto-generated method stub

	}

	private void initData() {
		
		// Set up the display
		mActiveCloakMediaPlayer.setupDisplay(mVideoSurface,
				mVideoFrame);
		
		// Example of how to limit bitrate to 100K or less. It is also possible
		// to set a minimum bitrate using setMinBitrate()
		// mActiveCloakMediaPlayer.setMaxBitrate(100);

		// Set the initial value of the closed caption property.
//		mActiveCloakMediaPlayer.setClosedCaptionEnabled(mClosedCaptionEnabled); 
					
		// Start playback in streaming/local file cases.
		// If we are doing a progressive download, a separate task will begin playback.
		if( mDownloadableVideo == false ) {
			play();	
		}
	}

	
	/**
	 * Helper to play content; opens the player, sets up the graphical information,
	 * and plays.
	 */
	private void play() {
		try {
			final String licenseOverrideUrl = null;
			
//			writeToLog("Now beginning playback of content "
//					+ mContentDescriptor.getLabel()
//					+ " with URL "
//					+ mContentDescriptor.getUrl()
//					+ "\n");
			
			long totalContentLength = 0;
//			if( mDownloaderService != null ) {
//				totalContentLength = mDownloaderService.getTotalFileSize(mContentDescriptor);
//			}
						
			// Open the media player. 
			
			mActiveCloakMediaPlayer.open(
					new ACMS_SendUrlRequestListener(
							licenseOverrideUrl, mActiveCloakAgent, getActivity()),
					new MyActiveCloakEventListener(mContentDescriptor, this), 
					mContentDescriptor.getType(), 
					mContentDescriptor.getUrl(),
					null,
					mPositionToRestore,
					totalContentLength
					);
			
			// Make the video frame and surface visible. If this is not done
			// the video is not visible.
			mVideoFrame.setVisibility(View.VISIBLE);
			mVideoSurface.setVisibility(View.VISIBLE);

			// Play the video.
			mActiveCloakMediaPlayer.play();
			
			
				
		} catch (ActiveCloakException e) {
			String errorMessage = "An ActiveCloakException was thrown by the PlayerActivity.\n"
					+ "Result = " + e.getResult() + "\n"
					+ "Message = " + e.getMessage();
			Log.e(getTag(), errorMessage);
//			writeToLog(errorMessage);
//			exitWithMessage(errorMessage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}



	// Common implementation of ActiveCloakEventListener 
	protected static class MyActiveCloakEventListener implements
			ActiveCloakEventListener {

		// Local handles on the content being played and the calling activity, for
		// logging purposes.
		private ACMS_ContentDescriptor content;
		private VideoFragment ctxFragment;
		
		// The last event type received.
		private ActiveCloakEventType lastEventType;
		
		MyActiveCloakEventListener(ACMS_ContentDescriptor content, VideoFragment ctxFragment) {
			this.content = content;
			this.ctxFragment = ctxFragment;
		}

		@Override
		public void onEvent(ActiveCloakEventType eventType, long result1,
				long result2, long result3, String resultString) {

			// When DRM decryption fails once it tends to fail over and over. To prevent 
			// flooding the log, ignore any consecutive event notifications of this type.
			if( lastEventType == ActiveCloakEventType.DRM_DECRYPT_FAILED
					&& eventType == ActiveCloakEventType.DRM_DECRYPT_FAILED) {
				return;
			}
	
			lastEventType = eventType;
			
			// Log the event.
			Log.d(ctxFragment.getTag(), "Event triggered for "
					+ content.getLabel() + ": " + eventType.toString() + "\n");
//			ctxFragment.writeToLog("Event triggered for "
//					+ content.getLabel() + ": " + eventType.toString() + "\n");
						
			if (eventType == ActiveCloakEventType.LICENSE_UPDATE_FAILED) {
				try {
					if (ctxFragment.mActiveCloakMediaPlayer.isOpen()) {
						ctxFragment.mActiveCloakMediaPlayer.close();
					}
				} catch (ActiveCloakException e) {
					Log.e(ctxFragment.getTag(), "An ActiveCloakException was thrown by the PlayerActivity.\n"
							+ "Result = " + e.getResult() + "\n"
							+ "Message = " + e.getMessage());
//					UIUtils.showDialog(ctxFragment,
//							"An ActiveCloakException was thrown by the PlayerActivity.\n"
//									+ "Result = " + e.getResult() + "\n"
//									+ "Message = " + e.getMessage());
				}
			} else if (eventType == ActiveCloakEventType.POSITION_CHANGED) {
				ctxFragment.mSeeking = 0; // We are no longer seeking.
			} else if (eventType == ActiveCloakEventType.PROVISION_NEEDED) {
				// Nothing, for now.
			} else if( eventType == ActiveCloakEventType.MULTIPLE_AUDIO_STREAMS) {
				try {
					// If multiple audio options exist, add the Audio button to the 
					// viewable screen.
					List<ActiveCloakLocaleOption> audioOptions = ctxFragment.mActiveCloakMediaPlayer.getAvailableAudioOptions();
					Log.d(ctxFragment.getTag(), "Number of audio options = " + audioOptions.size() + "\n");
//					ctxFragment.writeToLog( "Number of audio options = " + audioOptions.size() + "\n");
					for( ActiveCloakLocaleOption audioOption : audioOptions) {
						Log.d(ctxFragment.getTag(), "Audio Option: " + audioOption.getLanguageName() + "\n");
//						ctxFragment.writeToLog( "Audio Option: " + audioOption.getLanguageName() + "\n");
					}
					if( ctxFragment.mActiveCloakMediaPlayer.getAvailableAudioOptions().size() > 1 ) {
						// UI updates have to run on the UI thread.
						ctxFragment.getActivity().runOnUiThread(new Runnable() 
						{
							public void run() 
							{ 
								Button audioButton = (Button) ctxFragment.getView().findViewById(R.id.controlsAudioButton);
								audioButton.setVisibility(Button.VISIBLE);
							}
						}); 
					}
				} catch( ActiveCloakException e ) {
					Log.e(ctxFragment.getTag(), "An ActiveCloakException was thrown by the PlayerActivity.\n"
							+ "Result = " + e.getResult() + "\n"
							+ "Message = " + e.getMessage());
//					UIUtils.showDialog(ctxFragment,
//							"An ActiveCloakException was thrown by the PlayerActivity.\n"
//									+ "Result = " + e.getResult() + "\n"
//									+ "Message = " + e.getMessage());
				}
			} else if( eventType == ActiveCloakEventType.MULTIPLE_SUBTITLE_STREAMS) {
				try {
					// If multiple subtitle options exist, add the Subs button to the 
					// viewable screen.
					List<ActiveCloakLocaleOption> subtitleOptions = ctxFragment.mActiveCloakMediaPlayer.getAvailableSubtitleOptions();
					Log.d(ctxFragment.getTag(), "Number of subtitle options = " + subtitleOptions.size() + "\n");
//					ctxFragment.writeToLog( "Number of subtitle options = " + subtitleOptions.size() + "\n");
					for( ActiveCloakLocaleOption subtitleOption : subtitleOptions) {
						Log.d(ctxFragment.getTag(),  "Subtitle Option: " + subtitleOption.getLanguageName() + "\n");
//						ctxFragment.writeToLog( "Subtitle Option: " + subtitleOption.getLanguageName() + "\n");
					}
					if( ctxFragment.mActiveCloakMediaPlayer.getAvailableSubtitleOptions().size() > 1 ) {
						// UI updates have to run on the UI thread.
						ctxFragment.getActivity().runOnUiThread(new Runnable() 
						{
							public void run() 
							{ 
								Button subsButton = (Button) ctxFragment.getView().findViewById(R.id.controlsSubtitleButton);
								subsButton.setVisibility(Button.VISIBLE);
							}
						}); 
					}
				} catch( ActiveCloakException e ) {
					Log.e(ctxFragment.getTag(), 
							"An ActiveCloakException was thrown by the PlayerActivity.\n"
									+ "Result = " + e.getResult() + "\n"
									+ "Message = " + e.getMessage());
//					UIUtils.showDialog(ctxFragment,
//							"An ActiveCloakException was thrown by the PlayerActivity.\n"
//									+ "Result = " + e.getResult() + "\n"
//									+ "Message = " + e.getMessage());
				}
			} else if( eventType == ActiveCloakEventType.PLAYBACK_COMPLETED ) {
				Log.e(ctxFragment.getTag(), "Playback complete."); 	
//				ctxFragment.exitWithMessage("Playback complete."); 			
			} else if( eventType == ActiveCloakEventType.EXTERNAL_DISPLAY_RESTRICTION_CHANGED) {
				if( DEBUG ) {
					try {
						ActiveCloakOutputRestrictions opl = ctxFragment.mActiveCloakMediaPlayer.getOutputRestrictions();
						Log.d(ctxFragment.getTag(), "New OPL Levels: " +
								" AV = " + opl.getAnalogVideo() +
								" CDA = " + opl.getCompressedDigitalAudio() +
								" CDV = " + opl.getCompressedDigitalVideo() +
								" UDA = " + opl.getUncompressedDigitalAudio() +
								" UDV = " + opl.getUncompressedDigitalVideo() + "\n");
//						ctxFragment.writeToLog("New OPL Levels: " +
//						" AV = " + opl.getAnalogVideo() +
//						" CDA = " + opl.getCompressedDigitalAudio() +
//						" CDV = " + opl.getCompressedDigitalVideo() +
//						" UDA = " + opl.getUncompressedDigitalAudio() +
//						" UDV = " + opl.getUncompressedDigitalVideo() + "\n");
					} catch (ActiveCloakException e1) {
						Log.d(ctxFragment.getTag(), "Error trying to get new OPL levels.\n");
//						ctxFragment.writeToLog("Error trying to get new OPL levels.\n");
					}
				}
			} else if( eventType == ActiveCloakEventType.HOST_NOT_FOUND_ERROR ) {
				// Handling of events that are fatal to playback.
				Log.e(ctxFragment.getTag(), "Playback failed due to event: " + eventType.toString());
//				ctxFragment.exitWithMessage("Playback failed due to event: " + eventType.toString());
			}
		}
	}

	
	/*
	 * This is called both when the player is first opened, and when it is switched
	 * back to.
	 * 
	 * (non-Javadoc)
	 * @see android.app.Activity#onStart()
	 */
	@Override
	public void onStart() {
		super.onStart();
		
		// If the user is switching back to the app, and the video was paused, resume it.
		try {
			if (mActiveCloakMediaPlayer != null
					&& mActiveCloakMediaPlayer.isOpen()
					&& !mActiveCloakMediaPlayer.isPlaying()) {
				mActiveCloakMediaPlayer.play();
//				Button playPauseButton = (Button) findViewById(R.id.controlsPlayPauseButton);
//				playPauseButton.setText("Pause");
			}
		} catch (ActiveCloakException e) {

		}
	}
	
	/*
	 * This is called both when the Back button is hit, and when the application is
	 * switched away from.
	 * 
	 * (non-Javadoc)
	 * @see android.app.Activity#onStop()
	 */
	@Override
	public void onStop() {
		// If the user switches away, pause the video. If the activity is being
		// destroyed there is no harm in pausing the player prior to closing it.
		try {
			mActiveCloakMediaPlayer.pause();
		} catch( ActiveCloakException e ) {
			
		}
		
		super.onStop();
	}

	
	/*
	 * This is called when the Back button is hit or when the minimized activity
	 * is destroyed by the Android OS.
	 * 
	 * (non-Javadoc)
	 * @see android.app.Activity#onDestroy()
	 */
	@Override
	public void onDestroy() {
		
		// Close the media player.
		closeCommon();

		super.onDestroy();
	}
	
	/**
	 * Common code for closing the media player, and logging the performance information.
	 */
	private void closeCommon() {
		if (mActiveCloakMediaPlayer.isOpen()) {
			try {
				// Close the media player.
				mActiveCloakMediaPlayer.close();
				
				// Make the video frame and surface visible. If this is not done
				// the video is not visible.
				mVideoFrame.setVisibility(View.INVISIBLE);
				mVideoSurface.setVisibility(View.INVISIBLE);
			
//				if( PERFORMANCE_LOGGING ) {
					// Write performance information to the log. This can be skipped in
					// cases where the information is not useful.
					mActiveCloakAgent.dumpPerfReport();
//					InputStream is = openFileInput("cwsperf.log");
//					byte[] buffer = new byte[is.available()];
//					is.read(buffer);
//					String bufferString = new String(buffer);
					Log.d(getTag(), "Performance report for: "
							+ mContentDescriptor.getLabel() + "\n" + 
//							bufferString+ 
							"\n\n\n");
//					writeToLog("Performance report for: "
//							+ mContentDescriptor.getLabel() + "\n" + bufferString
//							+ "\n\n\n");
//				} 
			} catch (ActiveCloakException e) {
				Log.e(getTag(), "ActiveCloakException thrown trying to close the media player.");
//				UIUtils.showDialog(this, "ActiveCloakException thrown trying to close the media player.");
//			} catch (FileNotFoundException e) {
////				UIUtils.showDialog(this, "FileNotFoundException thrown trying to retrieve performance information." );
//			} catch (IOException e) {
////				UIUtils.showDialog(this, "IOException thrown trying to retrieve performance information.");
			}
		}
	}
}
