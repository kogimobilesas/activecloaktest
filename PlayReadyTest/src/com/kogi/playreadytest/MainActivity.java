package com.kogi.playreadytest;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.KogiActivity;

import com.kogi.playreadytest.video.VideoFragment;

public class MainActivity extends KogiActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initListeners() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initVars() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initViews(Bundle arg0) {

		FragmentTransaction ft = getSupportFragmentManager()
				.beginTransaction();
		ft.replace(R.id.content_frame, new VideoFragment());
		ft.commit();
	}

}
