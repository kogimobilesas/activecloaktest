#!/bin/bash 

#
# This script is to build an Android application with
# Irdeto ActiveCloak (IAC) and Java ActiveCloak (JAC) protection.
#
# The script performs the following steps:
# 
# (1) parses parameters and verifies the build environment
# (2) determines production distribution location
# (3) checks for a device or an emulator existence,
#     creates and runs avd if device or emulator not found (Production Only)
# (4) creates secure stores
# (5) copies JACRuntime.jar, cws.jar, libcws.so (Production Only),
#     libcws_ps.so (Development Only) to the application
#     performs IAC signing on libcws.so (Production Only),
#     libcws_ps.so (Development Only)
# (6) copies secure store files to the application project
# (7) builds the target Android application using either
#      - a user-defined pre-build command
#      - or, by default, the standard Apache Ant tool preparing a key store,
#        build files, and updating the target Android project with
#        'android update' command before running the tool
# (8) performs JAC signing (Production Only)
# (9) stops the emulator, if it was launched from the script, deletes avd
# (10) re-builds application after JAC signing using either
#      - a user-defined post-build command
#      - or, by default, the standard Apache Ant tool


# a constant string indicating an empty option in array
_EMPTY_VALUE="<empty>"

# logs out processing step information
# Parameters:
#   ${1} - number of the step
#   ${2} - title of the step
step() {
  echo ""
  echo "#"
  echo "# (${1}) ${2}"
  echo "#" 
  echo "" 
}

# removes content of a  directory
# if the directory exists
# Parameters:
#   ${1} - path to the directory to clean up
remove_dir_content() {
  if [ -d "${1}" ]; then 
    rm -rf "${1}"/*
  fi
}

# removes file if it exists
# Parameters: 
#   ${1} - file path to remove
#
remove_file() {
  if [ -f "${1}" ]; then 
    rm -f "${1}"
  fi
}

# verifies if a variable is set
# Parameters:
#   ${1} - name of variable
#   ${2} - value of the variable
varTest()
{
    if [ -z $2 ]
    then
        echo "$1 is not set."
        exit -1
    fi
}

# returns an unknown parameter error 
# message and shows help/usage info
# Parameters:
#   ${1} - name or value of the unknown parameter
#   ${2} - name of the script file
UnknownParameterError()
{
  echo ""
  echo "Error: \"${1}\" is an unknown parameter"
  _ERRORCODE=1
  ShowHelp ${2}
}

# shows the script usage information and exits
# from the script.
# Parameters:
#    ${1} - name of the script to be consistent with
#
ShowHelp()
{
  echo ""
  echo "The purpose of the script is to build an Android application with"
  echo "Irdeto ActiveCloak (IAC) and Java ActiveCloak (JAC) protection."
  echo ""
  echo "The script performs the following steps:"
  echo "" 
  echo "  (1) parses parameters and verifies the build environment"
  echo "  (2) determines production distribution location"
  echo "  (3) checks for a device or an emulator existence,"
  echo "      creates and runs avd if device or emulator not found (Production Only)"
  echo "  (4) creates secure stores"
  echo "  (5) copies JACRuntime.jar, cws.jar, libcws.so (Production Only),"
  echo "      libcws_ps.so (Development Only) to the application"
  echo "      performs IAC signing on libcws.so (Production Only) and"
  echo "      libcws_ps.so (Development Only)"
  echo "  (6) copies secure store files to the application project"
  echo "  (7) builds the target Android application using either"
  echo "       - a user-defined pre-build command"
  echo "       - or, by default, the standard Apache Ant tool preparing a key store,"
  echo "         build files, and updating the target Android project with "
  echo "         'android update' command before running the tool"
  echo "  (8) performs JAC signing (Production Only)"
  echo "  (9) stops the emulator, if it was launched from the script, deletes avd"
  echo "  (10) re-builds application after JAC signing using either"
  echo "       - a user-defined post-build command"
  echo "       - or, by default, the standard Apache Ant tool" 
  echo ""
  echo " Usage:"
  echo ""
  echo " ${1} mandatory-parameters optional-parameters"
  echo ""
  echo "  Mandatory parameters:"
  echo ""
  echo "    (-aname | -an) <application name>"
  echo "                      - this is a mandatory parameter with name of the application"
  echo "                        to build. It is equal to the application APK name"
  echo "                        without the \".apk\" extension. It should also not include any"
  echo "                        extra suffixes that are appended by the Android build process."
  echo "                        The script needs the name to JAC-sign the final APK file correctly."
  echo ""
  echo "    (-aproj | -ap) <application project location>"
  echo "                      - this is a mandatory parameter with absolute or relative path"
  echo "                        to the directory that contains the application project to build."
  echo "                        The script needs this location to build the application for JAC-signing."
  echo ""
  echo "  Optional parameters:"
  echo ""
  echo "    (-apkname | -apk) <full application name>"
  echo "                      - this is an optional parameter with name of the APK file to build."
  echo "                        It is equal to the full application APK name without the location path."
  echo "                        By default the script use an application name with '-release.apk' at the end."
  echo "                        This parameter is useful when you build your application when the pre-build" 
  echo "                        and post-build commands are different from the default ones."
  echo ""
  echo "    (-aprojoutput | -apo) <application project output location>"
  echo "                      - this is an optional parameter with absolute or relative path"
  echo "                        to the directory where the build system stores the compiled apk file."
  echo "                        By default it is a path to the <application project location>/'bin' directory."
  echo ""
  echo "    (-cwstreaming | -cws) <CloakwareStreaming distribution location>"
  echo "                      - this is an optional parameter of the CloakwareStreaming"
  echo "                        distribution parent directory location. This is a directory"
  echo "                        that contains CloakwareStreaming folder."
  echo "                        By default it is equal to ${_MY_HOME}/.."
  echo ""
  echo "    (-prebuildcommand | -pbc)"
  echo "                      - the first command that will be used to build the application apk"
  echo "                        that will be signed by the Java Access Control tool."
  echo "                        If do not wish to run this command, pass in 'donotrun' as the value." 
  echo "                        By default it is set to '${_DEFAULT_PRE_BUILD_COMMAND}'"
  echo ""
  echo "    (-postbuildcommand | -pobc)"
  echo "                      - the second command that will be used to build the application apk"
  echo "                        after it has been signed by the Java Access Control tool."
  echo "                        By default it is set to '${_DEFAULT_POST_BUILD_COMMAND}'"
  echo ""
  echo "    (-builddirectory | -bd)"
  echo "                      - this is an optional parameter with absolute or relative path"
  echo "                        to the directory where the build command should be run."
  echo "                        By default it is a path to the <application project location>."
  echo ""
  echo "    (-keystore | -ks) <key store file>"
  echo "                      - this parameter declares a key store file path for Android"
  echo "                        application signing. If it's skipped, the script will create"
  echo "                        key store automatically in the <application project location>/../KeyStore"
  echo "                        directory."
  echo ""
  echo "    (-keystore_pw | -kspw) <keystore password>"
  echo "                      - this parameter declares a password for the key store used"
  echo "                        in Android signing"
  echo ""
  echo "    (-keyalias | -ka) <key alias name>"
  echo "                      - this is a key alias name in the key store used for"
  echo "                        Android signing."
  echo ""
  echo "    (-keyalias_pw | -kapw) <key alias password>"
  echo "                      - this is a password to get an access to the key alias"
  echo ""
  echo "    (-specific_device | -s) <device_ID>"
  echo "                      - this option specifies an ID of the Android device attached"
  echo "                        to the hosting computer. JAC provisioning tools will use"
  echo "                        the device to JAC-sign the target application."
  echo "                        This option is helpful when multiple devices are attached."
  echo "                        If the device with particular ID is not attached, the "
  echo "                        script will fail."
  echo "                        Use 'adb devices' command to determine an ID of the device"
  echo "                        designated for the signing. If the option is skipped, the script"
  echo "                        will use the first device or emulator from the list of the"
  echo "                        attached devices. If the list is empty, the script will establish"
  echo "                        an emulator for the signing process"
  echo ""
  echo "    (-target_sdk | -ts) <target sdk symbolic value>"
  echo "                      - this option specifies an Android OS version ID of the"
  echo "                        target device. By default it is empty. If it is specified,"
  echo "                        it allows the build script to decrease the size of the"
  echo "                        final application by attaching only those CWS libraries"
  echo "                        that created specifically for the Android OS of the"
  echo "                        target device. If it is not defined, the script will attach"
  echo "                        all available libraries that increases size of the"
  echo "                        application."
  echo "                        Currently the script recognizes the following values for"
  echo "                        the option:"
  echo "                                       froyo | fr  | 2.2.x"
  echo "                                 gingerbread | fr3 | 2.3.x"
  echo "                                   honeycomb | hc  | 3.x "
  echo "                           icecream_sandwich | is  | 4.0.x"
  echo "                                   jellybean | jb  | 4.1.x/4.2.x"
  echo ""
  echo "    (-target_cpu | -tc) <target cpu symbolic value>"
  echo "                      - this option specifies a processor type of target devices."
  echo "                        By default it is empty. If it is specified,"
  echo "                        it allows the build script to decrease the size of the"
  echo "                        final application by attaching only those CWS libraries"
  echo "                        that created specifically for the target devices."
  echo "                        If it is not defined, the script will attach all available"
  echo "                        libraries that increases size of the application."
  echo "                        Currently the script recognizes the following values for"
  echo "                        the option:"
  echo "                           armv6"
  echo "                           armv7 | cortexa8"
  echo ""
  echo "    (-target_arch | -tarch) <target architecture value>"
  echo "                      - this option specifies a processor architecture category of"
  echo "                        target devices. By default it is empty. If it is specified,"
  echo "                        it allows the build script to decrease the size of the"
  echo "                        final application by attaching only those CWS libraries"
  echo "                        that created specifically for the target devices."
  echo "                        If it is not defined, the script will attach all available"
  echo "                        libraries that increases size of the application."
  echo "                        Currently the script recognizes the following values for"
  echo "                        the option:"
  echo "                           armeabi"
  echo "                           armeabi-v7a"
  echo ""
  echo "    (-target_api_level | -ta) <target API level number>"
  echo "                      - this option specifies a target Android API level number."
  echo "                        See the column 3 in the table below."
  echo "                        Default value is 9."
  echo ""
  echo "                          Code name             Version      API level    NDK"
  echo "                          ==================================================="
  echo "                          Eclair                2.0               5        2"
  echo "                          Eclair                2.0.1             6"
  echo "                          Eclair                2.1               7        3"
  echo "                          Froyo                 2.2.x             8        4"
  echo "                          Gingerbread           2.3 - 2.3.2       9        5"
  echo "                          Gingerbread           2.3.3 - 2.3.7    10"
  echo "                          Honeycomb             3.0              11"
  echo "                          Honeycomb             3.1              12        6"
  echo "                          Honeycomb             3.2.x            13"
  echo "                          Ice Cream Sandwich    4.0.1 - 4.0.2    14        7"
  echo "                          Ice Cream Sandwich    4.0.3 - 4.0.4    15        8"
  echo "                          Jelly Bean            4.1.x            16"
  echo ""
  echo "    (-development | -d)"
  echo "                      - builds the application using libcws_ps.so instead of"
  echo "                        libcws.so. This will disable IAC IV checking and the JAC"
  echo "                        signing process."
  echo ""
  echo "    (-emulator | -e)"
  echo "                      - uses an emulator to do the signing process. If an emulator"
  echo "                        does not already exist, one will be created." 
  echo ""
  echo "    (-nocopyjars | -ncj)"
  echo "                      - use this parameter if you do not want this script to add cws.jar and "
  echo "                        JACRuntime.jar to the apk. This parameter should only be used"
  echo "                        if you have integrated these two jars into your build process."
  echo ""
  echo "    (-disabledversions | -dv)"
  echo "                      - use this parameter if you are running this script in a"
  echo "                        disabled versions environment. When this parameter is enabled,"
  echo "                        irss.dat will not be added to the final package."
  echo ""
  echo "    (-help | -h)      - shows this help information"
  echo ""
  echo "Samples:"
  echo ""
  echo "  ${1} -aname MyApp -aproj ~/a/path/to/MyApp -cwstreaming /a/path/to/dist"
  echo ""
  echo "      The command above will build MyApp application located in the ~/a/path/to/MyApp folder."
  echo "      The final application MyApp-release.apk will be in the ~/a/path/to/MyApp/bin folder." 
  echo "      The CloakwareStreaming distribution is located in the /a/path/to/dist folder."
  echo ""
  echo "  ${1} -an MyApp -ap ~/a/path/to/MyApp -cws /a/path/to/dist"
  echo ""
  echo "      This is a version of the previous sample command with short keywords." 
  echo ""
  cleanupAndExit 1
}

# cleans up child processes
# and exits from the script with 
# an error code passed as the first parameter
cleanupAndExit()
{
    if [[ "${_USE_EMULATOR}" = "on" ]]; then
        if ! [ "${_emulator_pid}" = "" ]; then
            echo ""
            echo "Closing the '${_virtual_device_name}' emulator as a ${_emulator_pid} process."
            echo ""
            kill ${_emulator_pid}
            _emulator_pid=
  
            android delete avd -n ${_virtual_device_name}
            _virtual_device_name=
        fi
    fi

    exit ${1}
}

_device_count=
_device_names=
# obtains information about Android devices attached. 
# Parameters:
#   ${1} - type of information requesting to return:
#          'count' - return a number of devices attached
#                    into the _device_count variable
#          'names' - return a list of the attached device's IDs
#                    into the _device_names variable
#   ${2} - reqest information about emulators or devices or both:
#          ''      - empty parameter means to return information
#                    about devices and emulators
#          'emulators'
#                  - return information only for emulators
#          'devices'
#                  - return information only for devices
#      
getAttachedDevicesInfo ()
{
    local _device_list=( $(adb devices) )
    local _rem=0
  
    if [[ ${_device_list[0]} = List && ${_device_list[1]} = of && ${_device_list[2]} = devices && ${_device_list[3]} = attached ]]; then
        if [ "${2}" = "" ]; then 
            _device_list=( $(adb devices | egrep -w 'device|offline') )
        elif [ "${2}" = "emulators" ]; then 
            _device_list=( $(adb devices | egrep -w 'device|offline' | grep "emulator-") )
        elif [ "${2}" = "devices" ]; then 
            _device_list=( $(adb devices | egrep -w 'device|offline' | grep -v "emulator-") )
        else
            echo "Error: The second parameter for getAttachedDevicesInfo is incorrect. Should be either 'emulators' or 'devices' or empty."
            cleanupAndExit -2
        fi
        if [ "${1}" = "count" ]; then
          _device_count=$((( ${#_device_list[@]}) / 2))
        elif [ "${1}" = "names" ]; then
          _device_names=()
          for _index in $( seq 0 $((${#_device_list[@]} - 1)) )
          do
             _rem=$((${_index} % 2))
             if [ ${_rem} = 0 ]; then
               _device_names[((${_index}) / 2)]=${_device_list[${_index}]}
             fi
          done
        else
          echo "Error: Parameter for getAttachedDevicesInfo is incorrect. Should be either 'count' or 'names'."
          cleanupAndExit -3
        fi
    else
        echo "Error: could not obtain devices list using adb."
        cleanupAndExit -4
    fi
}

_free_emulator_port=
# returns a free Android emulator port that can be used
# to start a new instance of Android emulator
# No parameters
# The result is located in the _free_emulator_port variable
#
getFreeEmulatorPort ()
{
    _free_emulator_port=
    getAttachedDevicesInfo count emulators
    local _emulator_count=${_device_count}
    if [ ${_emulator_count} = 0 ]; then
        _free_emulator_port=$(( 5554 ))
    else
        local _found=false
        local _port_number=
        getAttachedDevicesInfo names emulators
        local _attached_emulators=("${_device_names[@]}")
        # iterate through all available even port numbers
        for _port_number in $(seq 5554 2 5584)
        do
            local _e_index=
            _found=false
            for _e_index in $(seq 0 $((${#_attached_emulators[@]} - 1)))
            do 
                if [ "${_attached_emulators[${_e_index}]}" = "emulator-${_port_number}" ]; then
                    _found=true
                    break
                fi
            done
            
            if [ "${_found}" = "false" ]; then
                # the <_port_number> and <_port_number>+1 are not taken, we can use it 
                _free_emulator_port=$(( ${_port_number} ))
                break;
            fi
        done
    fi
}

# This function verifies number of attached devices
# to the hosting computer.
# No parameters.
#
checkAttachedDevices()
{
    getAttachedDevicesInfo count
    local _attached_device_count=${_device_count}
  
    if ! [ ${_attached_device_count} = 1 ]; then
        # there are multiple devices attached to the hosting computer
        # let's list them
        getAttachedDevicesInfo names
        local _attached_device_ids=("${_device_names[@]}")
        local _index=0
        
        echo "This is a list of Android devices attached to the computer:"
        for _index in $(seq 0 $((${#_attached_device_ids[@]} - 1)))
        do 
           echo "device[${_index}] = ${_attached_device_ids[${_index}]}"
        done

        if [ "${_allow_attaching_multiple_devices}" == "no" ]; then
            echo ""
            echo "ERROR: There are ${_device_count} devices attached."
            echo "       This version of script does not support multiple devices attached to a computer."
            echo "       Please, leave only one device attached and restart the script."
            echo "       Aborting the script."
            cleanupAndExit -5
        else
            if [ "${_signer_device_id}" = "" ]; then 
                echo "         The script will use the first device in the list = '${_attached_device_ids[0]}'"
                _signer_device_id=${_attached_device_ids[0]}
            else
                local _found=false
                for _index in $(seq 0 $((${#_attached_device_ids[@]} - 1)))
                do
                    if [ "${_attached_device_ids[${_index}]}" = "${_signer_device_id}" ]; then
                        _found=true
                        break
                    fi
                done
                if [ "${_found}" = "false" ]; then
                    echo "ERROR: Device with ID = ${_signer_device_id} is not attached to the hosting computer."
                    echo "       This device was assigned to perform the application provisioning."
                    echo "       Aborting the script."
                    cleanupAndExit -6
                fi
            fi
        fi
    elif [ ${_attached_device_count} = 0 ]; then
        if [ "${_SPECIFIC_DEVICE}" = "" ]; then 
            echo "There is no emulator or device attached to this computer." 
            echo "An Android emulator or device is required for the JAC-signing process."
        else
            echo "There was a request to use a device with ID = ${_SPECIFIC_DEVICE}." 
            echo "This device is not attached to the hosting computer."
        fi    
        echo "If you would like to use a device for signing, please attach a device and"
        echo "re-run this script. Use option -s if there several attached devices."
        echo "If you would like to use an emulator for signing, please"
        echo "re-run this script with the -emulator or -e flag."
        echo ""
        cleanupAndExit -7
    else
        getAttachedDevicesInfo names
        local _attached_device_ids=("${_device_names[@]}")
        if [ "${_signer_device_id}" = "" ]; then 
            _signer_device_id=${_attached_device_ids[0]}
        elif [ "${_attached_device_ids[0]}" != "${_signer_device_id}" ]; then
            echo "ERROR: The device with ID = ${_signer_device_id} has been detached from the hosting computer."
            echo "       Another device with ID = ${_attached_device_ids[0]} was attached instead."
            echo "       Aborting the script."
            cleanupAndExit -8
        fi
            
    fi
    
    echo "The script is using the device with ID = ${_signer_device_id}"
}

# performs an adb (Android Debug Bridge) command
# Parameters:
#   ${1} - an adb command to perform.
#          See the adb help info.
#   ${2} - reference to a specific device on form of
#          '-s <specific_device_id>'
#
execAdbShellCommand ()
{
  local _result=$(adb ${2} shell ${1})
  if ! [ "${_result}" = "" ]; then
    echo "Error: Could not perform \"adb ${2} shell ${1}\""
    echo "       The command returns a message:"
    echo "       ${_result}"
    echo ""
    cleanupAndExit -9
  fi
}

# creates a default key store to sign
# the target Android application using Apache Ant build tool
#
createDefaultKeyStore()
{
    #
    # create KeyStore
    #
    
    if [ "${_KEY_STORE_PATH}" = "" ]; then
      _KEY_STORE_PATH=${_KEY_STORE_PATH_DEFAULT}
    fi
    
    if [ "${_KEY_STORE_PASSWORD}" = "" ]; then
      _KEY_STORE_PASSWORD=${_KEY_STORE_PASSWORD_DEFAULT}
    fi
    
    if [ "${_KEY_ALIAS}" = "" ]; then
      _KEY_ALIAS=${_KEY_ALIAS_DEFAULT}
    fi
    
    if [ "${_KEY_ALIAS_PASSWORD}" = "" ]; then
      _KEY_ALIAS_PASSWORD=${_KEY_ALIAS_PASSWORD_DEFAULT}
    fi
    
    if [[ -f "${_KEY_STORE_PATH}" ]]; then
       
        echo "Key store ${_KEY_STORE_PATH}. It already exists."
    
    else
    
       echo "Creating a key store - ${_KEY_STORE_PATH}"
       mkdir -p "${_KEY_STORE_ABS_DIR}"
       CommandCheck "Failed to create directory ${_KEY_STORE_ABS_DIR}"
       
       keytool -genkey -dname "cn=CloakwareStreaming-Based-Application, ou=MyOrganizationUnit, o=MyOrganization, l=MyLocalityName, c=MyCountryCode" -alias ${_KEY_ALIAS} -keypass ${_KEY_ALIAS_PASSWORD} -keystore "${_KEY_STORE_PATH}" -storepass ${_KEY_STORE_PASSWORD} -validity 10000
       CommandCheck "Failed to create key store"
    
    fi
}

# prepares build.properties files and updates the target Android project
# in order to properly build it with Apache Ant tool
#
prepareBuildPropertiesAndUpdateApplicationProject()
{
    #
    # remove local.properties, build.properties/ant.properties, build.xml
    #
    
    remove_file "${_APP_PROJ_PATH}/local.properties"
    CommandCheck "Failed to remove file ${_APP_PROJ_PATH}/local.properties"
    remove_file "${_APP_PROJ_PATH}/build.properties"
    CommandCheck "Failed to remove file ${_APP_PROJ_PATH}/build.properties"
    remove_file "${_APP_PROJ_PATH}/default.properties"
    CommandCheck "Failed to remove file ${_APP_PROJ_PATH}/default.properties"
    remove_file "${_APP_PROJ_PATH}/ant.properties"
    CommandCheck "Failed to remove file ${_APP_PROJ_PATH}/ant.properties"
    remove_file "${_APP_PROJ_PATH}/build.xml"
    CommandCheck "Failed to remove file ${_APP_PROJ_PATH}/build.xml"
    
    #
    # create build.properties with necessary data to sign an Android application
    #
    echo -e "# This file has been automatically generated by the ${_SCRIPT_NAME} script." >"${_APP_PROJ_PATH}/build.properties"
    echo -e "# This file is used to override default values used by the Ant build system." >>"${_APP_PROJ_PATH}/build.properties"
    echo -e "#" >>"${_APP_PROJ_PATH}/build.properties"
    echo -e "# This file must be checked in Version Control Systems, as it is" >>"${_APP_PROJ_PATH}/build.properties"
    echo -e "# integral to the build system of your project." >>"${_APP_PROJ_PATH}/build.properties"
    echo -e "" >>"${_APP_PROJ_PATH}/build.properties"
    echo -e "# This file is only used by the Ant script." >>"${_APP_PROJ_PATH}/build.properties"
    echo -e "" >>"${_APP_PROJ_PATH}/build.properties"
    echo -e "# You can use this to override default values such as" >>"${_APP_PROJ_PATH}/build.properties"
    echo -e "#  'source.dir' for the location of your java source folder and" >>"${_APP_PROJ_PATH}/build.properties"
    echo -e "#  'out.dir' for the location of your output folder." >>"${_APP_PROJ_PATH}/build.properties"
    echo -e "" >>"${_APP_PROJ_PATH}/build.properties"
    echo -e "# You can also use it define how the release builds are signed by declaring" >>"${_APP_PROJ_PATH}/build.properties"
    echo -e "# the following properties:" >>"${_APP_PROJ_PATH}/build.properties"
    echo -e "#  'key.store' for the location of your keystore and" >>"${_APP_PROJ_PATH}/build.properties"
    echo -e "#  'key.alias' for the name of the key to use." >>"${_APP_PROJ_PATH}/build.properties"
    echo -e "# The password will be asked during the build when you use the 'release' target." >>"${_APP_PROJ_PATH}/build.properties"
    echo -e "key.store=${_KEY_STORE_PATH}" >>"${_APP_PROJ_PATH}/build.properties"
    echo -e "key.store.password=${_KEY_STORE_PASSWORD}" >>"${_APP_PROJ_PATH}/build.properties"
    echo -e "key.alias=${_KEY_ALIAS}" >>"${_APP_PROJ_PATH}/build.properties"
    echo -e "key.alias.password=${_KEY_ALIAS_PASSWORD}" >>"${_APP_PROJ_PATH}/build.properties"
    
    #
    # updating the target project with necessary build scripts from Android SDK
    #
    echo "updating the target project with necessary build scripts from Android SDK"
    echo "android update project -p ${_APP_PROJ_PATH} -n ${_APP_NAME} -t ${_ANDROID_TARGET_OS_ID}"
    android update project -p "${_APP_PROJ_PATH}" -n ${_APP_NAME} -t ${_ANDROID_TARGET_OS_ID}
}

# returns an unknown parameter's value error message 
# and exits the script
# Parameters:
#   ${1} - parameter name
#   ${2} - value
#   ${3} - list of allowed values
UnknownParameterValueError()
{
  echo ""
  echo "Error: Parameter \"${1}\" has an invalid value \"${2}\"."
  echo "       The allowed values are: ${3}"
  echo ""
  _ERRORCODE=1
  cleanup
}

# validates a value of the option that 
# has a list predifined values and 
# returns the resulting value for the option
# if the value is valid.
# Otherwise, it fires an Unknown Parameter's Value Error
#
# Parameters:
#   ${1} - parameter/option name
#   ${2} - validating value
#   ${3} - list of allowed values
#   ${4} - list of the corresponding resulting values
#
# Returns:
# _VALIDATED VALUE
validateOptionValue()
{
    _VALIDATED_VALUE=
    local _option_name=${1}
    local _validating_value=${2}
    local _option_values=(${3})
    local _resulting_values=(${4})
    local _index=0

    if ! [ "${_validating_value}" = "" ]; then
        for (( _index=0; _index<=$(( ${#_option_values[*]} -1 )); _index++ ))
        do
           if [ "${_validating_value}" = "${_option_values[${_index}]}" ]; then
              _VALIDATED_VALUE=${_resulting_values[${_index}]}
              if [ "${_VALIDATED_VALUE}" = "${_EMPTY_VALUE}" ]; then
                _VALIDATED_VALUE=""
              fi
              break
           fi
        done
        if [[ "${_VALIDATED_VALUE}" = "" && "${_index}" -ge "${#_option_values[*]}" ]]; then
            UnknownParameterValueError "${_option_name}" "${_validating_value}" "${_option_values[*]}"
        fi
    fi
}

# Command check for each bash command, if not return 0, 
# the script will exit and pop up error message
CommandCheck()
{
    if [ $(echo $?) != 0 ]; then
        cd ${_INITIAL_DIR}
        echo "Error: \"${1}\""
        cleanupAndExit -10
    fi
}

# the function searches for particular files 
# in the location and returns a list of the
# file's basenames in the following format:
#   (basename1, basename2, ...)
# Parameters:
#   ${1} - path to the folder location
#   ${2} - template to search (*.apk, for instance)
# Result:
#   _LIST_OF_FILES variable contains result of
#   the search. It is empty, if functions found
#   nothing. 
getListOfFilesInFolderByTemplate() {

    local _folder_path=${1}
    local _template=${2}
    local _list_of_files=($(find "${_folder_path}" -name "${_template}"))

    local _list=""
    for _path in "${_list_of_files[@]}"
    do  
        _file_basename=$(basename "${_path}")
        if [ "${_list}" = "" ]; then
            _list="("
        else
            _list="${_list}, "
        fi
        _list="${_list}\"${_file_basename}\""
        
    done
    if [ "${_list}" != "" ]; then
        _list="${_list})"
    fi
    _LIST_OF_FILES=${_list}
}

###############################################################################
###############################################################################
###############################################################################

#
# Verify the platform before doing anything 
#
if [ "$(uname -a | grep Linux)" = "" ]; then
  echo "Error: This script should be run only under Linux platform!" 
  echo "       Aborting the script."
  cleanupAndExit -11
fi

# determine location of the bash script
if [ "${BASH_SOURCE}" = "" ]; then
    _SCRIPT_PATH=${0}
else
    _SCRIPT_PATH=${BASH_SOURCE}
fi
if [ "${_SCRIPT_PATH}" = "" ]; then
    echo "ERROR: Could not determine the bash script location."
    echo "       \${0} and \${BASH_SOURCE} values are empty."
    echo "       Abort the script."
    cleanupAndExit -12
fi
_SCRIPT_NAME=${_SCRIPT_PATH##*/}
_SCRIPT_DIR=${_SCRIPT_PATH%${_SCRIPT_NAME}}
# determine the full path of the bash script
if [ "${_SCRIPT_DIR}" = "" ]; then
    _MY_FULL_PATH=$(echo ${PWD}/${_SCRIPT_NAME})
else
    _MY_FULL_PATH=$(cd "${_SCRIPT_DIR}" && echo ${PWD}/${_SCRIPT_NAME})
fi 
_MY_HOME=`dirname "${_MY_FULL_PATH}"`
_INITIAL_DIR=$(pwd)

#
# Declare variables
#
_ERRORCODE=0
_CONFIG=android
# the _LIST_OF_FILES variable is need for getListOfFilesInFolderByTemplate function
_LIST_OF_FILES=
_ARCH_LIST=()
_ARCHDIR=
_DEFAULT_ARCH=armeabi
_PROD_DIST_DIR=
_ACM_DIST_DIR=
_APP_NAME=
_APP_APK_NAME=
_APP_PROJ_PATH=
_APP_PROJ_PATH_BIN=
_BUILD_EXECUTION_DIR=
_HELP_REQUEST=off

_DEFAULT_PRE_BUILD_COMMAND="ant release"
_DEFAULT_POST_BUILD_COMMAND="ant release"

_PRE_BUILD_COMMAND=
_POST_BUILD_COMMAND=

_USE_R14=on
_NO_COPY_JARS=off
_DISABLED_VERSIONS=off

_KEY_STORE=customer.keystore
_KEY_STORE_LOCATION=KeyStore

_KEY_STORE_ABS_DIR=
_KEY_STORE_PATH_DEFAULT=

_KEY_STORE_PASSWORD_DEFAULT=customer.password
_KEY_ALIAS_DEFAULT=customer_release_key
_KEY_ALIAS_PASSWORD_DEFAULT=customer.password

_KEY_STORE_PATH=
_KEY_STORE_PASSWORD=
_KEY_ALIAS=
_KEY_ALIAS_PASSWORD=

_DEVICE_APP_DIR=/data/local/tmp/irdeto

_USE_EMULATOR=off

_DEVELOPMENT_CWS_LIBRARY=off
_CWS_LIBRARY_NAME=libcws.so
_CWS_LIBRARY_PREFIX=libcws
_CWS_PS_LIBRARY_PREFIX=libcws_ps

_TARGET_CPU=
_TARGET_SDK=
_TARGET_ARCH=
_TARGET_CPU_SUFFIX=
_TARGET_SDK_SUFFIX=
_TARGET_ARCH_SUFFIX=
_TARGET_CPU_VALUES=("armv6" "armv7" "cortexa8")
_TARGET_CPU_SUFFIXES=("_armv6" "_armv7" "_armv7")
_TARGET_SDK_VALUES=("froyo" "fr"  "2.2.x" "gingerbread" "fr3" "2.3.x" "honeycomb" "hc" "3.x" "icecream_sandwich" "is" "icecream" "4.0.x" "jellybean" "jb" "4.1.x/4.2.x")
_TARGET_SDK_SUFFIXES=("_fr" "_fr" "_fr"   "_fr3"       "_fr3" "_fr3"  "_hc"      "_hc" "_hc" "_is"              "_is" "_is"      "_is"   "_jb"      "_jb" "_jb")
_TARGET_ARCH_VALUES=("armeabi-v7a" "armeabi")
_TARGET_ARCH_SUFFIXES=("_v7a" "${_EMPTY_VALUE}") # !!! important to keep the empty suffix as the last one

_VALIDATED_VALUE=
_PLATFORMS_LOCATION=
_ANDROID_TARGET_API_LEVEL_DEFAULT=10
_ANDROID_TARGET_API_LEVEL=

_wait_duration=120
_virtual_device_name_prefix=cw-jac-signer
_virtual_device_name=${_virtual_device_name_prefix}-$(date +%Y%m%d-%H%M%S)
_android_taget_sdk=android-10
_emulator_pid=
_signer_device_id=
_SPECIFIC_DEVICE=
_allow_attaching_multiple_devices=yes

_SDK_LOCATION=$(echo ${ANDROID_SDK_DIR})
_PLATFORMS_LOCATION=${_SDK_LOCATION}/platforms
_SDK_VERSION=
_ANT_LOCATION=${_SDK_LOCATION}/tools/ant
_BUILD_XML=${_ANT_LOCATION}/build.xml
_VERSION_UPDATE=0

_CURRENT_DIR=

_SCRIPT_NAME=${0}

#
# Parse Command Line Arguments
#
if [ -n "${1}" ];then
 until [ $# -eq 0 ]
 do
   if [ "${1}" = "-aname" ];  then shift; _APP_NAME=${1}
   elif [ "${1}" = "-an" ];       then shift; _APP_NAME=${1}
   elif [ "${1}" = "-aproj" ];  then shift; _APP_PROJ_PATH=${1}
   elif [ "${1}" = "-ap" ];       then shift; _APP_PROJ_PATH=${1}
   elif [ "${1}" = "-apkname" ];  then shift; _APP_APK_NAME=${1}
   elif [ "${1}" = "-apk" ];       then shift; _APP_APK_NAME=${1}
   elif [ "${1}" = "-aprojoutput" ];  then shift; _APP_PROJ_PATH_BIN=${1}
   elif [ "${1}" = "-apo" ];       then shift; _APP_PROJ_PATH_BIN=${1}
   elif [ "${1}" = "-prebuildcommand" ];  then shift; _PRE_BUILD_COMMAND="${1}"
   elif [ "${1}" = "-pbc" ];       then shift; _PRE_BUILD_COMMAND="${1}"
   elif [ "${1}" = "-postbuildcommand" ];  then shift; _POST_BUILD_COMMAND="${1}"
   elif [ "${1}" = "-pobc" ];       then shift; _POST_BUILD_COMMAND="${1}"
   elif [ "${1}" = "-builddirectory" ];  then shift; _BUILD_EXECUTION_DIR="${1}"
   elif [ "${1}" = "-bd" ];       then shift; _BUILD_EXECUTION_DIR="${1}"
   elif [ "${1}" = "-cwstreaming" ];  then shift; _ACM_DIST_DIR=${1}
   elif [ "${1}" = "-cws" ];       then shift; _ACM_DIST_DIR=${1}
   elif [ "${1}" = "-keystore" ];  then shift; _KEY_STORE_PATH=${1}
   elif [ "${1}" = "-ks" ];       then shift; _KEY_STORE_PATH=${1}
   elif [ "${1}" = "-keystore_pw" ];  then shift; _KEY_STORE_PASSWORD=${1}
   elif [ "${1}" = "-kspw" ];       then shift; _KEY_STORE_PASSWORD=${1}
   elif [ "${1}" = "-keyalias" ];  then shift; _KEY_ALIAS=${1}
   elif [ "${1}" = "-ka" ];       then shift; _KEY_ALIAS=${1}
   elif [ "${1}" = "-keyalias_pw" ];  then shift; _KEY_ALIAS_PASSWORD=${1}
   elif [ "${1}" = "-kapw" ];       then shift; _KEY_ALIAS_PASSWORD=${1}
   elif [ "${1}" = "-specific_device" ]; then shift; _SPECIFIC_DEVICE=${1}
   elif [ "${1}" = "-s" ];       then shift; _SPECIFIC_DEVICE=${1}
   elif [ "${1}" = "-target_sdk" ];  then shift; _TARGET_SDK=${1}
   elif [ "${1}" = "-ts" ];       then shift; _TARGET_SDK=${1}
   elif [ "${1}" = "-target_cpu" ];  then shift; _TARGET_CPU=${1}
   elif [ "${1}" = "-tc" ];       then shift; _TARGET_CPU=${1}
   elif [ "${1}" = "-target_arch" ]; then shift; _TARGET_ARCH=${1}
   elif [ "${1}" = "-tarch" ];       then shift; _TARGET_ARCH=${1}
   elif [ "${1}" = "-target_api_level" ]; then shift; _ANDROID_TARGET_API_LEVEL=${1}
   elif [ "${1}" = "-ta" ];       then shift; _ANDROID_TARGET_API_LEVEL=${1}
   elif [ "${1}" = "-help" ];    then _HELP_REQUEST=on
   elif [ "${1}" = "-h" ];    then _HELP_REQUEST=on
   elif [ "${1}" = "-development" ];    then _DEVELOPMENT_CWS_LIBRARY=on
   elif [ "${1}" = "-d" ];    then _DEVELOPMENT_CWS_LIBRARY=on
   elif [ "${1}" = "-emulator" ];    then _USE_EMULATOR=on
   elif [ "${1}" = "-e" ];    then _USE_EMULATOR=on
   elif [ "${1}" = "-r14" ];    then _USE_R14=on
   elif [ "${1}" = "-nocopyjars" ];    then _NO_COPY_JARS=on
   elif [ "${1}" = "-ncj" ];    then _NO_COPY_JARS=on
   elif [ "${1}" = "-disabledversions" ];    then _DISABLED_VERSIONS=on
   elif [ "${1}" = "-dv" ];    then _DISABLED_VERSIONS=on
   else UnknownParameterError "${1}" "${_SCRIPT_NAME}"
   fi
 shift
 done
fi

if [ "${_HELP_REQUEST}" = "on" ]; then
  ShowHelp ${_SCRIPT_NAME}
  CommandCheck "Failed to show help"
fi

#
# (0) check platform environment
#
step "0" "check platform environment"

if [ "${_SDK_LOCATION}" = "" ]; then
    echo "Error: Did not set up Android SDK dir!" 
    cleanupAndExit -14
fi

if [ ! -d "${_PLATFORMS_LOCATION}" ]; then
    echo "Error: No platforms in Android SDK"
    cleanupAndExit -15
fi

if [ ! -d "${_ANT_LOCATION}" ]; then
    echo "Error: No Ant in Android SDK"
    cleanupAndExit -16
fi

if [ ! -r "${_BUILD_XML}" ]; then
    echo "Error: No build.xml in Ant"
    cleanupAndExit -17
fi

DIRS=`ls -l "$_PLATFORMS_LOCATION" | egrep '^d' | awk '{print $8}'`
CommandCheck "Failed to list the platforms under \"${_PLATFORMS_LOCATION}\, Please check if \"${_PLATFORMS_LOCATION}\ is available or accessable"

if [ "${DIRS}" = "" ]; then
    echo "Error: No Android API in platforms"
    cleanupAndExit -18
fi

_CURRENT_DIR=`pwd`
cd "${_PLATFORMS_LOCATION}"
CommandCheck "Failed to cd folder  \"${_PLATFORMS_LOCATION}\", Please check if \"${_PLATFORMS_LOCATION}\" is available or accessable"

cd "${_CURRENT_DIR}"

#
# (1) validate parameters and verify the build settings
#
step "1" "validate parameters and verify the build settings"

if [[ "${_DEVELOPMENT_CWS_LIBRARY}" = "on" ]]; then
  _CWS_LIBRARY_NAME=libcws_ps.so
fi

# if _APP_NAME is not defined, abort the script with an error message
if [ "${_APP_NAME}" = "" ]; then
    echo ""
	echo "Error: Application name is not defined."
    echo "       You should call this script with -aname parameter passing a name of application you build."
    echo "       The name should NOT contain an .apk extension at the end." 
    echo "       The script needs the name to manipulate with the build application to sign it correctly."
    echo "       See the help information running '${_SCRIPT_NAME} -help' command."
    echo "       Aborting the script."
    echo ""
    cleanupAndExit -19
fi

# setup _APP_APK_NAME if it is not defined
# we will use this varaible in JAC signing procedure
if [ "${_APP_APK_NAME}" = "" ]; then
    _APP_APK_NAME=${_APP_NAME}-release.apk
fi

# setup _PRE_BUILD_COMMAND if it's not defined
if [ "${_PRE_BUILD_COMMAND}" = "" ]; then
    _PRE_BUILD_COMMAND=${_DEFAULT_PRE_BUILD_COMMAND}
fi

# setup _POST_BUILD_COMMAND if it's not defined
if [ "${_POST_BUILD_COMMAND}" = "" ]; then
    _POST_BUILD_COMMAND=${_DEFAULT_POST_BUILD_COMMAND}
fi

varTest "ANDROID_SDK_DIR" ${ANDROID_SDK_DIR} 
CommandCheck "Failed ot verify if ${ANDROID_SDK_DIR} is set, please check the availability of ${ANDROID_SDK_DIR}"

validateOptionValue "-target_sdk" "${_TARGET_SDK}" "${_TARGET_SDK_VALUES[*]}" "${_TARGET_SDK_SUFFIXES[*]}"
CommandCheck "Failed to validate the value of option /"-target_sdk"/, Please check if this option is valid"
_TARGET_SDK_SUFFIX="${_VALIDATED_VALUE}"

validateOptionValue "-target_cpu" "${_TARGET_CPU}" "${_TARGET_CPU_VALUES[*]}" "${_TARGET_CPU_SUFFIXES[*]}"
CommandCheck "Failed to validate the value of option /"-target_cpu"/, Please check if this option is valid"
_TARGET_CPU_SUFFIX="${_VALIDATED_VALUE}"

validateOptionValue "-target_arch" "${_TARGET_ARCH}" "${_TARGET_ARCH_VALUES[*]}" "${_TARGET_ARCH_SUFFIXES[*]}"
CommandCheck "Failed to validate the value of option /"-target_arch"/, Please check if this option is valid"
_TARGET_ARCH_SUFFIX="${_VALIDATED_VALUE}"


# setup the arch list to iterate in a loop
if [ "${_TARGET_ARCH}" = "" ]; then
    _ARCH_LIST=(${_TARGET_ARCH_VALUES[*]})
else
    _ARCH_LIST=("${_TARGET_ARCH}")
fi


# setup Android target OS image ID
if [ "${_ANDROID_TARGET_API_LEVEL}" = "" ]; then
    _ANDROID_TARGET_API_LEVEL=${_ANDROID_TARGET_API_LEVEL_DEFAULT}
fi
_ANDROID_TARGET_OS_IMAGE_NAME=android-${_ANDROID_TARGET_API_LEVEL}
_ANDROID_TARGET_OS_ID=$(android list target | grep ${_ANDROID_TARGET_OS_IMAGE_NAME} | cut -d' ' -f2)

#
# (2) determine production distribution location
#
step "2" "determine production distribution location"

if [ "${_ACM_DIST_DIR}" = "" ]; then
  if [ -d "${_MY_HOME}/../CloakwareStreaming" ]; then
    _ACM_DIST_DIR=${_MY_HOME}/..
  elif [ -d "${_MY_HOME}/../../ActiveCloakMedia/CloakwareStreaming" ]; then
    _ACM_DIST_DIR=${_MY_HOME}/../../ActiveCloakMedia
  fi
else
    # Convert _ACM_DIST_DIR to be an absolute path
    _ACM_DIST_DIR=`cd "${_ACM_DIST_DIR}"; pwd`
fi

_PROD_DIST_DIR=${_ACM_DIST_DIR}/CloakwareStreaming

if [ "${_APP_PROJ_PATH}" = "" ]; then
    echo ""
	echo "Error: Application project location is not defined."
    echo "       You should call this script with -aproj parameter passing a path to application project location."
    echo "       The script needs the location to build application for signing."
    echo "       See the help information running '${_SCRIPT_NAME} -help' command."
    echo "       Aborting the script..."
    echo ""
    cleanupAndExit -20
fi

if ! [ -d "${_APP_PROJ_PATH}" ]; then
  echo "Error: Could not locate the application project - ${_APP_PROJ_PATH}"
  echo "       Please, verify if it is a correct path."
  echo "       Aborting the script..."
  echo ""
  cleanupAndExit -21
fi

if ! [ -d "${_PROD_DIST_DIR}" ]; then
  echo "Error: Could not locate the production distribution"
  echo "          - ${_PROD_DIST_DIR}."
  echo "       Either the location should be passed as a parameter to the script or"
  echo "       the script should be put in the directory alongside to the"
  echo "       CloakwareStreaming distribution"
  echo "       See the help information running '${_SCRIPT_NAME} -help' command."
  echo "       Aborting the script..."
  echo ""
  cleanupAndExit -22
fi

#Convert ${_APP_PROJ_PATH} to Absolute Path
_CURRENT_DIR=`pwd`
_APP_PROJ_PATH=`cd "${_APP_PROJ_PATH}"; pwd`
CommandCheck "Failed to cd ${_APP_PROJ_PATH}, Please check if this folder is available or accessable"
cd "${_CURRENT_DIR}"

#Re-test to confirm
if ! [ -d "${_APP_PROJ_PATH}" ]; then
  echo "Error: Could not locate the application project - ${_APP_PROJ_PATH}"
  echo "       Aborting the script..."
  echo ""
  cleanupAndExit -23
fi

#Declare variables based on command-line variables
if [ "${_BUILD_EXECUTION_DIR}" = "" ]; then
  _BUILD_EXECUTION_DIR=${_APP_PROJ_PATH}
fi

if [ "${_APP_PROJ_PATH_BIN}" = "" ]; then
  _APP_PROJ_PATH_BIN=${_APP_PROJ_PATH}/bin
fi

_KEY_STORE_ABS_DIR=${_APP_PROJ_PATH}/../${_KEY_STORE_LOCATION}
_KEY_STORE_PATH_DEFAULT=${_KEY_STORE_ABS_DIR}/${_KEY_STORE}

#Convert ${_KEY_STORE_PATH} to Absolute Path if existing
_CURRENT_DIR=`pwd`
if [[ -f "${_KEY_STORE_PATH}" ]]; then
    _KEY_STORE=`basename "${_KEY_STORE_PATH}"`
    _KEY_STORE_PATH=`dirname "${_KEY_STORE_PATH}"`
    _KEY_STORE_PATH=`cd "${_KEY_STORE_PATH}"; pwd`
    _KEY_STORE_PATH=${_KEY_STORE_PATH}/${_KEY_STORE}
elif ! [ "${_KEY_STORE_PATH}" = "" ]; then
  echo "Error: Could not locate the key store file - ${_KEY_STORE_PATH}"
  echo "       Aborting the script..."
  echo ""
  cleanupAndExit -24
fi
cd "${_CURRENT_DIR}"

#Check ${_BUILD_EXECUTION_DIR}
if ! [ -d "${_BUILD_EXECUTION_DIR}" ]; then
  echo "Error: Could not locate the build execution directory - ${_BUILD_EXECUTION_DIR}"
  echo "       Aborting the script..."
  echo ""
  cleanupAndExit -25
fi

#Convert ${_BUILD_EXECUTION_DIR} to Absolute Path
_CURRENT_DIR=`pwd`
_BUILD_EXECUTION_DIR=`cd "${_BUILD_EXECUTION_DIR}"; pwd`
CommandCheck "Failed to cd ${_BUILD_EXECUTION_DIR}, Please check if this folder is available or accessable"
cd "${_CURRENT_DIR}"

#Re-test to confirm
if ! [ -d "${_BUILD_EXECUTION_DIR}" ]; then
  echo "Error: Could not locate the build execution directory - ${_BUILD_EXECUTION_DIR}"
  echo "       Aborting the script..."
  echo ""
  cleanupAndExit -26
fi

chmod -R a+w "${_ACM_DIST_DIR}"
#CommandCheck "Failed to change mode for ${_ACM_DIST_DIR}, Please check if this folder is available or accessable"

_EXPORTED_RANDOMSEED=$(cat "${_PROD_DIST_DIR}/bin/internal/css/dat/RandomSeed")
CommandCheck "Failed to read RandomSeed in ${_PROD_DIST_DIR}/bin/internal/css/dat/, please check if this file is available or accessable"

#
# (3) check for an emulator existence, create and run avd if emulator not found
#
if [ "${_DEVELOPMENT_CWS_LIBRARY}" = "on" ]; then
    step "3" "Development flag found. Skipping device/emulator manipulation."

else

    step "3" "check for an emulator or device attached, create and run '${_virtual_device_name}' avd if emulator not found"
  
    getAttachedDevicesInfo count
    _attached_device_count=${_device_count}
    echo "number of devices attached = ${_attached_device_count}" 
    echo ""

    if [[ ${_attached_device_count} = 0 || "${_USE_EMULATOR}" = "on" ]]; then

        if [ "${_USE_EMULATOR}" != "on" ]; then
            echo "There is no emulator or device attached to this computer." 
            echo "An Android emulator or device is required for the JAC-signing process."
            echo "If you would like to use a device for signing, please, attach a device and"
            echo "re-run this script."
            echo "An emulator will be used by default."            
            _USE_EMULATOR=on
        else
            echo "There is a request to use an Android emulator for the JAC-signing process."
            echo "Trying to create and launch an emulator from the script."
        fi
      
        echo ""
        echo "Create an Android virtual device with name '${_virtual_device_name}'"
        echo "for Android target SDK - ${_android_target_sdk} "
        echo ""
        echo -e "no" | android create avd -n ${_virtual_device_name} -t ${_android_taget_sdk} -f
      
        getFreeEmulatorPort
        _current_emulator_port=${_free_emulator_port}
        if [ -z ${_current_emulator_port} ]; then
            echo "ERROR: Could not obtain a free emulator port to run an Android emulator with."
            echo "       It means either there are 16 Android emulators running on the computer or"
            echo "       the 'adb' tool has a problem or the script was corrupted."
            echo "       Aborting the script."
            cleanupAndExit -27
        fi
        _signer_device_id=emulator-${_free_emulator_port}
        echo ""
        echo "Launch the '${_virtual_device_name}' emulator"
        echo "" 
        emulator -avd ${_virtual_device_name} -no-window -port ${_current_emulator_port} &
        _emulator_pid=$!
      
        echo -e "\n\nAndroid emulator has launched the '${_virtual_device_name}' virtual device"
        echo "as a process - ${_emulator_pid}"
        echo "with device ID = ${_signer_device_id}"
        echo "sleep for ${_wait_duration} seconds until the emulator is fully booted..."
        echo ""
        sleep ${_wait_duration}
    elif [ "${_SPECIFIC_DEVICE}" != "" ]; then
        _signer_device_id=${_SPECIFIC_DEVICE}
        checkAttachedDevices
    else
        checkAttachedDevices
    fi

    _specific_device="-s ${_signer_device_id}"
    ANDROID_SERIAL_BACKUP=${ANDROID_SERIAL}
    export ANDROID_SERIAL=${signer_device_id}

    # Create the ${_DEVICE_APP_DIR} directory
    # This command will fail if it already exists.
    _result=$(adb ${_specific_device} shell mkdir "${_DEVICE_APP_DIR}")
  
    # change access mode for ${_DEVICE_APP_DIR} directory
    execAdbShellCommand "chmod 777 \"${_DEVICE_APP_DIR}\"" "${_specific_device}"

    # verify that the ${_DEVICE_APP_DIR} contains files
    _result=$(adb ${_specific_device} shell ls "${_DEVICE_APP_DIR}")

    if ! [ "${_result}" = "" ]; then
        # clean up the ${_DEVICE_APP_DIR} directory
        execAdbShellCommand "rm -r ${_DEVICE_APP_DIR}/*" "${_specific_device}"
        _result=$(adb ${_specific_device} shell ls "${_DEVICE_APP_DIR}")
        if ! [ "${_result}" = "" ]; then
          echo "Error: Could not clean up the ${_DEVICE_APP_DIR} directory on the device or emulator."
          echo ""
          cleanupAndExit -28
        fi
    fi

    # verify that the ${_DEVICE_APP_DIR} directory is writable
    execAdbShellCommand "mkdir \"${_DEVICE_APP_DIR}/xxx\"" "${_specific_device}"
    execAdbShellCommand "rm -r \"${_DEVICE_APP_DIR}/xxx\"" "${_specific_device}"
fi


#
# (4) create secure stores
#
step "4" "create secure stores"

# remove old secure store and ACV files
rm -f "${_PROD_DIST_DIR}/provision/data/irss*.dat"
rm -f "${_PROD_DIST_DIR}/provision/data/acv*.dat"

_CURRENT_DIR=`pwd`
export ACM_DIST_DIR=${_ACM_DIST_DIR} && export EXPORTED_RANDOMSEED=${_EXPORTED_RANDOMSEED} && export CAT_INSTALL_DIR=${_PROD_DIST_DIR} && cd "${_PROD_DIST_DIR}/provision" && bash ./createKeyStore.sh android arm
_ERRORCODE=$?
if [ ${_ERRORCODE} -ne 0 ];then 
	echo "Possible errors:"
	echo "    (1) Failed to export ACM_DIST_DIR=${_ACM_DIST_DIR}, EXPORTED_RANDOMSEED=${_EXPORTED_RANDOMSEED}, or CAT_INSTALL_DIR=${_PROD_DIST_DIR}"
	echo "    (2) Failed to cd to the directory ${_PROD_DIST_DIR}/provision. Please check if this directory is available or accessable"
	echo "    (3) Possible error: Failed to run bash file: ./createKeyStore.sh. Please check this file exists"
    cleanupAndExit ${_ERRORCODE} 
fi
cd "${_CURRENT_DIR}"


#
# (5) copy JACRuntime.jar, cws.jar, and libraries started from ${_CWS_LIBRARY_PREFIX} prefix to the application project
#
step "5" "copy JACRuntime.jar, cws.jar, and libraries started from ${_CWS_LIBRARY_PREFIX} prefix to the ${_APP_PROJ_PATH} application project"

remove_file "${_APP_PROJ_PATH}/libs/JACRuntime.jar"
CommandCheck "Failed to remove ${_APP_PROJ_PATH}/libs/JACRuntime.jar"

remove_file "${_APP_PROJ_PATH}/libs/cws.jar"
CommandCheck "Failed to remove ${_APP_PROJ_PATH}/libs/cws.jar"

mkdir -p "${_APP_PROJ_PATH}/libs"
CommandCheck "Failed to create the directory ${_APP_PROJ_PATH}/libs"


if [ "${_NO_COPY_JARS}" = "off" ]; then
    cp "${_PROD_DIST_DIR}/lib/${_CONFIG}/cws.jar" "${_APP_PROJ_PATH}/libs/cws.jar"
	CommandCheck "Failed to copy ${_PROD_DIST_DIR}/lib/${_CONFIG}/cws.jar to ${_APP_PROJ_PATH}/libs/cws.jar. Please check if this file is available or accessable"
	
    cp "${_PROD_DIST_DIR}/lib/${_CONFIG}/JACRuntime.jar" "${_APP_PROJ_PATH}/libs/JACRuntime.jar"
	CommandCheck "Failed to copy ${_PROD_DIST_DIR}/lib/${_CONFIG}/JACRuntime.jar to ${_APP_PROJ_PATH}/libs/JACRuntime.jar. Please check if this file is available or accessable"
fi

# remove cws native libraries from all possible arches
for (( arch_i=0; arch_i<=$(( ${#_TARGET_ARCH_VALUES[*]} -1 )); arch_i++ ))
do
    rm -f "${_APP_PROJ_PATH}/libs/${_TARGET_ARCH_VALUES[${arch_i}]}/${_CWS_LIBRARY_PREFIX}*"
    CommandCheck "Failed to remove ${_APP_PROJ_PATH}/libs/${_TARGET_ARCH_VALUES[${arch_i}]}/${_CWS_LIBRARY_PREFIX}*"
done

    
#
# ARCH Iteration - BEGIN:
#                               perfrom the operations from step 4.1 to 5
#
for (( arch_i=0; arch_i<=$(( ${#_ARCH_LIST[*]} -1 )); arch_i++ ))
do
    # set the current architecture type
    _ARCHDIR="${_ARCH_LIST[${arch_i}]}"
    echo ""
    echo "#     processing ${_ARCHDIR} libs..."
    echo ""
    
    if ! [ -d "${_PROD_DIST_DIR}/lib/${_CONFIG}/${_ARCHDIR}" ]; then
        echo "Error: Could not locate the distribution directory - ${_PROD_DIST_DIR}/lib/${_CONFIG}/${_ARCHDIR}"
        echo "       Aborting the script..."
        echo "       (Option: a specific architecture can be built against by using the \"-target_arch\" option.)"
        echo ""
        cleanupAndExit -29
    fi
    
    validateOptionValue "-target_arch" "${_ARCHDIR}" "${_TARGET_ARCH_VALUES[*]}" "${_TARGET_ARCH_SUFFIXES[*]}"
    _TARGET_ARCH_SUFFIX="${_VALIDATED_VALUE}"

#
# (5.1) create list of libcws*.so libraries and the corresponding acv*.dat names
#
# The production distribution can contain several flavors of libcws.so dynamic 
# libraries produced for different types of device CPU and Android SDKs. For instance,
# the distribution can contain the following list of libraries in 
# the CloakwareStreaming/lib/android/armeabi directory:
#     libcws_fr_armv5.so    - libcws for Froyo and ARMv5
#     libcws_hc_armv6.so    - libcws for Honeycomb and ARMv6
#     libcws_is_armv7.so    - libcws for Ice Cream Sandwich and ARMv7
#     libcws_ps_fr_armv5.so - pass stub for Froyo and ARMv5
#     libcws_ps_hc_armv6.so - pass stub for Honeycomb and ARMv6
#     libcws_ps_is_armv7.so - pass stub for Ice Cream and ARMv7
#
# All the production libraries (not pass stubs) should be signed with 
# Irdeto Active Cloak (IAC) system.
# When IAC signs libraries, it generates the corresponding vouchers:
#     acv_fr_armv5.dat  - voucher for libcws_fr_armv5.so
#     acv_hc_armv6.dat  - voucher for libcws_hc_armv6.so
#     acv_is_armv7.dat  - voucher for libcws_is_armv7.so
#
# The following code creates three string arrays:
#   - _lib_list[]     - to keep names of libcws*.so libraries
#   - _acv_list[]     - to keep names of acv*.dat vouchers
#   - _lib_ps_list[]  - to keep names of libces_ps*.so pass stubs
#

_filename=
_fullname=
_suffix=
_ps_suffix=
_lib_list=()
_acv_list=()
_lib_ps_list=()
_lib_sign_list=()
_lib_total=
_target_suffix=${_TARGET_SDK_SUFFIX}${_TARGET_CPU_SUFFIX}

    
if [ ! -z ${_TARGET_SDK_SUFFIX} ]; then
    if [ -z ${_TARGET_CPU_SUFFIX} ]; then
        echo "If you specific a -target_sdk option, you must also specify a -target_cpu option!"        
        cleanupAndExit -30
    fi 
else
    if [ ! -z ${_TARGET_CPU_SUFFIX} ]; then
        echo "If you specific a -target_cpu option, you must also specify a -target_sdk option!"        
        cleanupAndExit -31
    fi 
fi


# the following loop will iterate through all files located in 
# the CloakwareStreaming/lib/android/armeabi directory and put
# the names of the files into the corresponding lists depending 
# on the prefix. If the prefix is "libcws" (without trailing _ps),
# it will put it into the _lib_list[] array. It also will generate
# the corresponding voucher name and include it into the _acv_list[].
# If the prefix is "libcws_ps", it will add it into the _lib_ps_list[].
#
# If _TARGET_SDK_SUFFIX and/or _TARGET_CPU_SUFFIX are not empty,
# only those libraries that has these suffixes will be included 
# into the lists.

while IFS= read -d $'\0' -r file ; do
        _fullname=$(basename "${file}")
        _filename=${_fullname%%.*}
		
        echo "_fullname = ${_fullname}, _filename = ${_filename}"
        _suffix=${_filename##${_CWS_LIBRARY_PREFIX}}
        _ps_suffix=${_filename##${_CWS_PS_LIBRARY_PREFIX}}
        if [ "${_filename}" = "${_ps_suffix}" ]; then
            # include either all file names if the _target_suffix is empty
            # or only those file names, which suffix is equal to the _target_suffix
            echo "_suffix = ${_suffix}, _target_suffix = ${_target_suffix}"
            if [[ "${_target_suffix}" = "" || "${_suffix}" = "${_target_suffix}" ]]; then
                # generate acv*.dat file name just adding the matching
                # suffix. For instance, for the libcws_is_armv7.so library
		        # it will produce the acv_is_armv7.dat voucher.
                _acvname=acv${_suffix}${_TARGET_ARCH_SUFFIX}.dat
                # add libcws*.so and acv*.dat name to the corresponding lists
                _lib_list=("${_lib_list[@]}" "${_fullname}")
                _acv_list=("${_acv_list[@]}" "${_acvname}")
            fi
        else
            # include either all file names if the _target_suffix is empty
            # or only those file names, which suffix is equal to the _target_suffix
            if [[ "${_target_suffix}" = "" || "${_ps_suffix}" = "${_target_suffix}" ]]; then
                # add libcws_ps*.so name to the _lib_ps_list[]
                _lib_ps_list=("${_lib_ps_list[@]}" "${_fullname}")
            fi
        fi
done < <(find "${_PROD_DIST_DIR}/lib/${_CONFIG}/${_ARCHDIR}" -iname "${_CWS_LIBRARY_PREFIX}*" -print0)

mkdir -p "${_APP_PROJ_PATH}/libs/${_ARCHDIR}"
CommandCheck "Failed to create the directory ${_APP_PROJ_PATH}/libs/${_ARCHDIR}"

# copy required libraries
if [[ "${_DEVELOPMENT_CWS_LIBRARY}" = "on" ]]; then
    # copy libcws_ps*.so libraries
    for libps in "${_lib_ps_list[@]}"
    do
      cp "${_PROD_DIST_DIR}/lib/${_CONFIG}/${_ARCHDIR}/${libps}" "${_APP_PROJ_PATH}/libs/${_ARCHDIR}/"
	  CommandCheck "Failed to copy ${_PROD_DIST_DIR}/lib/${_CONFIG}/${_ARCHDIR}/${libps} to ${_APP_PROJ_PATH}/libs/${_ARCHDIR}/. Please check if this file is available or accessable"
    done
    _lib_sign_list=(${_lib_ps_list[*]})
else
    # copy libcws*.so libraries
    for lib in "${_lib_list[@]}"
    do
      cp "${_PROD_DIST_DIR}/lib/${_CONFIG}/${_ARCHDIR}/${lib}" "${_APP_PROJ_PATH}/libs/${_ARCHDIR}/"
	  CommandCheck "Failed to copy ${_PROD_DIST_DIR}/lib/${_CONFIG}/${_ARCHDIR}/${libps} to ${_APP_PROJ_PATH}/libs/${_ARCHDIR}/. Please check if this file is available or accessable"
    done
    _lib_sign_list=(${_lib_list[*]})
fi
_lib_total=${#_lib_sign_list[*]}


#
# (5.2) perform IAC signing on all libraries started from ${_CWS_LIBRARY_PREFIX} prefix
# 
echo ""
echo "#     perform IAC signing on all libraries started from '${_CWS_LIBRARY_PREFIX}' prefix"
echo ""

#
# copy external libraries from extlibs.txt into provision/externlibs folder
# Ignore lines that start with comments (#)
#
mkdir -p "${_PROD_DIST_DIR}/provision/externlibs"
rm -f "${_PROD_DIST_DIR}/provision/externlibs/*"
CommandCheck "Failed to remove ${_PROD_DIST_DIR}/provision/externlibs/*, Please check if any files are in use"

# create a list of external library names (without 'lib' prefix and '.so' suffix)
# from the application's assets/extlibs.txt file
_extlib_list=$(cat "${_APP_PROJ_PATH}/assets/extlibs.txt" | grep ^[^#].*)
_extlib=
# iterate through the external library list
for extlib_name in ${_extlib_list}
do 
    echo "extlib_name = ${extlib_name}"
    if [ -f "${_APP_PROJ_PATH}/libs/${_ARCHDIR}/lib${extlib_name}.so" ]; then
        echo "lib${extlib_name}.so exists for the ${_ARCHDIR} architecture"
        _extlib=${_APP_PROJ_PATH}/libs/${_ARCHDIR}/lib${extlib_name}.so
    elif [ -f "${_APP_PROJ_PATH}/libs/${_DEFAULT_ARCH}/lib${extlib_name}.so" ]; then
        echo "lib${extlib_name}.so exists for the default ${_DEFAULT_ARCH} architecture"
        _extlib=${_APP_PROJ_PATH}/libs/${_DEFAULT_ARCH}/lib${extlib_name}.so
    else
        cd "${_INITIAL_DIR}" 
		echo "Error: Could not find the external lib${extlib_name}.so library in either "
		echo "         ${_APP_PROJ_PATH}/libs/${_DEFAULT_ARCH} directory or"
		echo "         ${_APP_PROJ_PATH}/libs/${_ARCHDIR} directory"
		echo "       The library name '${extlib_name}' is in the external library list - ${_APP_PROJ_PATH}/assets/extlibs.txt"
        cleanupAndExit -32 
    fi
    cp -f "${_extlib}" "${_PROD_DIST_DIR}/provision/externlibs"
done

# sign every 'libcws*.so' library from the _lib_list[] array 
_CURRENT_DIR=`pwd`
for (( i=0; i<=$(( ${_lib_total} -1 )); i++ ))
do
    echo "${_lib_sign_list[$i]} <=> ${_acv_list[$i]}"
    export ACM_DIST_DIR=${_ACM_DIST_DIR} && export EXPORTED_RANDOMSEED=${_EXPORTED_RANDOMSEED} && export CAT_INSTALL_DIR=${_PROD_DIST_DIR} && cd "${_PROD_DIST_DIR}/provision" && bash ./createACV.sh "${_APP_PROJ_PATH}/libs/${_ARCHDIR}" "${_lib_sign_list[$i]}" android arm
    _ERRORCODE=$?
    if [ ${_ERRORCODE} -ne 0 ]; then
        cd "${_INITIAL_DIR}" 
		echo "Possible errors:"
		echo "    (1) Failed to export ACM_DIST_DIR=${_ACM_DIST_DIR}, EXPORTED_RANDOMSEED=${_EXPORTED_RANDOMSEED}, or CAT_INSTALL_DIR=${_PROD_DIST_DIR}"
		echo "    (2) Failed to cd to the directory ${_PROD_DIST_DIR}/provision. Please check if this directory is available or accessable"
		echo "    (3) Failed to run bash file: ./createACV.sh. Please check if this file exists"
        cleanupAndExit ${_ERRORCODE} 
    fi
	# The createACV program generates an acv.dat file. It should be renamed into the name 
    # with the suffux that matches to the corresponding signed library
    if ! [ "${_acv_list[$i]}" = "acv.dat" ]; then
        mv "${_PROD_DIST_DIR}/provision/data/acv.dat" "${_PROD_DIST_DIR}/provision/data/${_acv_list[$i]}"
		CommandCheck "Failed to move ${_PROD_DIST_DIR}/provision/data/acv.dat to ${_PROD_DIST_DIR}/provision/data/${_acv_list[$i]}"
    fi
done
cd "${_CURRENT_DIR}"

done
# ARCH Iteration - END
#


#
# (6) copy secure store files to the application project: irss.dat, acv.dat
#
step "6" "copy secure store files to the ${_APP_PROJ_PATH} application project"

remove_dir_content "${_APP_PROJ_PATH}/assets/data"
CommandCheck "Failed to remove ${_APP_PROJ_PATH}/assets/data"

remove_file "${_APP_PROJ_PATH}/assets/jac_store.dat"
CommandCheck "Failed to remove ${_APP_PROJ_PATH}/assets/jac_store.dat"

# create necessary folders
mkdir -p "${_APP_PROJ_PATH}/assets/data"
CommandCheck "Failed to create the directory ${_APP_PROJ_PATH}/assets/data"

# copy provision data including irss.dat and acv.dat
if ! [[ "${_DISABLED_VERSIONS}" = "on" ]]; then
	cp "${_PROD_DIST_DIR}"/provision/data/irss.dat "${_APP_PROJ_PATH}"/assets/data/.
	CommandCheck "Failed to copy ${_PROD_DIST_DIR}/provision/data/irss.dat to ${_APP_PROJ_PATH}/assets/data/. Please check if this file is available or accessable"
fi
cp "${_PROD_DIST_DIR}"/provision/data/irss3.dat "${_APP_PROJ_PATH}"/assets/data/.
CommandCheck "Failed to copy ${_PROD_DIST_DIR}/provision/data/irss3.dat to ${_APP_PROJ_PATH}/assets/data/. Please check if this file is available or accessable"

cp "${_PROD_DIST_DIR}"/provision/data/acv*.dat "${_APP_PROJ_PATH}"/assets/data/.
CommandCheck "Failed to copy ${_PROD_DIST_DIR}/provision/data/acv*.dat to ${_APP_PROJ_PATH}/assets/data/. Please check if these files are available or accessable"

#
# (7) build application with either a user-defined pre-build command or the standard Apache Ant tool
#
_CURRENT_DIR=`pwd`
cd "${_BUILD_EXECUTION_DIR}"
CommandCheck "Failed to cd the directory ${_BUILD_EXECUTION_DIR}, please check if this directory is available or accessable"

if [ "${_PRE_BUILD_COMMAND}" = "donotrun" ]; then
    step "7" "Skipping pre-JAC-signing build."
    echo "'donotrun' flag detected. "
else
    if [ "${_PRE_BUILD_COMMAND}" = "${_DEFAULT_PRE_BUILD_COMMAND}" ]; then
        step "7" "build ${_APP_NAME} with the standard Apache Ant tool"
        createDefaultKeyStore
        prepareBuildPropertiesAndUpdateApplicationProject
    else
        step "7" "build ${_APP_NAME} with a user-defined pre-build command"
    fi
    ${_PRE_BUILD_COMMAND}
    _ERRORCODE=$?
    if [ ${_ERRORCODE} -ne 0 ];then 
        cd ${_INITIAL_DIR}
        echo "Error: '${_PRE_BUILD_COMMAND}' command failed with code ${_ERRORCODE}."
        if [ "${_PRE_BUILD_COMMAND}" == "${_DEFAULT_PRE_BUILD_COMMAND}" ]; then
            echo "       This problem usually happens if either the application project location path"
            echo "       points to a wrong place or the application name is not correct."
            echo "       Please, verify these parameters again, fix them, and restart the script."
        fi
        echo "       Aborting the script..."
        cleanupAndExit ${_ERRORCODE} 
    fi
fi

#Convert ${_APP_PROJ_PATH_BIN} to Absolute Path
_APP_PROJ_PATH_BIN=`cd "${_APP_PROJ_PATH_BIN}"; pwd`

#Check ${_APP_PROJ_PATH_BIN}
if ! [ -d "${_APP_PROJ_PATH_BIN}" ]; then
    echo "Error: Could not locate the application project output directory - ${_APP_PROJ_PATH_BIN}"
    if [ "${_PRE_BUILD_COMMAND}" != "${_DEFAULT_PRE_BUILD_COMMAND}" ]; then
        if [ "${_PRE_BUILD_COMMAND}" = "donotrun" ]; then
            echo "       Because you asked to not run build process by setting -prebuildcommand 'donotrun',"
        else 
            echo "       Because you asked to run your own prebuild command - ${_PRE_BUILD_COMMAND},"
        fi
        echo "       you also need to setup an application project output folder location" 
        echo "       by passing -aprojoutput parameter with a path to this folder."
        echo "       Please, restart your command with -aprojoutput parameter."
        echo "       See the corresponding help information by running '${_SCRIPT_NAME} -help' command."
    fi
    echo "       Aborting the script..."
    echo ""
    cleanupAndExit -33
fi

# check existence of the built APK
if ! [ -f "${_APP_PROJ_PATH_BIN}/${_APP_APK_NAME}" ]; then
    echo ""
    echo "Error: Could not locate the built apk file ${_APP_APK_NAME}"
    echo "       in the project output directory - ${_APP_PROJ_PATH_BIN}"

    # let's check what APK files do exist in the ${_APP_PROJ_PATH_BIN} directory
    getListOfFilesInFolderByTemplate "${_APP_PROJ_PATH_BIN}" "*.apk"

    if [ "${_LIST_OF_FILES}" != "" ]; then
        echo "       Found the following APK files instead - ${_LIST_OF_FILES}."
    else 
        echo "       There are no any APK files in the ${_APP_PROJ_PATH_BIN} directory."
    fi     
    if [ "${_PRE_BUILD_COMMAND}" != "${_DEFAULT_PRE_BUILD_COMMAND}" ]; then
        if [ "${_PRE_BUILD_COMMAND}" = "donotrun" ]; then
            echo "       Because you asked to not run build process by setting -prebuildcommand 'donotrun',"
        else 
            echo "       Because you asked to run your own prebuild command - ${_PRE_BUILD_COMMAND},"
        fi
        echo "       you also need to setup a resulting application APK name " 
        echo "       by passing -apkname parameter with name of the final application APK file."
    elif [ "${_LIST_OF_FILES}" != "" ]; then
        echo "       You probably incorrectly defined the resulting application APK name " 
        echo "       with -apkname parameter. You should either remove the parameter or use"
        echo "       an application APK file name from the list of found APKs above."
    fi
    echo "       Please, restart your command with -apkname parameter."
    echo "       See the corresponding help information by running '${_SCRIPT_NAME} -help' command."
    echo "       Aborting the script..."
    echo ""
    cleanupAndExit -34
fi

cd "${_CURRENT_DIR}"

#
# (8) perform JAC signing
#
 _CURRENT_DIR=`pwd`
if [[ "${_DEVELOPMENT_CWS_LIBRARY}" = "on" ]]; then
  step "8" "Development flag detected. Skipping JAC signing"
else
  step "8" "perform JAC signing"
  
  cd "${_PROD_DIST_DIR}/provision"
  CommandCheck "Failed to cd the directory ${_PROD_DIST_DIR}/provision, please check if this directory is available or accessable"
  
  checkAttachedDevices  
  chmod +x ./jac_signing.sh
  ./jac_signing.sh "${_APP_PROJ_PATH}" "${_APP_PROJ_PATH_BIN}" "${_APP_APK_NAME}" "${_DEVICE_APP_DIR}" ${_ANDROID_TARGET_OS_ID} "${_signer_device_id}" -s
  _ERRORCODE=$?
  
  if [ ${_ERRORCODE} -ne 0 ];then 
    cd "${_INITIAL_DIR}"
    echo "Error: JAC sign failed"
	echo "       Aborting the script..."
    cleanupAndExit ${_ERRORCODE} 
  fi
fi

cd "${_CURRENT_DIR}"

#
# (9) stop emulator, delete avd
#
if [ "${_DEVELOPMENT_CWS_LIBRARY}" = "on" ]; then
    step "9" "Development flag detected. Skipping emulator actions."
else

  if [[ "${_USE_EMULATOR}" = "on" ]]; then
    step "9" "stop emulator, delete '${_virtual_device_name}' avd"

    if ! [ "${_emulator_pid}" = "" ]; then
      echo ""
      echo "Closing the '${_virtual_device_name}' emulator as a ${_emulator_pid} process."
      echo ""
      kill ${_emulator_pid}
  
      android delete avd -n ${_virtual_device_name}
	  CommandCheck "Failed to delete ${_virtual_device_name}"
    fi
  fi
  
  export ANDROID_SERIAL=${ANDROID_SERIAL_BACKUP}
fi

#
# (10) build application with ANT after JAC signing
#
if [ "${_POST_BUILD_COMMAND}" = "${_DEFAULT_POST_BUILD_COMMAND}" ]; then
    step "10" "build ${_APP_NAME} with Apache Ant tool after JAC signing"
else
    step "10" "build ${_APP_NAME} with a user-defined post build command"
fi

_CURRENT_DIR=`pwd`
cd "${_BUILD_EXECUTION_DIR}"
CommandCheck "Failed to cd the directory ${_BUILD_EXECUTION_DIR}, please check if this directory is available or accessable"
${_POST_BUILD_COMMAND}
_ERRORCODE=$?

if [ ${_ERRORCODE} -ne 0 ];then 
  cd "${_INITIAL_DIR}"
  echo "Error: ${_POST_BUILD_COMMAND} failed"
  echo "       Aborting the script..."
  cleanupAndExit ${_ERRORCODE} 
fi

echo ""
echo " *** Successully built ${_APP_APK_NAME} application ***"
echo " *** It is located in the ${_APP_PROJ_PATH_BIN} directory *** "
echo ""

cd "${_CURRENT_DIR}"

