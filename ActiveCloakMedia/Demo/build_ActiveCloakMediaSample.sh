#!/bin/bash 

# This script runs ./build_activecloaked_apk.sh against ActiveCloakMediaSample/Android project
# that generates ActiveCloakMediaSample application

# determine location of the bash script
if [ "${BASH_SOURCE}" = "" ]; then
    _SCRIPT_PATH=${0}
else
    _SCRIPT_PATH=${BASH_SOURCE}
fi
if [ "${_SCRIPT_PATH}" = "" ]; then
    echo "ERROR: Could not determine the bash script location."
    echo "       \${0} and \${BASH_SOURCE} values are empty."
    echo "       Abort the script."
    exit 1
fi
_SCRIPT_NAME=${_SCRIPT_PATH##*/}
_SCRIPT_DIR=${_SCRIPT_PATH%${_SCRIPT_NAME}}
# determine the full path of the bash script
if [ "${_SCRIPT_DIR}" = "" ]; then
    _MY_FULL_PATH=$(echo ${PWD}/${_SCRIPT_NAME})
else
    _MY_FULL_PATH=$(cd "${_SCRIPT_DIR}" && echo ${PWD}/${_SCRIPT_NAME})
fi 
_MY_HOME=`dirname "${_MY_FULL_PATH}"`
_CURRENT_DIR=$(pwd)
_APPLICATION_NAME=ActiveCloakMediaSample
_PROJECT_DIR_NAME=ActiveCloakMediaSample/Android
_CLOAKWARESTREAMING_SDK_LOCATION=${_MY_HOME}/..

echo ""
echo "Running the following command to build ${_APPLICATION_NAME}:"
echo ""
echo "\"${_MY_HOME}/build_activecloaked_apk.sh\" -an ${_APPLICATION_NAME} -ap \"${_MY_HOME}/${_PROJECT_DIR_NAME}\" -cws \"${_CLOAKWARESTREAMING_SDK_LOCATION}\" ${1} ${2}"

"${_MY_HOME}/build_activecloaked_apk.sh" -an ${_APPLICATION_NAME} -ap "${_MY_HOME}/${_PROJECT_DIR_NAME}" -cws "${_CLOAKWARESTREAMING_SDK_LOCATION}" ${1} ${2}
