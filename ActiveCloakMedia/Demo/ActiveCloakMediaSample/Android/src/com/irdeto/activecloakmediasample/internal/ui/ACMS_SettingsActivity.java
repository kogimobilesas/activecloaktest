package com.irdeto.activecloakmediasample.internal.ui;

import static com.irdeto.activecloakmediasample.ACMS_LicenseManagerService.LICENSE_MANAGER_OPERATION_CLEAN_LICENSES;
import static com.irdeto.activecloakmediasample.ACMS_LicenseManagerService.LICENSE_MANAGER_OPERATION_REFRESH_LICENSES;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.irdeto.activecloakmediasample.ACMS_LicenseManagerService;
import com.irdeto.activecloakmediasample.R;

/**
 * NOTE: This class contains code that is relevant to this application,
 * but not essential to understanding how to integrate the 
 * ActiveCloakMediaPlayer. You are welcome to look at it and understand
 * how it works, and take any pieces that might be relevant to your own
 * player, but it is not necessary to do so.
 * 
 * This activity allows access to miscellaneous functionality such as clearing licenses
 * and viewing logs.
 * 
 * @author irdetodev
 *
 */
public class ACMS_SettingsActivity extends ACMS_Activity {
		
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settings);
	}
	


	public void cleanLicenses(View view) {
		writeToLog("User cleaning the license store.");
		Intent cleanLicenseIntent = new Intent(this, ACMS_LicenseManagerService.class);
		cleanLicenseIntent.putExtra(ACMS_LicenseManagerService.LICENSE_MANAGER_OPERATION_EXTRA, 
				LICENSE_MANAGER_OPERATION_CLEAN_LICENSES);
		startService(cleanLicenseIntent);
		UIUtils.showDialog(this, "Licenses have been cleaned.");
	}
	
	public void refreshLicenses(View view) {
		writeToLog("User refreshing all licenses.");
		Intent refreshLicenseIntent = new Intent(this, ACMS_LicenseManagerService.class);
		refreshLicenseIntent.putExtra(ACMS_LicenseManagerService.LICENSE_MANAGER_OPERATION_EXTRA, 
				LICENSE_MANAGER_OPERATION_REFRESH_LICENSES);
		startService(refreshLicenseIntent);
		UIUtils.showDialog(this, "Licenses have been refreshed.");
	}

	public void openDiagnostics(View view) {
		writeToLog("Opening diagnostics.");
		Intent intent = new Intent(this, ACMS_DiagnosticsActivity.class);
    	startActivity(intent);
	}


}
