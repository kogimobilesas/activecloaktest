package com.irdeto.activecloakmediasample.internal.ui;

import static com.irdeto.activecloakmediasample.ACMS_LicenseManagerService.LICENSE_MANAGER_OPERATION_ACQUIRE_LICENSE;
import static com.irdeto.activecloakmediasample.ACMS_LicenseManagerService.LICENSE_MANAGER_OPERATION_CLEAR_LICENSE;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import com.irdeto.activecloakmediasample.ACMS_LicenseManagerService;
import com.irdeto.activecloakmediasample.ACMS_LicenseManagerService.ACMS_LicenseManagerBinder;
import com.irdeto.activecloakmediasample.ACMS_PlayerActivity;
import com.irdeto.activecloakmediasample.R;
import com.irdeto.activecloakmediasample.internal.data.ACMS_ContentDescriptor;

public class ACMS_RightsViewActivity extends ACMS_Activity {

	private ACMS_ContentDescriptor mContentDescriptor;
	
	// Local handle on the LicenseManagerService and a boolean indicating whether the
	// service is currently bound.
	private ACMS_LicenseManagerService mLicenseManagerService;
	boolean mBoundToLicenseManagerService = false;

	// Last known licensing info, so that the screen is refreshed only when necessary.
	private String mLastLicenseInfo = "";
	
	// Handler and runnable for updating the display each second.
	private Handler mHandler;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rightsview);
		
		mContentDescriptor = getIntent().getParcelableExtra(
				ACMS_PlayerActivity.CONTENT_DESCRIPTOR_EXTRA);
		
		// Bind to the license manager service.
		Intent intent = new Intent(this, ACMS_LicenseManagerService.class);
		bindService(intent, mLicenseManagerConnection, Context.BIND_AUTO_CREATE);
				
		if (mHandler == null)
	    {
	    	// start a timer to update things, if not already started. 
	    	final int delay = 1000;
		    mHandler = new Handler();
		    
		    Runnable updateDisplayTask = new Runnable()
			{
	
				@Override
				public void run()
				{
				    showRights();
				    mHandler.postAtTime(this, SystemClock.uptimeMillis() + delay);
				}
			};
			mHandler.postAtTime(updateDisplayTask, SystemClock.uptimeMillis() + delay);
	    }	
		
	}
	
	/*
	 * Whenever this service gets destroyed, unbind from the license manager service.
	 * 
	 * (non-Javadoc)
	 * @see android.app.Service#onDestroy()
	 */
	@Override
	public void onDestroy() {
		
		// Unbind from the license manager service
        if (mBoundToLicenseManagerService) {
            unbindService(mLicenseManagerConnection);
            mBoundToLicenseManagerService = false;
        }
        mHandler.removeCallbacksAndMessages(null);
        
		super.onDestroy();
	}
	
	private void showRights() {
		
		
		if( mLicenseManagerService != null) {
		
			String licenseInfo = mLicenseManagerService.getLicenseInfo(
					mContentDescriptor.getUrl(),
					mContentDescriptor.getType());
			
			if( mLastLicenseInfo.equals(licenseInfo) == false) {
				setContentView(R.layout.rightsview);
				TextView textView = (TextView)findViewById(R.id.rights_display);
				textView.setMovementMethod(new ScrollingMovementMethod());
				textView.setText(licenseInfo);
				mLastLicenseInfo = licenseInfo;
			}
		}
	}
	
	/**
	 * Defines callbacks for service binding, passed to bindService()
	 */
	private ServiceConnection mLicenseManagerConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			ACMS_LicenseManagerBinder binder = (ACMS_LicenseManagerBinder) service;
			mLicenseManagerService = binder.getService();
			mBoundToLicenseManagerService = true;
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			mBoundToLicenseManagerService = false;
		}
	};
	
	 public void acquireLicense(View view) {
		 writeToLog("User acquiring license for " + mContentDescriptor.getLabel());
		 Intent acquireLicenseIntent = new Intent(this, ACMS_LicenseManagerService.class);
		 acquireLicenseIntent.putExtra(ACMS_LicenseManagerService.LICENSE_MANAGER_OPERATION_EXTRA, 
				 LICENSE_MANAGER_OPERATION_ACQUIRE_LICENSE);
		 acquireLicenseIntent.putExtra(ACMS_PlayerActivity.CONTENT_DESCRIPTOR_EXTRA, mContentDescriptor);
		 startService(acquireLicenseIntent);
	 }

	 public void clearLicense(View view) {
		 writeToLog("User clearing license for " + mContentDescriptor.getLabel());
		 Intent clearLicenseIntent = new Intent(this, ACMS_LicenseManagerService.class);
		 clearLicenseIntent.putExtra(ACMS_LicenseManagerService.LICENSE_MANAGER_OPERATION_EXTRA, 
				 LICENSE_MANAGER_OPERATION_CLEAR_LICENSE);
		 clearLicenseIntent.putExtra(ACMS_PlayerActivity.CONTENT_DESCRIPTOR_EXTRA, mContentDescriptor);
		 startService(clearLicenseIntent);
	 }
	 
	 public void copyToLog(View view) {
		 writeToLog("User copying license information to log.");
		 writeToLog(
				 "License info for " 
						 + mContentDescriptor.getLabel() 
						 + ", " 
						 + mContentDescriptor.getUrl() 
						 + ": \n"
						 + mLastLicenseInfo 
						 + "\n");
	 }

}
