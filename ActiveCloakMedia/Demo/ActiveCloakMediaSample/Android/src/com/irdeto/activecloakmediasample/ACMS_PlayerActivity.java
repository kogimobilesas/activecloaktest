package com.irdeto.activecloakmediasample;

import static com.irdeto.activecloakmediasample.ACMS_LicenseManagerService.LICENSE_MANAGER_OPERATION_ACQUIRE_LICENSE;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.List;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.irdeto.activecloakmediasample.ACMS_DownloaderService.ACMS_DownloaderBinder;
import com.irdeto.activecloakmediasample.ACMS_LicenseManagerService.ACMS_LicenseManagerBinder;
import com.irdeto.activecloakmediasample.internal.data.ACMS_ContentDescriptor;
import com.irdeto.activecloakmediasample.internal.data.ACMS_LibraryManager;
import com.irdeto.activecloakmediasample.internal.ui.ACMS_Activity;
import com.irdeto.activecloakmediasample.internal.ui.UIUtils;
import com.irdeto.media.ActiveCloakAgent;
import com.irdeto.media.ActiveCloakLocaleOption;
import com.irdeto.media.ActiveCloakDeviceIdChangedListener;
import com.irdeto.media.ActiveCloakEventListener;
import com.irdeto.media.ActiveCloakEventType;
import com.irdeto.media.ActiveCloakException;
import com.irdeto.media.ActiveCloakMediaPlayer;
import com.irdeto.media.ActiveCloakOutputRestrictions;
import com.irdeto.media.ActiveCloakUrlType;

/**
 * This activity plays content with the ActiveCloakMediaPlayer.
 * 
 * @author irdetodev
 *
 */
public class ACMS_PlayerActivity extends ACMS_Activity {

	public static final boolean DEBUG = true;
	
	public static final boolean PERFORMANCE_LOGGING = false;
	
	// Data structure containing information about the content to play. This
	// will be passed from another activity.
	private ACMS_ContentDescriptor mContentDescriptor = null;
	
	// String constant used to retrieve the content descriptor from the calling
	// activity.
	public final static String CONTENT_DESCRIPTOR_EXTRA = "com.irdeto.activecloakmediasample.contentdescriptor";
	
	// Local handles on the agent and media player. These are instantiated
	// each time this activity is created, e.g. every time a video is played.
	private ActiveCloakAgent mActiveCloakAgent = null;
	private ActiveCloakMediaPlayer mActiveCloakMediaPlayer = null;

	// UI components specifying the location where the video will play. These
	// are defined externally in the layout XML file but we keep a local 
	// reference here in order to associate them with the media player.
	private SurfaceView mVideoSurface;
	private FrameLayout mVideoFrame;
	
	// Local handle on the LicenseManagerService and a boolean indicating whether
	// the service is currently bound.
	private ACMS_LicenseManagerService mLicenseManagerService;
	boolean mBoundToLicenseManagerService = false;
	
	// Local handle on the DownloaderService and a boolean indicating whether
	// the service is currently bound.
	private ACMS_DownloaderService mDownloaderService;
	boolean mBoundToDownloaderService = false;
	
	// UI widget for seeking.
	private SeekBar mSeekBar = null;
	// Increment for each seek, decrement when POSITION_CHANGED event arrives.
	private int mSeeking = 0; 
	// UI widget to display the elapsed and total time.
	private TextView mText = null;
	// Handler for timer jobs.
	Handler mHandler = new Handler();
	// Table row in the UI to hold buttons.
    private TableRow mTableRow = null;
    // Whether to enable or disable closed captions.
    private boolean mClosedCaptionEnabled = false;
    // Whether the video is downloadable; indicates potential progressive download.
    private boolean mDownloadableVideo = false;
    // Position to start playback from.
    private int mPositionToRestore = 0;
    
	/*
	 *  In the onCreate method, we set up the base UI and initialize the Agent.
	 *  If the URL is of type envelope, we will bind to the downloader service.
	 * 
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		
		// Set the layout and get local handles on the video UI elements.
		setContentView(R.layout.player);
		mVideoSurface = (SurfaceView) findViewById(R.id.VideoViewOutput_Surface);
		mVideoFrame = (FrameLayout) findViewById(R.id.VideoViewOutput_Frame);
		
		// Retrieve information from the calling activity about what content
		// is to be played. With this descriptor, getUrl() returns the URL, 
		// and getType() returns the ActiveCloakUrlType.
		mContentDescriptor = getIntent().getParcelableExtra(
				CONTENT_DESCRIPTOR_EXTRA);
		
		// If the content type is envelope and the file is not local, it needs to be
		// downloaded.
		if( mContentDescriptor.getType() == ActiveCloakUrlType.ENVELOPE) {			
			mDownloadableVideo = true;
			// Bind to the downloader service.
			Intent downloaderIntent = new Intent(this, ACMS_DownloaderService.class);
			bindService(downloaderIntent, mDownloaderConnection, Context.BIND_AUTO_CREATE);
			if( mContentDescriptor.getIsProtected()) {
				// Bind to the license manager service.
				Intent licenseManagerIntent = new Intent(this, ACMS_LicenseManagerService.class);
				bindService(licenseManagerIntent, mLicenseManagerConnection, Context.BIND_AUTO_CREATE);
			}
		}

		// Our exception handling extends to catching ActiveCloakExceptions
		// thrown by our classes, and then any general exceptions. In both
		// cases this application simply throws up a dialog reporting the
		// exception, but other handling can be put in as appropriate.
		try {

			// Create the agent.
			mActiveCloakAgent = new ActiveCloakAgent(this,
					new ActiveCloakDeviceIdChangedListener() {

						@Override
						public void deviceIdChanged(
								String previousDeviceId,
								String currentDeviceId) {
						}
					});

			// Create the player
			mActiveCloakMediaPlayer = new ActiveCloakMediaPlayer(
					mActiveCloakAgent);

			// Add the various buttons, sliders, time display labels, etc.
			addPlayerControls();
			
			// Initialize the timer that will update time display etc.
			initializeTimer();

			// Reset performance capturing information.  
			mActiveCloakAgent.resetPerfReport();
	
			// Set up the display
			mActiveCloakMediaPlayer.setupDisplay(mVideoSurface,
					mVideoFrame);
			
			// Example of how to limit bitrate to 100K or less. It is also possible
			// to set a minimum bitrate using setMinBitrate()
			// mActiveCloakMediaPlayer.setMaxBitrate(100);
	
			// Set the initial value of the closed caption property.
			mActiveCloakMediaPlayer.setClosedCaptionEnabled(mClosedCaptionEnabled); 
						
			// Start playback in streaming/local file cases.
			// If we are doing a progressive download, a separate task will begin playback.
			if( mDownloadableVideo == false ) {
				play();	
			}

		} catch (ActiveCloakException e) {
			UIUtils.showDialog(this,
					"An ActiveCloakException was thrown by the PlayerActivity.\n"
							+ "Result = " + e.getResult() + "\n"
							+ "Message = " + e.getMessage());
		}		
	}
	
	/**
	 * Helper to play content; opens the player, sets up the graphical information,
	 * and plays.
	 */
	private void play() {
		try {
			final String licenseOverrideUrl = null;
			
			writeToLog("Now beginning playback of content "
					+ mContentDescriptor.getLabel()
					+ " with URL "
					+ mContentDescriptor.getUrl()
					+ "\n");
			
			long totalContentLength = 0;
			if( mDownloaderService != null ) {
				totalContentLength = mDownloaderService.getTotalFileSize(mContentDescriptor);
			}
						
			// Open the media player. 
			mActiveCloakMediaPlayer.open(
					new ACMS_SendUrlRequestListener(
							licenseOverrideUrl, mActiveCloakAgent, this),
					new MyActiveCloakEventListener(mContentDescriptor, this), 
					mContentDescriptor.getType(), 
					mContentDescriptor.getUrl(),
					null,
					mPositionToRestore,
					totalContentLength
					);
			
			// Make the video frame and surface visible. If this is not done
			// the video is not visible.
			mVideoFrame.setVisibility(View.VISIBLE);
			mVideoSurface.setVisibility(View.VISIBLE);

			// Play the video.
			mActiveCloakMediaPlayer.play();
			
			
				
		} catch (ActiveCloakException e) {
			String errorMessage = "An ActiveCloakException was thrown by the PlayerActivity.\n"
					+ "Result = " + e.getResult() + "\n"
					+ "Message = " + e.getMessage();
			writeToLog(errorMessage);
			exitWithMessage(errorMessage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

	// Common implementation of ActiveCloakEventListener 
	protected static class MyActiveCloakEventListener implements
			ActiveCloakEventListener {

		// Local handles on the content being played and the calling activity, for
		// logging purposes.
		private ACMS_ContentDescriptor content;
		private ACMS_PlayerActivity activity;
		
		// The last event type received.
		private ActiveCloakEventType lastEventType;
		
		MyActiveCloakEventListener(ACMS_ContentDescriptor content, ACMS_PlayerActivity activity) {
			this.content = content;
			this.activity = activity;
		}

		@Override
		public void onEvent(ActiveCloakEventType eventType, long result1,
				long result2, long result3, String resultString) {

			// When DRM decryption fails once it tends to fail over and over. To prevent 
			// flooding the log, ignore any consecutive event notifications of this type.
			if( lastEventType == ActiveCloakEventType.DRM_DECRYPT_FAILED
					&& eventType == ActiveCloakEventType.DRM_DECRYPT_FAILED) {
				return;
			}
	
			lastEventType = eventType;
			
			// Log the event.
			activity.writeToLog("Event triggered for "
					+ content.getLabel() + ": " + eventType.toString() + "\n");
						
			if (eventType == ActiveCloakEventType.LICENSE_UPDATE_FAILED) {
				try {
					if (activity.mActiveCloakMediaPlayer.isOpen()) {
						activity.mActiveCloakMediaPlayer.close();
					}
				} catch (ActiveCloakException e) {
					UIUtils.showDialog(activity,
							"An ActiveCloakException was thrown by the PlayerActivity.\n"
									+ "Result = " + e.getResult() + "\n"
									+ "Message = " + e.getMessage());
				}
			} else if (eventType == ActiveCloakEventType.POSITION_CHANGED) {
				activity.mSeeking = 0; // We are no longer seeking.
			} else if (eventType == ActiveCloakEventType.PROVISION_NEEDED) {
				// Nothing, for now.
			} else if( eventType == ActiveCloakEventType.MULTIPLE_AUDIO_STREAMS) {
				try {
					// If multiple audio options exist, add the Audio button to the 
					// viewable screen.
					List<ActiveCloakLocaleOption> audioOptions = activity.mActiveCloakMediaPlayer.getAvailableAudioOptions();
					activity.writeToLog( "Number of audio options = " + audioOptions.size() + "\n");
					for( ActiveCloakLocaleOption audioOption : audioOptions) {
						activity.writeToLog( "Audio Option: " + audioOption.getLanguageName() + "\n");
					}
					if( activity.mActiveCloakMediaPlayer.getAvailableAudioOptions().size() > 1 ) {
						// UI updates have to run on the UI thread.
						activity.runOnUiThread(new Runnable() 
						{
							public void run() 
							{ 
								Button audioButton = (Button) activity.findViewById(R.id.controlsAudioButton);
								audioButton.setVisibility(Button.VISIBLE);
							}
						}); 
					}
				} catch( ActiveCloakException e ) {
					UIUtils.showDialog(activity,
							"An ActiveCloakException was thrown by the PlayerActivity.\n"
									+ "Result = " + e.getResult() + "\n"
									+ "Message = " + e.getMessage());
				}
			} else if( eventType == ActiveCloakEventType.MULTIPLE_SUBTITLE_STREAMS) {
				try {
					// If multiple subtitle options exist, add the Subs button to the 
					// viewable screen.
					List<ActiveCloakLocaleOption> subtitleOptions = activity.mActiveCloakMediaPlayer.getAvailableSubtitleOptions();
					activity.writeToLog( "Number of subtitle options = " + subtitleOptions.size() + "\n");
					for( ActiveCloakLocaleOption subtitleOption : subtitleOptions) {
						activity.writeToLog( "Subtitle Option: " + subtitleOption.getLanguageName() + "\n");
					}
					if( activity.mActiveCloakMediaPlayer.getAvailableSubtitleOptions().size() > 1 ) {
						// UI updates have to run on the UI thread.
						activity.runOnUiThread(new Runnable() 
						{
							public void run() 
							{ 
								Button subsButton = (Button) activity.findViewById(R.id.controlsSubtitleButton);
								subsButton.setVisibility(Button.VISIBLE);
							}
						}); 
					}
				} catch( ActiveCloakException e ) {
					UIUtils.showDialog(activity,
							"An ActiveCloakException was thrown by the PlayerActivity.\n"
									+ "Result = " + e.getResult() + "\n"
									+ "Message = " + e.getMessage());
				}
			} else if( eventType == ActiveCloakEventType.PLAYBACK_COMPLETED ) {
				activity.exitWithMessage("Playback complete."); 			
			} else if( eventType == ActiveCloakEventType.EXTERNAL_DISPLAY_RESTRICTION_CHANGED) {
				if( DEBUG ) {
					try {
						ActiveCloakOutputRestrictions opl = activity.mActiveCloakMediaPlayer.getOutputRestrictions();
						activity.writeToLog("New OPL Levels: " +
						" AV = " + opl.getAnalogVideo() +
						" CDA = " + opl.getCompressedDigitalAudio() +
						" CDV = " + opl.getCompressedDigitalVideo() +
						" UDA = " + opl.getUncompressedDigitalAudio() +
						" UDV = " + opl.getUncompressedDigitalVideo() + "\n");
					} catch (ActiveCloakException e1) {
						activity.writeToLog("Error trying to get new OPL levels.\n");
					}
				}
			} else if( eventType == ActiveCloakEventType.HOST_NOT_FOUND_ERROR ) {
				// Handling of events that are fatal to playback.
				activity.exitWithMessage("Playback failed due to event: " + eventType.toString());
			}
		}
	}
	
	private void exitWithMessage(final String message) {
		final Activity activity = this;
		runOnUiThread(new Runnable() 
		{
			public void run() 
			{ 
				Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
			}
		}); 
		writeToLog("Player exit message: " + message);
		// Create a task that will finish the activity since playback is done.
		Runnable finishActivityTask = new Runnable() 
		{
			public void run() 
			{ 	
				finish();
			}
		}; 
		// Schedule the task to run in one second. This is less abrupt than closing right away.
		mHandler.postAtTime(finishActivityTask, SystemClock.uptimeMillis() + 1000);
	}
	 
	/*
	 * Called if the user hits the Back button.
	 *
	 * (non-Javadoc)
	 * @see android.app.Activity#onBackPressed()
	 */
	@Override
	public void onBackPressed() {
		closeCommon();
		
		super.onBackPressed();
	}
	
	
	/**
	 * Called when the user hits the play/pause button.
	 */
	public void onPlayPauseClick(View view) {
		
		if( mActiveCloakMediaPlayer != null ) {
			Button playPauseButton = (Button) findViewById(R.id.controlsPlayPauseButton);
			try {
				if( mActiveCloakMediaPlayer.isPlaying()) {
					writeToLog("User paused playback.");
					mActiveCloakMediaPlayer.pause();	
					playPauseButton.setText("Play");
				} else {
					writeToLog("User resumed playback.");
					mActiveCloakMediaPlayer.play();
					playPauseButton.setText("Pause");
				}
			} catch (ActiveCloakException e) {
				UIUtils.showDialog(this, "ActiveCloakException on touch event.");
			}
		}
	}
	
	/*
	 * This is called both when the player is first opened, and when it is switched
	 * back to.
	 * 
	 * (non-Javadoc)
	 * @see android.app.Activity#onStart()
	 */
	@Override
	public void onStart() {
		super.onStart();
		
		// If the user is switching back to the app, and the video was paused, resume it.
		try {
			if (mActiveCloakMediaPlayer != null
					&& mActiveCloakMediaPlayer.isOpen()
					&& !mActiveCloakMediaPlayer.isPlaying()) {
				mActiveCloakMediaPlayer.play();
				Button playPauseButton = (Button) findViewById(R.id.controlsPlayPauseButton);
				playPauseButton.setText("Pause");
			}
		} catch (ActiveCloakException e) {

		}
	}
	
	/*
	 * This is called both when the Back button is hit, and when the application is
	 * switched away from.
	 * 
	 * (non-Javadoc)
	 * @see android.app.Activity#onStop()
	 */
	@Override
	public void onStop() {
		// If the user switches away, pause the video. If the activity is being
		// destroyed there is no harm in pausing the player prior to closing it.
		try {
			mActiveCloakMediaPlayer.pause();
		} catch( ActiveCloakException e ) {
			
		}
		
		super.onStop();
	}
	
	/*
	 * This is called when the Back button is hit or when the minimized activity
	 * is destroyed by the Android OS.
	 * 
	 * (non-Javadoc)
	 * @see android.app.Activity#onDestroy()
	 */
	@Override
	public void onDestroy() {
		
		// Close the media player.
		closeCommon();
		
        if( mBoundToLicenseManagerService) {
        	unbindService(mLicenseManagerConnection);
        	mBoundToLicenseManagerService = false;
        }
        if( mBoundToDownloaderService ) {
        	unbindService(mDownloaderConnection);
        	mBoundToDownloaderService = false;
        }
        mHandler.removeCallbacksAndMessages(null);

		super.onDestroy();
	}
	
	/**
	 * Common code for closing the media player, and logging the performance information.
	 */
	private void closeCommon() {
		if (mActiveCloakMediaPlayer.isOpen()) {
			try {
				// Close the media player.
				mActiveCloakMediaPlayer.close();
				
				// Make the video frame and surface visible. If this is not done
				// the video is not visible.
				mVideoFrame.setVisibility(View.INVISIBLE);
				mVideoSurface.setVisibility(View.INVISIBLE);
			
				if( PERFORMANCE_LOGGING ) {
					// Write performance information to the log. This can be skipped in
					// cases where the information is not useful.
					mActiveCloakAgent.dumpPerfReport();
					InputStream is = openFileInput("cwsperf.log");
					byte[] buffer = new byte[is.available()];
					is.read(buffer);
					String bufferString = new String(buffer);
					writeToLog("Performance report for: "
							+ mContentDescriptor.getLabel() + "\n" + bufferString
							+ "\n\n\n");
				} 
			} catch (ActiveCloakException e) {
				UIUtils.showDialog(this, "ActiveCloakException thrown trying to close the media player.");
			} catch (FileNotFoundException e) {
				UIUtils.showDialog(this, "FileNotFoundException thrown trying to retrieve performance information." );
			} catch (IOException e) {
				UIUtils.showDialog(this, "IOException thrown trying to retrieve performance information.");
			}
		}
	}
	
	/**
	 * Defines callbacks for service binding, passed to bindService()
	 */
	private ServiceConnection mLicenseManagerConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			ACMS_LicenseManagerBinder binder = (ACMS_LicenseManagerBinder) service;
			mLicenseManagerService = binder.getService();
			mBoundToLicenseManagerService = true;
			URI uri = URI.create(mContentDescriptor.getUrl());
			final String scheme = uri.getScheme();
			if( scheme.equals("file") == false
				&& mContentDescriptor.getIsProtected() == true
				&& mLicenseManagerService.hasRights(mContentDescriptor) == false) 
			{
				// Explicit license acquisition.
				Intent acquireLicenseIntent = new Intent(ACMS_PlayerActivity.this, ACMS_LicenseManagerService.class);
				acquireLicenseIntent.putExtra(ACMS_LicenseManagerService.LICENSE_MANAGER_OPERATION_EXTRA, 
						LICENSE_MANAGER_OPERATION_ACQUIRE_LICENSE);
				acquireLicenseIntent.putExtra(ACMS_PlayerActivity.CONTENT_DESCRIPTOR_EXTRA, mContentDescriptor);
				startService(acquireLicenseIntent);		
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			mBoundToLicenseManagerService = false;
		}
	};
	
	
	/**
	 * Defines callbacks for service binding, passed to bindService()
	 */
	private ServiceConnection mDownloaderConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			ACMS_DownloaderBinder binder = (ACMS_DownloaderBinder) service;
			mDownloaderService = binder.getService();
			mBoundToDownloaderService = true;
			if( !ACMS_DownloaderService.isEnabled()) {
				ACMS_PlayerActivity.this.exitWithMessage("Downloader service is not operational. Cannot play this video.");
				return;
			}
			URI uri = URI.create(mContentDescriptor.getUrl());
			final String scheme = uri.getScheme();
			if( scheme.equals("file") == false) 
			{ 
				// remote file, download it.
				mDownloaderService.startDownload(mContentDescriptor, ACMS_DownloaderService.PRIORITY_HIGH);
			}
			else if( mDownloaderService.isPaused(mContentDescriptor)) 
			{
				// Local file in the library paused; must be resumed for playback to work properly.
				try {
					mDownloaderService.pauseAllDownloads();
					mDownloaderService.resumeDownload(mContentDescriptor);
				} catch( ActiveCloakException e ) {
					ACMS_PlayerActivity.this.exitWithMessage("Unable to resume paused download. Resume it from the library before playing again.");
				}
			}
			Runnable checkPlayStatusTask = new Runnable()
			{
				@Override
				public void run()
				{
					// For unprotected content, local content, or content with a license, 
					// play right away.
					// Otherwise wait for the license.
					if( mDownloaderService.getTotalFileSize(mContentDescriptor) > 0 &&
						(mContentDescriptor.getIsProtected() == false ||
						scheme.equals("file") ||
						(mLicenseManagerService != null 
						&& mLicenseManagerService.hasRights(mContentDescriptor)))) {					
						for( ACMS_ContentDescriptor cd : ACMS_LibraryManager.INSTANCE.getLibrary(getApplicationContext())) {
							if( cd.getUrl().equals(ACMS_DownloaderService.getLocalUriForRemoteUri(mContentDescriptor.getUrl()))) {
								mContentDescriptor = cd;
							}
						}
						play();
					} else {
						mHandler.postAtTime(this, SystemClock.uptimeMillis() + 1000);	
					}
				}
			};
			// Post the initial runnable.
			mHandler.postAtTime(checkPlayStatusTask, SystemClock.uptimeMillis() + 1000);
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			mBoundToDownloaderService = false;
		}
	};
	
	/**
	 * This function is called to add UI controls to the player dynamically. 
	 * These currently include a seek bar and the time display.
	 */
	void addPlayerControls() throws ActiveCloakException
	{
		TableLayout table = (TableLayout) findViewById(R.id.controlsFrameTable);
	
		mTableRow = (TableRow) findViewById(R.id.controlsTableRow);
	    mText = (TextView) findViewById(R.id.controlsTimeDisplay);
	    
	    table.setColumnShrinkable(mTableRow.indexOfChild(mText), true);
	    table.setColumnStretchable(mTableRow.indexOfChild(mText), true);
	    
	    mSeeking = 0;
	    mSeekBar = (SeekBar) findViewById(R.id.controlsSeekBar);
	    mSeekBar.setOnSeekBarChangeListener(
	    		new OnSeekBarChangeListener()
	    		{
	    			private boolean isTracking = false;
	    			private int seekPos = -1;
	    			private void doSeek(int progress)
	    			{
	    				if (progress == -1)
	    				{
	    					progress = seekPos;
	    				}
						if (progress != -1 )
						{
							if( mDownloadableVideo 
									&& mDownloaderService != null
									&& progress >= (int)(((long)mDownloaderService.getBytesDownloaded(mContentDescriptor) * mSeekBar.getMax()) / mDownloaderService.getTotalFileSize(mContentDescriptor))) 
							{		
								// User is trying to seek past the available video, so fix the
								// seekbar and ignore the request.
								updateSeekBar();
								return;
							}
							mSeeking = 1; // set seeking to true.
							seekPos = progress;
						    mSeekBar.setBackgroundColor(Color.WHITE); // set background color as soon as progress moves to indicate we are pending a seek operation
						    
							if (!isTracking && mActiveCloakMediaPlayer != null)
							{
								try
								{
									writeToLog("User seeking to position " + seekPos);
									mActiveCloakMediaPlayer.seekTo(seekPos);
									mSeeking = 1; // set seeking to 1 again because we could have gotten a notification in the background.
									seekPos = -1; // reset until next seek attempt.
								} catch (ActiveCloakException e)
								{
									writeToLog("Seek operation failed: " + e.getMessage());
								}
							}
							
							updateSeekBar();
						}	    				
	    			}
	    			
					@Override
					public void onProgressChanged(SeekBar seekBar,
							int progress, boolean fromUser)
					{
						if (fromUser)
						{
							doSeek(progress); // do the seek if we aren't tracking
						}
						
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar)
					{
						isTracking = true;
					}

					@Override
					public void onStopTrackingTouch(SeekBar seekBar)
					{
						isTracking = false;
						// do the seek now that we aren't tracking
						doSeek(-1);// -1 means 'the last progress value we received that was within range'
					}
	    			
	    		}
	    	);    
	}
	
	void initializeTimer() {
		
    	final int delay = 500;
	    
	    Runnable updateSeekBarTask = new Runnable()
		{
			@Override
			public void run()
			{
				// Update the seekbar and set up to run again in half a second.
			    updateSeekBar();
			    mHandler.postAtTime(this, SystemClock.uptimeMillis() + delay);
			}
		};
		// Post the initial runnable.
		mHandler.postAtTime(updateSeekBarTask, SystemClock.uptimeMillis() + delay);
	}
	
	public void onAudioClick(View view) {
		final ACMS_PlayerActivity activity = this;
		try
    	{
    		// If the "Audio" button is clicked, toggle to the next audio option.
    		// Note that the first time this is clicked, the toggle may select the
    		// audio option currently playing, if that one appeared first in the
    		// manifest.
			List<ActiveCloakLocaleOption> audioOptions = mActiveCloakMediaPlayer.getAvailableAudioOptions();
			String selectedLocale = mActiveCloakMediaPlayer.getSelectedAudioLocale();
			int selectedOptionIndex = -1;
			for( int i = 0; i < audioOptions.size(); i++ ) {
				if( audioOptions.get(i).getLanguageId().equals(selectedLocale)) {
					selectedOptionIndex = i;
				}
			}
			selectedOptionIndex = (selectedOptionIndex + 1) % audioOptions.size();
			String newSelectedLocale = audioOptions.get(selectedOptionIndex).getLanguageId();
			mActiveCloakMediaPlayer.setSelectedAudioLocale(newSelectedLocale);
			
			// Now that a new audio option has been selected, re-serve the current
			// URL to get the new audio.
			mPositionToRestore = (int) mActiveCloakMediaPlayer.getPosition();
			final CharSequence toastText = "Switching language to " + audioOptions.get(selectedOptionIndex).getLanguageName();
			writeToLog(toastText.toString() + "\n");
			runOnUiThread(new Runnable() 
			{
				public void run() 
				{ 
					// Let the user know we are switching languages.
					Toast.makeText(activity, toastText, Toast.LENGTH_LONG).show();	
				}
			}); 
			
			closeCommon();
			play();
		}
    	catch (Exception e)
    	{
    		writeToLog("Exception trying to switch audio streams: " + e.getMessage());
    	}
	}
	
	public void onSubtitlesClick(View view) {	
				
		try
    	{
    		// If the "Subs" button is clicked, toggle to the next subtitle option.
    		// Note that the first time this is clicked, the toggle may select the
    		// audio option currently playing, if that one appeared first in the
    		// manifest.			
			List<ActiveCloakLocaleOption> subtitleOptions = mActiveCloakMediaPlayer.getAvailableSubtitleOptions();
			String selectedLocale = mActiveCloakMediaPlayer.getSelectedSubtitlesLocale();
			int selectedOptionIndex = -1;
			for( int i = 0; i < subtitleOptions.size(); i++ ) {
				if( subtitleOptions.get(i).getLanguageId().equals(selectedLocale)) {
					selectedOptionIndex = i;
				}
			}
			selectedOptionIndex = (selectedOptionIndex + 1) % subtitleOptions.size();
			String newSelectedLocale = subtitleOptions.get(selectedOptionIndex).getLanguageId();
			mActiveCloakMediaPlayer.setSelectedSubtitlesLocale(newSelectedLocale);
			
			// Now that a new subtitle option has been selected, re-serve the current
			// URL to get the new subtitles.
			mPositionToRestore = (int) mActiveCloakMediaPlayer.getPosition();
			final CharSequence toastText = "Switching language to " + subtitleOptions.get(selectedOptionIndex).getLanguageName();
			writeToLog(toastText.toString() + "\n");
			runOnUiThread(new Runnable() 
			{
				public void run() 
				{ 
					// Let the user know we are switching languages.
					Toast.makeText(ACMS_PlayerActivity.this, toastText, Toast.LENGTH_LONG).show();	
				}
			}); 
			
			closeCommon();
			play();
		}
    	catch (Exception e)
    	{
    		writeToLog("Exception trying to switch subtitle streams: " + e.getMessage());
    	}
	}
	
	
	public void toggleClosedCaption(View view) {
		try
    	{
			final Activity activity = this;
			if( mClosedCaptionEnabled ) {
				mClosedCaptionEnabled = false;
			} else {
				mClosedCaptionEnabled = true;
			}
			mActiveCloakMediaPlayer.setClosedCaptionEnabled(mClosedCaptionEnabled);
			final CharSequence charSeq = "Closed captions are " + 
					(mClosedCaptionEnabled ? "on." : "off.");
			writeToLog("User action: " + charSeq.toString());
			runOnUiThread(new Runnable() 
			{
				public void run() 
				{ 
					Toast.makeText(activity, charSeq, Toast.LENGTH_SHORT).show();	
				}
			});
		}
    	catch (Exception e)
    	{
    		writeToLog("Exception trying to toggle closed captioning: " + e.getMessage());
    	}
	}
	
	void updateSeekBar()
	{
		// this helper is called periodically from a timer.
		// We just set the seek bar to that position in the stream with the duration as the max
		if (mSeekBar != null
				&& mSeeking == 0 // don't do this if there are pending seek operations 
				&& mActiveCloakMediaPlayer != null
				&& mActiveCloakMediaPlayer.isOpen())
		{
			// Update the color to show that we've recovered from a seek operation 
    	    mSeekBar.setBackgroundColor(Color.TRANSPARENT);
			
			long duration = 0;
			long position = 0;
			boolean isLive = false;
			
			try
			{
				position = mActiveCloakMediaPlayer.getPosition(); 
				duration = mActiveCloakMediaPlayer.getDuration(); 
				isLive = mActiveCloakMediaPlayer.isLive();				
			} catch (ActiveCloakException e) {
				writeToLog("Exception trying to update seekbar: " + e.getMessage());
			}
	
			// For a downloadable video, the available streaming area is the ratio of downloaded
			// video to total file size. For a stream, everything is available.
			if (mDownloadableVideo)
			{
				if( mDownloaderService != null ) {
					long m_PDTotalBytes = mDownloaderService.getTotalFileSize(mContentDescriptor);
					long m_PDProgress = mDownloaderService.getBytesDownloaded(mContentDescriptor);
					mSeekBar.setSecondaryProgress(
							m_PDTotalBytes > 0 ? 
									(int)(((long)m_PDProgress * duration) / ((long)m_PDTotalBytes) ) : 0);	
				}				
			}
			else
			{
				mSeekBar.setSecondaryProgress((int)duration);
			}
			
			// construct the time string 
			String curtime, totaltime;
			if (isLive)
			{				
				curtime = String.format("%02d:%02d", (int)(position/1000)/60, (int)(position/1000)%60);
				
				// If we more than 30 seconds back from what is showing live, show the distance.
				if (duration - position > 30000)    
				{
				    totaltime = String.format("-%02d:%02d", (int)(duration-position)/1000/60, (int)(duration-position)/1000%60);
				}
				else
				{
				    totaltime = String.format("Live");
				}
			}
			else
			{
				curtime = String.format("%02d:%02d", (int)(position/1000)/60, (int)(position/1000)%60);
				totaltime = String.format("%02d:%02d", (int)(duration/1000)/60, (int)(duration/1000)%60);
			}
			
			mText.setText(String.format("%s/%s", curtime, totaltime));
			
			mSeekBar.setProgress((int)position);
			mSeekBar.setMax((int)duration);
		}
	}
}
