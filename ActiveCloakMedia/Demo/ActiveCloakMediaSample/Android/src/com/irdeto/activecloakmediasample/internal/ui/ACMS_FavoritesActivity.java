package com.irdeto.activecloakmediasample.internal.ui;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.irdeto.activecloakmediasample.ACMS_PlayerActivity;
import com.irdeto.activecloakmediasample.R;
import com.irdeto.activecloakmediasample.internal.data.ACMS_ContentDescriptor;
import com.irdeto.activecloakmediasample.internal.data.ACMS_FavoritesManager;
import com.irdeto.media.ActiveCloakUrlType;

/**
 * NOTE: This class contains code that is relevant to this application,
 * but not essential to understanding how to integrate the 
 * ActiveCloakMediaPlayer. You are welcome to look at it and understand
 * how it works, and take any pieces that might be relevant to your own
 * player, but it is not necessary to do so.
 * 
 * This activity displays a user's favorites and allows them to enter more.
 * 
 * @author irdetodev
 *
 */
public class ACMS_FavoritesActivity extends ACMS_Activity {

	private ACMS_FavoritesActivity mThisActivity = this;

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		showDisplay();
	}
		
	private void showDisplay() {
		setContentView(R.layout.favorites);
		LinearLayout layout = (LinearLayout)findViewById(R.id.favorite_display);
		Resources res = getResources();
		
		List<ACMS_ContentDescriptor> favoriteList = ACMS_FavoritesManager.INSTANCE.getFavorites(this);
		
		// For each piece of favorited content, display a button that, when
		// pressed, activates a popup that allows the content to be played 
		// or deleted.
		for( final ACMS_ContentDescriptor cd : favoriteList ) {
			Button contentButton = new Button(this);
			Drawable right = null;
			if( cd.getType() == ActiveCloakUrlType.HLS) {
				right = res.getDrawable(R.drawable.hls);
			} else if( cd.getType() == ActiveCloakUrlType.IIS) {
				right = res.getDrawable(R.drawable.iisss);
			} else {
				right = res.getDrawable(R.drawable.envelope);
			}
			Drawable left = null;
			if( cd.getIsProtected()) {
				left = res.getDrawable(R.drawable.icon);
			}
			contentButton.setCompoundDrawablesWithIntrinsicBounds(left, null, right, null);
			
			contentButton.setText(cd.getLabel());
			
			contentButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					AlertDialog dialog = new AlertDialog.Builder(mThisActivity).create();
					String dialogMessage = cd.getLabel() + "\n" + cd.getUrl();
					if( cd.getIsProtected()) {
						dialogMessage += "\nThis is Irdeto-protected content.";
					}
					dialog.setMessage(dialogMessage);
					dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Play", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Intent intent = new Intent(mThisActivity, ACMS_PlayerActivity.class);
							intent.putExtra(ACMS_PlayerActivity.CONTENT_DESCRIPTOR_EXTRA, cd);
					    	startActivity(intent);
						}
					});
					dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Delete Favorite", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							try {
								writeToLog("Deleting favorite " + cd.getLabel());
								ACMS_FavoritesManager.INSTANCE.deleteFavorite(mThisActivity, cd);
								mThisActivity.showDisplay();
							} catch (IOException e) {
								UIUtils.showDialog(mThisActivity, "Exception deleting favorite.");
							}
						}
					});
					dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Edit Favorite", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							writeToLog("Editing favorite " + cd.getLabel());
							EditText editText = (EditText) findViewById(R.id.enter_favorite_label);
							editText.setText(cd.getLabel());
							editText = (EditText) findViewById(R.id.enter_favorite_url);
							editText.setText(cd.getUrl());
						}
					});
					dialog.show();	
				}
			});
			layout.addView(contentButton);
		}

	}
	
	public void addFavorite(View view) {
		EditText editText = (EditText) findViewById(R.id.enter_favorite_label);
		String favoriteLabel = editText.getText().toString().trim();
		editText = (EditText) findViewById(R.id.enter_favorite_url);
		String favoriteUrl = editText.getText().toString().trim();
		
		try {
			writeToLog("User added favorite with label " + favoriteLabel + " and URL " + favoriteUrl);
			ACMS_FavoritesManager.INSTANCE.addFavorite(this,
                    new ACMS_ContentDescriptor(favoriteLabel,
                                    favoriteUrl,
                                    favoriteUrl.endsWith(".prdy") || favoriteUrl.endsWith(".m3u8") ? ActiveCloakUrlType.HLS : ActiveCloakUrlType.IIS,
                                    false));
		} catch (FileNotFoundException e) {
			UIUtils.showDialog(this, "FileNotFoundException writing favorites.txt");
		} catch (IOException e) {
			UIUtils.showDialog(this, "IOException writing favorites.txt");
		}
		
		showDisplay();
	}
}
