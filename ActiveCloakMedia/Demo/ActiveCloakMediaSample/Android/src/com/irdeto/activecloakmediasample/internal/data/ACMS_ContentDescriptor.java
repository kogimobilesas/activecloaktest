package com.irdeto.activecloakmediasample.internal.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.irdeto.media.ActiveCloakUrlType;

/**
 * NOTE: This class contains code that is relevant to this application,
 * but not essential to understanding how to integrate the 
 * ActiveCloakMediaPlayer. You are welcome to look at it and understand
 * how it works, and take any pieces that might be relevant to your own
 * player, but it is not necessary to do so.
 * 
 * Data structure describing a piece of content to be played.
 * 
 * @author irdetodev
 *
 */
public class ACMS_ContentDescriptor implements Parcelable {

	private String mLabel;
	private String mURL;
	private ActiveCloakUrlType mType;
	private boolean mIsProtected;

	public ACMS_ContentDescriptor(String label, String url, ActiveCloakUrlType type, boolean isProtected) {
		this.mLabel = label;
		this.mURL = url;
		this.mType = type;
		this.mIsProtected = isProtected;
	}
	
	public String getLabel() {
		return mLabel;
	}

	public String getUrl() {
		return mURL;
	}
	
	public ActiveCloakUrlType getType() {
		return mType;
	}
	
	public boolean getIsProtected() {
		return mIsProtected;
	}
	

	//
	// The remainder of this class consists of an implementation of Parcelable
	// to allow this object to be passed between Activities in an Intent.
	//

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel arg0, int arg1) {
		arg0.writeString(mLabel);
		arg0.writeString(mURL);
		arg0.writeValue(mType);
		arg0.writeValue(Boolean.valueOf(mIsProtected));
	}

	public static final Parcelable.Creator<ACMS_ContentDescriptor> CREATOR = new Parcelable.Creator<ACMS_ContentDescriptor>() {
		public ACMS_ContentDescriptor createFromParcel(Parcel in) {
			return new ACMS_ContentDescriptor(in);
		}

		public ACMS_ContentDescriptor[] newArray(int size) {
			return new ACMS_ContentDescriptor[size];
		}
	};

	private ACMS_ContentDescriptor(Parcel in) {
		mLabel = in.readString();
		mURL = in.readString();
		mType = (ActiveCloakUrlType) in.readValue(null);
		mIsProtected = (Boolean)in.readValue(null);
	}
}
