package com.irdeto.activecloakmediasample;

import static com.irdeto.activecloakmediasample.ACMS_LicenseManagerService.LICENSE_MANAGER_OPERATION_ACQUIRE_LICENSE;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import com.irdeto.activecloakmediasample.internal.data.ACMS_ContentDescriptor;
import com.irdeto.activecloakmediasample.internal.data.ACMS_LibraryManager;
import com.irdeto.media.ActiveCloakAgent;
import com.irdeto.media.ActiveCloakContentInfo;
import com.irdeto.media.ActiveCloakContentManager;
import com.irdeto.media.ActiveCloakDeviceIdChangedListener;
import com.irdeto.media.ActiveCloakException;
import com.irdeto.media.ActiveCloakUrlType;

/**
 * 
 * @author irdetodev
 */
public class ACMS_DownloaderService extends Service  {
	
	
	// Binder for the service.
	private final IBinder mBinder = new ACMS_DownloaderBinder();

	// Agent for the content manager.
	private static ActiveCloakAgent mActiveCloakAgent = null;
	
	// Content manager to handle and keep track of the downloads.
	private static ActiveCloakContentManager mActiveCloakContentManager = null;
		
	// To notify the user that queuing failed, or other pertinent information.
	private Handler mHandler;
	
	// Extra to use to start the service with a download priority (otherwise normal is assumed).
	public final static String PRIORITY_EXTRA = "com.irdeto.activecloakmediasample.ACMS_DownloaderService.PRIORITY";
	
	// High-priority download constant.
	public final static String PRIORITY_HIGH = PRIORITY_EXTRA + ".HIGH";
	
	// Normal-priority download constant.
	public final static String PRIORITY_NORMAL = PRIORITY_EXTRA + ".NORMAL";
	
	// Whether the downloader service is currently operational.
	private static boolean isEnabled = true;
	
	public ACMS_DownloaderService() {
		
	}
	

	@Override
	public IBinder onBind(Intent arg0) {
		return mBinder;
	}
	
	/*
	 * 
	 * @see android.app.Service#onCreate()
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		
		isEnabled = createDownloadsDirectory();
		
		if( !isEnabled) {
			return;
		}
		
		try {
			if( mActiveCloakAgent == null ) {
				mActiveCloakAgent = new ActiveCloakAgent(
						this,
						new ActiveCloakDeviceIdChangedListener() {
							@Override
							public void deviceIdChanged(
									String previousDeviceId,
									String currentDeviceId) {
							}
						});
			}
			
			if( mActiveCloakContentManager == null ) {		
				mActiveCloakContentManager = new ActiveCloakContentManager(mActiveCloakAgent);				
				mActiveCloakContentManager.startDownloader(getDownloadsDirectory());
				// Resume all downloads.
				for( ACMS_ContentDescriptor content : ACMS_LibraryManager.INSTANCE.getLibrary(this)) {
					if( isPaused(content)) {
						resumeDownload(content);
					}
				}
				
			}
			
		} catch( Exception e ) {
			e.printStackTrace();
		}
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if( intent != null ) {
			ACMS_ContentDescriptor cd = intent.getParcelableExtra(
					ACMS_PlayerActivity.CONTENT_DESCRIPTOR_EXTRA);
			String priority = intent.getStringExtra(PRIORITY_EXTRA);
			
			startDownload(cd, priority);
		}
		
		// Service will continue running until explicitly stopped.
		return START_STICKY;
	}
	
	/**
	 * Start a download.
	 * 
	 * @param cd Content descriptor for the file to download.
	 * @param priority Priority of the download, high or normal. A high-priority download 
	 * will pause all other downloads.
	 */
	public void startDownload(ACMS_ContentDescriptor cd, String priority) {
		
		if( mHandler == null ) {
			mHandler = new Handler();
		}
				
		String destFilename = cd.getUrl().substring(cd.getUrl().lastIndexOf("/") + 1);
		long requiredBitrate = 0;
		String withCookie = "";
		String resultMsg = null;
		boolean queued = false;
		
		try {
			
			if( !isEnabled ) {
				resultMsg = "Downloader service is not operational. Cannot download.";
			}

			// Check that the remote URL points to a file that exists, so that a useful
			// error message can be given if not.
			try {
			    URL url = new URL(cd.getUrl());
			    URLConnection conn = url.openConnection();
			    conn.connect();
			    conn.getContent();
			} catch (MalformedURLException e) {
			    resultMsg = "File not added to queue. URL is malformed.";
			} catch (IOException e) {
			    resultMsg = "File not added to queue. No file found at URL given.";
			}
			
			if( resultMsg == null && PRIORITY_HIGH.equals(priority)) {
				// Pause all other downloads
				pauseAllDownloads();
			}
			
			if( resultMsg == null) {
				queued = mActiveCloakContentManager.queueDownload(cd.getUrl(), cd.getType(), destFilename, 
						requiredBitrate, withCookie);
				if( !queued ) {
					resultMsg = "File not added to queue. It may already exist on the external storage.";
					if( PRIORITY_HIGH.equals(priority)) {
						// Check for a paused download that we may need to resume.
						for( ACMS_ContentDescriptor content : ACMS_LibraryManager.INSTANCE.getLibrary(this)) {
							if( getLocalUriForRemoteUri(cd.getUrl()).equals(content.getUrl())) {
								resumeDownload(content);
							}
						}
					}
				}
			}
			
			
			if( queued ) {
				
				// Create a library entry.
				ACMS_ContentDescriptor libraryCd = new ACMS_ContentDescriptor(
						cd.getLabel(), 
						// Point to the local file in the download directory.
						"file://" + getDownloadsDirectory() + "/" + destFilename,
						ActiveCloakUrlType.ENVELOPE, 
						cd.getIsProtected());
				ACMS_LibraryManager.INSTANCE.addLibraryEntry(this, libraryCd);
				
				if( cd.getIsProtected()) {
					// Preacquire a license so that playback is faster.
					Intent acquireLicenseIntent = new Intent(this, ACMS_LicenseManagerService.class);
					acquireLicenseIntent.putExtra(ACMS_LicenseManagerService.LICENSE_MANAGER_OPERATION_EXTRA, 
							LICENSE_MANAGER_OPERATION_ACQUIRE_LICENSE);
					acquireLicenseIntent.putExtra(ACMS_PlayerActivity.CONTENT_DESCRIPTOR_EXTRA, cd);
					startService(acquireLicenseIntent);
					
					
				}
				resultMsg = "File added to download queue.";
				
			}
			
			final String toastMsg = resultMsg;
			
			mHandler.post(new Runnable() {
				
				@Override
				public void run() {
					Toast.makeText(getApplicationContext(), toastMsg, Toast.LENGTH_LONG).show();
				}
			});
			
			
		} catch( Exception e ) {
			e.printStackTrace();
		}
	}
	
	private boolean createDownloadsDirectory() {
		boolean ret = true;
		
		File file = getDownloadsDirectoryFile();
		if( !file.exists()) {
			if( !file.mkdirs()) {
				Log.e("ActiveCloakMediaSample :: ", "Failed to create Downloads directory.");
				ret = false;
			}
		}
		
		return ret;
	}
	
	public void pauseDownload(ACMS_ContentDescriptor cd) throws ActiveCloakException {
		ActiveCloakContentInfo info = getLocalFileInfo(cd);
		if( info != null ) {
			mActiveCloakContentManager.pauseDownload(info.getUrl());	
		}
	}
	
	public void pauseAllDownloads() throws ActiveCloakException {		
		for( ACMS_ContentDescriptor content : ACMS_LibraryManager.INSTANCE.getLibrary(this)) {
			pauseDownload(content);
		}
	}
	
	public void resumeDownload(ACMS_ContentDescriptor cd) throws ActiveCloakException {
		ActiveCloakContentInfo info = getLocalFileInfo(cd);
		if( info != null ) {
			mActiveCloakContentManager.resumeDownload(info.getUrl());	
		}

	}
	
	public void cancelDownload(ACMS_ContentDescriptor cd) throws ActiveCloakException {
		ActiveCloakContentInfo info = getLocalFileInfo(cd);
		if( info != null ) {
			mActiveCloakContentManager.cancelDownload(info.getUrl());	
		}

	}
	
	public float getPercentComplete( ACMS_ContentDescriptor cd ) {
		ActiveCloakContentInfo info = getLocalFileInfo(cd);
		if( info != null ) {
			return info.getPercentComplete();
    	}
    	return 0;
	}
		
	public boolean isPaused(ACMS_ContentDescriptor cd) {
		ActiveCloakContentInfo info = getLocalFileInfo(cd);
		if( info != null ) {
			return info.getDownloadState() == ActiveCloakContentInfo.ActiveCloakDownloadState.PAUSED;
    	}
    	
    	return false;
	}
	
	public boolean isCompleted(ACMS_ContentDescriptor cd) {
		ActiveCloakContentInfo info = getLocalFileInfo(cd);
		if( info != null ) {
			return info.getDownloadState() == ActiveCloakContentInfo.ActiveCloakDownloadState.COMPLETED;
    	}
    	
    	return true;
	}
	
	/**
	 * Given a piece of content, determine its total size and store that information.
	 */
	public long getTotalFileSize(ACMS_ContentDescriptor cd) {
		long fileSize = 0;
		ActiveCloakContentInfo info = getLocalFileInfo(cd);
		if( info != null ) {
    		fileSize = info.getTotalSize();    		
    	}   	
		if( fileSize == 0 ) {
			File file = new File(getLocalUriForRemoteUri(cd.getUrl()).substring(7));
			fileSize = file.length();
		}
		return fileSize;
	}
	
	/**
	 * Will return the number of bytes downloaded for a given piece of content. In the case
	 * of a file no longer downloading, will return the size of the file instead.
	 */
	public long getBytesDownloaded(ACMS_ContentDescriptor cd) {
		long fileSize = 0;
		ActiveCloakContentInfo info = getLocalFileInfo(cd);
		if( info != null ) {
    		fileSize = info.getBytesDownloaded();    		
    	}   	
		if( fileSize == 0 ) {
			File file = new File(getLocalUriForRemoteUri(cd.getUrl()).substring(7));
			fileSize = file.length();
		}
		return fileSize;
	}

	ActiveCloakContentInfo getLocalFileInfo(ACMS_ContentDescriptor cd) {
		String localFile = cd.getUrl().substring(cd.getUrl().lastIndexOf("/") + 1);
		
		Collection<ActiveCloakContentInfo> contentList = mActiveCloakContentManager.getActiveDownloads();
		for( ActiveCloakContentInfo info : contentList ) {
    		if( info.getLocalFile().equals(localFile)) {
    			return info;
    		}
    	}  
		return null;
	}

	/**
	 * Binder class for the service.
	 * 
	 * @author irdetodev
	 *
	 */
	public class ACMS_DownloaderBinder extends Binder {
		public ACMS_DownloaderService getService() {
			return ACMS_DownloaderService.this;
		}
	}

	/**
	 * Helper to return the local file URI for a given remote URI. Sometimes useful
	 * for clients that aren't connected to the service. Note that this only returns 
	 * what the local URI would be if it exists; it does not check for this existence
	 * and calling this method does not guarantee anything about the existence of the 
	 * file.
	 * 
	 * @param url The remote URL
	 * @return The local URL
	 */
	public static String getLocalUriForRemoteUri(String url) {
		if( url.startsWith("file://")) {
			return url;
		}
		File file = getDownloadsDirectoryFile();
		String downloadDirectory = file.getAbsolutePath();
		String remoteFileName = url.substring(url.lastIndexOf("/"));
		return "file://" + downloadDirectory + remoteFileName;		
	}
	
	/**
	 * Helper to return the downloads directory. The downloader is responsible for 
	 * this information, but other parts of the app may sometimes need to know about it. 
	 * 
	 * @return
	 */
	private static File getDownloadsDirectoryFile() {
		return new File(Environment.getExternalStorageDirectory(), "Downloads");
	}
	
    /**
	 * Helper to return the downloads directory. The downloader is responsible for 
	 * this information, but other parts of the app may sometimes need to know about it. 
	 * 
	 * @return
	 */
	public static String getDownloadsDirectory() {
		return getDownloadsDirectoryFile().getAbsolutePath();		
	}
	
	/**
	 * Returns whether the downloader service is currently operational.
	 * 
	 * @return
	 */
	public static boolean isEnabled() {
		return isEnabled;
	}
}
