package com.irdeto.activecloakmediasample;

import static com.irdeto.activecloakmediasample.ACMS_PlayerActivity.DEBUG;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.irdeto.activecloakmediasample.internal.data.ACMS_ContentDescriptor;
import com.irdeto.activecloakmediasample.internal.data.ACMS_LibraryManager;
import com.irdeto.activecloakmediasample.internal.log.ACMS_LogService;
import com.irdeto.media.ActiveCloakAgent;
import com.irdeto.media.ActiveCloakContentHeaderInfo;
import com.irdeto.media.ActiveCloakContentLicenseRights;
import com.irdeto.media.ActiveCloakContentManager;
import com.irdeto.media.ActiveCloakDeviceIdChangedListener;
import com.irdeto.media.ActiveCloakEventListener;
import com.irdeto.media.ActiveCloakEventType;
import com.irdeto.media.ActiveCloakException;
import com.irdeto.media.ActiveCloakLicenseRightType;
import com.irdeto.media.ActiveCloakUrlType;

public class ACMS_LicenseManagerService extends IntentService {
	
	// Default constructor
	public ACMS_LicenseManagerService() {
		super("LicenseManagerService");
	}

	public final static String LICENSE_MANAGER_OPERATION_EXTRA = "com.irdeto.activecloakmediasample.licensemanager";
	
	public final static String LICENSE_MANAGER_OPERATION_ACQUIRE_LICENSE = "com.irdeto.activecloakmediasample.licensemanager.AcquireLicense";
	public final static String LICENSE_MANAGER_OPERATION_CLEAR_LICENSE = "com.irdeto.activecloakmedia.licensemanager.ClearLicense";
	public final static String LICENSE_MANAGER_OPERATION_REFRESH_LICENSES = "com.irdeto.activecloakmedia.licensemanager.RefreshLicense";
	public final static String LICENSE_MANAGER_OPERATION_CLEAN_LICENSES = "com.irdeto.activecloakmedia.licensemanager.CleanLicenses";

	// Content manager for license acquisition.
	private ActiveCloakContentManager mActiveCloakContentManager;
	
	// Agent for content manager.
	private ActiveCloakAgent mActiveCloakAgent;
	
	// Binder for the service.
	private final IBinder mBinder = new ACMS_LicenseManagerBinder();
		
	@Override
	public IBinder onBind(Intent arg0) {
		return mBinder;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		try {
			// Set up the agent and the content manager.
			mActiveCloakAgent = new ActiveCloakAgent(this,
					new ActiveCloakDeviceIdChangedListener() {
				
				@Override
				public void deviceIdChanged(
						String previousDeviceId,
						String currentDeviceId) {
				}
			});
			mActiveCloakContentManager = new ActiveCloakContentManager(mActiveCloakAgent);
			
		} catch( ActiveCloakException ace ) {
			// Nothing we can really do here.
		}
	}
	
	/*
	 * Whenever this service gets destroyed, unbind from the log service.
	 * 
	 * (non-Javadoc)
	 * @see android.app.Service#onDestroy()
	 */
	@Override
	public void onDestroy() {
        
		super.onDestroy();
	}
	
	private void acquireLicense(final String url, final ActiveCloakUrlType type) {
		
		final Context context = this;
		
		try {
			String contentHeader = mActiveCloakContentManager.getContentHeader(
					url, type);
			mActiveCloakContentManager.acquireLicense(contentHeader,
					new ACMS_SendUrlRequestListener(null, mActiveCloakAgent,
							context), new ActiveCloakEventListener() {
						@Override
						public void onEvent(ActiveCloakEventType eventType,
								long result1, long result2, long result3,
								String resultString) {
							ACMS_LogService.writeToLog(context, "LicenseManager event: "
									+ eventType.toString() + "\n");
						}

					}, "TestCustomData" // simple custom data (ignored
										// by test servers, but real
										// servers may use)
			);
		} catch (ActiveCloakException ace) {
			ACMS_LogService.writeToLog(this, "Exception trying to acquire license: " + ace.getMessage() + "\n");

		}
		
	}
	
	private void clearLicense(final String url, final ActiveCloakUrlType type) {
		
		try {
			final ActiveCloakContentHeaderInfo info = mActiveCloakContentManager
					.getContentHeaderInfo(url, type);
			
			if( info != null) {
				mActiveCloakContentManager.deleteLicenseUsingKId(info.getKId());
			}
		} catch( ActiveCloakException ace ) {
			ACMS_LogService.writeToLog(this, "Exception trying to clear license: " + ace.getMessage() + "\n");
		}
	}

	private void cleanLicenses() {
		final Context context = this;
		try {
			mActiveCloakContentManager.startCleanLicenseStore(10,
					new ActiveCloakEventListener() {
						@Override
						public void onEvent(ActiveCloakEventType eventType,
								long result1, long result2, long result3,
								String resultString) {
							ACMS_LogService.writeToLog(context, "LicenseManager event: " 
									+ eventType.toString() + "\n");
						}

					});

		} catch (ActiveCloakException ace) {
			ACMS_LogService.writeToLog(this, "Exception trying to clean licenses: " + ace.getMessage() + "\n");
		}
	}
	
	private void refreshLicenses() {
		for( ACMS_ContentDescriptor content : ACMS_LibraryManager.INSTANCE.getLibrary(this)) {
			if( content.getIsProtected()) {
				acquireLicense(content.getUrl(), content.getType());
			}
		}
	}
	
	/**
	 * Retrieve the license information for a given URL and URL type.
	 * 
	 * @param url
	 * @param type
	 * @return
	 */
	public String getLicenseInfo(String url, ActiveCloakUrlType type) {
		
		StringBuilder retVal = new StringBuilder();
		
		final String UNRESTRICTED = "Unrestricted";
		
		try {
			final ActiveCloakContentHeaderInfo info = mActiveCloakContentManager
					.getContentHeaderInfo(url, type);
		
			if (info != null) {
				
				ActiveCloakContentLicenseRights rights = mActiveCloakContentManager.getLicenseState(
						info.getKId(),
	  					ActiveCloakLicenseRightType.PLAYBACK
		          );
				
				
				if (DEBUG) {
					
					// If debugging, print out absolutely everything.
					retVal.append("Content Header Info for: " + info.getContentUrl() + "\n");
					retVal.append("KID: " + info.getKId() + "\n");
					retVal.append("LA URL: " + info.getLaUrl() + "\n");
					retVal.append("CheckSum: " + info.getCheckSum() + "\n");
					
					retVal.append("License Rights for KID: " + rights.getKId() + "\n");
					retVal.append("Can Playback now: " + String.valueOf(rights.isPermitted()) + "\n");
					retVal.append("Counts Left: " + 
							((rights.getCountsLeft() == ActiveCloakContentLicenseRights.LICENSE_COUNT_UNRESTRICTED) ? UNRESTRICTED : rights.getCountsLeft()) + "\n");
					retVal.append("Counts Total: " + 
							((rights.getCountsTotal() == ActiveCloakContentLicenseRights.LICENSE_COUNT_UNRESTRICTED) ? UNRESTRICTED : rights.getCountsTotal()) + "\n");
					retVal.append("Start Time: " + 
							(rights.getStartTime() == null ? UNRESTRICTED : rights.getStartTime().toString()) + "\n");
					retVal.append("End Time: " + 
							(rights.getEndTime() == null ? UNRESTRICTED : rights.getEndTime().toString()) + "\n");
					retVal.append("Interval Duration: " + 
							(rights.getInterval() == ActiveCloakContentLicenseRights.LICENSE_TIMESPAN_UNRESTRICTED ? UNRESTRICTED : rights.getInterval()) + "\n");
					retVal.append("OPL License: " + String.valueOf(rights.isOPLLicense()) + "\n");
					retVal.append("SAP License: " + String.valueOf(rights.isSAPLicense()) + "\n");
					retVal.append("Vague License: " + String.valueOf(rights.isVagueLicense()) + "\n");
					retVal.append("Extensible Restrictions : " + String.valueOf(rights.hasExtensibleRestrictions()) + "\n");
					
					retVal.append("\nRaw Header : " + mActiveCloakContentManager.getContentHeader(url, type));
				} else {
					// In production, print something friendlier.
					if( rights.isPermitted() ) {
						retVal.append("This content has a valid license.\n");
						retVal.append("License Expires: " + 
								(rights.getEndTime() == null ? UNRESTRICTED : rights.getEndTime().toString()) + "\n");
						retVal.append("Playbacks Remaining: " + 
								((rights.getCountsLeft() == ActiveCloakContentLicenseRights.LICENSE_COUNT_UNRESTRICTED) ? UNRESTRICTED : rights.getCountsLeft()) + "\n");
					} else {
						retVal.append("This content does not have a valid license.\n");
					}
					
				}
			}
		} catch( ActiveCloakException ace ) {
			ACMS_LogService.writeToLog(this, "Exception trying to get license info: " + ace.getMessage() + "\n");
		}
		
		return retVal.toString();
	}
	
	/**
	 * Given a content descriptor, indicate whether or not a valid license exists for it.
	 * 
	 * @param cd The content descriptor to query.
	 * @return True if a valid license exists for the content, false otherwise.
	 */
	public boolean hasRights(ACMS_ContentDescriptor cd) {
		try {
			final ActiveCloakContentHeaderInfo info = mActiveCloakContentManager
					.getContentHeaderInfo(cd.getUrl(), cd.getType());
		
			if (info != null) {
				
				ActiveCloakContentLicenseRights rights = mActiveCloakContentManager.getLicenseState(
						info.getKId(),
	  					ActiveCloakLicenseRightType.PLAYBACK
		          );
				
				return rights.isPermitted();
			}
		} catch( ActiveCloakException ace ) {
			ACMS_LogService.writeToLog(this, "Exception trying to get license info: " + ace.getMessage() + "\n");
		}
	
		return false;
		
	}
	
	/**
	 * Binder class for the service.
	 * 
	 * @author irdetodev
	 *
	 */
	public class ACMS_LicenseManagerBinder extends Binder {
		public ACMS_LicenseManagerService getService() {
			return ACMS_LicenseManagerService.this;
		}
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// Because some of these operations can take a while, we subclass ServiceIntent and
		// use onHandleIntent to run them asynchronously.
		
		String operation = intent.getStringExtra(LICENSE_MANAGER_OPERATION_EXTRA);
		ACMS_ContentDescriptor cd = intent.getParcelableExtra(ACMS_PlayerActivity.CONTENT_DESCRIPTOR_EXTRA);
		
		if( operation.matches(LICENSE_MANAGER_OPERATION_ACQUIRE_LICENSE)) {
			acquireLicense(cd.getUrl(), cd.getType());
		} else if( operation.matches(LICENSE_MANAGER_OPERATION_CLEAR_LICENSE)) {
			clearLicense(cd.getUrl(), cd.getType());
		} else if( operation.matches(LICENSE_MANAGER_OPERATION_CLEAN_LICENSES)) {
			cleanLicenses();
		} else if( operation.matches(LICENSE_MANAGER_OPERATION_REFRESH_LICENSES)) {
			refreshLicenses();
		}
	}
	
}
