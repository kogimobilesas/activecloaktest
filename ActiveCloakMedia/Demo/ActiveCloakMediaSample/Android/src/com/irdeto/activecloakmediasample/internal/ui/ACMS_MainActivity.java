package com.irdeto.activecloakmediasample.internal.ui;


import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.view.View;

import com.irdeto.activecloakmediasample.ACMS_PlayerActivity;
import com.irdeto.activecloakmediasample.R;
import com.irdeto.media.ActiveCloakAgent;

/**
 * NOTE: This class contains code that is relevant to this application,
 * but not essential to understanding how to integrate the 
 * ActiveCloakMediaPlayer. You are welcome to look at it and understand
 * how it works, and take any pieces that might be relevant to your own
 * player, but it is not necessary to do so.
 * 
 * This is the main screen of the application. It doesn't do anything except
 * offer an interface into the other screens.
 * 
 * @author irdetodev
 *
 */
public class ACMS_MainActivity extends ACMS_Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
		// If debugging, log the versions of ACMS and ActiveCloak being used.
		if (ACMS_PlayerActivity.DEBUG) {
			try {
				PackageInfo pInfo = getPackageManager().getPackageInfo(
						getPackageName(), 0);
				String version = pInfo.versionName;
				writeToLog("Logging events for ActiveCloakMediaSample version "
								+ version
								+ ", ActiveCloak version "
								+ ActiveCloakAgent.getVersionString() + "\n");
			} catch (Exception e) {
				writeToLog("Failed to determine application version.\n");
			}
		}
        
        writeToLog("Created main activity.\n");
    }
       
    public void openStore(View view) {
    	writeToLog("Opening store.\n");
    	Intent intent = new Intent(this, ACMS_StoreActivity.class);
    	startActivity(intent);
    }
    
    public void openFavorites(View view) {
    	writeToLog("Opening favorites.\n");
    	Intent intent = new Intent(this, ACMS_FavoritesActivity.class);
    	startActivity(intent);
    }

    public void openLibrary(View view) {
    	writeToLog("Opening library.\n");
    	Intent intent = new Intent(this, ACMS_LibraryActivity.class);
    	startActivity(intent);
    }

    public void openSettings(View view) {
    	writeToLog("Opening settings.\n");
    	Intent intent = new Intent(this, ACMS_SettingsActivity.class);
    	startActivity(intent);
    }

}