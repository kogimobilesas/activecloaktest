package com.irdeto.activecloakmediasample.internal.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.irdeto.activecloakmediasample.R;
import com.irdeto.activecloakmediasample.internal.log.ACMS_LogService;
import com.irdeto.activecloakmediasample.internal.log.ACMS_LogService.ACMS_LogBinder;

/**
 * NOTE: This class contains code that is relevant to this application,
 * but not essential to understanding how to integrate the 
 * ActiveCloakMediaPlayer. You are welcome to look at it and understand
 * how it works, and take any pieces that might be relevant to your own
 * player, but it is not necessary to do so.
 * 
 * This activity displays diagnostic information to the user.
 * 
 * @author irdetodev
 *
 */
public class ACMS_DiagnosticsActivity extends ACMS_Activity {

	private ACMS_LogService mLogService;
	boolean mBoundToService = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.diagnostics);
	}
	
	/**
	 * Called when the user taps the "Email Logs" button.
	 * 
	 * @param view
	 */
	public void emailLogs(View view) {
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL  , new String[]{""});
		i.putExtra(Intent.EXTRA_SUBJECT, "Logs for ActiveCloakMediaSample");
		i.putExtra(Intent.EXTRA_TEXT   , "Logs for ActiveCloakMediaSample:\n\n" + mLogService.getLog());
		writeToLog("E-mailing logs.");
		try {
		    startActivity(Intent.createChooser(i, "Send mail..."));
		} catch (android.content.ActivityNotFoundException ex) {
		    Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
		}
	}
	
	/**
	 * Called when the user taps the "Clear Logs" button.
	 * 
	 * @param view
	 */
	public void clearLogs(View view) {
		mLogService.clearLog();
		refreshLog();
	}
	
	/*
	 * When switching to this activity, bind to the log service and
	 * retrieve whatever logs exist to display.
	 * 
	 * (non-Javadoc)
	 * @see android.app.Activity#onStart()
	 */
	@Override
	public void onStart() {
		super.onStart();
		
		Intent intent = new Intent(this, ACMS_LogService.class);
		bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
		
		if( mBoundToService ) {
			refreshLog();
		}
	}
	
	/*
	 * When switching away from this activity, unbind from the service.
	 * 
	 * (non-Javadoc)
	 * @see android.app.Activity#onStop()
	 */
	@Override
	public void onStop() {
		// Unbind from the service
        if (mBoundToService) {
            unbindService(mConnection);
            mBoundToService = false;
        }
        
		super.onStop();
	}
	
	private void refreshLog() {
		TextView textView = (TextView)findViewById(R.id.log_display);
		textView.setMovementMethod(new ScrollingMovementMethod());
		textView.setText(mLogService.getLog());
	}
	
	 /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ACMS_LogBinder binder = (ACMS_LogBinder) service;
            mLogService = binder.getService();
            mBoundToService = true;
            
        	refreshLog();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBoundToService = false;
        }
    };
}
