package com.irdeto.activecloakmediasample.internal.ui;

import android.app.Activity;

import com.irdeto.activecloakmediasample.internal.log.ACMS_LogService;

/**
 * NOTE: This class contains code that is relevant to this application,
 * but not essential to understanding how to integrate the 
 * ActiveCloakMediaPlayer. You are welcome to look at it and understand
 * how it works, and take any pieces that might be relevant to your own
 * player, but it is not necessary to do so.
 * 
 * This class contains reusable aspects of all ACMS Activities.
 * 
 * @author irdetodev
 *
 */
public abstract class ACMS_Activity extends Activity {

	@Override
	public void onBackPressed() {
		writeToLog("User hit back.\n");
		
		super.onBackPressed();
	}
	
	public void writeToLog(String msg ) {
		ACMS_LogService.writeToLog(this, msg);
	}
}
