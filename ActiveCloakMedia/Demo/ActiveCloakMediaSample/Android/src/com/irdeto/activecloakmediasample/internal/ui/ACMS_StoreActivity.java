package com.irdeto.activecloakmediasample.internal.ui;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.irdeto.activecloakmediasample.R;
import com.irdeto.activecloakmediasample.internal.data.ACMS_ContentDescriptor;
import com.irdeto.media.ActiveCloakUrlType;

/**
 * NOTE: This class contains code that is relevant to this application,
 * but not essential to understanding how to integrate the 
 * ActiveCloakMediaPlayer. You are welcome to look at it and understand
 * how it works, and take any pieces that might be relevant to your own
 * player, but it is not necessary to do so.
 * 
 * This activity displays a store login/password screen to the user.
 * 
 * @author irdetodev
 *
 */
public class ACMS_StoreActivity extends ACMS_Activity {

	// A simple file is used for previous store logins.
	private final String STORE_FILE = "store.txt";

	
	static class StoreXmlParser {
		
		private String getXmlFromUrl(String url) {
			String xml = null;
			String getUrl = url;
			if( !url.startsWith("http://") && !(url.startsWith("https://"))) {
				getUrl = "http://" + url;
			}
			try {
				DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpResponse httpResponse = httpClient.execute(new HttpGet(getUrl));
				HttpEntity httpEntity = httpResponse.getEntity();
				xml = EntityUtils.toString(httpEntity);
			} catch( Exception e) {
				e.printStackTrace();
			}
			return xml;
		}
		
		private Document getDomElement(String xml) {
			Document doc = null;
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			try {
				DocumentBuilder db = dbf.newDocumentBuilder();
				InputSource is = new InputSource();
				is.setCharacterStream(new StringReader(xml));
				doc = db.parse(is);
			} catch( Exception e ) {
				e.printStackTrace();
			}
			return doc;
		}
		
		private String getValue(Element item, String str ) {
			NodeList n = item.getElementsByTagName(str);
			return this.getElementValue(n.item(0));
		}
		
		private final String getElementValue(Node elem) {
			Node child;
			if( elem != null) {
				if( elem.hasChildNodes()) {
					for( child = elem.getFirstChild(); child != null; child = child.getNextSibling()) {
						if( child.getNodeType() == Node.TEXT_NODE ) {
							return child.getNodeValue();
						}
					}
				}
			}
			return "";
		}		
		
		public static List<ACMS_ContentDescriptor> getContentDescriptors(String URL) {
			List<ACMS_ContentDescriptor> returnValue = new ArrayList<ACMS_ContentDescriptor>();
			try {
				StoreXmlParser parser = new StoreXmlParser();
				String xml = parser.getXmlFromUrl(URL);
				Document doc = parser.getDomElement(xml);
				NodeList nl = doc.getElementsByTagName("entry");
				for( int i = 0; i < nl.getLength(); i++) {
					Node n = nl.item(i);
					Element e = (Element)n;
					Element linkElement = (Element)e.getElementsByTagName("link").item(0);
					String contentTypeString = linkElement.getAttribute("cw:urltype");
					ActiveCloakUrlType urlType = null;
					if( contentTypeString.equals("hls")) {
						urlType = ActiveCloakUrlType.HLS;
					} else if( contentTypeString.equals("iisss")) {
						urlType = ActiveCloakUrlType.IIS;
					} else if( contentTypeString.equals("envelope")) {
						urlType = ActiveCloakUrlType.ENVELOPE;
					} else {
						throw new IllegalArgumentException("Encountered URL with no recognizable cw:urltype attribute.");
					}
					boolean isProtected = false;
					String protectedString = linkElement.getAttribute("cw:protected");
					if( protectedString.equals("yes")) {
						isProtected = true;
					}
					ACMS_ContentDescriptor cd = new ACMS_ContentDescriptor(
							parser.getValue(e, "title"),
							linkElement.getAttribute("href"), 
							urlType,
							isProtected);
					returnValue.add(cd);
				}
			} catch( Exception e ) {
				returnValue.clear();
				e.printStackTrace();
			}
			return returnValue;
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.store);
		
		// Take the last-known values for the text fields, if they exist.
		String login = "";
		String password = "";
		String rootURL = "";
		try {
			InputStream is = openFileInput(STORE_FILE);
			byte[] buffer = new byte[is.available()];
			is.read(buffer);
			String bufferString = new String(buffer);
			String[] contentStrings = bufferString.split("\n");
			for (int i = 0; i < contentStrings.length; i++) {
				String[] contentDescriptorString = contentStrings[i]
						.split("\t");
				login = contentDescriptorString[0];
				password = contentDescriptorString[1];
				rootURL = contentDescriptorString[2];
			}
		} catch (Exception e) {
			// Do nothing. 
		}
		EditText editText = (EditText) findViewById(R.id.store_rootURL);
		editText.setText(rootURL);
		editText = (EditText) findViewById(R.id.store_login);
		editText.setText(login);
		editText = (EditText) findViewById(R.id.store_password);
		editText.setText(password);
	}
	
	public void storeSubmit(View view) {
		
		EditText editText = (EditText) findViewById(R.id.store_rootURL);
		String rootUrl = editText.getText().toString().trim();
		if( rootUrl.length() == 0) {
			UIUtils.showDialog(this, "Root URL must be specified.");
			return;
		}
		String url = rootUrl;
		editText = (EditText) findViewById(R.id.store_login);
		String login = editText.getText().toString().trim();
		if( login.length() == 0) {
			UIUtils.showDialog(this, "Login must be specified.");
			return;
		}
		url += "/" + login;
		editText = (EditText) findViewById(R.id.store_password);
		String password = editText.getText().toString().trim();
		if( password.length() == 0) {
			UIUtils.showDialog(this, "Password must be specified.");
			return;
		}
		url += "/" + password + "/store.xml";
		
		writeToLog("User logging in to store with credentials: \n\tlogin = " + login + "\n\tpassword = " + password + "\n\trootURL = " + rootUrl);
		
		List<ACMS_ContentDescriptor> contentList = StoreXmlParser.getContentDescriptors(url);
		if( contentList.size() == 0) {
			writeToLog("Error loading the store with credentials specified.");
			UIUtils.showDialog(this, "Error loading store at the location specified.");
			return;
		}
		
		try {
			FileOutputStream fos = openFileOutput(STORE_FILE,
					Context.MODE_PRIVATE);
			byte[] byteArray = (login + "\t" + password + "\t" + rootUrl + "\n").getBytes();
			fos.write(byteArray);
			fos.close();
		} catch( Exception e ) {
			// do nothing
		}

		Intent intent = new Intent(this, ACMS_ContentActivity.class);
		intent.putExtra(ACMS_ContentActivity.STORE_URL, url);
    	startActivity(intent);
	}
}
