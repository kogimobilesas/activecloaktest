package com.irdeto.activecloakmediasample.internal.log;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.irdeto.activecloakmediasample.ACMS_PlayerActivity;
import com.irdeto.activecloakmediasample.internal.ui.UIUtils;

/**
 * NOTE: This class contains code that is relevant to this application,
 * but not essential to understanding how to integrate the 
 * ActiveCloakMediaPlayer. You are welcome to look at it and understand
 * how it works, and take any pieces that might be relevant to your own
 * player, but it is not necessary to do so.
 * 
 * Simple logging service for video diagnostic information. At the moment uses
 * file I/O but another form of persistence would probably make sense for
 * Android.
 * 
 * @author irdetodev
 */
public class ACMS_LogService extends Service  {
	
	public static final String LOG_MESSAGE_EXTRA = "com.irdeto.activecloakmediasample.internal.log.LOG_MESSAGE";
	
	// Binder for the service.
	private final IBinder mBinder = new ACMS_LogBinder();
	
	// Lock for file-access.
	public static final Object[] FILE_LOCK = new Object[0];
	
	public ACMS_LogService() {
	}
	
	// Static version of writeToLog that can be called from anywhere.
	public static void writeToLog(Context context, String msg ) {
		Intent logIntent = new Intent(context, ACMS_LogService.class);
		logIntent.putExtra(ACMS_LogService.LOG_MESSAGE_EXTRA, msg);
		context.startService(logIntent);
	}
	
	public void writeToLog(String msg) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy h:mm:ss a: ");
		String formattedDate = sdf.format(date);

		String logMessage = formattedDate + msg;
		if( logMessage.endsWith("\n") == false) {
			logMessage += "\n";
		}
		
		try {
			synchronized (FILE_LOCK) {
				FileOutputStream fos = openFileOutput("log.txt", MODE_APPEND);
				byte[] writeBuffer = logMessage.getBytes();
				fos.write(writeBuffer);
				fos.close();
			}
		} catch( IOException ioException ) {
			
		}
		
		if( ACMS_PlayerActivity.DEBUG ) {
			Log.d("ACMS :: ", msg);
		}
	}
	
	public String getLog() {
		
		InputStream is;
		try {
			is = openFileInput("log.txt");
			byte[] buffer;
			buffer = new byte[is.available()];
			is.read(buffer);
			return new String(buffer);
		} catch (FileNotFoundException e) {
			// No problem, assume no log has been created yet.
		} catch (IOException e) {
			UIUtils.showDialog(this, "IOException trying to read log contents from device.");
		}
		
		return "";
	}
	
	public void clearLog() {
		try {
			synchronized (FILE_LOCK) {
				FileOutputStream fos = openFileOutput("log.txt", MODE_PRIVATE);
				byte[] writeBuffer = "".getBytes();
				fos.write(writeBuffer);
				fos.close();
			}
		} catch (FileNotFoundException e) {
			UIUtils.showDialog(this,
					"For some reason we care that a file we are creating isn't found.");
		} catch (IOException e) {
			UIUtils.showDialog(this,
					"IOException trying to write log to device.");
		}
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return mBinder;
	}
	
	/*
	 * Whenever this service gets created, read the log from disk.
	 * 
	 * @see android.app.Service#onCreate()
	 */
	@Override
	public void onCreate() {
		super.onCreate();
	}

	/*
	 * Whenever this service gets destroyed, write the log to disk.
	 * 
	 * (non-Javadoc)
	 * @see android.app.Service#onDestroy()
	 */
	@Override
	public void onDestroy() {
		super.onDestroy();	
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		if( intent != null ) {
			String logMsg = intent.getStringExtra(
					ACMS_LogService.LOG_MESSAGE_EXTRA);
			if( logMsg != null) {
				writeToLog(logMsg);
			}
		}
		
		return START_STICKY;
	}
	
	/**
	 * Binder class for the service.
	 * 
	 * @author irdetodev
	 *
	 */
	public class ACMS_LogBinder extends Binder {
		public ACMS_LogService getService() {
			return ACMS_LogService.this;
		}
	}
}
