package com.irdeto.activecloakmediasample.internal.data;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.content.Context;

import com.irdeto.media.ActiveCloakUrlType;

/**
 * NOTE: This class contains code that is relevant to this application,
 * but not essential to understanding how to integrate the 
 * ActiveCloakMediaPlayer. You are welcome to look at it and understand
 * how it works, and take any pieces that might be relevant to your own
 * player, but it is not necessary to do so.
 * 
 * This class manages the list of user favorited videos.
 * 
 * @author irdetodev
 *
 */
public class ACMS_FavoritesManager {
	
	// A simple file is used for I/O. This makes delete inefficient but the
	// total number of favorites is typically small.
	private final String FAVORITES_FILE = "favorites.txt";
	
	// Lock for file-access.
	public static final Object[] FILE_LOCK = new Object[0];

	private ACMS_FavoritesManager() {
		
	}
	
	public static ACMS_FavoritesManager INSTANCE = new ACMS_FavoritesManager();
	
	public void addFavorite(Context context, ACMS_ContentDescriptor favorite)
			throws IOException {

		for( ACMS_ContentDescriptor content : getFavorites(context)) {
			if( content.getLabel().equals(favorite.getLabel()) 
					&& content.getUrl().equals(favorite.getUrl())) {
				return;
			}
		}
		
		if( favorite != null && favorite.getLabel() != null && favorite.getLabel().length() > 0
				&& favorite.getUrl() != null && favorite.getUrl().length() > 0 ) {
			synchronized(FILE_LOCK) {
				FileOutputStream fos = context.openFileOutput(FAVORITES_FILE,
						Context.MODE_APPEND);
				byte[] byteArray = (favorite.getLabel() + "\t" + favorite.getUrl() + "\t" + favorite.getType().ordinal() + "\t" + favorite.getIsProtected() + "\n").getBytes();
				fos.write(byteArray);
				fos.close();
			}
		}
	}
	
	public void deleteFavorite(Context context, ACMS_ContentDescriptor favoriteToDelete) throws IOException {
		
		// This actually goes through the file and rewrites out every favorite
		// except the one being removed.
		Collection<ACMS_ContentDescriptor> favoriteCollection = getFavorites(context);
		synchronized(FILE_LOCK) {
			FileOutputStream fos = context.openFileOutput(FAVORITES_FILE, Context.MODE_PRIVATE);
			for( ACMS_ContentDescriptor favorite : favoriteCollection ) {
				if( ! favoriteToDelete.getUrl().equals(favorite.getUrl())) {
					byte[] byteArray = (favorite.getLabel() + "\t" + favorite.getUrl() + "\t" + favorite.getType().ordinal() + "\t" + favorite.getIsProtected() + "\n").getBytes();
					fos.write(byteArray);
				}
			}
			fos.close();
		}
	}
	
	public List<ACMS_ContentDescriptor> getFavorites(Context context) {
		List<ACMS_ContentDescriptor> favoriteCollection = new ArrayList<ACMS_ContentDescriptor>();
		try {
			InputStream is = context.openFileInput(FAVORITES_FILE);
			byte[] buffer = new byte[is.available()];
			is.read(buffer);
			String bufferString = new String(buffer);
			String[] contentStrings = bufferString.split("\n");
			for (int i = 0; i < contentStrings.length; i++) {
				String[] contentDescriptorString = contentStrings[i]
						.split("\t");
				ACMS_ContentDescriptor cd = new ACMS_ContentDescriptor(
						contentDescriptorString[0], 
						contentDescriptorString[1],
						ActiveCloakUrlType.values()[Integer.parseInt(contentDescriptorString[2])],
						Boolean.parseBoolean(contentDescriptorString[3]));
				favoriteCollection.add(cd);
			}
		} catch (Exception e) {
			// Do nothing. 
		}
		return favoriteCollection;
	}
}
