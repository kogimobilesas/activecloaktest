package com.irdeto.activecloakmediasample.internal.data;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.content.Context;

import com.irdeto.activecloakmediasample.ACMS_DownloaderService;
import com.irdeto.media.ActiveCloakUrlType;

/**
 * NOTE: This class contains code that is relevant to this application,
 * but not essential to understanding how to integrate the 
 * ActiveCloakMediaPlayer. You are welcome to look at it and understand
 * how it works, and take any pieces that might be relevant to your own
 * player, but it is not necessary to do so.
 * 
 * This class manages the library of downloaded content.
 * 
 * @author irdetodev
 *
 */
public class ACMS_LibraryManager {
	
	// A simple file is used for I/O. This makes delete inefficient but the
	// total number of favorites is typically small.
	private final String LIBRARY_FILE = "library.txt";
	
	// Lock for file-access.
	public static final Object[] FILE_LOCK = new Object[0];
	
	private ACMS_LibraryManager() {
		
	}
	
	public static ACMS_LibraryManager INSTANCE = new ACMS_LibraryManager();

	public void discoverLibraryContents(Context context) {
		// attempt to automatically add files to the library that are already on
		// the sd card
		try {
			String downloadDir = ACMS_DownloaderService.getDownloadsDirectory();
			autoAddDir(context,
					downloadDir, new String[] { ".mp4" },
					new String[] { ".prdy" });

		} catch (IOException e1) {
			// Do nothing.
		}
	}
	
	// Automatically add all files in the specified directory that have one of
	// the valid extensions.
	private void autoAddDir(Context context, String dirname,
			String[] clearExtensions, String[] protectedExtensions)
			throws IOException {
		// Directory path here
		String path = dirname;

		File folder = new File(path);
		if( !folder.exists()) {
			return;
		}
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
			File f = listOfFiles[i];
			if (f.isFile()) {
				boolean addClear = false;
				boolean addProtected = false;

				for (String ext : clearExtensions) {
					if (f.getName().endsWith(ext)) {
						addClear = true;
						break;
					}
				}

				for (String ext : protectedExtensions) {
					if (f.getName().endsWith(ext)) {
						addProtected = true;
						break;
					}
				}

				if (addClear || addProtected) {
					ACMS_ContentDescriptor desc = new ACMS_ContentDescriptor(
							f.getName(), "file://" + f.getAbsolutePath(),
							ActiveCloakUrlType.ENVELOPE, addProtected);					
					addLibraryEntry(context, desc);
				}

			}
		}
	}


                                                      
	public void addLibraryEntry(Context context, ACMS_ContentDescriptor cd)
			throws IOException {
		
		for( ACMS_ContentDescriptor content : getLibrary(context)) {
			if( content.getUrl().equals(cd.getUrl())) {
				return;
			}
		}
		
		if( cd != null && cd.getLabel() != null && cd.getLabel().length() > 0
				&& cd.getUrl() != null && cd.getUrl().length() > 0 ) {
			synchronized(FILE_LOCK) {
				FileOutputStream fos = context.openFileOutput(LIBRARY_FILE,
						Context.MODE_APPEND);
				byte[] byteArray = (cd.getLabel() + "\t" + cd.getUrl() + "\t" + cd.getIsProtected() + "\n")
						.getBytes();
				fos.write(byteArray);
				fos.close();
			}
		}
	
	}
	
	public void deleteLibraryEntry(Context context,
			ACMS_ContentDescriptor cd) throws IOException {
		
		// This actually goes through the file and rewrites out every favorite
		// except the one being removed.
		Collection<ACMS_ContentDescriptor> libraryCollection = getLibrary(context);
		synchronized(FILE_LOCK) {
			FileOutputStream fos = context.openFileOutput(LIBRARY_FILE,
					Context.MODE_PRIVATE);
			for (ACMS_ContentDescriptor libraryEntry : libraryCollection) {
				if (!cd.getUrl().equals(libraryEntry.getUrl())) {
					byte[] byteArray = (libraryEntry.getLabel() + "\t"
							+ libraryEntry.getUrl() + "\t" + libraryEntry.getIsProtected() + "\n").getBytes();
					fos.write(byteArray);
				}
			}
			fos.close();
		}
		
		URI uri = URI.create(cd.getUrl());
		File fileFromUri = new File(uri);
		fileFromUri.delete();
	}

	public List<ACMS_ContentDescriptor> getLibrary(
			Context context) {
		
		List<ACMS_ContentDescriptor> library = new ArrayList<ACMS_ContentDescriptor>();
		try {
			InputStream is = context.openFileInput(LIBRARY_FILE);
			byte[] buffer = new byte[is.available()];
			is.read(buffer);
			String bufferString = new String(buffer);
			String[] contentStrings = bufferString.split("\n");
			for (int i = 0; i < contentStrings.length; i++) {
				String[] contentDescriptorString = contentStrings[i]
						.split("\t");
				ACMS_ContentDescriptor cd = new ACMS_ContentDescriptor(
						contentDescriptorString[0], contentDescriptorString[1],
						ActiveCloakUrlType.ENVELOPE,
						Boolean.parseBoolean(contentDescriptorString[2]));
				library.add(cd);
			}
		} catch (Exception e) {
			// Do nothing. 
		}
		return library;
		
	}

}
