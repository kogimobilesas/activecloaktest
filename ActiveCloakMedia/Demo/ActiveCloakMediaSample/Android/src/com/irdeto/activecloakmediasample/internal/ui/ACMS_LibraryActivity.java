package com.irdeto.activecloakmediasample.internal.ui;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

import com.irdeto.activecloakmediasample.ACMS_DownloaderService;
import com.irdeto.activecloakmediasample.ACMS_DownloaderService.ACMS_DownloaderBinder;
import com.irdeto.activecloakmediasample.ACMS_PlayerActivity;
import com.irdeto.activecloakmediasample.R;
import com.irdeto.activecloakmediasample.internal.data.ACMS_ContentDescriptor;
import com.irdeto.activecloakmediasample.internal.data.ACMS_LibraryManager;

/**
 * NOTE: This class contains code that is relevant to this application,
 * but not essential to understanding how to integrate the 
 * ActiveCloakMediaPlayer. You are welcome to look at it and understand
 * how it works, and take any pieces that might be relevant to your own
 * player, but it is not necessary to do so.
 * 
 * This activity shows all content that the user has previously downloaded.
 * 
 * @author irdetodev
 *
 */
public class ACMS_LibraryActivity extends ACMS_Activity {
	
	private ACMS_LibraryActivity mThisActivity = this;
	
	// Local handle on the DownloaderService and a boolean indicating whether the
	// service is currently bound.
	private ACMS_DownloaderService mDownloaderService;
	boolean mBoundToService = false;
	
	private Handler mHandler;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Bind to the downloader service.
		Intent intent = new Intent(this, ACMS_DownloaderService.class);
		bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
					
		if (mHandler == null)
	    {
	    	// start a timer to update things, if not already started. 
	    	final int delay = 1000;
		    mHandler = new Handler();
		    
		    Runnable updateDisplayTask = new Runnable()
			{
	
				@Override
				public void run()
				{
				    showDisplay(false);
				    mHandler.postAtTime(this, SystemClock.uptimeMillis() + delay);
				}
			};
			mHandler.postAtTime(updateDisplayTask, SystemClock.uptimeMillis() + delay);
	    }	
	}
	
	/*
	 * This is called when the Back button is hit or when the minimized activity
	 * is destroyed by the Android OS.
	 * 
	 * (non-Javadoc)
	 * @see android.app.Activity#onDestroy()
	 */
	@Override
	public void onDestroy() {
		
        if (mBoundToService) {
            unbindService(mConnection);
            mBoundToService = false;
        }
        mHandler.removeCallbacksAndMessages(null);
		
		super.onDestroy();
	}
	
	private void showDisplay(boolean bRedraw) {
		
		List<ACMS_ContentDescriptor> libraryList = ACMS_LibraryManager.INSTANCE.getLibrary(this);
		
		if( bRedraw ) {
			// Redraw all the buttons. Called when we first bind to the downloader service, and
			// anytime we delete a button (due to deleting the underlying file or cancelling a 
			// download).
			setContentView(R.layout.library);
	
			if( mDownloaderService == null) {
				return;
			}
			
			LinearLayout layout = (LinearLayout)findViewById(R.id.library_display);	
			Resources res = getResources();
			
			// For each piece of library content, display a button that, when
			// pressed, activates a popup that allows the content to be played 
			// or deleted.
			for( ACMS_ContentDescriptor cd : libraryList ) {	
				Button contentButton = new Button(this);
				
				Drawable left = null;
				if( cd.getIsProtected()) {
					left = res.getDrawable(R.drawable.icon);
				}
				contentButton.setCompoundDrawablesWithIntrinsicBounds(left, null, null, null);
				
				updateText(contentButton, cd);
				updateListener(contentButton, cd);
				
				contentButton.setTag(cd);
				layout.addView(contentButton);
			}
		} else {
			// Just update the text on the buttons that exist.
			LinearLayout layout = (LinearLayout)findViewById(R.id.library_display);
			if( layout != null ) {
				for( int i = 0; i < layout.getChildCount(); i++ ) {
					// The assumption is that the layout only contains buttons, which for
					// now is correct.
					Button contentButton = (Button)layout.getChildAt(i);
					if( contentButton != null ) {
						ACMS_ContentDescriptor cd = (ACMS_ContentDescriptor)contentButton.getTag();
						if( cd != null ) {
							updateText(contentButton, cd);
							updateListener(contentButton, cd);
						}
					}
				}
			}
		}

	}
	
	// Helper to update button text based on the state of the download.
	private void updateText(Button contentButton, ACMS_ContentDescriptor cd) {
		float percentComplete = mDownloaderService.getPercentComplete(cd);
		if( percentComplete != 0 ) {
			DecimalFormat df = new DecimalFormat("##.##");
			df.setRoundingMode(RoundingMode.DOWN);
			contentButton.setText(cd.getLabel() 
					+ " - " + df.format(percentComplete * 100) 
					+ "% downloaded");
		} else if( mDownloaderService.isPaused(cd) ) {
			contentButton.setText(cd.getLabel() + " -- PAUSED ");
		} else if( !mDownloaderService.isCompleted(cd)) {
			contentButton.setText(cd.getLabel() + " -- PENDING ");
		} else {
			contentButton.setText(cd.getLabel());
		}

	}
	
	// Helper to update button listeners to handle changes in state of the download.
	private void updateListener(Button contentButton, final ACMS_ContentDescriptor cd) {
		if( mDownloaderService.isCompleted(cd) ) {
			contentButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					AlertDialog dialog = new AlertDialog.Builder(mThisActivity).create();
					dialog.setMessage(cd.getLabel() + "\n" + cd.getUrl());
					dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Play", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							writeToLog("Playing library entry " + cd.getLabel());
							Intent intent = new Intent(mThisActivity, ACMS_PlayerActivity.class);								
							intent.putExtra(ACMS_PlayerActivity.CONTENT_DESCRIPTOR_EXTRA, cd);
					    	startActivity(intent);
						}
					});
					dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Delete File", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							try {
								writeToLog("Deleting library entry " + cd.getLabel());
								ACMS_LibraryManager.INSTANCE.deleteLibraryEntry(mThisActivity, cd);
								mThisActivity.showDisplay(true);
							} catch (IOException e) {
								UIUtils.showDialog(mThisActivity, "Exception deleting file.");
							}
						}
					});
					if( cd.getIsProtected() ) {
						dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "View Rights", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								writeToLog("Opening rights view.");
								Intent intent = new Intent(mThisActivity, ACMS_RightsViewActivity.class);
								intent.putExtra(ACMS_PlayerActivity.CONTENT_DESCRIPTOR_EXTRA, cd);
						    	startActivity(intent);								
							}
						});
					}
					dialog.show();	
				}
			});
		} else {
			contentButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					AlertDialog dialog = new AlertDialog.Builder(mThisActivity).create();
					dialog.setMessage(cd.getLabel() + "\n" + cd.getUrl());
					dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Play", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							writeToLog("Playing library entry " + cd.getLabel());
							Intent intent = new Intent(mThisActivity, ACMS_PlayerActivity.class);
							intent.putExtra(ACMS_PlayerActivity.CONTENT_DESCRIPTOR_EXTRA, cd);
					    	startActivity(intent);
						}
					});
					dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Cancel Download", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							try {
								writeToLog("User cancelling download: " + cd.getLabel());
								mDownloaderService.cancelDownload(cd);
								ACMS_LibraryManager.INSTANCE.deleteLibraryEntry(mThisActivity, cd);
								mThisActivity.showDisplay(true);
							} catch (Exception e) {
								UIUtils.showDialog(mThisActivity, "Exception canceling download.");
							} 
						}
					});
					if( ! mDownloaderService.isPaused(cd) ) {
						dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Pause Download", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								try {
									writeToLog("User pausing download: " + cd.getLabel());
									mDownloaderService.pauseDownload(cd);
								} catch (Exception e) {
									UIUtils.showDialog(mThisActivity, "Exception pausing download.");
								}
							}
						});
					} else {
						dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Resume Download", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								try {
									writeToLog("User resuming download: " + cd.getLabel());
									mDownloaderService.resumeDownload(cd);
								} catch (Exception e) {
									UIUtils.showDialog(mThisActivity, "Exception resuming download.");
								}
							}
						});
					}
					dialog.show();	
				}
			});
		}
		
	}
	
	/**
	 * Defines callbacks for service binding, passed to bindService()
	 */
	private ServiceConnection mConnection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName className, IBinder service) {
			ACMS_DownloaderBinder binder = (ACMS_DownloaderBinder) service;
			mDownloaderService = binder.getService();
			mBoundToService = true;
			showDisplay(true); // update to show any applicable download progress.
			
			ACMS_LibraryManager.INSTANCE.discoverLibraryContents(ACMS_LibraryActivity.this);
		}

		@Override
		public void onServiceDisconnected(ComponentName arg0) {
			mBoundToService = false;
		}
	};
}
