package com.irdeto.activecloakmediasample.internal.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

import com.irdeto.activecloakmediasample.ACMS_DownloaderService;
import com.irdeto.activecloakmediasample.ACMS_PlayerActivity;
import com.irdeto.activecloakmediasample.R;
import com.irdeto.activecloakmediasample.internal.data.ACMS_ContentDescriptor;
import com.irdeto.activecloakmediasample.internal.data.ACMS_FavoritesManager;
import com.irdeto.media.ActiveCloakUrlType;

/**
 * NOTE: This class contains code that is relevant to this application,
 * but not essential to understanding how to integrate the 
 * ActiveCloakMediaPlayer. You are welcome to look at it and understand
 * how it works, and take any pieces that might be relevant to your own
 * player, but it is not necessary to do so.
 * 
 * This activity displays the content available to the user to favorite or play.
 * 
 * @author irdetodev
 *
 */
public class ACMS_ContentActivity extends ACMS_Activity {

	private List<ACMS_ContentDescriptor> mContentList = new ArrayList<ACMS_ContentDescriptor>();
	private ACMS_ContentActivity mThisActivity = this;
	
	public static final String STORE_URL = "com.irdeto.activecloakmediasample.ACMS_ContentActivity.StoreURL";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.content);
		
		LinearLayout layout = (LinearLayout)findViewById(R.id.content_display);
		
		final String url = getIntent().getStringExtra(STORE_URL);
		
		Resources res = getResources();
		
		mContentList = ACMS_StoreActivity.StoreXmlParser.getContentDescriptors(url);
		
		// Each piece of content displays a button that, when
		// pressed, activates a popup that allows it to be played
		// or favorited.
		for( final ACMS_ContentDescriptor cd : mContentList ) {
			Button contentButton = new Button(this);
			Drawable right = null;
			if( cd.getType() == ActiveCloakUrlType.HLS) {
				right = res.getDrawable(R.drawable.hls);
			} else if( cd.getType() == ActiveCloakUrlType.IIS) {
				right = res.getDrawable(R.drawable.iisss);
			} else {
				right = res.getDrawable(R.drawable.envelope);
			}
			Drawable left = null;
			if( cd.getIsProtected()) {
				left = res.getDrawable(R.drawable.icon);
			}
			contentButton.setCompoundDrawablesWithIntrinsicBounds(left, null, right, null);

			contentButton.setText(cd.getLabel());
			contentButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					AlertDialog dialog = new AlertDialog.Builder(mThisActivity).create();
					String dialogMessage = cd.getLabel() + "\n" + cd.getUrl();
					if( cd.getIsProtected()) {
						dialogMessage += "\nThis is Irdeto-protected content.";
					}
					dialog.setMessage(dialogMessage);
					dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Play", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							Intent intent = new Intent(mThisActivity, ACMS_PlayerActivity.class);
							intent.putExtra(ACMS_PlayerActivity.CONTENT_DESCRIPTOR_EXTRA, cd);
							startActivity(intent);
						}
					});
					String secondButtonText = "Add to Favorites";
					if( cd.getType() == ActiveCloakUrlType.ENVELOPE ) {
						secondButtonText = "Download to Library";
					}
					dialog.setButton(DialogInterface.BUTTON_NEGATIVE, secondButtonText, new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							try {
								if( cd.getType() != ActiveCloakUrlType.ENVELOPE ) {
									writeToLog("Adding favorite " + cd.getLabel() + "\n");
									ACMS_FavoritesManager.INSTANCE.addFavorite(mThisActivity, cd);
								} else {
									writeToLog("Starting download of content " + cd.getLabel() + "\n");
									Intent downloaderIntent = new Intent(mThisActivity, ACMS_DownloaderService.class);
									downloaderIntent.putExtra(ACMS_PlayerActivity.CONTENT_DESCRIPTOR_EXTRA, cd);
									downloaderIntent.putExtra(ACMS_DownloaderService.PRIORITY_EXTRA, ACMS_DownloaderService.PRIORITY_NORMAL);
									mThisActivity.startService(downloaderIntent);
								}
							} catch (IOException e) {
								UIUtils.showDialog(mThisActivity, "Error trying to add favorite.");
							} catch( Exception e ) {
								UIUtils.showDialog(mThisActivity, e.getMessage());
							}
						}
					});
					dialog.show();	
				}
			});
			layout.addView(contentButton);
		}
	}

}
