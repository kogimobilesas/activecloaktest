package com.irdeto.activecloakmediasample.internal.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * NOTE: This class contains code that is relevant to this application,
 * but not essential to understanding how to integrate the 
 * ActiveCloakMediaPlayer. You are welcome to look at it and understand
 * how it works, and take any pieces that might be relevant to your own
 * player, but it is not necessary to do so.
 * 
 * Utility class containing common UI calls.
 * 
 * @author irdetodev
 *
 */
public class UIUtils {
	
	public static void showDialog(Context context, String message) {
		AlertDialog dialog = new AlertDialog.Builder(context).create();
		dialog.setMessage(message);
		dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {

			}
		});
		dialog.show();
	}

}
