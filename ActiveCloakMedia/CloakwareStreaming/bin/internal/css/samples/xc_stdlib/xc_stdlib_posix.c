/***************************************
*
*	Cloakware Security Suite (CSS)
*
*	May 26, 2008
*	Cloakware Corporation
*
*	FILE: xc_stdlib_posix.c
*
*	The software and information in this package contain proprietary
*	technology and are confidential properties of Cloakware Corporation. If
*	you acquired this package without the appropiate agreements; please
*	contact Cloakware Corporation at:
*
*	phone (613) 271-9446,  email: info@cloakware.com
*
*	This package is provided "as is" with no warranties, expressed or
*	implied, including but not limited to any implied warranty of
*	merchantability, fitness for a particular purpose, or freedom from
*	infringement
*
*	Cloakware Corporation may have patents or pending patent applications,
*	trademarks, copyrights or other intellectual property rights that relate
*	to the described subject matter. The furnishing of this package does not
*	provide any license, expressed or implied, by estoppel or otherwise,
*	to any such patents, trademarks, copyrights, or other intellectual
*	property rights
*
***************************************/

#ifndef XC_RESTRICTED_ISO
#define XC_RESTRICTED_ISO
#endif

#include "xc/xc_stdint.h"
#include "xc/xc_stdlib.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdarg.h>
#include <time.h>

#if defined ( __cplusplus )
extern  "C"
{
#endif	/*	#if defined ( __cplusplus ) */

void XC_exit (int32_t nStatus) {
	exit((int) nStatus);
}

// ******************************************************
// *                      malloc.h                      *
// ******************************************************

void* XC_malloc ( XC_size_t a_Size ) {
	return (void *) malloc ( (size_t) a_Size );
}

void* XC_calloc ( XC_size_t a_Num, XC_size_t a_Size ) {
	return (void *) calloc ( (size_t) a_Num, (size_t) a_Size );
}

void  XC_free ( void* a_Memblock ) {
	free ( a_Memblock );
}

// ******************************************************
// *                      stdlib.h                      *
// ******************************************************
XC_int    XC_rand(void) {  
  return (XC_int) rand();  
}

void    XC_srand(XC_uint seed) {  
  srand(seed);  
}

// ******************************************************
// *                      string.h                      *
// ******************************************************

void*  XC_memcpy (  void* a_Dst, const void* a_Src, XC_size_t a_Size  ) {
	return (void*) memcpy ( a_Dst, a_Src, (size_t) a_Size );
}

XC_int  XC_memcmp (  const void* a_Dst, const void* a_Src, XC_size_t a_Size  ) {
	return (XC_int) memcmp ( a_Dst, a_Src, (size_t) a_Size );
}

void*  XC_memset (  void* a_Dst, XC_int a_Value, XC_size_t a_Size  ) {
	return memset ( a_Dst, (int) a_Value, (size_t) a_Size );
}

void*  XC_memmove ( void* a_Dst, const void* a_Src, XC_size_t a_Size ) {
	return memmove ( a_Dst, a_Src, (size_t) a_Size );
}

XC_char*  XC_strstr ( const XC_char* a_Cs, const XC_char* a_Ct ) {
	return (XC_char*) strstr ( (const char *) a_Cs, (const char *) a_Ct );
}

XC_char*  XC_strcpy ( XC_char* a_Dst, const XC_char* a_Src ) {
	return (XC_char *) strcpy ( (char *) a_Dst, (const char *) a_Src );
}

XC_char*  XC_strncpy ( XC_char* a_Dst, const XC_char * a_Src, XC_size_t a_N ) {
	return (XC_char *) strncpy ( (char *) a_Dst, (const char *) a_Src, (size_t) a_N );
}

XC_char*  XC_strcat ( XC_char* a_Dst, const XC_char* a_Src ) {
	return (XC_char*) strcat ( (char *) a_Dst, (const char *)a_Src );
}

XC_int    XC_strcmp ( const XC_char* a_Dst, const XC_char* a_Src ) {
	return (XC_int) strcmp ( (const char *) a_Dst, (const char *) a_Src );
}

XC_int    XC_strncmp ( const XC_char*  a_Dst, const XC_char*  a_Src, XC_int a_N ) {
	return (XC_int) strncmp ( (const char *)a_Dst, (const char *)a_Src, (int)a_N );
}

XC_char*  XC_strchr ( const XC_char* a_Src, XC_int a_C ) {
	return (XC_char*) strchr ( (const char *)a_Src, (int)a_C );
}

XC_char*  XC_strrchr ( const XC_char* a_Src, XC_int a_C ) {
	return (XC_char*) strrchr ( (const char *)a_Src, (int)a_C );
}

XC_size_t XC_strlen ( const XC_char* a_Dst ) {
	return (XC_size_t) strlen ( (const char *)a_Dst );
}

XC_char*  XC_strncat ( XC_char* a_Dst, const XC_char* a_Src, XC_size_t a_Count ) {
	return (XC_char*) strncat ( (char *)a_Dst, (const char *)a_Src, (size_t) a_Count );
}

XC_long XC_strtol ( const XC_char* a_Ptr, XC_char** a_Endptr, XC_int a_Base ) {
        return (XC_long) strtol((const char *)a_Ptr, (char **)a_Endptr, (int)a_Base);
}

XC_ulong  XC_strtoul ( const XC_char* a_Ptr, XC_char** a_Endptr, XC_int a_Base ) {
        return (XC_ulong) strtoul((const char *)a_Ptr, (char **)a_Endptr, (int)a_Base);
}

XC_char* XC_strpbrk ( const XC_char* a_Src, const XC_char* a_Bytes)
{
  return (XC_char*) *strpbrk((const char *) a_Src, (const char *)a_Bytes);
}

// ******************************************************
// *                      stdio.h                       *
// ******************************************************

XC_int       XC_Simple_printf ( const XC_char* a_String ) {
	return (int32_t) printf((const char *) a_String);
}

XC_int	XC_printf ( const XC_char* a_Fmt, ... ) {
	int retval= 0;
	va_list ap;

	va_start(ap, a_Fmt);
	retval= vprintf((const char *)a_Fmt, ap);
	va_end(ap);

	return (int32_t) retval;
}

XC_int	XC_sprintf ( XC_char* a_Dst, const XC_char* a_Fmt, ... ) {
	int retval= 0;
	va_list ap;

	va_start(ap, a_Fmt);
	retval= vsprintf((char *)a_Dst, (const char*)a_Fmt, ap);
	va_end(ap);

	return (int32_t) retval;
}

XC_FILE*  XC_fopen ( const XC_char* a_Filename, const XC_char* a_Mode ) {
	return (XC_FILE*) fopen ( (const char *) a_Filename, (const char *) a_Mode );
}

XC_size_t    XC_fread ( void* a_Buffer, XC_size_t a_Size, XC_size_t a_Count, XC_FILE* a_Stream ) {
	return fread ( a_Buffer, (size_t) a_Size, (size_t) a_Count, (FILE*)a_Stream );
}

XC_size_t    XC_fwrite ( void* a_Buffer, XC_size_t a_Size, XC_size_t a_Count, XC_FILE* a_Stream ) {
	return fwrite ( a_Buffer, a_Size, a_Count, (FILE*)a_Stream );
}

XC_int	    XC_fseek ( XC_FILE* a_Stream, XC_long a_Offset, XC_int a_Whence ) {
	int a_Whence_local = -1;
	
	if (a_Whence == XC_SEEK_SET)
		a_Whence_local = SEEK_SET;
	else if (a_Whence == XC_SEEK_END)
		a_Whence_local = SEEK_END;
	else if (a_Whence == XC_SEEK_CUR)
		a_Whence_local = SEEK_CUR;
	
	return (XC_int) fseek ( (FILE*)a_Stream, a_Offset, a_Whence_local );
}

XC_int    XC_putc ( XC_int a_C, XC_FILE* a_Stream ) {
	return (XC_int) putc ( (int) a_C, (FILE*)a_Stream );
}

XC_long XC_ftell ( XC_FILE* stream ) {
	return (XC_long) ftell ( (FILE*)stream );
}

XC_int XC_feof ( XC_FILE* stream ) {
	return (XC_int) feof ( (FILE*)stream );
}

XC_int XC_fprintf ( XC_FILE* a_Stream, const XC_char* a_Fmt, ... ) {
	int retval= 0;
	va_list ap;

	va_start(ap, a_Fmt);
	retval= vfprintf((FILE*)a_Stream, (const char *)a_Fmt, ap);
	va_end(ap);

	return (int32_t)retval;
}

XC_int	    XC_fclose ( XC_FILE* a_Stream ) {
	return (XC_int) fclose((FILE*)a_Stream);
}

void	  XC_clearerr (XC_FILE *stream) {
	clearerr((FILE *)stream);
}

void	  XC_fflush (XC_FILE *stream) {
	fflush((FILE *)stream);
}

// ******************************************************
// *                      assert.h                      *
// ******************************************************

#ifdef  DEBUG
void       XC_assert( int32_t expression ) {
	assert((int) expression);
}
#endif

#if defined ( __cplusplus )
}
#endif	/*	if defined ( __cplusplus ) */

