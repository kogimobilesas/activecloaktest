This is the README file for the sample code pt_deny.c, which is anti-debug for iOS by calling ptrace() with PT_DENY_ATTACH

INTRODUCTION:

Ptrace() is a system API on OSX (include iOS). A process �A� can call ptrace() with PT_ATTACH option to attach to another process �B�. Then the A become the parent process of the B. And the A can call ptrace() with other options to trace the B, to peek and poke the context of the B and to do all the things a debugger can do to a debugee. 
But, before the process B is attached, the B can call ptrace() with PT_DENY_ATTACH option to prevent it been attached by process A or any other process.
Generally, following code will do the denying attach on OSX.
	#include <sys/ptrace.h>
	Ptrace(PT_DENY_ATTACH, 0, 0, 0);

But on iOS, the ptrace() is not a documented API.  It cannot to be explicitly called. We have to use dlopen() and dlsym() to find the function address and then call it indirectly. The actually code is as below,
	#include <dlfcn.h>
	#include <sys/types.h>
	typedef int (*ptrace_ptr_t)(int _request, pid_t _pid, caddr_t _addr, int _data);
	#define PT_DENY_ATTACH 31
	void* handle = dlopen(0, RTLD_GLOBAL | RTLD_NOW);
	ptrace_ptr_t ptrace_ptr = dlsym(handle, "ptrace");
	rval = ptrace_ptr(PT_DENY_ATTACH, 0, 0, 0);
	dlclose(handle);

Because the ptrace() is a system API,  following assembly code will be called finally.   
	mov r0, #31
	mov r1, #0
	mov r2, #0
	mov r3, #0
	mov ip, 	#26
	svc   #0x80
So to be more secure, we can call the assembly code directly.  


IMPLEMENTATION:

Three functions are implemented in this sample code:
	/* return: 0, on success      */
	/*        -1, on fail         */
	int pt_deny_in_c() {
		int rval;
		void* handle = dlopen(0, RTLD_GLOBAL | RTLD_NOW);
		if (handle == NULL)
			return -1;
		ptrace_ptr_t ptrace_ptr = dlsym(handle, "ptrace");
		if (ptrace_ptr == NULL)
			rval = -1;
		else
			rval = ptrace_ptr(PT_DENY_ATTACH, 0, 0, 0);
		dlclose(handle);
		return rval;
	}

	/* return: 0, on success      */
	/*        -1, on fail         */
	int pt_deny_in_asm(void) {
		register int ret asm("r0");
		asm volatile("mov r0, #31");
		asm volatile("mov r1, #0");
		asm volatile("mov r2, #0");
		asm volatile("mov r3, #0");
		asm volatile("mov ip, #26");
		asm volatile("svc #0x80" : "=r"(ret));
		return ret;
	}

	/* return: True, on success      */
	/*        False, on fail         */
	bool  am_i_being_debugged(void)
	{
		int                 junk;
		int                 mib[4];
		struct kinfo_proc   info;
		size_t              size;
		info.kp_proc.p_flag = 0;
		mib[0] = CTL_KERN;
		mib[1] = KERN_PROC;
		mib[2] = KERN_PROC_PID;
		mib[3] = getpid();
		size = sizeof(info);
		junk = sysctl(mib, sizeof(mib) / sizeof(*mib), &info, &size, NULL, 0);
		assert(junk == 0);
		return ( (info.kp_proc.p_flag & P_TRACED) != 0 );
	}

If the app is already under debugging, calling to the function pt_deny_in_c() or pt_deny_in_asm() will cause the app crashed. The function am_i_being_debugged() will tell whether the app is under debugging. 
Conclusion
Both C level code and assembly level code works fine and the application calling those functions in the sample code was approved to be listed in app store for downloading. But, they are not called in main() function of the application.
 
The function am_i_being_debugged() really helps to check if the game is debugged. It may be a good strategy to call it before call any other two to avoid app crashing. 

USE WITH PFIV SECURE CALL:

The function pt_deny_in_c() and pt_deny_in_asm() could be protected from tamping attack by using PFIV. 
The application that use pt_deny_in_c() or pt_deny_in_asm() should also use PFIV. The secure call API of the PFIV is designed to call a function with the function verified before the calling. 

To protect pt_deny_in_c() and pt_deny_in_asm() from tamping attack, invoke the PFIV secure call API and let PFIV call pt_deny_in_c() or pt_deny_in_asm() indirectly. The PFIV will call pt_deny_in_c() or pt_deny_in_asm() only when it is not tampered.  Because the secure call API of the PFIV only support the function with following definition to be called, 
	void  <function_name>(void *)
the pt_deny_in_c() and pt_deny_in_asm() have to be modified as below to use the secure call. 

	/* ouput:   pRval  0, on success      */
	/*        		  -1, on fail             */
	void  pt_deny_in_c(void *pRval) {
		int rval;
		void* handle = dlopen(0, RTLD_GLOBAL | RTLD_NOW);
		if (handle == NULL) {
			*(int*)pRval =-1;
			return;
		}
		ptrace_ptr_t ptrace_ptr = dlsym(handle, "ptrace");
		if (ptrace_ptr == NULL)
			rval = -1;
		else
			rval = ptrace_ptr(PT_DENY_ATTACH, 0, 0, 0);
		dlclose(handle);
		*(int*)pRval = rval;
	}

	/* ouput:   pRval  0, on success      */
	/*        		  -1, on fail             */
	void  pt_deny_in_asm(void *pRval) {
		register int ret asm("r0");
		asm volatile("mov r0, #31");
		asm volatile("mov r1, #0");
		asm volatile("mov r2, #0");
		asm volatile("mov r3, #0");
		asm volatile("mov ip, #26");
		asm volatile("svc #0x80" : "=r"(ret));
		*(int*)pRval = ret;
	}

After that change, pt_deny_in_c() or pt_deny_in_asm() can be call with PFIV secure call in a sample as below:
	XC_int32  ret;
	XC_int32  pt_deny_ret;
	XC_IV_Handle_t mhandle;
	XC_V_Handle_t vhandle;
	XC_SC_Handle_t pScHandle=NULL;

	ret = XC_IV_Load_Voucher_From_File(<voucher-name>,&vhandle);
	if (ret != XC_OK)  goto fail;
	ret = XC_IV_Open_Module(vhandle,<module-name>,&mhandle);
	if (ret != XC_OK)  goto fail;

	ret = XC_SC_Open(vhandle1,(void *)pt_deny_in_c,&pScHandle);
	if (ret != XC_OK)  goto fail;
	ret = XC_SC_Set_Arg(pScHandle,(void*)&pt_deny_ret);
	if (ret != XC_OK)  goto fail;
               /* pt_deny_in_c will be called and the return value is in pt_deny_ret */
	ret = XC_SC_Call(pScHandle);   
           fail:
