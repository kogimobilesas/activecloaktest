#include <stdio.h>
#include <dlfcn.h>
#include <sys/types.h>
#include <sys/sysctl.h>
#include <assert.h>

typedef int (*ptrace_ptr_t)(int _request, pid_t _pid, caddr_t _addr, int _data);
#define PT_DENY_ATTACH 31
/* return: 0, on success      */
/*        -1, on fail         */
int pt_deny_in_c() {
    int rval;
	
    void* handle = dlopen(0, RTLD_GLOBAL | RTLD_NOW);
	if (handle == NULL)
		return -1;

	ptrace_ptr_t ptrace_ptr = dlsym(handle, "ptrace");
	if (ptrace_ptr == NULL)
		rval = -1;
	else
		rval = ptrace_ptr(PT_DENY_ATTACH, 0, 0, 0);
		
    dlclose(handle);
    return rval;
}

/* return: 0, on success      */
/*        -1, on fail         */
int pt_deny_in_asm(void) {
    register int ret asm("r0");
    
    asm volatile("mov r0, #31");
    asm volatile("mov r1, #0");
    asm volatile("mov r2, #0");
    asm volatile("mov r3, #0");
    asm volatile("mov ip, #26");
    asm volatile("svc #0x80" : "=r"(ret));

    return ret;
}

/* return: True, on success      */
/*        False, on fail         */
bool am_i_being_debugged(void)
{
    int                 junk;
    int                 mib[4];
    struct kinfo_proc   info;
    size_t              size;
    info.kp_proc.p_flag = 0;
    mib[0] = CTL_KERN;
    mib[1] = KERN_PROC;
    mib[2] = KERN_PROC_PID;
    mib[3] = getpid();
    size = sizeof(info);
    junk = sysctl(mib, sizeof(mib) / sizeof(*mib), &info, &size, NULL, 0);
    assert(junk == 0);
    return ( (info.kp_proc.p_flag & P_TRACED) != 0 );
}

