/*

	Cloakware Security Suite (CSS)

	July 03, 2009
	Cloakware Corporation

	FILE: xc_stdsys_mac.c

	The software and information in this package contain proprietary
	technology and are confidential properties of Cloakware Corporation. If
	you acquired this package without the appropiate agreements; please
	contact Cloakware Corporation at:

	phone (613) 271-9446,  email: info@cloakware.com

	This package is provided "as is" with no warranties, expressed or
	implied, including but not limited to any implied warranty of
	merchantability, fitness for a particular purpose, or freedom from
	infringement

	Cloakware Corporation may have patents or pending patent applications,
	trademarks, copyrights or other intellectual property rights that relate
	to the described subject matter. The furnishing of this package does not
	provide any license, expressed or implied, by estoppel or otherwise,
	to any such patents, trademarks, copyrights, or other intellectual
	property rights

*/
#include "xc/xc_stdsys.h"
#include <time.h>
#include <libgen.h>
#include <mach-o/dyld.h>
#define __private_extern__ extern
#include <sys/time.h>

/* Platform GetTick */
XC_ulong XC_STDSYS_GetTick()
{
    struct timeval tv;
    XC_ulong utime;
    gettimeofday(&tv, NULL);
    utime = (XC_ulong)tv.tv_sec*1000 + tv.tv_usec/1000;
    return utime;
}

void  XC_GetModuleCpuType(const XC_char* pModuleName, XC_int32 *cpu_major, XC_int32  *cpu_minor)
{
	XC_uint8 *pBaseAddress= XC_GetBaseAddress(pModuleName);
	if (pBaseAddress)
	{
		*cpu_major = ((struct mach_header*)pBaseAddress)->cputype;
		*cpu_minor = ((struct mach_header*)pBaseAddress)->cpusubtype;
	}else{
		*cpu_major = 0;
		*cpu_minor = 0;
	}
}

XC_uint8 *XC_GetBaseAddress(const XC_char *pModuleName)
{
	int	i;
	int		ImageCnt = 0;
	XC_uint8 *pBaseAddress=0;
	char	*pBaseName = NULL;
	char	*pIamgeName = NULL;
	if (pModuleName == NULL) return 0;
	
	ImageCnt=_dyld_image_count();
	if (ImageCnt == 0)	return 0;
	for (i = 0; i < ImageCnt; i++)
	{
		pIamgeName =(char *)_dyld_get_image_name(i);
		if (pIamgeName == NULL) continue;
		pBaseName = basename(pIamgeName);
		if (strcmp(pBaseName,pModuleName) == 0)
		{
			pBaseAddress =(XC_uint8 *)_dyld_get_image_header(i);
			break;
		}
	}
	if (pBaseAddress != NULL) {
		/* When module is SL loaded, the loading address of the app is in the offset LOADADDR_OFFSET from beginning of stub*/
		LOADADDR_GLVAR* pLoadAddr;
		XC_uint32 offset;
		offset = *(XC_uint32*)(void *)(pBaseAddress + LOADADDR_OFFSET);
		DE_OFFSET(offset);
		if ( offset != 0) {
			pLoadAddr = (LOADADDR_GLVAR*)(void *)(pBaseAddress + offset);
			if (pLoadAddr->isSlLoaded == SL_FLAG) pBaseAddress = pLoadAddr->loadAddress;
		}
	}
	return pBaseAddress;
}
