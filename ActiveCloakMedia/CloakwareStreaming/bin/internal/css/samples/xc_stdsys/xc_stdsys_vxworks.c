/*

	Cloakware Security Suite (CSS)

	July 03, 2009
	Cloakware Corporation

	FILE: xc_stdsys_vxworks.c

	The software and information in this package contain proprietary
	technology and are confidential properties of Cloakware Corporation. If
	you acquired this package without the appropiate agreements; please
	contact Cloakware Corporation at:

	phone (613) 271-9446,  email: info@cloakware.com

	This package is provided "as is" with no warranties, expressed or
	implied, including but not limited to any implied warranty of
	merchantability, fitness for a particular purpose, or freedom from
	infringement

	Cloakware Corporation may have patents or pending patent applications,
	trademarks, copyrights or other intellectual property rights that relate
	to the described subject matter. The furnishing of this package does not
	provide any license, expressed or implied, by estoppel or otherwise,
	to any such patents, trademarks, copyrights, or other intellectual
	property rights

*/

#include "xc\xc_stdsys.h"
#include <tickLib.h>

/* Platform GetTick */
XC_ulong XC_STDSYS_GetTick() 
{
  return tickGet();
}
