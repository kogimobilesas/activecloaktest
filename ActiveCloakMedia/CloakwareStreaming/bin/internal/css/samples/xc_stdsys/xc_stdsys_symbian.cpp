/*

	Cloakware Security Suite (CSS)

	July 03, 2009
	Cloakware Corporation

	FILE: xc_stdsys_palm.c

	The software and information in this package contain proprietary
	technology and are confidential properties of Cloakware Corporation. If
	you acquired this package without the appropiate agreements; please
	contact Cloakware Corporation at:

	phone (613) 271-9446,  email: info@cloakware.com

	This package is provided "as is" with no warranties, expressed or
	implied, including but not limited to any implied warranty of
	merchantability, fitness for a particular purpose, or freedom from
	infringement

	Cloakware Corporation may have patents or pending patent applications,
	trademarks, copyrights or other intellectual property rights that relate
	to the described subject matter. The furnishing of this package does not
	provide any license, expressed or implied, by estoppel or otherwise,
	to any such patents, trademarks, copyrights, or other intellectual
	property rights

*/

#include "xc\xc_stdsys.h"
#include <f32file.h>
#ifdef __cplusplus
extern "C" {
#endif

/* Platform GetTick */
XC_ulong XC_STDSYS_GetTick() 
{
  return User::TickCount() * 1000 / 64;
}

/*
This function will get cpu type of module binary not  machine cpu type 
it is supporting the FAT (or universal binary) application
If the target platform doesn't support fat application, then set the cput type to be zero
*/

void  XC_GetModuleCpuType(const XC_char* pModuleName, XC_int32 *cpu_major, XC_int32  *cpu_minor)
{
	*cpu_major = 0;
	*cpu_minor = 0;
}


void stringToDescriptor(const char* aString, TDes& aDescriptor)
{
    TPtrC8 ptr(reinterpret_cast<const TUint8*>(aString));
    aDescriptor.Copy(ptr);
}

unsigned char *GetExeModuleHandle(TDesC& exeModuleName)
{

	unsigned char *baseAddress = 0;
	//Get current exe name 
	RProcess exeProc;
	TFileName exeName = exeProc.FileName();
	exeName.LowerCase();
	//Parse the exe name
	TParse p;
	if (p.Set(exeName,NULL,NULL) == KErrNone)
	{
		TPtrC exeNameAndExt = p.NameAndExt();
		if (exeNameAndExt == exeModuleName)
		{
			TProcessMemoryInfo Info;
			if (exeProc.GetMemoryInfo(Info) == KErrNone)
			{
				baseAddress = (unsigned char *)Info.iCodeBase;
			}
		}
	}
	return baseAddress;
}


unsigned char *GetDllModuleHandle(TDesC& dllModuleName)
{
	unsigned char *baseAddress = 0;
	//For the symbian 9.1, it will return the address
	// of first export function
	RLibrary DllHandle;
	if (DllHandle.Load(dllModuleName)== KErrNone)
	{
		TLibraryFunction fun = DllHandle.Lookup(1);
		if (fun != NULL)
		{
			baseAddress= (unsigned char *)fun;
		}
		DllHandle.Close();
	}
	return baseAddress;
}

XC_uint8 *XC_GetBaseAddress(const XC_char *pModuleName)
{
	unsigned char *pBaseAddress=NULL;

	//Get test module name
	TBuf<256> testModulename;
	stringToDescriptor(pModuleName, testModulename);
	testModulename.LowerCase();
	pBaseAddress = GetExeModuleHandle(testModulename);
	if (pBaseAddress == 0)
	{
		pBaseAddress = GetDllModuleHandle(testModulename);
	}
	return pBaseAddress;
}
#if defined ( __cplusplus )
}
#endif	/*	if defined ( __cplusplus ) */
