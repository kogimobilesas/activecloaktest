/*

	Cloakware Security Suite (CSS)

	July 03, 2009
	Cloakware Corporation

	FILE: xc_stdsys_posix.c

	The software and information in this package contain proprietary
	technology and are confidential properties of Cloakware Corporation. If
	you acquired this package without the appropiate agreements; please
	contact Cloakware Corporation at:

	phone (613) 271-9446,  email: info@cloakware.com

	This package is provided "as is" with no warranties, expressed or
	implied, including but not limited to any implied warranty of
	merchantability, fitness for a particular purpose, or freedom from
	infringement

	Cloakware Corporation may have patents or pending patent applications,
	trademarks, copyrights or other intellectual property rights that relate
	to the described subject matter. The furnishing of this package does not
	provide any license, expressed or implied, by estoppel or otherwise,
	to any such patents, trademarks, copyrights, or other intellectual
	property rights

*/
#include <stdio.h>
#include <string.h>
#include "xc/xc_stdsys.h"
#include <time.h>
#include <sys/time.h>

/* Platform GetTick */
XC_ulong XC_STDSYS_GetTick()
{
    struct timeval tv;
    XC_ulong utime;
    gettimeofday(&tv, NULL);
    utime = (XC_ulong)tv.tv_sec*1000 + tv.tv_usec/1000;
    return utime;
}

/* 
This function will get cpu type of module binary not  machine cpu type 
it is supporting the FAT (or universal binary) application
If the target platform doesn't support fat application, then set the cput type to be zero
*/ 
void  XC_GetModuleCpuType(const XC_char* pModuleName, XC_int32 *cpu_major, XC_int32  *cpu_minor)
{
	*cpu_major = 0;
	*cpu_minor = 0;
}

#define PROC_MAPS_FILE     "/proc/self/maps"    
unsigned char  *GetModuleHandleFromProcessFile(const char *pModuleName)
{
        FILE    *fp;
        char    *pfound;
        char    line[256];
        char    r,w,x;
        unsigned long startAddr=0;
        unsigned long endAddr=0;
        unsigned char *pBaseAddress= NULL;

        if (pModuleName == NULL)  goto end;

        fp = fopen(PROC_MAPS_FILE, "r");
        if (fp == NULL)  goto end;
        
       	memset(line,0,256);
        while (fgets(line, 256, fp) != NULL)
        {
                int num;
                pfound =strrchr(line,'/');
                if (pfound == NULL) continue;
                pfound++;
                if (strlen(pfound)-1 != strlen(pModuleName)) continue;
                if (strncasecmp(pfound,pModuleName,strlen(pModuleName)) != 0) continue;

                #if defined (__x86_64__)
				num = sscanf(line, "%lx-%lx %c%c%c", &startAddr, &endAddr, &r,&w,&x);
                #else
                num = sscanf(line, "%x-%x %c%c%c", &startAddr, &endAddr, &r,&w,&x);
                #endif
                if(num != 5) goto end;
                else if (x == 'x')  break;
                startAddr = 0;
                memset(line,0,256);
        }
	    pBaseAddress = (unsigned char *)startAddr;
end:
	    if (fp) fclose(fp);
	    return pBaseAddress;
}
XC_uint8 *XC_GetBaseAddress(const XC_char *pModuleName)
{
        XC_uint8 *pBaseAddress=NULL;
        char *pBaseName = NULL;

        if (pModuleName == NULL) return pBaseAddress;
        pBaseAddress=(XC_uint8 *)GetModuleHandleFromProcessFile(pModuleName);
        if (pBaseAddress == 0)
        {
                char symLinkName[256];
                memset(symLinkName,0,256);
                if ( readlink(pModuleName,symLinkName,256) > 0 )
                {
                        pBaseName = (char*)basename(symLinkName);
                        pBaseAddress=(unsigned char *)GetModuleHandleFromProcessFile(pBaseName);
                }
        }
        if (pBaseAddress != 0)
        {
            /* When module is SL loaded, the loading address of the app is in the offset LOADADDR_OFFSET from beginning of stub*/
            LOADADDR_GLVAR* pLoadAddr;
            XC_uint32 offset;
            offset = *(XC_uint32*)(void *)(pBaseAddress + LOADADDR_OFFSET);
            DE_OFFSET(offset);
            if ( offset != 0)
			{
                pLoadAddr = (LOADADDR_GLVAR*)(void *)(pBaseAddress + offset);
                if (pLoadAddr->isSlLoaded == SL_FLAG) pBaseAddress = pLoadAddr->loadAddress;
            }
        }
        return pBaseAddress;
}

