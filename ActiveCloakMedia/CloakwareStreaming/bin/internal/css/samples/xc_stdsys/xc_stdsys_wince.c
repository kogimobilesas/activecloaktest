/*

	Cloakware Security Suite (CSS)

	May 16, 2011
	Irdeto Corporation

	FILE: xc_stdsys_wince.c

	The software and information in this package contain proprietary
	technology and are confidential properties of Irdeto Corporation. If
	you acquired this package without the appropiate agreements; please
	contact Cloakware Corporation at:

	phone (613) 271-9446,  email: info@cloakware.com

	This package is provided "as is" with no warranties, expressed or
	implied, including but not limited to any implied warranty of
	merchantability, fitness for a particular purpose, or freedom from
	infringement

	Irdeto Corporation may have patents or pending patent applications,
	trademarks, copyrights or other intellectual property rights that relate
	to the described subject matter. The furnishing of this package does not
	provide any license, expressed or implied, by estoppel or otherwise,
	to any such patents, trademarks, copyrights, or other intellectual
	property rights

*/
#include "xc\xc_stdsys.h"
#include <windows.h>
#include <psapi.h>


/* Platform GetTick */
unsigned long XC_STDSYS_GetTick() 
{
  return ( unsigned long ) GetTickCount();
}

/*
This function will get cpu type of module binary not  machine cpu type 
it is supporting the FAT (or universal binary) application
If the target platform doesn't support fat application, then set the cput type to be zero
*/

void  XC_GetModuleCpuType(const char* pModuleName, int *cpu_major, int  *cpu_minor)
{
	*cpu_major = 0;
	*cpu_minor = 0;
}

static const char *helper_GetExtension(const char *path, char *pExtension, int extensionSize)
{
    const char *ptr;
    if ((pExtension == NULL) || (extensionSize <= 0))
      return NULL;

    memset(pExtension, 0, extensionSize);
    
    if (path == NULL)
	return NULL;
    
    if ((ptr = strrchr(path, '.' )) == NULL)
        ptr = path;
    else
        ptr++;
    
    strncpy(pExtension, ptr, extensionSize - 1);
    return pExtension;
}
XC_uint8 *XC_GetBaseAddress(const XC_char *pModuleName)
{
    HMODULE hModule;
    MODULEINFO moduleInfo;
    int ret;
    char ext[_MAX_PATH];

    if (helper_GetExtension (pModuleName, ext, _MAX_PATH) == 0)
      return 0;

    if (_stricmp(ext, "exe") == 0)
    {
        hModule = NULL;
    }
    else
    {
        TCHAR pTcharModuleName[_MAX_PATH + 1];
        int i, len;
	len = strlen (pModuleName);
	for (i = 0; i < len; i++)
	{
	  pTcharModuleName[i]=pModuleName[i];
	}
	pTcharModuleName[len] = '\0';
	
        hModule = GetModuleHandle(pTcharModuleName);
	if (hModule == NULL)
	{
	    return 0;
	}
    }
    if ((ret = GetModuleInformation(GetCurrentProcess(), hModule, &moduleInfo, sizeof(MODULEINFO))) == 0)
    {
        return 0;
    }
    return (unsigned char *) (moduleInfo.lpBaseOfDll);

}

