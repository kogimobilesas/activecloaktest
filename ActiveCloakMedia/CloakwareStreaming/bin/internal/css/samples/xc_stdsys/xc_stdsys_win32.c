/*

	Cloakware Security Suite (CSS)

	July 03, 2009
	Cloakware Corporation

	FILE: xc_stdsys_win32.c

	The software and information in this package contain proprietary
	technology and are confidential properties of Cloakware Corporation. If
	you acquired this package without the appropiate agreements; please
	contact Cloakware Corporation at:

	phone (613) 271-9446,  email: info@cloakware.com

	This package is provided "as is" with no warranties, expressed or
	implied, including but not limited to any implied warranty of
	merchantability, fitness for a particular purpose, or freedom from
	infringement

	Cloakware Corporation may have patents or pending patent applications,
	trademarks, copyrights or other intellectual property rights that relate
	to the described subject matter. The furnishing of this package does not
	provide any license, expressed or implied, by estoppel or otherwise,
	to any such patents, trademarks, copyrights, or other intellectual
	property rights

*/

#include "xc\xc_stdsys.h"
#include <windows.h>

/* Platform GetTick */
XC_ulong XC_STDSYS_GetTick() 
{
  return ( XC_ulong ) GetTickCount();
}

XC_uint8 *XC_GetBaseAddress(const XC_char* pModuleName){
	XC_uint8 *pBaseAddress=0;
	pBaseAddress = (XC_uint8 *)(GetModuleHandle(pModuleName));

	if (pBaseAddress != NULL) {
		/* When module is SL loaded, the loading address of the app is in the offset LOADADDR_OFFSET from beginning of stub*/
		LOADADDR_GLVAR* pLoadAddr;
		XC_uint32 offset;

		offset = *(XC_uint32*)(void *)(pBaseAddress + LOADADDR_OFFSET);
		DE_OFFSET(offset);
		if ( offset != 0) {
			pLoadAddr = (LOADADDR_GLVAR*)(void *)(pBaseAddress + offset);
			if (pLoadAddr->isSlLoaded == SL_FLAG)pBaseAddress = pLoadAddr->loadAddress;
		}
	}
	return pBaseAddress;

}

/*
This function will get cpu type of module binary not  machine cpu type 
it is supporting the FAT (or universal binary) application
If the target platform doesn't support fat application, then set the cput type to be zero
*/
void  XC_GetModuleCpuType(const XC_char* pModuleName, XC_int32 *cpu_major, XC_int32  *cpu_minor)
{
	*cpu_major = 0;
	*cpu_minor = 0;
}


