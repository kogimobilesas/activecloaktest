/*
	Cloakware Security Suite (CSS)

	July 03, 2012
	Cloakware Corporation

	FILE: xc_stdsys_qnx.c

	The software and information in this package contain proprietary
	technology and are confidential properties of Cloakware Corporation. If
	you acquired this package without the appropiate agreements; please
	contact Cloakware Corporation at:

	phone (613) 271-9446,  email: info@cloakware.com

	This package is provided "as is" with no warranties, expressed or
	implied, including but not limited to any implied warranty of
	merchantability, fitness for a particular purpose, or freedom from
	infringement

	Cloakware Corporation may have patents or pending patent applications,
	trademarks, copyrights or other intellectual property rights that relate
	to the described subject matter. The furnishing of this package does not
	provide any license, expressed or implied, by estoppel or otherwise,
	to any such patents, trademarks, copyrights, or other intellectual
	property rights

*/
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/procfs.h>
#include "xc/xc_stdsys.h"

XC_ulong XC_STDSYS_GetTick() 
{
  time_t myTime=time(NULL);
  XC_ulong utime = (XC_ulong)myTime;
  return utime * 1000;
}

void  XC_GetModuleCpuType(const XC_char* pModuleName, XC_int32 *cpu_major, XC_int32  *cpu_minor)
{
	*cpu_major = 0;
	*cpu_minor = 0;
}

XC_uint8 *XC_GetBaseAddress(const XC_char *pModuleName)
{
	int i, num, proc_fd;
	char   *pName;
	char procas[_POSIX_PATH_MAX + 1] = "/proc/self/as";
	procfs_mapinfo *mapinfos = NULL;
	XC_uint8  *pBaseAddress=NULL;
	
	struct
	{
		procfs_debuginfo info;
		char path[_POSIX_PATH_MAX];
	}map;

	if (pModuleName == NULL)
	{
		return pBaseAddress;
	}

	if( (proc_fd = open(procas, O_RDONLY)) == -1 ) 
	{
		return pBaseAddress;
	}

	// Get the number of map entrys. 
	if (devctl(proc_fd, DCMD_PROC_MAPINFO, NULL, 0, &num) != EOK)
	{
		close(proc_fd);
		return pBaseAddress;
	}

	mapinfos = malloc (num * sizeof (procfs_mapinfo));
	if (!mapinfos) return pBaseAddress;

	// Fill the map entrys. 
	if (devctl (proc_fd, DCMD_PROC_MAPINFO, mapinfos, num * sizeof (procfs_mapinfo), &num) != EOK)
	{
		close(proc_fd);
		free (mapinfos);
		return pBaseAddress;
	}

	// Run through the list of mapinfos 
	for (i = 0; i < num; i++)
	{
		if (mapinfos[i].vaddr != mapinfos[i].ino)
			continue;
			
		map.info.vaddr = mapinfos[i].vaddr;
		if (devctl (proc_fd, DCMD_PROC_MAPDEBUG, &map, sizeof (map), 0) != EOK)
			continue;
		
		pName = strrchr(map.info.path,'/');
		if (pName == NULL) 
			pName = map.info.path;
		else
			pName++;

		if (strlen(pName) != strlen(pModuleName)) 
			continue;
			
		if (strnicmp(pName,pModuleName,strlen(pModuleName)) != 0) 
			continue;

		pBaseAddress = (XC_uint8 *)mapinfos[i].vaddr;
		break;
	}
	free (mapinfos);
	close(proc_fd);
	return pBaseAddress;
}
