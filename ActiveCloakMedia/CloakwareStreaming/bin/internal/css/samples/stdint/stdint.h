/*  Provides definitions of fixed width types for x86 system */

#ifndef _STDINT_H
#define _STDINT_H

typedef signed char        int8_t;
typedef unsigned char      uint8_t;
typedef signed short       int16_t;
typedef unsigned short     uint16_t;
typedef signed int         int32_t;
typedef unsigned int       uint32_t;

#ifndef XC_RESTRICTED_ISO_32BIT
typedef signed long long   int64_t;
typedef unsigned long long uint64_t;
#endif
#endif
