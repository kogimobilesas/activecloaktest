/*
 * xc_pl_utility
 *	Set of high level utility to manipulation parametric value
 *
 *  This is a generic file that works with all node
 */

/*
 * Make sure we only include it once
 */
#ifndef _XC_PL_UTILITY_
#define _XC_PL_UTILITY_

/* Basic include */
#include "xc/xc_types.h"
#include "xc/xc_pl_usage.h"

/* Make sure it works for C++ */
#ifdef __cplusplus
extern "C" {
#endif

/* Struct use to move around parametric while they are building */
typedef struct ParametricBuildingStruct
{
	XC_uint32 sizeOfParametric;
	XC_uint8 data[MAX_NLPARAMETRIC_LENGTH];
} ParametricBuilding;

#define MAX_NLPARAMETRIC_IN_SET 256
typedef struct ParametricBuildingSetStruct
{
	XC_uint32 sizeOfSet;
	ParametricBuilding* data[MAX_NLPARAMETRIC_IN_SET];

} ParametricBuildingSet;

/* Diversity number, use to create diversity in the algorithm */
typedef XC_uint32 ParametricDiversity;
static const ParametricDiversity debugMode = 0;
static const ParametricDiversity simpleMode = 0;

/*
 * Convertion function from ParametricBuild to correct return form
 */

ParametricFastReturn *convertBuildToFastReturn(ParametricBuilding *theParametric);
void convertBuildToSecureReturn(ParametricBuilding *theParametric, XC_uint8** returnValue, XC_uint32* sizeReturnValue);
ParametricReturn *convertBuildToReturn(ParametricBuilding *theParametric);

/*
 * Conversion function from number to ParametricBuild
 */
ParametricBuilding *convertNumberToParametric(XC_uint32 number);

/*
 * Parametric manipulation function:
 *	
 *	This set of function is used to modify the parametric from a size, entropy distribution, ...
 *	They all use the same concept:
 *		They receive a parametric which is modify (the receive copy is modified) and return
 *			The reason we also do the return is to support function calling inlining.
 */

/*
 * Size manipulation
 */

/* Set the size of the parametric */
ParametricBuilding* setToSize(ParametricBuilding *theParametric, XC_uint8 const correctSize, const ParametricDiversity diversity);

/*
 * Parametric Merging
 */
ParametricBuilding* mergeParametric(ParametricBuilding *theParametric1, ParametricBuilding *theParametric2, const ParametricDiversity diversity);
ParametricBuilding* mergeParametricSet(ParametricBuildingSet *theParametricSet, const ParametricDiversity diversity);

/* Entropy smearing */
ParametricBuilding* smearEntropy(ParametricBuilding *theParametric, const ParametricDiversity diversity);

/*  Parametric manipulation */
ParametricBuilding* manipulateParametric(ParametricBuilding *theParametric, const ParametricDiversity diversity);



/* Print utility */
void printParametricBuildind(ParametricBuilding const *theParametric);
void printParametricSecureReturn(ParametricSecureReturn const *theParametric);

/*
 * Utility functions.
 */
_xc_preserve_interface void XC_FP_Free_Buffer(XC_uint8* buffer);

_xc_preserve_interface void XC_FP_Secure_Free_Buffer(XC_uint8* buffer, XC_uint32 sizeBuffer);


#ifdef __cplusplus
}
#endif

#endif
