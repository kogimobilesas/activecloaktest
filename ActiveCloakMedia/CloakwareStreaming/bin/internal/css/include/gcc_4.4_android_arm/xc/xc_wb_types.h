#ifndef _XC_WB_TYPES_H_
#define _XC_WB_TYPES_H_

#include "xc/xc_types.h"

#if defined(XPP_ENABLED) || defined(XPP_SMOOTH_ENABLED)

#pragma  xc_riso_portable_id(XC_DK_AES_EXCLUDE_BETA)
#pragma  xc_riso_portable_id(XC_DK_AES_EXCLUDE_DELTA)
#pragma  xc_riso_portable_id(XC_DK_AES_EXCLUDE_ETA)
#pragma  xc_riso_portable_id(XC_DK_AES_EXCLUDE_GAMMA)
#pragma  xc_riso_portable_id(XC_DK_AES_EXCLUDE_KAPPA)
#pragma  xc_riso_portable_id(XC_DK_AES_EXCLUDE_ZETA)
#pragma  xc_riso_portable_id(XC_DK_AES_EXCLUDE_CUSTOM_ETA)
#pragma  xc_riso_portable_id(XC_FK_AES_EXCLUDE_BETA)
#pragma  xc_riso_portable_id(XC_FK_AES_EXCLUDE_DELTA)
#pragma  xc_riso_portable_id(XC_FK_AES_EXCLUDE_ETA)
#pragma  xc_riso_portable_id(XC_FK_AES_EXCLUDE_GAMMA)
#pragma  xc_riso_portable_id(XC_FK_AES_EXCLUDE_KAPPA)
#pragma  xc_riso_portable_id(XC_FK_AES_EXCLUDE_ZETA)
#pragma  xc_riso_portable_id(XC_SHUFFLE_ON)

#pragma  xc_riso_portable_id(XC_DK_TDES_EXCLUDE_GAMMA)

#endif /* defined(XPP_ENABLED) || defined(XPP_SMOOTH_ENABLED) */

#define XC_EMPTY 17

#ifdef __cplusplus
extern "C" {
#endif

/* Common (to all crypto algorithms) typedefs */
typedef struct _XCWBImportBuffer * XC_WBImportBuffer;
typedef struct _XCWBDataHolder * XC_WBDataHolder;

/* BlockCipher typedefs */
typedef struct _XCBlockCipherRKS * XC_BlockCipherRKS;
typedef struct _XCBlockCipherOptions {
    XC_WBDataHolder dataHolder;
    XC_char *       tableLabel;
    XC_int32        oldEtype;
    XC_Octet *      reserved;
    XC_int32        reservedLength;
} XC_BlockCipherOptions;

/* Asymmetric typedefs */
typedef struct _XCAsymmetricPublicKey * XC_AsymmetricPublicKey;
typedef struct _XCAsymmetricPublicKeyBuffer * XC_AsymmetricPublicKeyBuffer;
typedef struct _XCAsymmetricPrivateKey * XC_AsymmetricPrivateKey;
typedef struct _XCAsymmetricPrivateKeyBuffer * XC_AsymmetricPrivateKeyBuffer;
typedef struct _XCAsymmetricCipherOptions {
    XC_WBDataHolder dataHolder;
    XC_char *       tableLabel;
    XC_int32        hashFunction;
    XC_int32        saltLength;
} XC_AsymmetricCipherOptions;

/* HashFunction typedefs */
typedef struct _XCHashContext * XC_HashContext;
typedef struct _XCHashFunctionOptions {
    XC_WBDataHolder dataHolder;
    XC_char * tableLabel;
} XC_HashFunctionOptions;

/* PRNG typedefs */
typedef struct _XCPRNGState * XC_PRNGState;
typedef struct _XCPRNGOptions {
    XC_WBDataHolder dataHolder;
    XC_char * tableLabel;
} XC_PRNGOptions;
typedef XC_uint32 (*XC_PRNGEntropyFunction)(void);


/* These constants are defined in the exact same way in both 3.x and 4.x.
 * We guard them so they are not included twice.
 */
#ifndef _WB_CONSTS_
#define _WB_CONSTS_
#define XC_ELEM_8          256  /* Number of entries in an 8-bit table. */
#define XC_ELEM_8_AND      255  /* For modular arithmetic. */
#define XC_ELEM_4          16   /* Number of entries in a 4-bit table. */
#define XC_ELEM_4_AND      15   /* For modular arithmetic. */
#define XC_ELEM_6          64   /* Number of entries in an 6-bit table. */
#define XC_ELEM_6_AND      63   /* For modular arithmetic. */
#define XC_INT             32   /* Number of bits in an int. */
#define XC_BYTE            8    /* Number of bits in a byte. */
#define XC_NIBBLE          4    /* Number of bits in a nibble. */
#define XC_NPB             2    /* Nibbles per byte. */
#define XC_NPI             8    /* Nibbles per int. */
#define XC_BPI             4    /* Bytes per int. */
#define XC_BPIM1           3    /* Bytes per int - 1. */
#define XC_BPID2           2    /* Bytes per int / 2. */
#define XC_SPI             2    /* Shorts per int. */
#define XC_BPL             8    /* Bytes per long. */
#define XC_BPLM1           7    /* Bytes per long - 1. */
#define XC_BPLD2           4    /* Bytes per long / 2. */
#define XC_BPLD4           2    /* Bytes per long / 4. */
#define XC_IPL             2    /* Ints per long. */
#define XC_LPA             2    /* Longs per AES block. */

#define XC_AES_BLOCKSIZE    16
#define XC_AES_BLOCKROWS    4
#define XC_AES_BLOCKCOLS    (XC_AES_BLOCKSIZE/XC_AES_BLOCKROWS)
#define XC_AES128_KEYSIZE   16
#define XC_AES_KEYROWS      4
#define XC_AES128_KEYCOLS   (XC_AES128_KEYSIZE/XC_AES_KEYROWS)
#define XC_AES128_MAXROUNDS 10
#define XC_AES256_KEYSIZE   32
#define XC_AES256_KEYCOLS   (XC_AES256_KEYSIZE/XC_AES_KEYROWS)
#define XC_AES256_MAXROUNDS 14
#define XC_AES_WORDSPERBLOCK 4

#endif /* _WB_CONSTS_ */

#define XC_DES_BLOCKSIZE     8
#define XC_TDES_BLOCKSIZE    XC_DES_BLOCKSIZE
#define XC_DES_KEYSIZE       7
#define XC_2KEY_TDES_KEYSIZE (2*XC_DES_KEYSIZE)
#define XC_DES_RKEYSIZE      6
#define XC_TDES_RKEYSIZE     XC_DES_RKEYSIZE
#define XC_DES_MAXNROUNDS    16
#define XC_TDES_MAXROUNDS    48
#define XC_DES_HALFBLOCK     4
#define XC_DES_EHALFBLOCK    6

#define XC_TDES_NSBOXES 8
#define XC_TDES_GAMMA_DKBA_ROUND_SIZE 35
#define XC_TDES_GAMMA_PEMBA_SIZE 16
#define XC_TDES_GAMMA_LRBA_SIZE 16
#define XC_TDES_GAMMA_FR_SIZE 16
#define XC_TDES_GAMMA_BLOCKSIZE 16
#define XC_TDES_GAMMA_BLOCKSIZEW (XC_TDES_GAMMA_BLOCKSIZE/XC_BPI)
#define XC_TDES_GAMMA_HALFBLOCK (XC_TDES_GAMMA_BLOCKSIZE/2)
#define XC_TDES_GAMMA_HALFBLOCKW (XC_TDES_GAMMA_HALFBLOCK/XC_BPI)
#define XC_TDES_GAMMA_RKEYSIZE 8
#define XC_TDES_GAMMA_RKEYSIZEW (XC_TDES_GAMMA_RKEYSIZE/XC_BPI)

#define XC_WB_FUNCTYPE_BLOCKCIPHER 0
#define XC_WB_FUNCTYPE_ASYMMETRICCIPHER 1
#define XC_WB_FUNCTYPE_HASHFUNCTION 2
#define XC_WB_FUNCTYPE_PRNG 3

#define XC_WB_KEYLOCATION_FIXED 0
#define XC_WB_KEYLOCATION_DYNAMIC 1
#define XC_WB_KEYLOCATION_Fixed XC_WB_KEYLOCATION_FIXED
#define XC_WB_KEYLOCATION_Dynamic XC_WB_KEYLOCATION_DYNAMIC

#define XC_WB_OPERATION_ENCRYPT 0
#define XC_WB_OPERATION_DECRYPT 1
#define XC_WB_OPERATION_SIGN 2
#define XC_WB_OPERATION_VERIFY 3
#define XC_WB_OPERATION_CREATEKEY 4
#define XC_WB_OPERATION_CREATESHAREDKEY 5

#define XC_WB_OPERATION_Encrypt XC_WB_OPERATION_ENCRYPT
#define XC_WB_OPERATION_Decrypt XC_WB_OPERATION_DECRYPT
#define XC_WB_OPERATION_Sign XC_WB_OPERATION_SIGN
#define XC_WB_OPERATION_Verify XC_WB_OPERATION_VERIFY
#define XC_WB_OPERATION_CreateKey XC_WB_OPERATION_CREATEKEY
#define XC_WB_OPERATION_CreateSharedKey XC_WB_OPERATION_CREATESHAREDKEY

#define XC_WB_ALGORITHM_AES 0
#define XC_WB_ALGORITHM_DES 1
#define XC_WB_ALGORITHM_TDES 2
#define XC_WB_ALGORITHM_RSA 3
#define XC_WB_ALGORITHM_ECC 4
#define XC_WB_ALGORITHM_SHA1 5
#define XC_WB_ALGORITHM_CTR_DRBG 6
#define XC_WB_ALGORITHM_SHA224 7
#define XC_WB_ALGORITHM_SHA256 8
#define XC_WB_ALGORITHM_SHA384 9
#define XC_WB_ALGORITHM_SHA512 10

#define XC_WB_MODE_ECB  0
#define XC_WB_MODE_CBC  1
#define XC_WB_MODE_OFB  2
#define XC_WB_MODE_CTR  3

#define XC_WB_KEYTYPE_STANDARD 0
#define XC_WB_KEYTYPE_INDEPENDENT 1
#define XC_WB_KEYTYPE_Standard XC_WB_KEYTYPE_STANDARD
#define XC_WB_KEYTYPE_Independent XC_WB_KEYTYPE_INDEPENDENT

#define XC_WB_PADDING_NONE -1
#define XC_WB_PADDING_OAEP 0
#define XC_WB_PADDING_PKCS1_15_EME 1
#define XC_WB_PADDING_ZEROES 2
#define XC_WB_PADDING_PSS 3
#define XC_WB_PADDING_PSS_NO_HASH 4
#define XC_WB_PADDING_PKCS1_15_EMSA 5
#define XC_WB_PADDING_TLS 6
#define XC_WB_PADDING_Zeroes XC_WB_PADDING_ZEROES
#define XC_WB_PADDING_PSS_No_Hash XC_WB_PADDING_PSS_NO_HASH

#define XC_WB_HASHFUNCTIONCOUNT 6
#define XC_WB_HASHFUNCTION_NONE -1
#define XC_WB_HASHFUNCTION_SHA1 0
#define XC_WB_HASHFUNCTION_SHA256 1
#define XC_WB_HASHFUNCTION_SHA384 2
#define XC_WB_HASHFUNCTION_SHA512 3
#define XC_WB_HASHFUNCTION_MD5 4
#define XC_WB_HASHFUNCTION_SHA224 5
#define XC_WB_HASHFUNCTION_RUNTIME 99999

#define XC_WB_SALTLENGTH_RUNTIME 99999

#define XC_WB_SHA1_OCTET_LENGTH 20
#define XC_WB_SHA256_OCTET_LENGTH 32
#define XC_WB_SHA384_OCTET_LENGTH 48
#define XC_WB_SHA512_OCTET_LENGTH 64
#define XC_WB_MD5_OCTET_LENGTH 16
#define XC_WB_SHA224_OCTET_LENGTH 28

#define XC_WB_MASKGENERATIONFUNCTION_NONE -1
#define XC_WB_MASKGENERATIONFUNCTION_MGF1 0

#define XC_WB_KEYDERIVATIONFUNCTION_NONE    -1
#define XC_WB_KEYDERIVATIONFUNCTION_KDF_XY  0

#define XC_WB_ETYPE_BETA 0
#define XC_WB_ETYPE_GAMMA 1
#define XC_WB_ETYPE_ETA 2
#define XC_WB_ETYPE_DELTA 3
#define XC_WB_ETYPE_KAPPA 4
#define XC_WB_ETYPE_ZETA 5
#define XC_WB_ETYPE_HELIUM 6
#define XC_WB_ETYPE_LITHIUM 7
#define XC_WB_ETYPE_HYDROGEN 8
#define XC_WB_ETYPE_Beta XC_WB_ETYPE_BETA
#define XC_WB_ETYPE_Gamma XC_WB_ETYPE_GAMMA
#define XC_WB_ETYPE_Eta XC_WB_ETYPE_ETA
#define XC_WB_ETYPE_Delta XC_WB_ETYPE_DELTA
#define XC_WB_ETYPE_Kappa XC_WB_ETYPE_KAPPA
#define XC_WB_ETYPE_Zeta XC_WB_ETYPE_ZETA
#define XC_WB_ETYPE_Helium XC_WB_ETYPE_HELIUM
#define XC_WB_ETYPE_Lithium XC_WB_ETYPE_LITHIUM
#define XC_WB_ETYPE_Hydrogen XC_WB_ETYPE_HYDROGEN
#define XC_WB_ETYPE_CustomEta XC_WB_ETYPE_ETA

#define XC_WB_HTYPE_MERCURY 0
#define XC_WB_HTYPE_Mercury XC_WB_HTYPE_MERCURY

#define XC_WB_SHUFFLE_OFF 0

#define XC_WB_KEYFORMAT_PKCS_RSAPUBLIC 0
#define XC_WB_KEYFORMAT_PKCS_RSAPRIVATE 1
#define XC_WB_KEYFORMAT_PEM 2
#define XC_WB_KEYFORMAT_RAW_ECCPRIVATE 3
#define XC_WB_KEYFORMAT_RAW_ECCPUBLIC  4
#define XC_WB_KEYFORMAT_Raw_ECCPrivate  XC_WB_KEYFORMAT_RAW_ECCPRIVATE 
#define XC_WB_KEYFORMAT_Raw_ECCPublic   XC_WB_KEYFORMAT_RAW_ECCPUBLIC  

#define XC_WB_KEYFORMAT_PKCS_RSAPublic XC_WB_KEYFORMAT_PKCS_RSAPUBLIC
#define XC_WB_KEYFORMAT_PKCS_RSAPrivate XC_WB_KEYFORMAT_PKCS_RSAPRIVATE
/* Deprecated values, but kept for backward compatibility purposes */
#define XC_WB_KEYFORMAT_PKCS1_21 XC_WB_KEYFORMAT_PKCS_RSAPUBLIC
#define XC_WB_KEYFORMAT_PKCS8_12 XC_WB_KEYFORMAT_PKCS_RSAPRIVATE

#define XC_WB_CPT_NONE 0
#define XC_WB_CPT_XOR 1
#define XC_WB_CPT_Xor XC_WB_CPT_XOR
#define XC_WB_CPT_CUSTOM_ETA 2

#define XC_WB_CCT_NONE 0

#define XC_WB_CIT_NONE 0

#define XC_WB_CKT_NONE 0
#define XC_WB_CKT_EXPANDED 1
#define XC_WB_CKT_Expanded XC_WB_CKT_EXPANDED
#define XC_WB_CKT_CUSTOM_ETA 2


#define XC_WB_ELLIPTICCURVE_NONE -1
#define XC_WB_ELLIPTICCURVE_NISTP192 0
#define XC_WB_ELLIPTICCURVE_NISTP224 1
#define XC_WB_ELLIPTICCURVE_NISTP256 2
#define XC_WB_ELLIPTICCURVE_NISTP384 3
#define XC_WB_ELLIPTICCURVE_NISTP521 4
#define XC_WB_ELLIPTICCURVE_CUSTOM 5
#define XC_WB_ELLIPTICCURVE_MSP160 6
#define XC_WB_ELLIPTICCURVE_nistp192 XC_WB_ELLIPTICCURVE_NISTP192
#define XC_WB_ELLIPTICCURVE_nistp224 XC_WB_ELLIPTICCURVE_NISTP224
#define XC_WB_ELLIPTICCURVE_nistp256 XC_WB_ELLIPTICCURVE_NISTP256
#define XC_WB_ELLIPTICCURVE_nistp384 XC_WB_ELLIPTICCURVE_NISTP384
#define XC_WB_ELLIPTICCURVE_nistp521 XC_WB_ELLIPTICCURVE_NISTP521
#define XC_WB_ELLIPTICCURVE_Custom XC_WB_ELLIPTICCURVE_CUSTOM
#define XC_WB_ELLIPTICCURVE_msp160 XC_WB_ELLIPTICCURVE_MSP160 

/* plain, iv, ciphertext */
#define XC_BC_ElementSizeTypes_111 0
#define XC_BC_ElementSizeTypes_114 1
#define XC_BC_ElementSizeTypes_141 2
#define XC_BC_ElementSizeTypes_144 3
#define XC_BC_ElementSizeTypes_411 4
#define XC_BC_ElementSizeTypes_414 5
#define XC_BC_ElementSizeTypes_441 6
#define XC_BC_ElementSizeTypes_444 7

/* key */
#define XC_BC_ElementSizeTypes_1 0
#define XC_BC_ElementSizeTypes_4 1

/*back comp version history*/
#define LAST_UNSUPPORTED_AES_VERSION 1 /* We cannot read export tables of this version or less. */
#define LAST_UNSUPPORTED_RSA_VERSION 18 /* Two new fk-rsa tables added with Version 19 (Montgomery parameters r2 and nprime0).  Major change to tables for word-wise-wb-rsa with Version 18.  Version 15 RSA tables changed the way the public key is stored. */
#define LAST_UNSUPPORTED_TDES_VERSION 9 /* We had no TDES tables of this version or less. */
#define LAST_UNSUPPORTED_SHA2_VERSION 13 /* We had no SHA2 tables of this version or less. */
#define LAST_UNSUPPORTED_ECC_VERSION 17 /* Major change to tables for word-wise-wb-rsa with Version 18.  Version 15 ECC tables changed the way the public key is stored, but need to support old tables. */

#define LAST_NO_MC_VERSION 2 /* Export tables of this version or less pre-date AES+. */
#define LAST_LABEL_VERSION 8 /* Export tables of this version or less are CSS 3.x tables. */
#define LAST_NO_CUSTOM_VERSION 9 /* Export tables of this version or less do not support custom transforms. */
#define LAST_NO_TRANSFORM_VERSION 11 /* Tables between LAST_LABEL_VERSION and this version had no data transforms. */
#define LAST_UNENCRYPTED_VERSION 14 /* Export tables of this version or less did not have light-weight encryption applied to them */

#define LAST_NAIL_TRANSFORM_VERSION  16 /*Export tables of this version or less have nailed library transform, after that those transforms become auto (for diverse white box library transform purpose)*/

/* Version 18: new table formats for ww-wb-rsa and ww-wb-ecc */
/* Version 19: new tables for fk-rsa to hold montgomery parameters r2 and nprime */
#define LATEST_VERSION 19

#define NO_SWAP 0
#define SWAP 1
#define NO_MATCH 0
#define MATCH 1

/* Transform flag values */
#define ONEWISE 0
#define EIGHTWISE 1
#define WORDWISE 2

#ifdef __cplusplus
}
#endif

#endif /* _XC_WB_TYPES_H_ */

