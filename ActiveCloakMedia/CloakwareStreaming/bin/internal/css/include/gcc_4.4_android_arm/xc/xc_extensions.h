/*****************************************************************************
* Irdeto Canada Corporation                                                  *
*                                                                            *
* FILE: xc_extensions.h                                                      *
*                                                                            *
* The software and information contained in this package ("Software") is     *
* owned by Irdeto Canada Corporation, its affiliates or licensors            *
* ("Software Owners").  The Software is protected by U.S., Canadian, and     *
* international intellectual property laws and treaties and may contain      *
* patents, trademarks, copyrights, or other intellectual property rights of  *
* the Software Owners.  If you acquired this package without an appropriate  *
* agreement, please contact Irdeto Canada Corporation at:                    *
*                                                                            *
* email: support@irdeto.com                                                  *
*                                                                            *
* The furnishing of this package does not constitute a license, express or   *
* implied, by estoppel or otherwise, to any patents, trademarks, copyrights, *
* or other intellectual property rights of the Software Owners.  Use of the  *
* Software without an appropriate agreement may constitute an infringement   *
* of the Software Owners' intellectual property rights and could subject you *
* to legal action.                                                           *
******************************************************************************/

#ifndef _XC_EXTENSIONS_H_
#define _XC_EXTENSIONS_H_


/* Short-cut Aliases: */
#define xc_cfl(x) _xc_controlflowlevel(x)
#define xc_dtl(x) _xc_datatransformlevel(x)
#define xc_preserve _xc_preserve
#define xc_preservetype _xc_preservetype
#define xc_preserve_interface _xc_preserve_interface
#define xc_preserve_return _xc_preserve_return
#define xc_xform _xc_transform
#define xc_xformtype(x) _xc_transformtype(x)
#define xc_xformcast _xc_transformcast
#define xc_xformrecode _xc_transformrecode
#define xc_if _xc_protectif
#define xc_while _xc_protectwhile
#define xc_for _xc_protectfor
#define xc_q   _xc_protectquestion
#define xc_switch _xc_protectswitch
#define xc_no_init               _xc_no_implicit_init
#define xc_xform_no_init         _xc_no_implicit_init _xc_transform
#define xc_xformtype_no_init(x)  _xc_no_implicit_init _xc_transformtype(x)
#define xc_may_inline           _xc_may_inline
#define xc_inline               _xc_inline
#define xc_must_inline          _xc_must_inline
#define xc_must_inline_remove   _xc_must_inline_remove
#define xc_ondemand_inline      _xc_ondemand_inline
#define xc_inlinecalls          _xc_inlinecalls
#define xc_noinlinecalls        _xc_noinlinecalls
#define xc_st                   _xc_signature_transform
#define xc_no_st                _xc_no_signature_transform

#if !defined(XPP_ENABLED) 
/* Disable extensions smooth */
#define _xc_protect_target(x) x
#define _xc_controlflowlevel(x) 
#define _xc_datatransformlevel(x)
#define _xc_preserve
#define _xc_preservetype
#define _xc_preserve_interface
#define _xc_preserve_return
#define _xc_transform
#define _xc_transformtype(x)
#define _xc_transformcast
#define _xc_transformrecode
#define _xc_protectif if
#define _xc_protectwhile while
#define _xc_protectfor for
#define _xc_protectquestion ?
#define _xc_protectswitch switch
#define _xc_builtin(x)
#define _xc_no_implicit_init
#define _xc_may_inline
#define _xc_inline
#define _xc_must_inline
#define _xc_must_inline_remove
#define _xc_ondemand_inline
#define _xc_inlinecalls
#define _xc_noinlinecalls
#define _xc_signature_transform
#define _xc_no_signature_transform
#define _xc_functiontransform
#define _xc_functiontransformtype(x)
#define _xc_nofunctiontransform
#if !defined(XPP_SMOOTH_ENABLED) 
#define _xc_staticif(x) if(0)
#define _xc_wb_function(id)
#endif/* XPP_SMOOTH_ENABLED */
#endif /* XPP_ENABLED */

#if !defined(XPP_ENABLED) && defined(__cplusplus)

#undef _xc_transformcast
#define _xc_transformcast reinterpret_cast
#undef _xc_transformrecode
#define _xc_transformrecode static_cast

#endif /* !defined(XPP_ENABLED) && defined(__cplusplus) */

#endif /* _XC_EXTENSIONS_H_ */

