/*****************************************************************************
* Irdeto Canada Corporation                                                  *
*                                                                            *
* FILE: xc_types.h                                                           *
*                                                                            *
* The software and information contained in this package ("Software") is     *
* owned by Irdeto Canada Corporation, its affiliates or licensors            *
* ("Software Owners").  The Software is protected by U.S., Canadian, and     *
* international intellectual property laws and treaties and may contain      *
* patents, trademarks, copyrights, or other intellectual property rights of  *
* the Software Owners.  If you acquired this package without an appropriate  *
* agreement, please contact Irdeto Canada Corporation at:                    *
*                                                                            *
* email: support@irdeto.com                                                  *
*                                                                            *
* The furnishing of this package does not constitute a license, express or   *
* implied, by estoppel or otherwise, to any patents, trademarks, copyrights, *
* or other intellectual property rights of the Software Owners.  Use of the  *
* Software without an appropriate agreement may constitute an infringement   *
* of the Software Owners' intellectual property rights and could subject you *
* to legal action.                                                           *
******************************************************************************/

#ifndef __XC_TYPES_H__
#define __XC_TYPES_H__
#include "xsys/xc_platform.h"

#ifdef  XC_NO_FILE_SUPPORT
#ifndef XC_NO_FOPEN_SUPPORT
#define XC_NO_FOPEN_SUPPORT
#endif /* XC_NO_FOPEN_SUPPORT  */
#ifndef XC_NO_WFOPEN_SUPPORT
#define XC_NO_WFOPEN_SUPPORT
#endif /* XC_NO_WFOPEN_SUPPORT  */
#endif /* XC_NO_FILE_SUPPORT  */

#if !defined(XC_SUPPORT_WCHAR_T) && !defined(XC_NO_WFOPEN_SUPPORT)
#define XC_NO_WFOPEN_SUPPORT
#endif

/* cloakware data type definition */
typedef unsigned char      XC_uint8;    
typedef unsigned short     XC_uint16;    
typedef unsigned int       XC_uint32;   
typedef unsigned long      XC_ulong; 
typedef unsigned int       XC_uint;
typedef signed char        XC_int8;    
typedef signed short       XC_int16;    
typedef signed int         XC_int32;    
typedef signed long        XC_long; 
typedef signed int         XC_int;
typedef char               XC_char;
typedef signed long long   XC_int64;    
typedef unsigned long long XC_uint64;    

#ifdef XC_STDLIB_REQUIRED_PLATFORM
typedef struct _XC_FileStruct XC_FILE;
#define XC_SEEK_SET 0
#define XC_SEEK_CUR 1
#define XC_SEEK_END 2
#define XC_size_t XC_uint

#ifdef XC_SYMBIAN
#define _STDARG_H
#define _STDARG_E_H
#define __NO_CLASS_CONSTS__
#define __NORETURN__ __declspec(noreturn)
#define __NORETURN_TERMINATOR()
#define IMPORT_C __declspec(dllimport) 
#define EXPORT_C __declspec(dllexport)
#define NONSHARABLE_CLASS(x) class __declspec(notshared) x
#define NONSHARABLE_STRUCT(x) struct __declspec(notshared) x
#define __NO_THROW throw ()
#define __THROW(t) throw (t)
#define TEMPLATE_SPECIALIZATION template<>
#ifndef __int64
#define __int64  long long
#endif
#define __VALUE_IN_REGS__
#define	I64LIT(x)	x##LL
#define	UI64LIT(x)	x##ULL
#define __SOFTFP
 /*  weirdness with Symbian's C++ cross compiler */
#if defined(__cplusplus) && defined(EKA2)
#define XC_wchar_t wchar_t
#endif
#endif /* end of ifdef XC_SYMBIAN */

/* definition for XC_wchar_t */
#ifndef XC_wchar_t
#if defined(__cplusplus)
#define XC_wchar_t wchar_t
#else /* __cplusplus*/
typedef XC_uint8  wchar_t;
#define XC_wchar_t wchar_t
#endif /* end of if defined(__cplusplus) */
#endif /* end of ifndef XC_wchar_t */

#if !defined ( NULL )
	#if defined ( __cplusplus )
		#define NULL	0
	#else	/*	#if defined ( __cplusplus ) */
		#define NULL	(( void* ) 0 )
	#endif	/*	#if defined ( __cplusplus ) */
#endif	/*	#if !defined ( NULL ) */

/* end of XC_STDLIB_REQUIRED_PLATFORM */
#else
/* start for XC_STDLIB_SUPPORT_PLATFORM */
#include <stdlib.h>
#define XC_FILE FILE 
#define XC_SEEK_SET SEEK_SET
#define XC_SEEK_CUR SEEK_CUR
#define XC_SEEK_END SEEK_END

#define XC_size_t   size_t
#define XC_wchar_t  wchar_t
/* definition for XC_CHAR */
/* required for ADVILS */
#ifndef XC_CHAR
#ifdef XC_WINCE
	#define XC_CHAR XC_wchar_t
#else
	#define XC_CHAR XC_char
#endif /* end of XC_WINCE */
#endif /* end of ifndef XC_CHAR */
#endif /* end of if  XC_STDLIB_REQUIRED_PLATFORM */

#define XC_Octet  XC_uint8  
#define XC_Word   XC_uint32  
typedef struct {
	XC_long       length;    /* number of bytes in 'value'        */
	XC_Octet      *value;    /* memory address of the array       */
} XCData;

#define XC_FILE_LEN_ERR ( -1 )

#endif /* end of define __XC_TYPES_H__ */
