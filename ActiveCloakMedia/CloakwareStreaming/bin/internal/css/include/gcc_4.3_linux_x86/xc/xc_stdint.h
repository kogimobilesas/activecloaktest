/*****************************************************************************
* Irdeto Canada Corporation                                                  *
*                                                                            *
* FILE: xc_stdint.h                                                          *
*                                                                            *
* The software and information contained in this package ("Software") is     *
* owned by Irdeto Canada Corporation, its affiliates or licensors            *
* ("Software Owners").  The Software is protected by U.S., Canadian, and     *
* international intellectual property laws and treaties and may contain      *
* patents, trademarks, copyrights, or other intellectual property rights of  *
* the Software Owners.  If you acquired this package without an appropriate  *
* agreement, please contact Irdeto Canada Corporation at:                    *
*                                                                            *
* email: support@irdeto.com                                                  *
*                                                                            *
* The furnishing of this package does not constitute a license, express or   *
* implied, by estoppel or otherwise, to any patents, trademarks, copyrights, *
* or other intellectual property rights of the Software Owners.  Use of the  *
* Software without an appropriate agreement may constitute an infringement   *
* of the Software Owners' intellectual property rights and could subject you *
* to legal action.                                                           *
******************************************************************************/

/******************************************************************************
 This file contains fixed width type definitions for a target platform.
 This file must be edited for each target platform.
******************************************************************************/

#ifndef XC_STDINT_H
#define XC_STDINT_H

/* When transcoding in generic type mode, fixed width types are keywords and
   should not be defined.  #include <stdint.h> will be automatically inserted 
   into each compilation unit in the generated code. */

#if !(XC_C99_TYPES_DEFINED)
#include "stdint.h"
#endif

typedef char char_t;
typedef const char_t * string_t;

#endif /* XC_STDINT_H  */
