#ifndef _XCP_EXTENSIONS_H_
#define _XCP_EXTENSIONS_H_

#if !defined(XPP_ENABLED) 
/* Disable extensions smooth */
#define _xcp_export(x)
#define _xcp_exportdep
#define _xcp_mangle_name

#define _xcp_morpher_code(x)
#define _xcp_encoding_function_code(x)
#define _xcp_decoding_function_code(x)
#define _xcp_coeff_function_code(x)
#define _xcp_aux_coeff_function_code(x)
#define _xcp_compose_coeff_function_code(x)

#define _xcp_bp_target_function_code(x)
#define _xcp_bp_shadow_function_code(x)

/* Equivalent types for xc_types */

#define _xci_sint1_    signed char
#define _xci_sint2_    signed short
#define _xci_sint4_    signed int
#define _xci_uint1_    unsigned char
#define _xci_uint2_    unsigned short
#define _xci_uint4_    unsigned int
#define _xci_sint8_    signed long long
#define _xci_uint8_    unsigned long long
#define _xci_float4_   float
#define _xci_float8_   double
#define _xci_float12_  long double

#ifdef __cplusplus
#define _xci_bool_     bool
#else
#define _xci_bool_     char
#endif

#if __xc_riso_allow_native_integer_types__
#define _xci_string_   const signed char *
#else
#define _xci_string_   const int8_t *
#endif

#endif /* XPP_ENABLED */

#define _xcp_pragma(x) _Pragma(#x)

#endif /* _XCP_EXTENSIONS_H_ */
