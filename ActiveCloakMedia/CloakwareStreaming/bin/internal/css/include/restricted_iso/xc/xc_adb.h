/*
******************************************************************************
*                                                                            *
* Cloakware antidebug header file                                            *
*                                                                            *
* Febuary 25, 2008                                                           *
* Cloakware Corporation                                                      *
*                                                                            *
* FILE: xc_adb                                                               *
*                                                                            *
* The software and information in this package contain proprietary           *
* technology and are confidential properties of Cloakware Corporation. If    *
* you acquired this package without the appropiate agreements; please        *
* contact Cloakware Corporation at:                                          *
*                                                                            *
*        phone (613) 271-9446,  email: info@cloakware.com                    *
*                                                                            *
* This package is provided "as is" with no warranties, expressed or          *
* implied, including but not limited to any implied warranty of              *
* merchantability, fitness for a particular purpose, or freedom from         *
* infringement.                                                              *
*                                                                            *
* Cloakware Corporation may have patents or pending patent applications,     *
* trademarks, copyrights or other intellectual property rights that relate   *
* to the described subject matter. The furnishing of this package does not   *
* provide any license, expressed or implied, by estoppel or otherwise,       *
* to any such patents, trademarks, copyrights, or other intellectual         *
* property rights.                                                           *
*                                                                            *
******************************************************************************
*/

#if !defined ( _XC_ADB_H_ )
#define _XC_ADB_H_

 
#include "xc/xc_stdlib.h" 
 

#if defined ( __cplusplus )
extern "C" {
#endif /*       __cplusplus */

/* SBAD for Android */
typedef void ( *XC_CB_FUNC )( void * );
#define XC_AD_OK   	        0
#define XC_AD_DETECTED        	1
#define XC_AD_USEDID        	2
#define XC_AD_STARTED        	3
#define XC_AD_UNREGISTERED      4

XC_int XC_AD_SC_Register(XC_CB_FUNC callback, XC_int funcID);
XC_int XC_AD_Start( XC_int funcID, void * cbData);
XC_int XC_AD_Check( XC_int funcID, void * cbData);
XC_int XC_AD_End( XC_int funcID, void * cbData);

#define XC_ANTIDEBUG_FAIL       0

/* EBAD and SBAD related declarations */
    typedef void ( *XC_ANTIDEBUG_SUCCESS_PROC )( void * );
    typedef void ( *XC_ANTIDEBUG_FAILURE_PROC )( );

    XC_uint  XC_Antidebug_Start(XC_ANTIDEBUG_SUCCESS_PROC       successCallbackProc, 
                                XC_ANTIDEBUG_FAILURE_PROC       failureCallbackProc, 
                                void *   callbackData,
                                XC_uint  userReturnValue );

    XC_uint  XC_Antidebug_Check(XC_ANTIDEBUG_SUCCESS_PROC       successCallbackProc, 
                                XC_ANTIDEBUG_FAILURE_PROC       failureCallbackProc, 
                                void *   callbackData,
                                XC_uint  userReturnValue );

    XC_uint  XC_Antidebug_End(XC_ANTIDEBUG_SUCCESS_PROC       successCallbackProc, 
                              XC_ANTIDEBUG_FAILURE_PROC       failureCallbackProc, 
                              void *   callbackData,
                              XC_uint  userReturnValue );                   

/*      TBAD related declarations */

#pragma xc_riso_portable_id(XC_ADBTS_ENABLED)
#pragma xc_riso_portable_id(CW_TRACE)

#ifdef XC_ADBTS_ENABLED
/*
 * Error codes returned by XC_ADBTS
 */
#define XC_ADBTS_SUCCESS         0
#define XC_ADBTS_BAD_PARAMETER  (-1)
#define XC_ADBTS_BAD_CID        (-2)
#define XC_ADBTS_BAD_TIME       (-3)
#define XC_ADBTS_MEMORY_ERROR   (-4)

    typedef XC_size_t ( * XC_VERIFY_RESULT_PROC )( XC_size_t data );

#pragma xc_riso_portable_id(WIN64)

    typedef struct
    {
        XC_int             CID;            /*collection poXC_int ID*/
        XC_int             VID;            /*verification poXC_int ID*/
#if defined ( WIN64 )
        XC_size_t          ts;             /*      last timestamp ( 64 bits )*/
        XC_size_t          c_delta;        /*      an accumulator for this collection poXC_int */
        XC_size_t          c_delta_max;    /*      highest expected delta for this collection poXC_int (single cid) */
        XC_size_t          v_delta_max;    /*      highest expected delta for this group of collection poXC_ints (same vid) */
#else
        XC_ulong   ts;                     /*last timestamp (lo value of rdtsc) */
        XC_int             c_delta;        /*an accumulator for this collection poXC_int */
        XC_int             c_delta_max;    /*highest expected delta for this collection poXC_int (single cid) */
        XC_int             v_delta_max;    /*highest expected delta for this group of collection poXC_ints (same vid)  */
#endif
    } XC_ADBTS_DataPointStruct;

/*
 *       Initializes the current time for each record.  
 *       Returns:  
 *  XC_ADBTS_SUCCESS if successful.  
 *  XC_ADBTS_BAD_PARAMETER if bad parameter were encountered.  
 */
    xc_preserve_interface XC_int XC_ADBTS_Init(XC_ADBTS_DataPointStruct *pXC_DataPoXC_intStruct, XC_int size, XC_size_t seed );

/*
 *       Collects the current time for the given CID
 *       Returns:
 *       XC_ADBTS_SUCCESS if successful.
 *       XC_ADBTS_BAD_PARAMETER if bad parameter were encountered.
 *       XC_ADBTS_BAD_CID if a bad CID was given.        
 *       XC_ADBTS_BAD_TIME if the calculated time delta is negative.
 */
    xc_preserve_interface XC_int XC_ADBTS_Collect(XC_ADBTS_DataPointStruct *pXC_DataPoXC_intStruct, XC_int size, XC_int cid);

/*
 * Checks time deltas to ensure that they fall within range. idata is modified
 * if the check fails. If the callback is non-null, it is envoked and the return value
 * of the callback is returned.
 * WARNING: The poXC_inter value of the callback is NOT verified. This must correspond to 
 * an addressable function within user code.
 */
    xc_preserve_interface XC_size_t XC_ADBTS_Verify (XC_ADBTS_DataPointStruct *pXC_DataPoXC_intStruct, 
                                                     XC_int                    size, 
                                                     XC_size_t                 idata, 
                                                     XC_VERIFY_RESULT_PROC     proc, 
                                                     XC_int                    vid);
                                                        
                                                        

#if defined(CW_TRACE)
/*
 * This dumps the datapoXC_int array.
 */
    void XC_ADBTS_Dump(XC_ADBTS_DataPointStruct *pXC_DataPoXC_intStruct, XC_int size);

#define XC_ADBTS_DUMP(_p_) XC_ADBTS_Dump(_p_, sizeof(_p_) / sizeof(XC_ADBTS_DataPointStruct));
#else
#define XC_ADBTS_DUMP(_p_)
#endif /*CW_TRACE */

/*
 * Some usefull macros that hide some of the details
 */
#define XC_ADBTS_INIT_TS(_p_,_seed_)               XC_ADBTS_Init(_p_, sizeof(_p_) / sizeof(XC_ADBTS_DataPointStruct), _seed_);
#define XC_ADBTS_COLLECT(_p_,_cid_)                XC_ADBTS_Collect(_p_, sizeof(_p_) / sizeof(XC_ADBTS_DataPointStruct), _cid_);
#define XC_ADBTS_VERIFY(_p_,_data_,_proc_,_vid_)   XC_ADBTS_Verify(_p_, sizeof(_p_) / sizeof(XC_ADBTS_DataPointStruct), (XC_size_t)_data_,_proc_,_vid_);

#pragma xc_riso_portable_id(__SYMBIAN32__)

#ifdef __SYMBIAN32__
#define XC_ADBTS_SHUTDOWN()                        XC_ADBTS_Shutdown();
#endif

#else
#define XC_ADBTS_INIT_TS(_p_,_seed_)
#define XC_ADBTS_COLLECT(_p_,_cid_)
#define XC_ADBTS_VERIFY(_p_, _data_,_proc_,_vid_) _data_
#define XC_ADBTS_DUMP(_p_)

#endif /*ifdef XC_ADBTS_ENABLED  */
                                                        

#if defined ( __cplusplus )
}
#endif /*__cplusplus  */

#endif /*       #if !defined ( _XC_ADB_H_ )  */

