#ifndef _XC_NL_MOFN_
#define _XC_NL_MOFN_


#include "xc/xc_types.h"
#include "xc/xc_extensions.h"
#include "xc/xc_pl_usage.h"

#ifdef __cplusplus
extern "C" {
#endif

_xc_preserve_interface XC_uint32 XC_FP_MofN_Generate_Template(XC_uint8* randomSeed, XC_uint sizeRandomSeed, 
                                                             XC_uint8** templateMofN, XC_uint32* sizeTemplateMofn);
									   
_xc_preserve_interface XC_uint32 XC_FP_MofN_Generate_Table(const XC_uint8* templateMofN, XC_uint32 sizeTemplateMofn, 
                                                          const XC_uint8* systemValues, XC_uint32 sizeSystemValues,
                                                          XC_FP_Table* table);

_xc_preserve_interface XC_uint32 XC_FP_MofN_Serialize_Table(XC_FP_Table* table, XC_uint8** buf, XC_uint32* sizeBuf);

_xc_preserve_interface XC_uint32 XC_FP_MofN_DeSerialize_Table(XC_uint8* buf, XC_uint32 sizeBuf, XC_FP_Table* table);

void XC_FP_MofN_Free_Table(XC_FP_Table* table);


#ifdef __cplusplus
}
#endif

#endif
