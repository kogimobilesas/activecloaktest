/*
 * xc_pl_usage.h
 */

#ifndef _XC_PL_USAGE_H_
#define _XC_PL_USAGE_H_

#include "xc/xc_types.h"
#include "xc/xc_extensions.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Endianness conversion. */
#if defined(__BIG_ENDIAN__) || defined(_BIG_ENDIAN)
#define BYTESWAP_UINT32(x) ( (x>>24) | ((x<<8) & 0x00FF0000) | ((x>>8) & 0x0000FF00) | (x<<24) )
#else
#define BYTESWAP_UINT32(x) x
#endif

typedef XC_uint32 ParametricID;
typedef XC_int32 ParametricFunctionInput;

/* Complex structure return for secure parametric */
#define MAX_NLPARAMETRIC_LENGTH 256
struct ParametricSecureReturnStruct
{
	XC_uint32 sizeOfParametric;
	XC_uint8 data[MAX_NLPARAMETRIC_LENGTH];
};

/* Return type */
typedef XC_uint32 ParametricFastReturn;
#define ParametricFastReturnSize sizeof(XC_int32)
typedef struct ParametricSecureReturnStruct ParametricSecureReturn;
typedef XC_uint32 ParametricReturn;
#define ParametricReturnSize sizeof(XC_int32)

/* MofN table type */
typedef struct _XC_FP_Table
{
	XC_uint32 	M;
	XC_uint32 	N;
    XC_uint32   type;
	XC_uint32**	value;
} XC_FP_Table;

typedef struct 
{
    XC_uint32 elements;
    XCData  *list;
} XCDataList;

/* Error Code */
#define XC_FP_OK						    0
#define XC_FP_ERR_INVALID_PARAMETER			1
#define XC_FP_ERR_SYSTEM_VALUE_NOT_FOUND	2
#define XC_FP_ERR_SYSTEM_VALUE_FAILED		3
#define XC_FP_ERR_UNMATCHED_N				4
#define XC_FP_ERR_UNMATCHED_TABLE			5
#define XC_FP_ERR_UNKNOWN_TYPE				6
#define XC_FP_ERR_PRNG_FAIL                 7
#define XC_FP_ERR_INVALID_ID                8
#define XC_FP_ERR_FINDMODE_FAIL             9
#define XC_FP_ERR_TEMPLATE_SIZE             11
#define XC_FP_ERR_SYSTEM_VALUES_SIZE        12
#define XC_FP_ERR_TABLE_BUFFER_SIZE         13
#define XC_FP_ERR_INVALID_DIGEST_TYPE       14
#define XC_FP_ERR_INVALID_LOCK_TYPE         15
#define XC_FP_ERR_HEAP_ALLOCATION_FAILED    16
#define XC_FP_ERR_MEMORY_FAILED              17
#define XC_FP_ERR_SHA256_FAILED             18
#define XC_FP_ERR_FAIL						100

enum digestTypes
{
    OTSC_NL_HASH_TYPE_NO_FP_NO_HASH    = 0x0,
    OTSC_NL_HASH_TYPE_NO_HASH          = 0x1,
    OTSC_NL_HASH_TYPE_POC              = 0x2,
    OTSC_NL_HASH_TYPE_SHA256_USER_INIT = 0x3,
    OTSC_NL_HASH_TYPE_SHA256           = 0x4,
    OTSC_NL_HASH_TYPE_HMAC_SHA256      = 0x5
};    

enum lockTypes
{
    OTSC_NL_LOCK_TYPE_USER_INIT           = 0x0,
    OTSC_NL_LOCK_TYPE_USER_INIT_LOCK_ONLY = 0x1,
    OTSC_NL_LOCK_TYPE_POC                 = 0x2,
    OTSC_NL_LOCK_TYPE_POC_LOCK_ONLY       = 0x3,
    OTSC_NL_LOCK_TYPE_XFORM               = 0x4,
    OTSC_NL_LOCK_TYPE_XFORM_LOCK_ONLY     = 0x5
};
/*
 * Get constant system values.
 */

/* Direct access: Fast */
XC_uint32 XC_FP_Get_Constant_Fast0(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Fast1(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Fast2(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Fast3(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Fast4(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Fast5(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Fast6(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Fast7(XC_uint8** returnValue, XC_uint32* sizeReturnValue);


/* Generic access: Fast */
XC_uint32 XC_FP_Get_Constant_Fast(XC_uint32 id, XC_uint8** returnValue, XC_uint32* sizeReturnValue);

/* Direct access: Secure */
XC_uint32 XC_FP_Get_Constant_Secure0(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Secure1(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Secure2(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Secure3(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Secure4(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Secure5(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Secure6(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Secure7(XC_uint8** returnValue, XC_uint32* sizeReturnValue);

/* Generic access: Secure */
XC_uint32 XC_FP_Get_Constant_Secure(XC_uint32 id, XC_uint8** returnValue, XC_uint32* sizeReturnValue);

/* Direct access: Custom */
XC_uint32 XC_FP_Get_Constant_Custom0(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Custom1(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Custom2(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Custom3(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Custom4(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Custom5(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Custom6(XC_uint8** returnValue, XC_uint32* sizeReturnValue);
XC_uint32 XC_FP_Get_Constant_Custom7(XC_uint8** returnValue, XC_uint32* sizeReturnValue);

/* Generic access: custom */
XC_uint32 XC_FP_Get_Constant_Custom(XC_uint32 id, XC_uint8** returnValue, XC_uint32* sizeReturnValue);

/*
 * Transformation
 */

/* Direct access: Fast */
XC_uint32 XC_FP_Get_Transformation_Fast0(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Fast1(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Fast2(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Fast3(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Fast4(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Fast5(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Fast6(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Fast7(XC_uint32 inValue, XC_uint32* outValue);


/* Generic access: Fast */
XC_uint32 XC_FP_Get_Transformation_Fast(XC_uint32 id, XC_uint32 inValue, XC_uint32* outValue);

/* Direct access: Secure */
XC_uint32 XC_FP_Get_Transformation_Secure0(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Secure1(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Secure2(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Secure3(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Secure4(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Secure5(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Secure6(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Secure7(XC_uint32 inValue, XC_uint32* outValue);


/* Generic access: Secure */
XC_uint32 XC_FP_Get_Transformation_Secure(XC_uint32 id, XC_uint32 inValue, XC_uint32* outValue);

/* Direct access: Custom */
XC_uint32 XC_FP_Get_Transformation_Custom0(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Custom1(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Custom2(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Custom3(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Custom4(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Custom5(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Custom6(XC_uint32 inValue, XC_uint32* outValue);
XC_uint32 XC_FP_Get_Transformation_Custom7(XC_uint32 inValue, XC_uint32* outValue);

/* Generic access: Custom */
XC_uint32 XC_FP_Get_Transformation_Custom(XC_uint32 id, XC_uint32 inValue, XC_uint32* outValue);


/* 
 * MofN functions.
 */
XC_uint32 XC_FP_Get_System_Values_Fast(XC_uint8** systemValues, XC_uint32* sizeSystemValues);

XC_uint32 XC_FP_Get_System_Values_Secure(XC_uint8** systemValues, XC_uint32* sizeSystemValues);

XC_uint32 XC_FP_Get_System_Values_Custom(XC_uint8** systemValues, XC_uint32* sizeSystemValues);

XC_uint32 XC_FP_Get_MofN_Fast(const XC_FP_Table* table, XC_uint8** returnValue, XC_uint32* sizeReturnValue);

XC_uint32 XC_FP_Get_MofN_Secure(const XC_FP_Table* table, XC_uint8** returnValue, XC_uint32* sizeReturnValue);

XC_uint32 XC_FP_Get_MofN_Custom(const XC_FP_Table* table, XC_uint8** returnValue, XC_uint32* sizeReturnValue);


XC_uint32 XC_FP_LockData(XCDataList *assets, XCDataList *userRefs,XC_uint32 lockType);

XC_uint32 XC_FP_UnlockData(XCDataList *assets,XCDataList *userRefs,XC_uint32 lockType);

XC_uint32 XC_FP_GetNodeLockVector(XCData *vectorData, XCDataList *userSalts,XC_uint32 opType);

/*
 * Internal functons.
 */
XC_uint32 _getNLParametricParameterFast_internal(const ParametricID theID, XC_uint32* returnValue);
XC_uint32 _getNLParametricParameterSecure_internal(const ParametricID theID, XC_uint32* returnValue);
XC_uint32 _getNLParametricParameter_internal(const ParametricID theID, XC_uint32* returnValue);

/* APIs from IAC-CT Merging
 *
 */
_xc_preserve_interface XC_uint32 XC_FP_GetUniqueDeviceID(XC_uint32 salt, XC_uint8** IDdata, XC_uint32* IDlength);


#ifdef __cplusplus
}
#endif

#endif

