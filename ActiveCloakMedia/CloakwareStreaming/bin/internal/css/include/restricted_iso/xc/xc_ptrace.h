#if ! defined _XC_PTRACE_H_
#define _XC_PTRACE_H_

typedef void * XC_PT_Context;

/****
 * WARNING:  These interfaces must be transformed.  Do not preserve these APIs.  
 */
#define XC_PTRACE_ERR_IVALID_PARM       0x100001
/****
 * XC_PT_Initialize
 * 
 * Gets the transformed pointer to the ptrace function
 */
uint32_t XC_PT_Initialize(XC_PT_Context * ptrace_context);

/****
 * XC_PT_Deny_C
 *
 * Run the ptrace function
 * 
 * ctx		- Context returned by XC_PT_Initialize.  May be NULL, but this slows down the operation
 * inVal	- Input data that will be transformed and assigned to outVal
 * outVal	- Output data that is a transformed copy of inVal
 */
uint32_t XC_PT_Deny_C (  XC_PT_Context ctx,
                         _xc_transformtype(IN_VAL)  uint32_t inVal,
                         _xc_transformtype(OUT_VAL) uint32_t *outVal);


#endif

