/*****************************************************************************
* Irdeto Canada Corporation                                                  *
*                                                                            *
* FILE: xc_stdlib.h                                                          *
*                                                                            *
* The software and information contained in this package ("Software") is     *
* owned by Irdeto Canada Corporation, its affiliates or licensors            *
* ("Software Owners").  The Software is protected by U.S., Canadian, and     *
* international intellectual property laws and treaties and may contain      *
* patents, trademarks, copyrights, or other intellectual property rights of  *
* the Software Owners.  If you acquired this package without an appropriate  *
* agreement, please contact Irdeto Canada Corporation at:                    *
*                                                                            *
* email: support@irdeto.com                                                  *
*                                                                            *
* The furnishing of this package does not constitute a license, express or   *
* implied, by estoppel or otherwise, to any patents, trademarks, copyrights, *
* or other intellectual property rights of the Software Owners.  Use of the  *
* Software without an appropriate agreement may constitute an infringement   *
* of the Software Owners' intellectual property rights and could subject you *
* to legal action.                                                           *
******************************************************************************/

#ifndef __XC_STDLIB_H__
#define __XC_STDLIB_H__
#include "xc/xc_types.h"
#include "xc/xc_extensions.h"

#if defined(__cplusplus) 
extern  "C"
{
#endif	/* end of if defined ( __cplusplus ) */

#ifdef XC_STDLIB_REQUIRED_PLATFORM
/* The following functions are for White Box and RSA */
extern _xc_preserve_interface void*      XC_memcpy ( void* a_Dst, const void* a_Src, XC_size_t a_Size );
extern _xc_preserve_interface XC_int     XC_memcmp ( const void* a_Dst, const void* a_Src, XC_size_t a_Size );
extern _xc_preserve_interface void*      XC_memset ( void* a_Dst, XC_int a_Value, XC_size_t a_Size );
extern _xc_preserve_interface void*      XC_memmove ( void* a_Dst, const void* a_Src, XC_size_t a_Size );
extern _xc_preserve_interface void*      XC_malloc ( XC_size_t a_Size);
extern _xc_preserve_interface void*  	 XC_calloc ( XC_size_t a_Num, XC_size_t a_Size );
extern _xc_preserve_interface void       XC_free   ( void* a_Memblock );
extern _xc_preserve_interface XC_char*   XC_strstr ( const XC_char* a_Cs, const XC_char* a_Ct );
extern _xc_preserve_interface XC_char*   XC_strcpy ( XC_char* a_Dst, const XC_char* a_Src );
extern _xc_preserve_interface XC_char*   XC_strncpy( XC_char* a_Dst, const XC_char* a_Src, XC_size_t a_N );
extern _xc_preserve_interface XC_char*   XC_strcat ( XC_char* a_Dst, const XC_char* a_Src );
extern _xc_preserve_interface XC_char*   XC_strncat ( XC_char* a_Dst, const XC_char* a_Src, XC_size_t a_Count );
extern _xc_preserve_interface XC_int     XC_strcmp ( const XC_char* a_Dst, const XC_char* a_Src );
extern _xc_preserve_interface XC_int     XC_strncmp( const XC_char* a_Dst, const XC_char* a_Src, XC_int a_N );
extern _xc_preserve_interface XC_char*   XC_strchr ( const XC_char* a_Src, XC_int a_C );
extern _xc_preserve_interface XC_char*   XC_strrchr ( const XC_char* a_Src, XC_int a_C );
extern _xc_preserve_interface XC_size_t  XC_strlen ( const XC_char* a_Dst );
extern _xc_preserve_interface XC_char*   XC_strpbrk ( const XC_char* a_Src, const XC_char* a_Bytes);

extern _xc_preserve_interface XC_FILE*   XC_fopen  ( const XC_char* a_Filename, const XC_char* a_Mode );
extern _xc_preserve_interface XC_long    XC_ftell ( XC_FILE* stream );
extern _xc_preserve_interface void       XC_exit   ( XC_int );

#ifdef XC_SUPPORT_WCHAR_T
/* for the platform that support wchar_t */
extern XC_FILE*  _xc_preserve_interface XC_wfopen ( const XC_wchar_t* a_Filename, const XC_wchar_t* a_Mode );
#endif /* end of ifdef XC_SUPPORT_WCHAR_T */
extern XC_size_t _xc_preserve_interface XC_fread  ( void* a_Buffer, XC_size_t a_Size, XC_size_t a_Count, XC_FILE *a_Stream );
extern XC_size_t _xc_preserve_interface XC_fwrite ( void* a_Buffer, XC_size_t a_Size, XC_size_t a_Count, XC_FILE *a_Stream );
extern XC_int    _xc_preserve_interface XC_fseek  ( XC_FILE *a_Stream, XC_long a_Offset, XC_int a_Whence );
extern XC_int    _xc_preserve_interface XC_feof   ( XC_FILE *stream );
extern XC_int    _xc_preserve_interface XC_fclose ( XC_FILE *a_Stream );
extern void      _xc_preserve_interface XC_fflush ( XC_FILE *a_Stream );
extern XC_int    _xc_preserve_interface XC_fprintf ( XC_FILE* a_Stream, const XC_char * a_Fmt, ... );
extern void      _xc_preserve_interface XC_clearerr( XC_FILE *stream );
extern void      _xc_preserve_interface XC_srand(XC_uint);
extern XC_int    _xc_preserve_interface XC_rand(void);
extern XC_int    _xc_preserve_interface XC_Simple_printf( const XC_char * a_String );
extern XC_int    _xc_preserve_interface XC_printf (const XC_char * a_Fmt, ... );

/* Assert */

#if defined(XPP_ENABLED) || defined(XPP_SMOOTH_ENABLED)
#pragma xc_riso_portable_id(DEBUG)
#endif

#ifdef  DEBUG

extern void      _xc_preserve_interface XC_assert( int32_t expression );
#else

#define XC_assert(_Expression)     ((void)0)

#endif

#define XC_SEEK_SET 0
#define XC_SEEK_CUR 1
#define XC_SEEK_END 2


/* The following functions are for ADVILS */
/* end of ifdef XC_STDLIB_REQUIRED_PLATFORM  */
#else
/* for runtime standard library support platform  */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <stddef.h>
#include <assert.h>
#include <string.h>
#include <stdarg.h>

#if defined (XC_WIN32) || defined(XC_WINCE)
#include <tchar.h>
#endif

#if defined (XC_WIN32) || defined(XC_WINCE) || defined (XC_WIN64) || \
	defined (XC_LINUX) || defined(XC_SUNOS)
#include <malloc.h>
#include <memory.h> 
#if defined(_MSC_VER) && _MSC_VER >= 1400
#pragma intrinsic ( memcpy, memset, memcmp )
#endif
#endif

#if defined (XC_LINUX) || defined(XC_APPLE)
#include <strings.h>
#endif

#if !defined (XC_VXWORKS) && !defined(XC_KERNEL) 
#include <wchar.h>
#endif

/**********************************************************************
 C RunTime equivalent functions
 Define a library that provides an abstraction to C RunTime functions 
 that allow users to be both OS and user / kernel mode agnostic
-----------------------------------------------------------------------
	stdlib.h
***********************************************************************/
#ifdef __GNUC__
__attribute__((unused))
#endif
static _xc_preserve_interface void* XC_memcpy( void* a_Dst, const void* a_Src, XC_size_t a_Size )
{
    void * dest = a_Dst;
    while (a_Size--) {
        *(char*)dest = *(char *)a_Src;
        dest = (char*)dest + 1;
        a_Src = (char*)a_Src + 1;
    }
    return a_Dst;
}

#ifdef __GNUC__
__attribute__((unused))
#endif
static _xc_preserve_interface XC_int XC_memcmp( const void* a_Dst, const void* a_Src, XC_size_t a_Size )
{
    XC_uint8* buf1 = (XC_uint8*)a_Dst;
    XC_uint8* buf2 = (XC_uint8*)a_Src;
    XC_int diff = 0;

    if (a_Size) {
        while ( --a_Size && *buf1 == *buf2 ) {
            buf1++;
            buf2++;
        }
        diff = (*buf1) - (*buf2);
    }
    return diff;
}


#define XC_memset( dst, value, size ) ( memset (( dst ), ( value ), ( size )))
#define XC_rand()                     ( rand() )
#define XC_srand( val )               ( srand( val ) )
#define XC_exit( val )                ( exit( val ) )

/*****************************************************************************
	malloc.h
******************************************************************************/
#define XC_malloc(a_Size)   (malloc((a_Size)))
#define XC_free(a_Memblock) (free((a_Memblock)))
#define XC_calloc(a_Num, a_Size) (calloc((a_Num), (a_Size)))
/*****************************************************************************
	string.h
******************************************************************************/
#define XC_memmove(a_Dst, a_Src, a_Size) (memmove((a_Dst), (a_Src), (a_Size)))
#define XC_strstr(a_Cs, a_Ct) (strstr((a_Cs), (a_Ct)))
#define XC_strcpy(a_Dst, a_Src) (strcpy((a_Dst), (a_Src)))
#define XC_strncpy(a_Dst, a_Src, a_N) (strncpy((a_Dst), (a_Src), (a_N)))
#define XC_strcat(a_Dst, a_Src) (strcat((a_Dst), (a_Src)))
#define XC_strcmp(a_Dst, a_Src) (strcmp((a_Dst), (a_Src)))
#define XC_strncmp(a_Dst, a_Src, a_N) (strncmp((a_Dst), (a_Src), (a_N)))
#define XC_strchr(a_Src, a_C) (strchr((a_Src), (a_C)))
#define XC_strrchr(a_Src, a_C) (strrchr((a_Src), (a_C)))
#define XC_strlen(a_Dst) (strlen((a_Dst)))
#define XC_strncat(a_Dst, a_Src, a_Count) (strncat((a_Dst), (a_Src), (a_Count)))
#define XC_strtol( a_Ptr, a_Endptr, a_Base ) ( strtol (( a_Ptr ), ( a_Endptr ), ( a_Base )))
#define XC_strtoul( a_Ptr, a_Endptr, a_Base ) ( strtoul (( a_Ptr ), ( a_Endptr ), ( a_Base )))
#define XC_strpbrk( a_Src, a_Bytes) (strpbrk ((a_Src), (a_Bytes)))

/******************************************************************
	Wide Character Routines
******************************************************************/
#define XC_mbstowcs(a_wcstr, a_mbstr, a_Count) (mbstowcs((a_wcstr), (a_mbstr), (a_Count)))
#define XC_tcsicmp(a_String1, a_String2) (wcsicmp((a_String1), (a_String2)))
#define XC_tcsrchr(a_String, a_C) (wcsrchr((a_String), (a_C)))
#define XC_tcschr(a_String, a_C) (wcschr((a_String), (a_C)))
#define XC_tcscpy(a_Dst, a_Src) (wcscpy((a_Dst), (a_Src)))
#define XC_tcsncpy(a_Dst, a_Src, a_Count) (wcsncpy((a_Dst), (a_Src), (a_Count)))
#define XC_tcscat(a_Dst, a_Src) (wcscat((a_Dst), (a_Src)))
#define XC_tcslen(a_String) (wcslen((a_String)))
#define XC_wcstoul(a_Nptr, a_Endptr, a_Base) (wcstoul((a_Nptr), (a_Endptr), (a_Base)))
#define XC_swprintf swprintf
#define XC_Simple_printf(mess)  ( printf(mess) )
#define XC_wfopen(a_Filename, a_Mode) (_wfopen((a_Filename), (a_Mode)))
#define XC_atol(a_Dst) (atol((a_Dst)))
/* #define XC_strtoul(a_Src, a_End, a_Base) (strtoul((a_Src), (a_End), (a_Base))) */
#define XC_tolower(a_Dst) (tolower((a_Dst)))
/*************************************************************************
	stdio.h
*************************************************************************/
#define XC_sprintf sprintf
#define XC_scanf scanf
#define XC_sscanf sscanf
#define XC_fscanf fscanf
#define XC_printf printf
#define XC_vfprintf vfprintf
#define XC_fopen fopen
#define XC_fgets fgets
#define XC_fread fread
#define XC_fwrite fwrite
#define XC_ftell ftell
#define XC_fseek fseek
#define XC_putc putc
#define XC_fprintf fprintf
#define XC_fclose fclose
#define XC_fflush fflush
#define XC_feof feof
#define XC_fclearerr clearerr
#define XC_clearerr( x ) 0	
#define XC_assert assert
#define XC_getenv getenv
#define XC_time time
#define XC_tzset tzset
#define XC_localtime localtime
#define XC_mktime mktime
/*************************************************************************
	stdarg.h
*************************************************************************/
#define XC_va_list va_list
#define XC_va_start va_start
#endif /*  end of if def XC_STDLIB_REQUIRED_PLATFORM */

#define XC_wipe_mem(buf, size) \
{ \
    if (buf) { \
        volatile XC_uint8* p = (volatile XC_uint8*)((void*)(buf)); \
        XC_size_t i = size; \
        while (i--) {*p++ = 0; }\
    } \
}

#define XC_secure_free(a_Memblock, a_Size) \
    { \
       XC_wipe_mem(a_Memblock, a_Size) \
       XC_free(a_Memblock); \
    }

#define XC_pre_wipe(a_Memblock, a_Size) XC_wipe_mem(a_Memblock, a_Size)

#if defined ( __cplusplus )
}
#endif	/* end of if defined ( __cplusplus )  */
#endif	/* end of ifndef __XC_STDLIB_H__ */
