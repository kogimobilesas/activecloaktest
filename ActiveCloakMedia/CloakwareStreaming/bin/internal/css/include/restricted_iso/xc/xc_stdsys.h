/*****************************************************************************
* Irdeto Canada Corporation                                                  *
*                                                                            *
* FILE: xc_errors.h                                                          *
*                                                                            *
* The software and information contained in this package ("Software") is     *
* owned by Irdeto Canada Corporation, its affiliates or licensors            *
* ("Software Owners").  The Software is protected by U.S., Canadian, and     *
* international intellectual property laws and treaties and may contain      *
* patents, trademarks, copyrights, or other intellectual property rights of  *
* the Software Owners.  If you acquired this package without an appropriate  *
* agreement, please contact Irdeto Canada Corporation at:                    *
*                                                                            *
* email: support@irdeto.com                                                  *
*                                                                            *
* The furnishing of this package does not constitute a license, express or   *
* implied, by estoppel or otherwise, to any patents, trademarks, copyrights, *
* or other intellectual property rights of the Software Owners.  Use of the  *
* Software without an appropriate agreement may constitute an infringement   *
* of the Software Owners' intellectual property rights and could subject you *
* to legal action.                                                           *
******************************************************************************/

#ifndef _XC_STDSYS_H_ 
#define _XC_STDSYS_H_ 

#include "xc/xc_stdlib.h"


#define LOADADDR_OFFSET 12              /* ELF: padding bytes. PE: MS-DOS stub. Mach-o: file type */
#define SL_FLAG         0x49ca0856      /* The flag to indicate that the application is SL loaded */

#if defined ( __cplusplus )
extern  "C"
{
#endif	/*	#if defined ( __cplusplus ) */ 

typedef struct _LOADADDR_GLVAR
{
    XC_uint32 isSlLoaded;             
    XC_uint8 *loadAddress;            
} LOADADDR_GLVAR;

#define OFFSETSIZE 16          /* We need to change 16 bytes to writable */
                               /* So we can use 4 bytes from offset 12 */
                               /* Among the 4 bytes, 3 bytes will be offset, 1 byte is for checksum */
/* We do Redundancy Checksum to the 3 bytes offset with following start and end text */
#define STX        0x51    
#define EOT        0x3a
#define SL_MASK    0x001a89b5

#define EN_OFFSET(offset) {     \
                XC_uint32 chLRC = 0; \
                XC_uint8 *pOff = (XC_uint8 *)(void *)&offset; \
                offset ^= SL_MASK; \
                chLRC = STX ^ pOff[0] ^ pOff[1] ^ pOff[2] ^ pOff[3] ^ EOT; \
                offset ^= (chLRC<<24); \
        }
                
#define DE_OFFSET(offset) { \
                XC_uint32 chLRC; \
                XC_uint8 *pOff = (XC_uint8 *)(void *)&offset; \
                chLRC = offset; \
                chLRC >>= 24; \
                offset &= 0xffffff; \
                if (chLRC == (STX ^ pOff[0] ^ pOff[1] ^ pOff[2] ^ pOff[3] ^ EOT)) \
                        offset ^= SL_MASK; \
                else \
                        offset = 0; \
        }


void  XC_GetModuleCpuType(const XC_char* pModuleName, XC_int32 *cpu_major, XC_int32  *cpu_minor);
XC_ulong XC_STDSYS_GetTick();
XC_uint8 *XC_GetBaseAddress(const XC_char *pModuleName);

#if defined ( __cplusplus )
}
#endif	/*	#if defined ( __cplusplus ) */ 

#endif /* __XC_STDSYS_H_  */
