/*
  xc_pl_config
  Configuration for the parametric layer of node locking

  This is a generic file that works with all node
*/

#ifndef _XC_PL_CONFIG_H_
#define _XC_PL_CONFIG_H_

/* Basic include */
#include "xc_pl_usage.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Type */

    typedef XC_uint32 EntropySize;

/* Entropy size function */

    EntropySize getEntropySizeForParametricConstantFast(const ParametricID);
    EntropySize getEntropySizeForParametricConstantSecure(const ParametricID);
    EntropySize getEntropySizeForParametricConstant(const ParametricID);

    EntropySize getEntropySizeForParametricVariableFast(const ParametricID);
    EntropySize getEntropySizeForParametricVariableSecure(const ParametricID);
    EntropySize getEntropySizeForParametricVariable(const ParametricID);

    EntropySize getEntropySizeForParametricParameterFast(const ParametricID);
    EntropySize getEntropySizeForParametricParameterSecure(const ParametricID);
    EntropySize getEntropySizeForParametricParameter(const ParametricID);

#ifdef __cplusplus
}
#endif

#endif /* _XC_PL_CONFIG_H_ */
