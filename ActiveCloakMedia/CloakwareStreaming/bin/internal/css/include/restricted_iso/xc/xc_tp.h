#ifndef _XC_TP_H_
#define _XC_TP_H_

#include <xc/xc_stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

#define XC_JB_OK                    0

/* This value is returned if an invalid parameter was supplied. */
#define XC_JB_INVALID_PARAMETER     1

/* 
 * The XC_JB_Handle is invalid.
 */
#define XC_JB_INVALID_HANDLE        2

/* 
 * This value is returned by XC_JailBreak_Detect_Level_Two if XC_JailBreak_Detect_Level_One
 * has not been called successfully.
 */
#define XC_JB_LEVEL_ONE_NOT_PASSED  3

/* 
 * This value is returned by XC_JailBreak_Detect_Level_Three if XC_JailBreak_Detect_Level_Two
 * has not been called successfully.
 */
#define XC_JB_LEVEL_TWO_NOT_PASSED  4

/* This value is returned if unregular internal error occurred. */
#define XC_JB_FAILURE               101

#define XC_JB_ANDROID_SYSTEM_ERR    0x00010101
#define XC_JB_ANDROID_SSU_ERROR     0x00010102
#define XC_JB_ANDROID_WALK_ERROR    0x00010103
#define XC_JB_ANDROID_PROG_ERROR    0x00010104
#define XC_JB_ANDROID_MEM_ERR       0x00010105
#define XC_JB_ANDROID_PATH_ERR      0x00010106

#define XC_JB_IOS_MEM_ERR           0x00020001
#define XC_JB_IOS_SYSCTL_ERR        0x00020002

/* Opaque handle. */
typedef void* XC_JB_Handle;

typedef void ( *XC_JAILBREAK_CALLBACK_PROC )( void* );

typedef struct _XCJailbreakOptions
{
	XC_char * tempDir;
} XC_JailbreakOptions;

/* Initialize the jailbreak detection handle. */
 XC_int XC_JailBreak_Init(XC_JailbreakOptions options, XC_JB_Handle *handle);

/* Close the jailbreak detection handle. */
 void XC_JailBreak_Close(XC_JB_Handle handle);

/* compromised / uncompromised Callback function management */
 XC_int XC_JailBreak_Set_Compromised (XC_JB_Handle handle, XC_JAILBREAK_CALLBACK_PROC pMy_Compromised_Function, void *pMy_Callback_Data);

 XC_int XC_JailBreak_Set_Uncompromised (XC_JB_Handle handle, XC_JAILBREAK_CALLBACK_PROC pMy_Uncompromised_Function, void *pMy_Callback_Data);


/*
 * Detect if the application has been hacked. This function must be called before
 * XC_JailBreak_Detect_Level_Two and XC_JailBreak_Detect_Level_Two can be called.
 */
 XC_int XC_JailBreak_Detect_Level_One(XC_JB_Handle handle);


/*
 * Perform the initial jailbreak detection. It uses simple and less reliable techniques
 * for the detection. One must call XC_JailBreak_Detect_Level_One successfuly before
 * calling this function.
 */
 XC_int XC_JailBreak_Detect_Level_Two(XC_JB_Handle handle);

/*
 * Perform advanced jailbreak detection. One must call XC_JailBreak_Detect_Level_Two successfully
 * before calling this function.
 */
 XC_int XC_JailBreak_Detect_Level_Three(XC_JB_Handle handle);

 
/*
 * Root detection by detecting the active process ssh(d), adb and gdb
 */
 XC_int XC_JailBreak_Detect_Active_Process(XC_JB_Handle handle);

#ifdef __cplusplus
}
#endif


#endif /* _XC_TP_H_ */
