/*  This is a generic file that works with all node. */

#ifndef _XC_NODEABS_H_
#define _XC_NODEABS_H_

#include "xc/xc_types.h"
#include "xc/xc_stdlib.h"

#ifdef __cplusplus
extern "C" {
#endif

#define XC_MAX_NLASSET_LENGTH		256

typedef struct _XC_NLNodeAsset 
{
	XC_uint32	Characteristic;
	XC_uint32	Entropy;
	XC_uint32	Length;
	XC_uint8	Data[XC_MAX_NLASSET_LENGTH];
} XC_NLNodeAsset, *XC_PNLNodeAsset;

typedef XC_int32 (*XC_NODE_ASSET_RETRIEVER)(XC_uint8 *pHwinfo, XC_uint32 *pHwinfoLen);

typedef struct _XC_NODE_ASSETLIST {
	XC_uint32				idx;
	XC_NODE_ASSET_RETRIEVER	Retriever;
	XC_uint32				Characteristic;
	XC_uint8				Entropy;
	XC_uint8				Fixedlength;
} XC_NODE_ASSETLIST, *PXC_NODE_ASSETLIST;


/*  The following are masks for the Characeristic of the asset. */
#define ASSET_SPEED(n)                  ((n << 28) & 0xF0000000L)
#define ASSET_SECURITY(n)               ((n << 24) & 0x0F000000L)
#define ASSET_CHANGEABLE(n)             ((n << 20) & 0x00F00000L)
#define ASSET_UNIQUE                    0x00000004L
#define ASSET_CLASSOFHARDWARE           0x00000002L
#define ASSET_GENERATED                 0x00000001L

XC_uint32 getNLNodeConstantAssetAtRun(XC_uint32 idx, XC_uint32* length, XC_uint8 data[XC_MAX_NLASSET_LENGTH]);

/* The return error code */
#define XC_NODEABS_OK					0
#define XC_NODEABS_INVALID_PARAMETER	1
#define XC_NODEABS_FAILED_HASH			2
#define XC_NODEABS_FAILED_SOCKET		3
#define XC_NODEABS_FAILED_IOCTL			4
#define XC_NODEABS_FAILED_IOREG			5
#define XC_NODEABS_FAILED_SYSCTL		6
#define XC_NODEABS_FAILED_WMI			7
#define XC_NODEABS_FAILED_ADPADDR		8
#define XC_NODEABS_FAILED				9
#define XC_NODEABS_FAILED_NOFOUND		10

#ifdef __cplusplus
}
#endif

#endif
