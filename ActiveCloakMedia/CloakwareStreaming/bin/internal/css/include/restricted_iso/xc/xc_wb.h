/*
 * Main include-file for Cloakware WB Crypto feature.  This header
 * must be included by any application which contains WB Crypto
 * operations.
 */
#ifndef _XC_WB_H_
#define _XC_WB_H_

#include "xc/xc_extensions.h"
#include "xc/xc_errors.h"
#include "xsys/xc_wb_funcs.h"
#include "xsys/xc_wb_codegen.h"

#ifndef XC_WBCODEGEN

#ifdef XC_WB_STUB

/*************************************************************************************************
 * This section describes the Stub-build API:
 */

/*
 * All Crypto:
 **********************************************************************************************/

#ifndef XC_NO_FOPEN_SUPPORT
#define XC_Import_Data_From_File(ID, OPTIONS) \
    XC_OK
#define XC_Import_Data_From_Named_File(ID, INPUTFILE, OPTIONS) \
    XC_OK
#endif /* Xc_NO_FOPEN_SUPPORT */
#ifndef XC_NO_WFOPEN_SUPPORT
#define XC_WImport_Data_From_Named_File(ID, INPUTFILE, OPTIONS) \
    XC_OK
#endif /* XC_NO_WFOPEN_SUPPORT */
#define XC_Import_Data_From_Buffer(ID, INPUTBUFFER, INPUTLENGTH, OPTIONS) \
    XC_OK
#define XC_Free_Data_Holder(ID, OPTIONS)

/*
 * Block-Ciphers:
 **********************************************************************************************/

#define XC_Dynamic_Key_Block_Cipher_Key_Schedule(ID, KEY, KEYSIZE, PRKS, OPTIONS) \
    XC_Dynamic_Key_Block_Cipher_Key_Schedule_Stub(ID, (XC_Octet *)(void *)KEY, KEYSIZE, PRKS, OPTIONS)
#define XC_Dynamic_Key_Block_Cipher_Encrypt(ID, PLAIN, PLAINSIZE, CIPHER, PCIPHERSIZE, IV, IVSIZE, RKS, OPTIONS) \
    XC_Dynamic_Key_Block_Cipher_Encrypt_Stub(ID, (XC_Octet *)(void *)PLAIN, PLAINSIZE, (XC_Octet *)(void *)CIPHER, PCIPHERSIZE, (XC_Octet *)(void *)IV, IVSIZE, RKS, OPTIONS)
#define XC_Dynamic_Key_Block_Cipher_Decrypt(ID, CIPHER, CIPHERSIZE, PLAIN, PPLAINSIZE, IV, IVSIZE, RKS, OPTIONS) \
    XC_Dynamic_Key_Block_Cipher_Decrypt_Stub(ID, (XC_Octet *)(void *)CIPHER, CIPHERSIZE, (XC_Octet *)(void *)PLAIN, PPLAINSIZE, (XC_Octet *)(void *)IV, IVSIZE, RKS, OPTIONS)
#define XC_Dynamic_Key_Block_Cipher_Free_RKS(ID, PRKS) \
    XC_Dynamic_Key_Block_Cipher_Free_RKS_Stub(PRKS)
/* in stub mode, we simply free the space allocated to the key
   schedule and do not zeroize its contents. therefore, we do not
   need the ID parameter. */
#define XC_Fixed_Key_Block_Cipher_Encrypt(ID, PLAIN, PLAINSIZE, CIPHER, PCIPHERSIZE, IV, IVSIZE, OPTIONS) \
    XC_Fixed_Key_Block_Cipher_Encrypt_Stub(ID, (XC_Octet *)(void *)PLAIN, PLAINSIZE, (XC_Octet *)(void *)CIPHER, PCIPHERSIZE, (XC_Octet *)(void *)IV, IVSIZE, OPTIONS)
#define XC_Fixed_Key_Block_Cipher_Decrypt(ID, CIPHER, CIPHERSIZE, PLAIN, PPLAINSIZE, IV, IVSIZE, OPTIONS) \
    XC_Fixed_Key_Block_Cipher_Decrypt_Stub(ID, (XC_Octet *)(void *)CIPHER, CIPHERSIZE, (XC_Octet *)(void *)PLAIN, PPLAINSIZE, (XC_Octet *)(void *)IV, IVSIZE, OPTIONS)
#define XC_Dynamic_Key_Block_Cipher_Create_Key(ID, KEY, KEYSIZE, OPTIONS) \
    XC_Dynamic_Key_Block_Cipher_Create_Key_Stub(ID, KEY, KEYSIZE, OPTIONS)

/*
 * Asymmetric-Ciphers:
 **********************************************************************************************/

#define XC_Dynamic_Key_Asymmetric_Cipher_Encrypt(ID, KEY, PLAIN, PLAINSIZE, CIPHER, PCIPHERSIZE, OPTIONS) \
    XC_Dynamic_Key_Asymmetric_Cipher_Encrypt_Stub(ID, KEY, (XC_Octet *)(void *)PLAIN, PLAINSIZE, (XC_Octet *)(void *)CIPHER, PCIPHERSIZE, OPTIONS)
#define XC_Dynamic_Key_Asymmetric_Cipher_Decrypt(ID, KEY, CIPHER, CIPHERSIZE, PLAIN, PPLAINSIZE, OPTIONS) \
    XC_Dynamic_Key_Asymmetric_Cipher_Decrypt_Stub(ID, KEY, (XC_Octet *)(void *)CIPHER, CIPHERSIZE, (XC_Octet *)(void *)PLAIN, PPLAINSIZE, OPTIONS)
#define XC_Dynamic_Key_Asymmetric_Cipher_Sign(ID, KEY, MESSAGE, MESSAGESIZE, SIGNATURE, PSIGNATURESIZE, OPTIONS) \
    XC_Dynamic_Key_Asymmetric_Cipher_Sign_Stub(ID, KEY, (XC_Octet *)(void *)MESSAGE, MESSAGESIZE, (XC_Octet *)(void *)SIGNATURE, PSIGNATURESIZE, OPTIONS)
#define XC_Dynamic_Key_Asymmetric_Cipher_Verify(ID, KEY, MESSAGE, MESSAGESIZE, SIGNATURE, SIGNATURESIZE, OPTIONS) \
    XC_Dynamic_Key_Asymmetric_Cipher_Verify_Stub(ID, KEY, (XC_Octet *)(void *)MESSAGE, MESSAGESIZE, (XC_Octet *)(void *)SIGNATURE, SIGNATURESIZE, OPTIONS)
#define XC_Fixed_Key_Asymmetric_Cipher_Encrypt(ID, PLAIN, PLAINSIZE, CIPHER, PCIPHERSIZE, OPTIONS) \
    XC_Fixed_Key_Asymmetric_Cipher_Encrypt_Stub(ID, (XC_Octet *)(void *)PLAIN, PLAINSIZE, (XC_Octet *)(void *)CIPHER, PCIPHERSIZE, OPTIONS)
#define XC_Fixed_Key_Asymmetric_Cipher_Decrypt(ID, CIPHER, CIPHERSIZE, PLAIN, PPLAINSIZE, OPTIONS) \
    XC_Fixed_Key_Asymmetric_Cipher_Decrypt_Stub(ID, (XC_Octet *)(void *)CIPHER, CIPHERSIZE, (XC_Octet *)(void *)PLAIN, PPLAINSIZE, OPTIONS)
#define XC_Fixed_Key_Asymmetric_Cipher_Sign(ID, MESSAGE, MESSAGESIZE, SIGNATURE, PSIGNATURESIZE, OPTIONS) \
    XC_Fixed_Key_Asymmetric_Cipher_Sign_Stub(ID, (XC_Octet *)(void *)MESSAGE, MESSAGESIZE, (XC_Octet *)(void *)SIGNATURE, PSIGNATURESIZE, OPTIONS)
#define XC_Fixed_Key_Asymmetric_Cipher_Verify(ID, MESSAGE, MESSAGESIZE, SIGNATURE, SIGNATURESIZE, OPTIONS) \
    XC_Fixed_Key_Asymmetric_Cipher_Verify_Stub(ID, (XC_Octet *)(void *)MESSAGE, MESSAGESIZE, (XC_Octet *)(void *)SIGNATURE, SIGNATURESIZE, OPTIONS)
#define XC_Dynamic_Key_Asymmetric_Cipher_Load_Public_Key(ID, INPUTBUFFER, INPUTLENGTH, PKEY, OPTIONS) \
    XC_Dynamic_Key_Asymmetric_Cipher_Load_Public_Key_From_Buffer_Stub(ID, (XC_Octet *)(void *)INPUTBUFFER, INPUTLENGTH, PKEY, OPTIONS)
#define XC_Dynamic_Key_Asymmetric_Cipher_Free_Public_Key(ID, PKEY) \
    XC_Dynamic_Key_Asymmetric_Cipher_Free_Public_Key_Stub(PKEY)
#define XC_Dynamic_Key_Asymmetric_Cipher_Load_Private_Key(ID, INPUTBUFFER, INPUTLENGTH, PKEY, OPTIONS) \
    XC_Dynamic_Key_Asymmetric_Cipher_Load_Private_Key_From_Buffer_Stub(ID, (XC_Octet *)(void *)INPUTBUFFER, INPUTLENGTH, PKEY, OPTIONS)
#define XC_Dynamic_Key_Asymmetric_Cipher_Free_Private_Key(ID, PKEY) \
    XC_Dynamic_Key_Asymmetric_Cipher_Free_Private_Key_Stub(PKEY)
#define XC_Dynamic_Key_Asymmetric_Cipher_Create_Key_Pair(ID, PRIKEY, PRIKEYSIZE, PUBKEY, PUBKEYSIZE, OPTIONS) \
    XC_Dynamic_Key_Asymmetric_Cipher_Create_Key_Pair_Stub(ID, (XC_Octet *)(void *)PRIKEY, PRIKEYSIZE, (XC_Octet *)(void *)PUBKEY, PUBKEYSIZE, OPTIONS)

#define XC_Dynamic_Key_Asymmetric_Cipher_Create_Shared_Key(ID, SHAREDKEY, PSHAREDKEYSIZE, OTHERKEY, OWNKEY, OPTIONS) \
    XC_Dynamic_Key_Asymmetric_Cipher_Create_Shared_Key_Stub(ID, (XC_Octet *)(void *)SHAREDKEY, PSHAREDKEYSIZE, OTHERKEY, OWNKEY, OPTIONS)
    
#define XC_Fixed_Key_Asymmetric_Cipher_Create_Shared_Key(ID, SHAREDKEY, PSHAREDKEYSIZE, OTHERKEY,  OPTIONS) \
    XC_Fixed_Key_Asymmetric_Cipher_Create_Shared_Key_Stub(ID, (XC_Octet *)(void *)SHAREDKEY, PSHAREDKEYSIZE, OTHERKEY, OPTIONS)    
    
/*
 * Hash-Functions:
 **********************************************************************************************/

#define XC_Hash_Initialize(ID, PCONTEXT, OPTIONS) \
    XC_Hash_Initialize_Stub(ID, PCONTEXT, OPTIONS)
#define XC_Hash_Update(ID, CONTEXT, MESSAGE, MESSAGESIZE, OPTIONS) \
    XC_Hash_Update_Stub(ID, CONTEXT, (XC_Octet*)(void*)MESSAGE, MESSAGESIZE, OPTIONS)
#define XC_Hash_Calculate_Digest(ID, CONTEXT, DIGEST, PDIGESTSIZE, OPTIONS) \
    XC_Hash_Calculate_Digest_Stub(ID, CONTEXT, (XC_Octet*)(void*)DIGEST, PDIGESTSIZE, OPTIONS)
#define XC_Hash_Free_Context(ID,PCONTEXT) \
    XC_Hash_Free_Context_Stub(ID,PCONTEXT)

/*
 * PRNGs:
 *********************************************************************************************/

#define XC_PRNG_Initialize(ID, ENTROPYFUNCTION, ENTROPYFUNCTIONSTRENGTH, PSTATE, OPTIONS) \
    XC_PRNG_Initialize_Stub(ID, ENTROPYFUNCTION, ENTROPYFUNCTIONSTRENGTH, PSTATE, OPTIONS)
#define XC_PRNG_Reseed(ID, ENTROPYFUNCTION, ENTROPYFUNCTIONSTRENGTH, ADDITIONAL, ADDITIONALSIZE, STATE, OPTIONS) \
    XC_PRNG_Reseed_Stub(ID, ENTROPYFUNCTION, ENTROPYFUNCTIONSTRENGTH, ADDITIONAL, ADDITIONALSIZE, STATE, OPTIONS)
#define XC_PRNG_Generate(ID, RANDOMOUTPUTSIZE, ENTROPYFUNCTION, ENTROPYFUNCTIONSTRENGTH, ADDITIONAL, ADDITIONALSIZE, \
        STATE, RANDOMOUTPUT, OPTIONS) \
    XC_PRNG_Generate_Stub(ID, RANDOMOUTPUTSIZE, ENTROPYFUNCTION, ENTROPYFUNCTIONSTRENGTH, ADDITIONAL, ADDITIONALSIZE, \
        STATE, RANDOMOUTPUT, OPTIONS)
#define XC_PRNG_Finalize(ID, PSTATE) \
    XC_PRNG_Finalize_Stub(ID, PSTATE)

#else /* not XC_WB_STUB */

/*************************************************************************************************
 * Smooth or Robust build (these are identical at macro-expansion level)
 */

/*
 * All Crypto:
 **********************************************************************************************/

#ifndef XC_NO_FOPEN_SUPPORT
#define XC_Import_Data_From_File(ID, OPTIONS) \
    cat(XC_Import_Data_From_File_,ID) (OPTIONS)
#define XC_Import_Data_From_Named_File(ID, INPUTFILE, OPTIONS) \
    cat(XC_Import_Data_From_Named_File_,ID) (INPUTFILE, OPTIONS)
#endif /* XC_NO_FOPEN_SUPPORT */
#ifndef XC_NO_WFOPEN_SUPPORT
#define XC_WImport_Data_From_Named_File(ID, INPUTFILE, OPTIONS) \
    cat(XC_WImport_Data_From_NamedFile_,ID) (INPUTFILE, OPTIONS)
#endif /* XC_NO_WFOPEN_SUPPORT */
#define XC_Import_Data_From_Buffer(ID, INPUTBUFFER, INPUTLENGTH, OPTIONS) \
    cat(XC_Import_Data_From_Buffer_,ID) (INPUTBUFFER, INPUTLENGTH, OPTIONS)
#define XC_Free_Data_Holder(ID, POPTIONS) \
    cat(XC_Free_Data_Holder_,ID) (POPTIONS)

/*
 * Block-Ciphers:
 **********************************************************************************************/

#define XC_Dynamic_Key_Block_Cipher_Key_Schedule(ID, KEY, KEYSIZE, PRKS, OPTIONS) \
    cat(XC_Dynamic_Key_Block_Cipher_Key_Schedule_,ID) (KEY, KEYSIZE, PRKS, OPTIONS)
#define XC_Dynamic_Key_Block_Cipher_Encrypt(ID, PLAIN, PLAINSIZE, CIPHER, PCIPHERSIZE, IV, IVSIZE, RKS, OPTIONS) \
    cat(XC_Dynamic_Key_Block_Cipher_Encrypt_,ID) (PLAIN, PLAINSIZE, CIPHER, PCIPHERSIZE, IV, IVSIZE, RKS, OPTIONS)
#define XC_Dynamic_Key_Block_Cipher_Decrypt(ID, CIPHER, CIPHERSIZE, PLAIN, PPLAINSIZE, IV, IVSIZE, RKS, OPTIONS) \
    cat(XC_Dynamic_Key_Block_Cipher_Decrypt_,ID) (CIPHER, CIPHERSIZE, PLAIN, PPLAINSIZE, IV, IVSIZE, RKS, OPTIONS)
#define XC_Dynamic_Key_Block_Cipher_Free_RKS(ID, PRKS) \
    cat(XC_Dynamic_Key_Block_Cipher_Free_RKS_,ID) (PRKS)
#define XC_Fixed_Key_Block_Cipher_Encrypt(ID, PLAIN, PLAINSIZE, CIPHER, PCIPHERSIZE, IV, IVSIZE, OPTIONS) \
    cat(XC_Fixed_Key_Block_Cipher_Encrypt_,ID) (PLAIN, PLAINSIZE, CIPHER, PCIPHERSIZE, IV, IVSIZE, OPTIONS)
#define XC_Fixed_Key_Block_Cipher_Decrypt(ID, CIPHER, CIPHERSIZE, PLAIN, PPLAINSIZE, IV, IVSIZE, OPTIONS) \
    cat(XC_Fixed_Key_Block_Cipher_Decrypt_,ID) (CIPHER, CIPHERSIZE, PLAIN, PPLAINSIZE, IV, IVSIZE, OPTIONS)
#define XC_Dynamic_Key_Block_Cipher_Create_Key(ID, KEY, KEYSIZE, OPTIONS) \
    cat(XC_Dynamic_Key_Block_Cipher_Create_Key_, ID) ( KEY, KEYSIZE, OPTIONS)

/*
 * Asymmetric-Ciphers:
 **********************************************************************************************/

#define XC_Dynamic_Key_Asymmetric_Cipher_Encrypt(ID, KEY, PLAIN, PLAINSIZE, CIPHER, PCIPHERSIZE, OPTIONS) \
    cat(XC_Dynamic_Key_Asymmetric_Cipher_Encrypt_,ID) (KEY, PLAIN, PLAINSIZE, CIPHER, PCIPHERSIZE, OPTIONS)
#define XC_Dynamic_Key_Asymmetric_Cipher_Decrypt(ID, KEY, CIPHER, CIPHERSIZE, PLAIN, PPLAINSIZE, OPTIONS) \
    cat(XC_Dynamic_Key_Asymmetric_Cipher_Decrypt_,ID) (KEY, CIPHER, CIPHERSIZE, PLAIN, PPLAINSIZE, OPTIONS)
#define XC_Dynamic_Key_Asymmetric_Cipher_Sign(ID, KEY, MESSAGE, MESSAGESIZE, SIGNATURE, PSIGNATURESIZE, OPTIONS) \
    cat(XC_Dynamic_Key_Asymmetric_Cipher_Sign_,ID) (KEY, MESSAGE, MESSAGESIZE, SIGNATURE, PSIGNATURESIZE, OPTIONS)
#define XC_Dynamic_Key_Asymmetric_Cipher_Verify(ID, KEY, MESSAGE, MESSAGESIZE, SIGNATURE, SIGNATURESIZE, OPTIONS) \
    cat(XC_Dynamic_Key_Asymmetric_Cipher_Verify_,ID) (KEY, MESSAGE, MESSAGESIZE, SIGNATURE, SIGNATURESIZE, OPTIONS)
#define XC_Fixed_Key_Asymmetric_Cipher_Encrypt(ID, PLAIN, PLAINSIZE, CIPHER, PCIPHERSIZE, OPTIONS) \
    cat(XC_Fixed_Key_Asymmetric_Cipher_Encrypt_,ID) (PLAIN, PLAINSIZE, CIPHER, PCIPHERSIZE, OPTIONS)
#define XC_Fixed_Key_Asymmetric_Cipher_Decrypt(ID, CIPHER, CIPHERSIZE, PLAIN, PPLAINSIZE, OPTIONS) \
    cat(XC_Fixed_Key_Asymmetric_Cipher_Decrypt_,ID) (CIPHER, CIPHERSIZE, PLAIN, PPLAINSIZE, OPTIONS)
#define XC_Fixed_Key_Asymmetric_Cipher_Sign(ID, MESSAGE, MESSAGESIZE, SIGNATURE, PSIGNATURESIZE, OPTIONS) \
    cat(XC_Fixed_Key_Asymmetric_Cipher_Sign_,ID) (MESSAGE, MESSAGESIZE, SIGNATURE, PSIGNATURESIZE, OPTIONS)
#define XC_Fixed_Key_Asymmetric_Cipher_Verify(ID, MESSAGE, MESSAGESIZE, SIGNATURE, SIGNATURESIZE, OPTIONS) \
    cat(XC_Fixed_Key_Asymmetric_Cipher_Verify_,ID) (MESSAGE, MESSAGESIZE, SIGNATURE, SIGNATURESIZE, OPTIONS)
#define XC_Dynamic_Key_Asymmetric_Cipher_Load_Public_Key(ID, INPUTBUFFER, INPUTLENGTH, PKEY, OPTIONS) \
    cat(XC_Dynamic_Key_Asymmetric_Cipher_Load_Public_Key_,ID) (INPUTBUFFER, INPUTLENGTH, PKEY, OPTIONS)
#define XC_Dynamic_Key_Asymmetric_Cipher_Free_Public_Key(ID, PKEY) \
    cat(XC_Dynamic_Key_Asymmetric_Cipher_Free_Public_Key_,ID) (PKEY)
#define XC_Dynamic_Key_Asymmetric_Cipher_Load_Private_Key(ID, INPUTBUFFER, INPUTLENGTH, PKEY, OPTIONS) \
    cat(XC_Dynamic_Key_Asymmetric_Cipher_Load_Private_Key_,ID) (INPUTBUFFER, INPUTLENGTH, PKEY, OPTIONS)
#define XC_Dynamic_Key_Asymmetric_Cipher_Free_Private_Key(ID, PKEY) \
    cat(XC_Dynamic_Key_Asymmetric_Cipher_Free_Private_Key_,ID) (PKEY)
#define XC_Dynamic_Key_Asymmetric_Cipher_Create_Key_Pair(ID, PRIKEY, PRIKEYSIZE, PUBKEY, PUBKEYSIZE, OPTIONS) \
    cat(XC_Dynamic_Key_Asymmetric_Cipher_Create_Key_Pair_,ID)(PRIKEY, PRIKEYSIZE, PUBKEY, PUBKEYSIZE, OPTIONS)

#define XC_Dynamic_Key_Asymmetric_Cipher_Create_Shared_Key(ID, SHAREDKEY, PSHAREDKEYSIZE, OTHERKEY, OWNKEY, OPTIONS) \
    cat(XC_Dynamic_Key_Asymmetric_Cipher_Create_Shared_Key_,ID)(SHAREDKEY, PSHAREDKEYSIZE, OTHERKEY, OWNKEY, OPTIONS)

/* not support yet    
#define XC_Fixed_Key_Asymmetric_Cipher_Create_Shared_Key(ID, SHAREDKEY, PSHAREDKEYSIZE, OTHERKEY,  OPTIONS) \
    cat(XC_Fixed_Key_Asymmetric_Cipher_Create_Shared_Key_,ID)( SHAREDKEY, PSHAREDKEYSIZE, OTHERKEY, OPTIONS) 
*/
    
/*
 * Hash-Functions:
 **********************************************************************************************/

#define XC_Hash_Initialize(ID, PCONTEXT, OPTIONS) \
    cat(XC_Hash_Initialize_,ID)(PCONTEXT, OPTIONS)
#define XC_Hash_Update(ID, CONTEXT, MESSAGE, MESSAGESIZE, OPTIONS) \
    cat(XC_Hash_Update_,ID)(CONTEXT, MESSAGE, MESSAGESIZE, OPTIONS)
#define XC_Hash_Calculate_Digest(ID, CONTEXT, DIGEST, PDIGESTSIZE, OPTIONS) \
    cat(XC_Hash_Calculate_Digest_,ID)(CONTEXT, DIGEST, PDIGESTSIZE, OPTIONS)
#define XC_Hash_Free_Context(ID,PCONTEXT) \
    cat(XC_Hash_Free_Context_,ID)(PCONTEXT)

/*
 * PRNGs:
 *********************************************************************************************/

#define XC_PRNG_Initialize(ID, ENTROPYFUNCTION, ENTROPYFUNCTIONSTRENGTH, PSTATE, OPTIONS) \
    cat(XC_PRNG_Initialize_,ID)(ENTROPYFUNCTION, ENTROPYFUNCTIONSTRENGTH, PSTATE, OPTIONS)
#define XC_PRNG_Reseed(ID, ENTROPYFUNCTION, ENTROPYFUNCTIONSTRENGTH, ADDITIONAL, ADDITIONALSIZE, STATE, OPTIONS) \
    cat(XC_PRNG_Reseed_,ID)(ENTROPYFUNCTION, ENTROPYFUNCTIONSTRENGTH, ADDITIONAL, ADDITIONALSIZE, STATE, OPTIONS)
#define XC_PRNG_Generate(ID, RANDOMOUTPUTSIZE, ENTROPYFUNCTION, ENTROPYFUNCTIONSTRENGTH, ADDITIONAL, ADDITIONALSIZE, \
        STATE, RANDOMOUTPUT, OPTIONS) \
    cat(XC_PRNG_Generate_,ID)(RANDOMOUTPUTSIZE, ENTROPYFUNCTION, ENTROPYFUNCTIONSTRENGTH, ADDITIONAL, ADDITIONALSIZE, \
        STATE, RANDOMOUTPUT, OPTIONS)
#define XC_PRNG_Finalize(ID, PSTATE) \
    cat(XC_PRNG_Finalize_,ID)(PSTATE)

#endif /* XC_WB_STUB */

#endif /* XC_WBCODEGEN */

#endif /* _XC_WB_H_ */

