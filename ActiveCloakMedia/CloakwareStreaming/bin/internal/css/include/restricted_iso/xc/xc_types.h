/*****************************************************************************
* Irdeto Canada Corporation                                                  *
*                                                                            *
* FILE: xc_types.h                                                           *
*                                                                            *
* The software and information contained in this package ("Software") is     *
* owned by Irdeto Canada Corporation, its affiliates or licensors            *
* ("Software Owners").  The Software is protected by U.S., Canadian, and     *
* international intellectual property laws and treaties and may contain      *
* patents, trademarks, copyrights, or other intellectual property rights of  *
* the Software Owners.  If you acquired this package without an appropriate  *
* agreement, please contact Irdeto Canada Corporation at:                    *
*                                                                            *
* email: support@irdeto.com                                                  *
*                                                                            *
* The furnishing of this package does not constitute a license, express or   *
* implied, by estoppel or otherwise, to any patents, trademarks, copyrights, *
* or other intellectual property rights of the Software Owners.  Use of the  *
* Software without an appropriate agreement may constitute an infringement   *
* of the Software Owners' intellectual property rights and could subject you *
* to legal action.                                                           *
******************************************************************************/

#ifndef __XC_TYPES_H__
#define __XC_TYPES_H__
#include "xsys/xc_platform.h"

#ifdef  XC_NO_FILE_SUPPORT
#ifndef XC_NO_FOPEN_SUPPORT
#define XC_NO_FOPEN_SUPPORT
#endif /* XC_NO_FOPEN_SUPPORT  */
#ifndef XC_NO_WFOPEN_SUPPORT
#define XC_NO_WFOPEN_SUPPORT
#endif /* XC_NO_WFOPEN_SUPPORT  */
#endif /* XC_NO_FILE_SUPPORT  */

#if !defined(XC_SUPPORT_WCHAR_T) && !defined(XC_NO_WFOPEN_SUPPORT)
#define XC_NO_WFOPEN_SUPPORT
#endif

/* cloakware data type definition */
#include <xc/xc_stdint.h>
typedef uint8_t        XC_uint8;
typedef uint16_t       XC_uint16;
typedef uint32_t       XC_uint32;
typedef uint32_t       XC_ulong;
typedef uint32_t       XC_uint;
typedef int8_t         XC_int8;
typedef int16_t        XC_int16;
typedef int32_t        XC_int32;
typedef int32_t        XC_long;
typedef int32_t        XC_int;
typedef int8_t         XC_char;
typedef uint64_t       XC_uint64;
typedef int64_t        XC_int64;

#ifdef XC_STDLIB_REQUIRED_PLATFORM
typedef struct _XC_FileStruct XC_FILE;
#define XC_SEEK_SET 0
#define XC_SEEK_CUR 1
#define XC_SEEK_END 2
#define XC_size_t XC_uint

#if !defined ( NULL )
	#if defined ( __cplusplus )
		#define NULL	0
	#else	/*	#if defined ( __cplusplus ) */
		#define NULL	(( void* ) 0 )
	#endif	/*	#if defined ( __cplusplus ) */
#endif	/*	#if !defined ( NULL ) */

/* end of XC_STDLIB_REQUIRED_PLATFORM */
#else
/* start for XC_STDLIB_SUPPORT_PLATFORM */
#include <stdlib.h>
#define XC_FILE FILE 
#define XC_SEEK_SET SEEK_SET
#define XC_SEEK_CUR SEEK_CUR
#define XC_SEEK_END SEEK_END

#define XC_size_t   size_t
#define XC_wchar_t  wchar_t
/* definition for XC_CHAR */
/* required for ADVILS */
#ifndef XC_CHAR
#ifdef XC_WINCE
	#define XC_CHAR XC_wchar_t
#else
	#define XC_CHAR XC_char
#endif /* end of XC_WINCE */
#endif /* end of ifndef XC_CHAR */
#endif /* end of if  XC_STDLIB_REQUIRED_PLATFORM */

#define XC_Octet  XC_uint8  
#define XC_Word   XC_uint32  
typedef struct {
	XC_long       length;    /* number of bytes in 'value'        */
	XC_Octet      *value;    /* memory address of the array       */
} XCData;

#define XC_FILE_LEN_ERR ( -1 )

#endif /* end of define __XC_TYPES_H__ */
