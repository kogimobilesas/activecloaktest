/*
 ******************************************************************************
 *                                                                            *
 *                          Irdeto Canada Corporation                         *
 *                                                                            *
 * FILE:  xc_pfiv.h                                                           *
 *                                                                            *
 * The software and information contained in this package (Software) is owned *
 * by  Irdeto  Canada  Corporation, its  affiliates  or  licensors  (Software *
 * Owners).  The Software is  protected by U.S., Canadian,  and international *
 * intellectual   property  laws  and  treaties   and  may  contain  patents, *
 * trademarks,  copyrights,  or  other  intellectual  property rights  of the *
 * Software  Owners.  If  you  acquired this  package without  an appropriate *
 * agreement, please contact Irdeto Canada Corporation at:                    *
 *                                                                            *
 * phone +1 613 271 9446, email: info@irdeto.com                              *
 *                                                                            *
 * The furnishing of this  package does not constitute  a license, express or *
 * implied, by estoppel or otherwise, to any patents, trademarks, copyrights, *
 * or other intellectual property rights of  the Software Owners.  Use of the *
 * Software without  an appropriate agreement may constitute  an infringement *
 * of the Software Owners' intellectual property rights and could subject you *
 * to legal action.                                                           *
 *                                                                            *
 ******************************************************************************
*/

#ifndef _XC_PFIV_H_
#define _XC_PFIV_H_

#include "xc/xc_stdlib.h"
#include "xc/xc_errors.h"

#ifdef __cplusplus
extern "C" {
#endif

#define XC_PFIV_ERROR_FIRST                     XC_PFIV_ERROR_BASE + 1   /* first error number in use.                                                          */
/* Return Error codes */
#define XC_PFIV_ERR_INVALID_HANDLE              XC_PFIV_ERROR_BASE + 1   /* Invalid module handle.                                                              */
#define XC_PFIV_ERR_FAILED_LOAD_VOUCHER         XC_PFIV_ERROR_BASE + 2   /* Failed to open or read voucher data from the voucher file.                          */
#define XC_PFIV_ERR_INVALID_RAW_FILE            XC_PFIV_ERROR_BASE + 3   /* Failed to open or read data from the raw file.                                      */
#define XC_PFIV_ERR_FAILED_CREATE_MEMORY        XC_PFIV_ERROR_BASE + 4   /* Memory error encountered.                                                           */
#define XC_PFIV_ERR_INVALID_MODULE              XC_PFIV_ERROR_BASE + 5   /* Invalid module name, no voucher data for the module, unloaded module.               */
#define XC_PFIV_ERR_NO_VOUCHER_DATA             XC_PFIV_ERROR_BASE + 6   /* No voucher data is in memory. (Always load voucher file before any other API calls.)*/
#define XC_PFIV_ERR_NO_MODE_SET                 XC_PFIV_ERROR_BASE + 7   /* The mode has not be set.                                                            */
#define XC_PFIV_ERR_INVALID_MODE                XC_PFIV_ERROR_BASE + 8   /* Require liner mode for Mem_Range IV or The mode is neither linear nor stride.       */
#define XC_PFIV_ERR_NO_LEVEL_SET                XC_PFIV_ERROR_BASE + 9   /* The level has not been set.                                                         */
#define XC_PFIV_ERR_INVALID_ADDRESS             XC_PFIV_ERROR_BASE + 10  /* The address of the memory area is invalid.                                          */
#define XC_PFIV_ERR_INVALID_LENGTH              XC_PFIV_ERROR_BASE + 11  /* The length of the memory area is invalid.                                           */
#define XC_PFIV_ERR_INVALID_HASH_METHOD         XC_PFIV_ERROR_BASE + 12  /* Hash method not found or can't initialize the hash method.                          */
#define XC_PFIV_ERR_CREATE_HANDLE_ERROR         XC_PFIV_ERROR_BASE + 13  /* Can't create module handle.                                                         */
#define XC_PFIV_ERR_INVALID_MODE_SRC_TYPE       XC_PFIV_ERROR_BASE + 14  /* Signature for specified mode and type not found.                                    */
#define XC_PFIV_ERR_FAILED_HASH_SIGNATURE       XC_PFIV_ERROR_BASE + 15  /* Failed to hash the signature.                                                       */
#define XC_PFIV_ERR_FAILED_UNLOAD_VOUCHER       XC_PFIV_ERROR_BASE + 16  /* Failed to unload the voucher data.                                                  */
#define XC_PFIV_ERR_INVALID_FUNC_ADDRESS        XC_PFIV_ERROR_BASE + 17  /* invalid function address. or the module for ths func has not been loaded.           */
#define XC_PFIV_ERR_INVALID_OPERATION           XC_PFIV_ERROR_BASE + 18  /* invalid operation on the function module handle.                                    */
#define XC_PFIV_ERR_INVALID_PARAMETER           XC_PFIV_ERROR_BASE + 19  /* invalid parameter. the pointer of the parameter is NULL.                            */
#define XC_PFIV_ERR_INVALID_DATABOX_ID          XC_PFIV_ERROR_BASE + 20  /* databox id is invalid or no voucher data has been loaded.                           */
#define XC_PFIV_ERR_VOUCHER_BUSY                XC_PFIV_ERROR_BASE + 21  /* a voucher data is in used and can't be closed.                                      */
#define XC_PFIV_ERR_INVALID_VOUCHER_HANDLE      XC_PFIV_ERROR_BASE + 22  /* invalid voucher handle.                                                             */
#define XC_PFIV_ERR_NO_FUN_SET                  XC_PFIV_ERROR_BASE + 23  /* no target function specified for secure call.                                       */
#define XC_PFIV_ERR_NO_FUN_ARG                  XC_PFIV_ERROR_BASE + 24  /* no parameter specified for target function on the  secure call.                     */

#define XC_PFIV_ERROR_LAST                      XC_PFIV_ERROR_BASE + 24  /* Last error number in use.                                                           */

/* the value of iv mode (0 is reserved) */
#define	XC_PFIV_MODE_LINEAR	0x00000001
#define	XC_PFIV_MODE_STRIDE	0x00000002
#define	XC_PFIV_MODE_PAGE	0x00000003

typedef void ( *XC_PFIV_CALLBACK_PROC )(void *);
typedef struct XC_V_Handle_s   *XC_V_Handle_t;
typedef struct XC_IV_Handle_s  *XC_IV_Handle_t;
typedef struct XC_SC_Handle_s  *XC_SC_Handle_t;

/* API for Secure Call */
XC_int32 XC_SC_Open (XC_V_Handle_t vHandle, void *pFuncAddr, XC_SC_Handle_t *pSCHandle); 
XC_int32 XC_SC_Set_Arg (XC_SC_Handle_t pSCHandle, void *pArg); 
XC_int32 XC_SC_Call (XC_SC_Handle_t pSCHandle); 
XC_int32 XC_SC_Close (XC_SC_Handle_t pSCHandle); 

/* API for Databox */
XC_int32 XC_DB_Size (XC_V_Handle_t vHandle, XC_int id, XC_uint32 *size);
XC_int32 XC_DB_Copy (XC_V_Handle_t vHandle, XC_int id, void *pAddr, XC_uint32 nbytes);
XC_int32 XC_DB_Destroy (XC_V_Handle_t vHandle, XC_int id);

/* Voucher data management */
XC_int32 XC_IV_Load_Voucher_From_File (XC_char *pVoucherNameWithPath, XC_V_Handle_t *pVHandle);
XC_int32 XC_IV_Load_Voucher_From_Buffer(XC_uint8 *pVData, XC_uint32 VLen, XC_V_Handle_t *pVHandle);
XC_int32 XC_IV_Unload_Voucher (XC_V_Handle_t vHandle);

/* APIs for verification */
/* module handle management */
XC_int32 XC_IV_Open_Function (XC_V_Handle_t pVHandle , void *pFuncAddr, XC_IV_Handle_t *pHandle); 
XC_int32 XC_IV_Open_Module (XC_V_Handle_t vHandle, XC_char *pModuleName, XC_IV_Handle_t *pHandle); 
XC_int32 XC_IV_Close (XC_IV_Handle_t pHandle);

/* verification mode management */
typedef struct XC_IV_Signature_Info_s
{
	XC_int32    nGranules;   /* number of the signatures for specified source (raw or memory) and mode */
	XC_int32    nAtoms;      /* number of the atoms for code section in the specifed module            */
	XC_int32    nBytes;      /* total valid bytes of the code section in the specified module          */
} XC_IV_Signature_Info_t;

/* Memory  verification set  */
XC_int32 XC_IV_Set_Mode (XC_IV_Handle_t pHandle, XC_int ivMode);
XC_int32 XC_IV_Set_Mem_Range (XC_IV_Handle_t pHandle, void *pAddr, XC_size_t length);
XC_int32 XC_IV_Set_Mem (XC_IV_Handle_t pHandle);
XC_int32 XC_IV_Set_Mem_Page (XC_IV_Handle_t pHandle, void *pAddr, XC_int32 PageNUm);
XC_int32 XC_IV_Set_Level (XC_IV_Handle_t pHandle, XC_int ivLevel);
XC_int32 XC_IV_Get_Signature_Info (XC_IV_Handle_t pHandle, XC_IV_Signature_Info_t *pSInfo);

/* Raw data verification set */
XC_int32 XC_IV_Set_Raw_Path (XC_IV_Handle_t pHandle, XC_char *pPathName);
XC_int32 XC_IV_Get_Raw_Mem_Len (void *pAddr, XC_size_t length, XC_size_t *RetLen);
XC_int32 XC_IV_Set_Raw_Addr (XC_IV_Handle_t pHandle, void *pAddr, XC_size_t length);

/* verification */
XC_int32 XC_IV_Verify (XC_IV_Handle_t pHandle);

/* Failure / Success Callback function management */
XC_int32 XC_IV_Set_Fail (XC_IV_Handle_t pHandle, XC_PFIV_CALLBACK_PROC pMy_Failure_Function, void *pMy_Callback_Data);
XC_int32 XC_IV_Set_Success (XC_IV_Handle_t pHandle, XC_PFIV_CALLBACK_PROC pMy_Success_Function, void *pMy_Callback_Data);

#ifdef __cplusplus
}
#endif  /* __cplusplus */
#endif
