/*****************************************************************************
* Irdeto Canada Corporation                                                  *
*                                                                            *
* FILE: xc_errors.h                                                          *
*                                                                            *
* The software and information contained in this package ("Software") is     *
* owned by Irdeto Canada Corporation, its affiliates or licensors            *
* ("Software Owners").  The Software is protected by U.S., Canadian, and     *
* international intellectual property laws and treaties and may contain      *
* patents, trademarks, copyrights, or other intellectual property rights of  *
* the Software Owners.  If you acquired this package without an appropriate  *
* agreement, please contact Irdeto Canada Corporation at:                    *
*                                                                            *
* email: support@irdeto.com                                                  *
*                                                                            *
* The furnishing of this package does not constitute a license, express or   *
* implied, by estoppel or otherwise, to any patents, trademarks, copyrights, *
* or other intellectual property rights of the Software Owners.  Use of the  *
* Software without an appropriate agreement may constitute an infringement   *
* of the Software Owners' intellectual property rights and could subject you *
* to legal action.                                                           *
******************************************************************************/

#ifndef _XC_ERRORS_H_
#define _XC_ERRORS_H_

/*********************************************************
* General return codes
*********************************************************/
#define XC_FAIL                                     -1
#define XC_OK                                       0x0
#define XC_BAD_INPUT                                0x2

/*********************************************************
* Whitebox return codes
*********************************************************/
#define XC_WB_ERR_NULL_INPUT                        0x801
#define XC_WB_ERR_NULL_OUTPUT                       0x802
#define XC_WB_ERR_NULL_INIT_VEC                     0x803
#define XC_WB_ERR_NULL_KEY                          0x804
#define XC_WB_ERR_NULL_RKS                          0x805
#define XC_WB_ERR_INVALID_DATA_SIZE                 0x806
#define XC_WB_ERR_INVALID_INIT_VEC_SIZE             0x807
#define XC_WB_ERR_INVALID_KEY_SIZE                  0x808
#define XC_WB_ERR_ALLOC_FAIL                        0x809
#define XC_WB_ERR_MISMATCHED_ID                     0x80a
#define XC_WB_ERR_BAD_PARAMS                        0x80b
#define XC_WB_ERR_BAD_VERSION                       0x80c
#define XC_IT_ERR_INVALID_FILE                      0x80d
#define XC_IT_ERR_ENDIAN_READ_FAIL                  0x80e
#define XC_IT_ERR_UNKNOWN_ENDIAN                    0x80f
#define XC_IT_ERR_BAD_VERSION                       0x810
#define XC_IT_ERR_UNKNOWN_VERSION                   0x811
#define XC_IT_ERR_BAD_SIZE                          0x812
#define XC_IT_ERR_BAD_HEADER                        0x813
#define XC_IT_ERR_BAD_ID                            0x814
#define XC_IT_ERR_BAD_RECORD                        0x815
#define XC_IT_ERR_ID_NOT_FOUND                      0x816
#define XC_IT_ERR_BAD_CHECKSUM                      0x817
#define XC_IT_ERR_ALLOC_FAIL                        0x818
#define XC_WB_ERR_MISMATCHED_FUNC_TYPE              0x819
#define XC_WB_ERR_MISMATCHED_KEY_LOC                0x81a
#define XC_WB_ERR_MISMATCHED_OP                     0x81b
#define XC_WB_ERR_MISMATCHED_ALG                    0x81c
#define XC_WB_ERR_MISMATCHED_MODE                   0x81d
#define XC_WB_ERR_MISMATCHED_CTR_SIZE               0x81e
#define XC_WB_ERR_MISMATCHED_KEY_SIZE               0x81f
#define XC_WB_ERR_MISMATCHED_KEY_TYPE               0x820
#define XC_WB_ERR_MISMATCHED_NUMROUNDS              0x821
#define XC_WB_ERR_MISMATCHED_ENC_TYPE               0x822
#define XC_WB_ERR_MISMATCHED_SHUFFLE_LEVEL          0x823
#define XC_IT_ERR_BAD_LABEL_SIZE                    0x824
#define XC_IT_ERR_INVALID_LABEL_SIZE                0x825
#define XC_IT_ERR_BAD_LABEL                         0x826
#define XC_IT_ERR_UNSUPPORTED_ENC_TYPE              0x827
#define XC_WB_ERR_FIXEDKEY_WRONG_SIZE               0x828
#define XC_IT_ERR_INVALID_BUFFER                    0x829
#define XC_IT_ERR_INVALID_OPTIONS                   0x82a
#define XC_WB_ERR_NULL_DATAHOLDER                   0x82b
#define XC_IT_ERR_UNSUPPORTED_OLD_VERSION           0x82c
#define XC_WB_ERR_UNSUPPORTED_SHUFFLE               0x82d
#define XC_IT_ERR_FOPEN_FAIL                        0x82e
#define XC_IT_ERR_INVALID_BUFFER_ALIGNMENT          0x82f
#define XC_WB_ERR_NULL_MESSAGE                      0x830
#define XC_WB_ERR_NULL_SIGNATURE                    0x831
#define XC_WB_ERR_INVALID_SIGNATURE_SIZE            0x832
#define XC_WB_ERR_VERIFY_FAIL                       0x833
#define XC_WB_ERR_MISMATCHED_PADDING                0x834
#define XC_WB_ERR_MISMATCHED_HASH_FUNC              0x835
#define XC_WB_ERR_MISMATCHED_MASK_GEN_FUNC          0x836
#define XC_WB_ERR_MISMATCHED_SALT_LEN               0x837
#define XC_WB_ERR_OAEP_CODING                       0x838
#define XC_WB_ERR_HASH_FAIL                         0x839
#define XC_WB_ERR_PKCS1_15_EME_CODING               0x83a
#define XC_WB_ERR_PKCS_PARSE_FAIL                   0x83b
#define XC_WB_ERR_CORRUPT_KEY                       0x83c
#define XC_WB_ERR_MISMATCHED_CPT                    0x83d
#define XC_WB_ERR_MISMATCHED_CCT                    0x83e
#define XC_WB_ERR_MISMATCHED_CIT                    0x83f
#define XC_WB_ERR_MISMATCHED_CKT                    0x840
#define XC_WB_ERR_INVALID_ZEROES_INPUT              0x841
#define XC_WB_ERR_MGF1_OUTPUT_TOO_LONG              0x842
#define XC_WB_ERR_DYNAMIC_KEY_WRONG_SIZE            0x843
#define XC_WB_ERR_MISMATCHED_KEY_FORMAT             0x844
#define XC_WB_ERR_INVALID_CPT                       0x845
#define XC_WB_ERR_INVALID_CCT                       0x846
#define XC_WB_ERR_INVALID_CIT                       0x847
#define XC_WB_ERR_INVALID_CKT                       0x848
#define XC_WB_ERR_ECC_EQUAL_POINT                   0x849
#define XC_WB_ERR_ECC_INVALID_PUBKEY                0x84a
#define XC_WB_ERR_ECC_INVALID_CURVE_ID              0x84b
#define XC_WB_ERR_ECC_BAD_NONCE_LENGTH              0x84c
#define XC_WB_ERR_ECC_LENGTH_CHECK_FAILED           0x84d
#define XC_WB_ERR_ECC_INVALID_OCTET_STRING          0x84e
#define XC_WB_ERR_NULL_CONTEXT                      0x84f
#define XC_WB_ERR_NULL_DIGEST                       0x850
#define XC_WB_ERR_NULL_ENTROPY                      0x851
#define XC_WB_ERR_INSUFFICIENT_ENTROPY              0x852
#define XC_WB_ERR_NULL_STATE                        0x853
#define XC_WB_ERR_RESEED_REQUIRED                   0x854
#define XC_WB_ERR_ECC_NONCE_GENERATION_FAILURE      0x855
#define XC_WB_ERR_MISMATCHED_ELLIPTIC_CURVE         0x856
#define XC_WB_ERR_ECC_CURVE_NOT_SUPPORTED           0x857
#define XC_WB_ERR_ECC_AFFINIFY_INFINITY             0x858
#define XC_WB_ERR_KEYSIZE_TOO_SMALL                 0x859
#define XC_WB_ERR_KEYSIZE_TOO_BIG                   0x85a
#define XC_WB_ERR_KEYSIZE_NOT_MULTI_8               0x85b
#define XC_WB_ERR_INVALID_DK_SIZE                   0x85c
#define XC_WB_ERR_INVALID_HASH_FUNCITON             0x85d
#define XC_WB_ERR_INVALID_STATE                     0x85e
#define XC_WB_ERR_MESSAGE_TOO_LONG                  0x85f
#define XC_WB_ERR_INVALID_PLAIN_DATA_TYPE           0x860
#define XC_WB_ERR_INVALID_CIPHER_DATA_TYPE          0x861
#define XC_WB_ERR_INVALID_IV_DATA_TYPE              0x862
#define XC_WB_ERR_INVALID_KEY_DATA_TYPE             0x863
#define XC_WB_ERR_CORRUPT_PRIVATE_KEY               0x864
#define XC_WB_ERR_CORRUPT_PUBLIC_KEY                0x865
#define XC_WB_ERR_CORRUPT_RKS                       0x866
#define XC_WB_ERR_ECC_MX_2_POINT_FAIL               0x867
#define XC_WB_ERR_ECC_INVALID_POINT                 0x868
#define XC_WB_ERR_UNSUPPORTED_KEY_FORMAT            0x869
#define XC_WB_ERR_COMPUTER_SQRT_FAIL                0x86A
#define XC_WB_ERR_COMPUTER_MOD_INVERSE_FAIL         0x86B
#define XC_WB_ECDH_AB_KEYSIZE_MISMATCH              0x86C
#define XC_WB_ECDH_INVALID_KEYGENFUN_OPTION         0x86D

/*********************************************************
* Big Number return codes
*********************************************************/
#define XC_BN_ERR_NULL_INPUT                        0x900
#define XC_BN_ERR_NULL_OUTPUT                       0x901
#define XC_BN_ERR_INTERNAL_ERROR                    0x902
#define XC_BN_ERR_UNSUPPORTED_SUBTRACT              0x903
#define XC_BN_ERR_DIVIDE_BY_ZERO                    0x904
#define XC_BN_ERR_UNMANAGED_CARRY                   0x905
#define XC_BN_ERR_UNMANAGED_BORROW                  0x906
#define XC_BN_ERR_INVERSE_DOES_NOT_EXIST            0x907

/*********************************************************
* Property Dependent Transforms return codes
*********************************************************/
#define XC_PDT_ERR_CREATE_CONTEXT                   0xA00
#define XC_PDT_ERR_FREE_CONTEXT                     0xA01
#define XC_PDT_ERR_CONTEXT_NULL                     0xA02
#define XC_PDT_ERR_CREATE_MACHINE                   0xA03
#define XC_PDT_ERR_CONTEXT_MACHINE                  0xA04
#define XC_PDT_ERR_CREATE_TRANSFORMATION            0xA05
#define XC_PDT_ERR_NO_IMPLEMENTATION                0xA06
#define XC_PDT_ERR_INVALID_LEVEL_NUMBER             0xA07

/*********************************************************
* Platform-Flexible IV return codes
*********************************************************/
#define XC_PFIV_ERROR_BASE					0x500
#define XC_PFIV_ERROR_END					0x5ff

#endif /* _XC_ERRORS_H_ */
