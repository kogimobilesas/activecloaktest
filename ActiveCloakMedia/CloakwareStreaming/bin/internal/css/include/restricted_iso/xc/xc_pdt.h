/**********************************************************************************

	Cloakware Security Suite (CSS)

	December 10, 2008
	Cloakware Corporation

	FILE: xc_pdt.h

	The software and information in this package contain proprietary
	technology and are confidential properties of Cloakware Corporation. If
	you acquired this package without the appropiate agreements; please
	contact Cloakware Corporation at:

	phone (613) 271-9446,  email: info@cloakware.com

	This package is provided "as is" with no warranties, expressed or
	implied, including but not limited to any implied warranty of
	merchantability, fitness for a particular purpose, or freedom from
	infringement

	Cloakware Corporation may have patents or pending patent applications,
	trademarks, copyrights or other intellectual property rights that relate
	to the described subject matter. The furnishing of this package does not
	provide any license, expressed or implied, by estoppel or otherwise,
	to any such patents, trademarks, copyrights, or other intellectual
	property rights

**********************************************************************************/

#ifndef __XC_PDT_H__
#define __XC_PDT_H__

#include "xc/xc_extensions.h"
#include "xc/xc_types.h"
#include "xc/xc_stdlib.h"
#include "xc/xc_errors.h"


/* For C++ */
#ifdef __cplusplus
extern "C" {
#endif

/*********** Comparison(s) to define a property *********/
enum {
	XC_PDT_COMP_EQ
};
typedef XC_int32 XC_PDT_Comp;

typedef struct XC_PDT_Context* XC_PDT_Context_Handle;


/*********************** PDT Context functions *******************************/

_xc_preserve_interface XC_PDT_Context_Handle XC_PDT_Init_Context(XC_uint32 randomSeed, XC_PDT_Comp comp, XC_int32 level);

_xc_preserve_interface XC_PDT_Context_Handle XC_PDT_Init_Array_Context(XC_uint32 randomSeed, XC_PDT_Comp comp, XC_int32 level, XC_uint32 length);

_xc_preserve_interface XC_int32 XC_PDT_Free_Context(XC_PDT_Context_Handle *context);

_xc_preserve_interface void XC_PDT_Set_Property_sint4(
	XC_int32        val,
	XC_int32        dummy,
	XC_PDT_Context_Handle pContext);

_xc_preserve_interface void XC_PDT_Set_Property_Reference_sint4(
	XC_int32*       pval,
	XC_int32        dummy,
	XC_PDT_Context_Handle pContext);

_xc_preserve_interface void XC_PDT_Set_Property_uint4(
	XC_uint32        val,
	XC_uint32        dummy,
	XC_PDT_Context_Handle pContext);

_xc_preserve_interface void XC_PDT_Set_Property_Reference_uint4(
	XC_uint32*       pval,
	XC_uint32        dummy,
	XC_PDT_Context_Handle pContext);

_xc_preserve_interface void XC_PDT_Evaluate_Properties(
    XC_uint32 dummy,
    XC_PDT_Context_Handle pContext);
    
_xc_preserve_interface void XC_PDT_Apply_Property_To_Scalar_uint4(
    XC_uint32 x,
    XC_uint32 dummy,
    XC_PDT_Context_Handle pContext);

_xc_preserve_interface void XC_PDT_Apply_Property_To_Add_uint4(
    XC_uint32 x,
    XC_uint32 y,
    XC_uint32 dummy,
    XC_PDT_Context_Handle pContext);

_xc_preserve_interface void XC_PDT_Apply_Property_To_Scalar_sint4(
    XC_int32 x,
    XC_int32 dummy,
    XC_PDT_Context_Handle pContext);

_xc_preserve_interface void XC_PDT_Apply_Property_To_Add_sint4(
    XC_int32 x,
    XC_int32 y,
    XC_int32 dummy,
    XC_PDT_Context_Handle pContext);
    
_xc_preserve_interface XC_int32 XC_PDT_Get_Result_sint4(
    XC_int32 dummy,
    XC_PDT_Context_Handle pContext);
    
_xc_preserve_interface XC_uint32 XC_PDT_Get_Result_uint4(
    XC_uint32 dummy,
    XC_PDT_Context_Handle pContext);


#ifdef __cplusplus
}
#endif

#endif /* PDT_H */
