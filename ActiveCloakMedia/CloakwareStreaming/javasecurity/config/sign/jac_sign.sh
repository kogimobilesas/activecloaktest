#!/bin/bash 

MY_HOME=`pwd`

CAT_HOME=/home/test/jrong/iac/rpm/packages/iac-master-2.6.0-343_standard_restricted_iso_nl_gcc_4.4_android_arm

DEX_HOME=/home/test/jrong/tools/poisoner

WORK_SPACE_HOME=/home/test/workspace
EMULATOR_HOME=/data/app-private
RPM_HOME=/home/test/jrong/scripts/perl
MY_SIZE=""
OPERATION=""
JAVA_SIGNING_PACKAGE=""
JAVA_SIGNING_PACKAGE_ASSET_DIR=""
SIGNING_CONFIG_FILE=""

#this is a fixed name
HMACSHA256_HANDLE="YYT76H"
HMACSHA256_KEY="87c7e446787a827f9aa0a247eb32f3a987c7e446787a827f9aa0a247eb32f3a9";
STORE_KEY="87c7e446787a827f9aa0a247eb32f3a9"
STORE_IV="4bd74865f96326d8dd891a4c1fad2162"
VOCHER_HANDLE="PPOJUJ"
#Standard seed:
RANDOM_SEED=ADAD57A20A409876ABCD5432EF123123
#This seed is from US3
#RANDOM_SEED=37FE6A76FB908BAB6543AB4CCFD8D939
#CMG seed
#RANDOM_SEED=B86AEA350A5823A9D245D3BD406D8409
#OMP seed
#RANDOM_SEED=89274890712BA8897C234EB563F45987
#OTT Eval
#RANDOM_SEED=A900BBEA49C91620A91B0D18FB350AC2

#For standard build
INSTANCE_DIR=evaluation
#For tated build
#INSTANCE_DIR=i$RANDOM_SEED

OPTIONS="-target-arch arm -target-os android"
#For standard build
OPTION_STD="-evaluation"

OPTION_SL="-sl_disable"


#these are fixed names
MYSTORE="jac_store.dat"
MY_SHARED_LIB="libmegjb.so" 
MY_SHARED_LIB_PATH="$WORK_SPACE_HOME/JACRuntimeNew/libs/armeabi" 
MY_SHARED_LIB_TARGET_PATH="/data/data/com.jacruntimenew/lib" 

MY_SIGN_SHARED_LIB="libjavasign.so" 
MY_SIGN_SHARED_LIB_PATH="$WORK_SPACE_HOME/commandLineSign/libs/armeabi" 
MY_SIGN_SHARED_LIB_TARGET_PATH="/data/data/com.irdeto.commandline.signing/lib"

CLASS_PATH=""
DEX_APK_FILE=""

setupEnv()
{
    export XC_LICENSE=$MY_HOME/../../css/license_all_dialects.xml
    export PATH=/opt/toolchain/android/ndk/android-ndk-r5/toolchains/arm-linux-androideabi-4.4.3/prebuilt/linux-x86/bin:/opt/toolchain/java/32/jdk1.6.0_22/bin:$PATH
    CLASS_PATH="$DEX_HOME/DexConvertor.jar:$DEX_HOME/commons-codec-1.6.jar:$DEX_HOME/commons-io-2.1.jar"
}

generateBytecode()
{
    if [ ! -f $DEX_APK_FILE ];then
     	echo "Poisoning dex/apk file, $DEX_APK_FILE not exist"
	return 1
    fi
    cmd="java -DConfigureFilePath="$MY_HOME/dexconfig.xml" -DIndexFilePath="$MY_HOME/data/index.txt" -DBytecodeFilePath="$MY_HOME/data/bytecode.dat" -DPoinsonedFilePath="$MY_HOME/data/poisoned.dex" -classpath $CLASS_PATH com.irdeto.secureaccess.android.dexreader.IrdetoDexConvertor $DEX_APK_FILE"
    echo "Executing: $cmd"
    $cmd
    cp $MY_HOME/data/classes.dex .
    if [ $? != 0 ];then
   	echo "unable to copy classes.dex"
	return 1
    fi
    jar cvf $MY_HOME/data/Poisoned.jar classes.dex 
    if [ $? != 0 ];then
        echo "Unable to JAR classes.dex"
        return 1
    fi
    rm classes.dex
    return 0
}

runSigningProcOnDevice()
{

    rm -f $CAT_HOME/bin/irdeto_java_access.dat  > /dev/null 2>&1
    adb shell rm /data/app-private/irdeto_java_access_ns.dat  > /dev/null 2>&1
    adb shell rm /data/app-private/irdeto_java_access.dat  > /dev/null 2>&1

    echo "Copying $MYSTORE to Android emulator at $EMULATOR_HOME/local/"
    adb push ./$MYSTORE $EMULATOR_HOME/local/

    echo "Copy runtime application: $JAVA_SIGNING_PACKAGE to emulator at $EMULATOR_HOME/local/ for signing"
    adb push $JAVA_SIGNING_PACKAGE $EMULATOR_HOME/local/ 

    cd $CAT_HOME/bin
    ./ACResourceProtection -compatible_agent $CAT_HOME/lib/$INSTANCE_DIR -randomseed $RANDOM_SEED $OPTIONS -java_sign build -config_file $SIGNING_CONFIG_FILE 

}

checkForCompletion()
{
    echo ""
    echo "Perform voucher completion checking, wait if it is incomplete"
    while [  1 ]; do
        adb pull /data/app-private/irdeto_java_access.dat .  > /dev/null 2>&1
        myTok=`cat ./irdeto_java_access.dat | grep "#EOF"`
        if [ "$myTok" = "#EOF" ]; then
        echo "Found 'end of file'"
        break;
        fi
        sleep 5
    done
}

usage()
{
    echo ""
    echo "Command line:"
    echo ""
    echo "Prompt> $0 -s -p java_signing_package_path -c signing_config_file"
    echo ""
    echo "where, -s sign operation" 
    echo ""
    exit 1
}

parseCommandLine(){

    while getopts sp:c:a: OPT; do
	case "$OPT" in
	    s) 	
		OPERATION="1"
		;;
	    p)	
		JAVA_SIGNING_PACKAGE=$OPTARG
		;;
	    c)
		SIGNING_CONFIG_FILE=$OPTARG
		;;
	    a)
                DEX_APK_FILE=$OPTARG
                ;;
	    *)
		usage
		;;
	esac
    done
    if [ "$JAVA_SIGNING_PACKAGE" = "" ]; then
	usage
    elif  [ "$SIGNING_CONFIG_FILE" = "" ]; then
	usgae
    elif  [ "$OPERATION" != "1" ]; then
	usage
    fi
    JAVA_SIGNING_PACKAGE_ASSET_DIR=`echo ${JAVA_SIGNING_PACKAGE%/*}`
    JAVA_SIGNING_PACKAGE_ASSET_DIR=`echo ${JAVA_SIGNING_PACKAGE_ASSET_DIR%/*}`
    JAVA_SIGNING_PACKAGE_ASSET_DIR=$JAVA_SIGNING_PACKAGE_ASSET_DIR/assets
    echo ""
    if [ ! -f "$JAVA_SIGNING_PACKAGE" ]; then
	echo "$JAVA_SIGNING_PACKAGE not exist..."
	usage
    elif [ ! -f "$SIGNING_CONFIG_FILE" ]; then
	echo "$SIGNING_CONFIG_FILE not exist..."
	usage
    fi
}

initSecureStore(){

    echo ""
    echo "Initializing secure store"
    echo ""
    rm $MYSTORE  2>/dev/null

    echo "Creating Secure Store"
    $CAT_HOME/bin/ACResourceProtection  -compatible_agent $CAT_HOME/lib/$INSTANCE_DIR -randomseed $RANDOM_SEED $OPTIONS -create_secure_store $MYSTORE -algorithm aes -key_data $STORE_KEY -iv_data $STORE_IV -debug
    MY_SIZE=`ls -al |grep $MYSTORE | awk '{print($5)}'`
    echo "OK. size: $MY_SIZE"

    echo "Inserting HAMC Key"
    $CAT_HOME/bin/ACResourceProtection -compatible_agent $CAT_HOME/lib/$INSTANCE_DIR -randomseed $RANDOM_SEED  $OPTIONS -protect_data $MYSTORE -handle $HMACSHA256_HANDLE -data $HMACSHA256_KEY -debug
    MY_SIZE=`ls -al |grep $MYSTORE | awk '{print($5)}'`
    echo "OK. size: $MY_SIZE"
}

sign() {

#create secure store for signing
    initSecureStore
#have to sign the offline shared library
    myCommand="$CAT_HOME/bin/ACResourceProtection -compatible_agent $CAT_HOME/lib/$INSTANCE_DIR -randomseed $RANDOM_SEED $OPTION_SL $OPTION_STD -target-arch arm -target-os android -protect_application jac_store.dat -module_signed_in_store 5 $MY_SIGN_SHARED_LIB_PATH/$MY_SIGN_SHARED_LIB $MY_SIGN_SHARED_LIB_TARGET_PATH/ -root_module $MY_SIGN_SHARED_LIB_PATH/$MY_SIGN_SHARED_LIB -ac_agent $MY_SIGN_SHARED_LIB_PATH/$MY_SIGN_SHARED_LIB -pfiv -debug"
    echo "Executig $myCommand"
    $myCommand
    MY_SIZE=`ls -al |grep $MYSTORE | awk '{print($5)}'`
    echo "OK. size: $MY_SIZE"

    echo "Run individualization tool"
#    $CAT_HOME/bin/ACIndividualization -compatible_agent $CAT_HOME/lib/i$RANDOM_SEED $MYSTORE -set_access_mode RW -set_nodelocking_mode Client  -debug
    $CAT_HOME/bin/ACIndividualization -compatible_agent $CAT_HOME/lib/$INSTANCE_DIR $MYSTORE -set_access_mode RW -debug

    runSigningProcOnDevice
    checkForCompletion

    echo "Java sign completed"

# final delivery of secure store
    initSecureStore

    echo "Inserting JAVA voucher"
    $CAT_HOME/bin/ACResourceProtection -compatible_agent $CAT_HOME/lib/$INSTANCE_DIR -randomseed $RANDOM_SEED $OPTIONS -protect_data $MYSTORE -handle $VOCHER_HANDLE -data_file ./irdeto_java_access.dat 
    MY_SIZE=`ls -al |grep $MYSTORE | awk '{print($5)}'`
    echo "OK. size: $MY_SIZE"

    generateBytecode
    if [ $? = 0 ];then
	echo ""
    	echo "Inserting JAC config item, contents:"
	cat $MY_HOME/data/JacConfigFile.txt
    	$CAT_HOME/bin/ACResourceProtection -compatible_agent $CAT_HOME/lib/$INSTANCE_DIR -randomseed $RANDOM_SEED $OPTIONS  -protect_data $MYSTORE -handle JACConfigHandle -data_file $MY_HOME/data/JacConfigFile.txt
	MY_SIZE=`ls -al |grep $MYSTORE | awk '{print($5)}'`
    	echo "OK. size: $MY_SIZE"

	echo ""
    	echo "Inserting poisoned JAR file"
	$CAT_HOME/bin/ACResourceProtection -compatible_agent $CAT_HOME/lib/$INSTANCE_DIR -randomseed $RANDOM_SEED $OPTIONS  -protect_data $MYSTORE -handle Poisoned.jar -data_file $MY_HOME/data/Poisoned.jar
	MY_SIZE=`ls -al |grep $MYSTORE | awk '{print($5)}'`
    	echo "OK. size: $MY_SIZE"

	echo ""
    	echo "Inserting index file"
	$CAT_HOME/bin/ACResourceProtection -compatible_agent $CAT_HOME/lib/$INSTANCE_DIR -randomseed $RANDOM_SEED $OPTIONS  -protect_data $MYSTORE -handle index.txt -data_file $MY_HOME/data/index.txt
	MY_SIZE=`ls -al |grep $MYSTORE | awk '{print($5)}'`
    	echo "OK. size: $MY_SIZE"
	
	echo ""
    	echo "Inserting bytecode file"
	$CAT_HOME/bin/ACResourceProtection -compatible_agent $CAT_HOME/lib/$INSTANCE_DIR -randomseed $RANDOM_SEED $OPTIONS  -protect_data $MYSTORE -handle bytecode.dat -data_file $MY_HOME/data/bytecode.dat
	MY_SIZE=`ls -al |grep $MYSTORE | awk '{print($5)}'`
    	echo "OK. size: $MY_SIZE"
	
#	echo ""
#    	echo "Inserting optimized bytecode file"
#	$CAT_HOME/bin/ACResourceProtection -compatible_agent $CAT_HOME/lib/$INSTANCE_DIR -randomseed $RANDOM_SEED $OPTIONS  -protect_data $MYSTORE -handle optBytecode.dat -data_file $MY_HOME/data/optBytecode.dat
#	MY_SIZE=`ls -al |grep $MYSTORE | awk '{print($5)}'`
#    	echo "OK. size: $MY_SIZE"

    fi

    myCommand="$CAT_HOME/bin/ACResourceProtection -compatible_agent $CAT_HOME/lib/$INSTANCE_DIR -randomseed $RANDOM_SEED $OPTION_STD $OPTION_SL -target-arch arm -target-os android -protect_application jac_store.dat -module_signed_in_store 10 $MY_SHARED_LIB_PATH/$MY_SHARED_LIB $MY_SHARED_LIB_TARGET_PATH/ -root_module $MY_SHARED_LIB_PATH/$MY_SHARED_LIB -ac_agent $MY_SHARED_LIB_PATH/$MY_SHARED_LIB -pfiv -debug"
    echo "Executig $myCommand" 
    $myCommand
    MY_SIZE=`ls -al |grep $MYSTORE | awk '{print($5)}'`
    echo "OK. size: $MY_SIZE"

    echo "Run individualization tool"
#    $CAT_HOME/bin/ACIndividualization -compatible_agent $CAT_HOME/lib/$INSTANCE_DIR $MYSTORE -set_access_mode RW -set_nodelocking_mode Client  -debug
    $CAT_HOME/bin/ACIndividualization -compatible_agent $CAT_HOME/lib/$INSTANCE_DIR $MYSTORE -set_access_mode RW  -debug
    MY_SIZE=`ls -al |grep $MYSTORE | awk '{print($5)}'`
    echo "OK. size: $MY_SIZE"

    echo "Copying $MYSTORE to $JAVA_SIGNING_PACKAGE_ASSET_DIR"
    cp ./$MYSTORE $JAVA_SIGNING_PACKAGE_ASSET_DIR
}

####################  main  ###########################

parseCommandLine $@
setupEnv
sign
