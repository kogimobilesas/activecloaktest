#! /bin/bash
#
#
# Irdeto Canada Corporation                                                  
#                                                                            
# FILE: createKeyStore.sh                                                    
#                                                                            
# The software and information contained in this package ("Software") is     
# owned by Irdeto Canada Corporation, its affiliates or licensors			 
# ("Software Owners").  The Software is protected by U.S., Canadian, and     
# international intellectual property laws and treaties and may contain      
# patents, trademarks, copyrights, or other intellectual property rights of  
# the Software Owners.  If you acquired this package without an appropriate  
# agreement, please contact Irdeto Canada Corporation at:					 
#                                                                            
# phone +1 613 271 9446, email: info@irdeto.com							     
#                                                                            
# The furnishing of this package does not constitute a license, express or   
# implied, by estoppel or otherwise, to any patents, trademarks, copyrights, 
# or other intellectual property rights of the Software Owners.  Use of the  
# Software without an appropriate agreement may constitute an infringement   
# of the Software Owners' intellectual property rights and could subject you 
# to legal action.															 
#                                                                            
#
# Syntax:  createKeyStore.sh $1 $2
#    $1 = TARGET_OS, e.g. android
#    $2 = TARGET_ARCH, e.g. arm
#
# Prerequisite environment variables:
#   CAT_INSTALL_DIR - parent directory of the BIN folder containing 
#                     the ACResourceProtection and ACIndividualization
#
# Input files:
#   All files under the 'values' subdirectory of the current directory 
#   are inserted into the keystore.
#
# Output:
#   This scripts creates 'irss.dat' in the subdir 'data' of the current 
#   directory. This is the key store that contains provisioned secrets.
#
#

. ./setenv.sh ${0} ${1} ${2}
if [ $? -gt 0 ]; then
	exit 1
fi

# fileTest: Determine if a file exists and is readable
# $1 = name of variable containing filename
# $2 = filename
fileTest()
{
    if [ -z "${2}" ]
    then
        echo "${1} is not set."
        exit 1
    elif [ ! -r "${2}" ]
    then
        echo "${2} does not exist or is unreadable."
        exit 1
    fi
}

# test to see if a variable is set
# $1 = name of variable
# $2 = value of the variable
varTest()
{
    if [ -z ${2} ]
    then
        echo "${1} is not set."
        exit 1
    fi
}

#map for data and key file names and labels
put() {
    if [ "$#" != 3 ]; then exit 1; fi
    mapName=$1; key=$2; value=`echo $3 | sed -e "s/ /:SP:/g"`
    eval map="\"\$$mapName\""
    map="`echo "$map" | sed -e "s/--$key=[^ ]*//g"` --$key=$value"
    eval $mapName="\"$map\""
}

get() {
    mapName=$1; key=$2; valueFound="false"
    echo MapName:$mapName Key: $key 

    eval map=\$$mapName

    for keyValuePair in ${map};
    do
        case "$keyValuePair" in
            --$key=*) value=`echo "$keyValuePair" | sed -e 's/^[^=]*=//'`
                      valueFound="true"
        esac
        if [ "$valueFound" == "true" ]; then break; fi
    done
    mapEntryValue=`echo $value | sed -e "s/:SP:/ /g"`
}

KEYSTORE_2_NAME=irss3.dat
PRODUCTION_2_SS=production/irss3.dat
KEYSTORE_NAME=irss.dat
PRODUCTION_SS=production/irss.dat

checkforProductionSS()
{
    echo Checking if the $1 Secure Store exists
    if [ -r "${1}" ]
    then
        cp -f "${1}" "data/${2}"
        if [ $? -gt 0 ]; then
            echo "Error: Unable to copy ${1} to data/${2} !"
            exit 1
        fi
        echo "Production Secure Store ${1} was found and copied to the data folder."
        echo "A new ${2} was not created"
        return 1
    else
        return 0
    fi
}


createStore()
{
    # test for a few values we'll need later
    if [ "${1}" == "$KEYSTORE_NAME" ]; then
        varTest "KEYSTORE_NAME" ${1} 
    elif [ "${1}" == "$KEYSTORE_2_NAME" ]; then
        varTest "KEYSTORE_2_NAME" ${1} 
    fi

    echo Creating secure store: ${1} ...

    # turn off the PERLLIB if it is set. This prevents conflicts with PERL2EXE and installed versions of PERL. 
    unset PERLLIB
    KEK=efbeaddeefbeaddeefbeaddeefbeadde
    KEKIV=efbebebaefbebebaefbebebaefbebeba


    # save the current path
    OLDPWD=`pwd`

    # delete the previous KEYSTORE
    rm -f "${1}"
    rm -f "data/${1}"

    # create the output directory and working directory
    mkdir -p "${RPM_WORKING_FOLDER}"
    if [ $? -gt 0 ]; then
        echo "Error: Unable to create ${RPM_WORKING_FOLDER} !"
        exit 1
    fi
    chmod a+w "${RPM_WORKING_FOLDER}"

    # clean and copy files to working folder, then cd to that location
    rm -rf "${RPM_WORKING_FOLDER}/*"


    if [ "${1}" == "${KEYSTORE_NAME}" ]; then
        #all mapped labels used in security store for entry handles to hide meaningful entry handle
        put "NAMEDIDMap" "ID_DRM_DKT_WMDRMPD_FALLBACK" "353839353136303236"
        put "NAMEDIDMap" "ID_DRM_DKT_WMDRMPD_GROUP" "2D31383135393533353933"
        put "NAMEDIDMap" "ID_DRM_DKT_PLAYREADY_MODEL" "31323030303632343939"
        put "NAMEDIDMap" "ID_DRM_DKT_PLAYREADY_DEVICE_SIGN" "2D32303833313735353230"
        put "NAMEDIDMap" "ID_DRM_DKT_PLAYREADY_DEVICE_ENCRYPT" "2D31323939393437313334"
        put "NAMEDIDMap" "ID_DRM_DKT_PLAYREADY_DEVICE_SIGN_ECC" "31323439323931383436"
        put "NAMEDIDMap" "ID_DRM_DKT_PLAYREADY_DEVICE_ENCRYPT_ECC" "373834333337373034"
        put "NAMEDIDMap" "ID_DRM_DKT_PLAYREADY_DEVICE_ENCRYPT_AES" "373834333333393338"
        put "NAMEDIDMap" "ID_DRM_DKT_PLAYREADY_NDR" "2D31373332383236383930"
        put "NAMEDIDMap" "ID_DRM_DKT_PLAYREADY_NDR_MODEL" "31373532343635303234"
        put "NAMEDIDMap" "ID_DRM_DCT_WMDRMPD" "383035373033323135"
        put "NAMEDIDMap" "ID_DRM_DCT_PLAYREADY" "31303438323934383831"
        put "NAMEDIDMap" "ID_DRM_DCT_NETRECEIVER" "373635383637323632"
        put "NAMEDIDMap" "ID_DRM_DCT_WMDRMPD_TEMPLATE" "393637353333373036"
        put "NAMEDIDMap" "ID_DRM_DCT_PLAYREADY_TEMPLATE" "31363535383530323634"
        put "NAMEDIDMap" "ID_DRM_DCT_NETRECEIVER_TEMPLATE" "2D3239383534303533"
        put "NAMEDIDMap" "ID_SST_GLOBAL_PASSWORD" "2D31363030333931363038"
        put "NAMEDIDMap" "ID_DRM_DKT_PLAYREADY_MODEL_SIGN_KEY" "2D31353136323330333131"
        put "NAMEDIDMap" "ID_DRM_DKT_WMDRMPD_FALLBACK_SIGN" "1C4830813AB937343411"
        put "NAMEDIDMap" "ID_DRM_DKT_WMDRMPD_GROUP_DECRYPT" "4A50217312C134302002"
        put "NAMEDIDMap" "ID_DRM_DKT_WMDRMPD_GROUP_SIGN" "2ABC0006393937340911"


        # create the version key
        date > versionkey.txt

        if [ ${PRIV_DAT} ]
        then
	        fileTest priv.dat ${PRIV_DAT}
	        perl convertWMDRMKeys.pl ${PRIV_DAT} ${OLDPWD}
	        if [ $? -gt 0 ]; then
		        echo "Error: Unable to convert ${PRIV_DAT}.!"
		        exit 1
	        fi
        fi

        # copy files to working folder and then cd to that location
        cp -rf * "${RPM_WORKING_FOLDER}"
        if [ $? -gt 0 ]; then
            echo "Error: Unable to copy provisioning files to ${RPM_WORKING_FOLDER} !"
            exit 1
        fi
        cd "${RPM_WORKING_FOLDER}"
		chmod -R +rwx *

        # enumerate through all the files in the 'values' subdirectory and 
        # provision them as secret values, identifiable by their filename.


        # Create the keystore we'll be appending to
        echo "\"${ASSET_PROTECTION[@]}\" -create_secure_store ${KEYSTORE_NAME} -algorithm AES -key_data ${KEK} -iv_data ${KEKIV}"
        "${ASSET_PROTECTION[@]}" -create_secure_store ${KEYSTORE_NAME} -algorithm AES -key_data ${KEK} -iv_data ${KEKIV}
        if [ $? -gt 0 ]; then
            echo "Error: Unable to create keystore file!"
            exit 1
        fi

        # Add the version key for IV
        echo "\"${ASSET_PROTECTION[@]}\" -protect_data ${KEYSTORE_NAME} -data_file versionkey.txt -handle version"
        "${ASSET_PROTECTION[@]}" -protect_data ${KEYSTORE_NAME} -data_file versionkey.txt -handle version
        if [ $? -gt 0 ]; then
            echo "Error: Unable to protect the above data!"
            exit 1
        fi

        # Add the assets from values directory
        for file in values/*
        do
            _namedidmap=$(basename "${file}")
            get "NAMEDIDMap" "${_namedidmap}"
            handle=${mapEntryValue}
            echo "\"${ASSET_PROTECTION[@]}\" -protect_data ${KEYSTORE_NAME} -data_file \"${file}\" -handle ${handle}"
            "${ASSET_PROTECTION[@]}" -protect_data ${KEYSTORE_NAME} -data_file "${file}" -handle ${handle}
	        if [ $? -gt 0 ]; then
		        echo "Error: Unable to protect the above data!"
		        exit 1
	        fi
        done

        # Add the assets from keys/sign/playready directory
        for file in keys/sign/playready/*
        do
            _namedidmap=$(basename "${file}")
            get "NAMEDIDMap" "${_namedidmap}" 
            handle=${mapEntryValue}
            rm -f key.pem
            perl create-model-key.pl "${file}"
	        if [ $? -gt 0 ]; then
		        echo "Error: Unable to create the ${file} key!"
		        exit 1
	        fi

            echo "\"${ASSET_PROTECTION[@]}\"  -protect_key ${KEYSTORE_NAME} -key_file key.pem -algorithm ECC -operation Sign -ecc_curve nistp256 -handle ${handle}"
	        "${ASSET_PROTECTION[@]}" -protect_key ${KEYSTORE_NAME} -key_file key.pem -algorithm ECC -operation Sign -ecc_curve nistp256 -handle ${handle}
	        if [ $? -gt 0 ]; then
		        echo "Error: Unable to protect the above key!"
		        exit 1
	        fi
        done

        # Add the assets from keys/sign/wmdrm directory
        for file in keys/sign/wmdrm/*
        do
            _namedidmap=$(basename "${file}")
            get "NAMEDIDMap" "${_namedidmap}" 
            handle=${mapEntryValue}
            rm -f key.pem
            perl create-wmdrm-key.pl "${file}"
	        if [ $? -gt 0 ]; then
		        echo "Error: Unable to create the ${file} key!"
		        exit 1
	        fi

            echo "\"${ASSET_PROTECTION[@]}\" -protect_key ${KEYSTORE_NAME} -key_file key.pem -algorithm ECC -operation Sign -ecc_curve msp160 -handle ${handle}"
	        "${ASSET_PROTECTION[@]}" -protect_key ${KEYSTORE_NAME} -key_file key.pem -algorithm ECC -operation Sign -ecc_curve msp160 -handle ${handle}
	        if [ $? -gt 0 ]; then
		        echo "Error: Unable to protect the above key!"
		        exit 1
	        fi
        done

        # Individualize
        echo "\"${INDIVIDUALIZATION[@]}\"  \"${KEYSTORE_NAME}\""
        "${INDIVIDUALIZATION[@]}" "${KEYSTORE_NAME}"
        if [ $? -gt 0 ]; then
	        echo "Error: Unable to perform individualization!"
	        exit 1
        fi

        cp -f "${KEYSTORE_NAME}" "${OLDPWD}/data/."
        if [ $? -gt 0 ]; then
	        echo "Error: Unable to copy ${KEYSTORE_NAME} to ${OLDPWD}/data !"
	        exit 1
        fi

        # end of the 1st store creation

    elif [ "${1}" == "${KEYSTORE_2_NAME}" ]; then
        #all mapped labels used in security store for entry handles to hide meaningful entry handle
        put "NAMEDIDMap" "ID_HTTPS_CERTCHAIN" "32303933303030303337"
        put "NAMEDIDMap" "ID_HTTPS_CERTCHAIN_2" "43303930503030303373"
        put "NAMEDIDMap" "ID_HTTPS_CERTCHAIN_DER" "54303930503030303373"


        # copy files to working folder and then cd to that location
        cp -rf * "${RPM_WORKING_FOLDER}"
        if [ $? -gt 0 ]; then
            echo "Error: Unable to copy provisioning files to ${RPM_WORKING_FOLDER} !"
            exit 1
        fi
        cd "${RPM_WORKING_FOLDER}"
		chmod -R +rwx *


        # enumerate through all the files in the 'values' subdirectory and 
        # provision them as secret values, identifiable by their filename.

        # Create the keystore we'll be appending to
        echo "\"${ASSET_PROTECTION[@]}\" -create_secure_store ${KEYSTORE_2_NAME} -algorithm AES -key_data ${KEK} -iv_data ${KEKIV}"
        "${ASSET_PROTECTION[@]}" -create_secure_store ${KEYSTORE_2_NAME} -algorithm AES -key_data ${KEK} -iv_data ${KEKIV}
        if [ $? -gt 0 ]; then
            echo "Error: Unable to create keystore file!"
            exit 1
        fi
        
        # Add the assets from certs directory
        for file in certs/*
        do
            _namedidmap=$(basename "${file}")
            get "NAMEDIDMap" ${_namedidmap}
            handle=${mapEntryValue}
            echo "\"${ASSET_PROTECTION[@]}\" -protect_data ${KEYSTORE_2_NAME} -data_file \"${file}\" -handle ${handle}"
            "${ASSET_PROTECTION[@]}" -protect_data ${KEYSTORE_2_NAME} -data_file "${file}" -handle ${handle}
	        if [ $? -gt 0 ]; then
		        echo "Error: Unable to protect the above data!"
		        exit 1
	        fi
        done
        
        # Individualize
        echo "\"${INDIVIDUALIZATION[@]}\" \"${KEYSTORE_2_NAME}\""
        "${INDIVIDUALIZATION[@]}" "${KEYSTORE_2_NAME}"
        if [ $? -gt 0 ]; then
	        echo "Error: Unable to perform individualization!"
	        exit 1
        fi

        cp -f "${KEYSTORE_2_NAME}" "${OLDPWD}/data/."
        if [ $? -gt 0 ]; then
	        echo "Error: Unable to copy ${KEYSTORE_2_NAME} to ${OLDPWD}/data !"
	        exit 1
        fi
        
        # end of the 2nd store creation
    fi

    # go back to the script execuation directory
    cd "${OLDPWD}"
    rm -rf "${RPM_WORKING_FOLDER}"
}


# check and create the 1st secure store
checkforProductionSS ${PRODUCTION_SS} "${KEYSTORE_NAME}"
if [ $? -eq 0 ]; then
    createStore "${KEYSTORE_NAME}"
	if [ $? -gt 0 ]; then
	    echo "Error: Unable to create key store ${KEYSTORE_NAME} !"
	    exit 1
    fi
fi

# check and create the 2nd secure store
checkforProductionSS ${PRODUCTION_2_SS} "${KEYSTORE_2_NAME}"
if [ $? -eq 0 ]; then
    createStore ${KEYSTORE_2_NAME}
	if [ $? -gt 0 ]; then
	    echo "Error: Unable to create key store ${KEYSTORE_2_NAME} !"
	    exit 1
    fi
fi

exit 0
