#!/usr/bin/env python

import sys
import os
import os.path
import platform
import re
import xml.dom
import xml.dom.minidom
import math
import datetime
import sys
import glob
import shutil
import time

from datetime import date, time
from xml.dom import xmlbuilder, minidom, Node
from stat import *

__all__ = [
    "Arguments",
    "Output",
    "Combine",
    "GetElementData",
    "GetChildNodeValue",
    "GetGlobalVariableValue",
    "CheckGlobalVariable",
    "Quote",
    "EnsureFolder",
    "SubstitutionClass",
    "Chdir",
    "GetDateStamp",
    "GetDateStampFromObject",
    "GetFancyStamp",
    "GetFancyDateFromObject",
    "ModificationDate"
]

def GetDateStamp():
    return GetDateStampFromObject(datetime.datetime.now())

def GetDateStampFromObject(dateObject):
    year = dateObject.year
    month = dateObject.month
    date = dateObject.day
    hours = dateObject.hour
    minutes = dateObject.minute
    seconds = dateObject.second
    return "{0:04d}{1:02d}{2:02d}-{3:02d}{4:02d}{5:02d}".format(year, month, date, hours, minutes, seconds)

def GetFancyStamp():
    return GetFancyDateFromObject(datetime.datetime.now())

def GetFancyDateFromObject(dateObject):
    year = dateObject.year
    month = dateObject.month
    date = dateObject.day
    hours = dateObject.hour
    minutes = dateObject.minute
    seconds = dateObject.second
    return "{0:04d}/{1:02d}/{2:02d}-{3:02d}:{4:02d}:{5:02d}".format(year, month, date, hours, minutes, seconds)

def ModificationDate(filename):
    file_modification_time = os.path.getmtime(filename)
    dateObject = datetime.datetime.fromtimestamp(file_modification_time)
    return GetDateStampFromObject(dateObject)

def Output(line):
    print line

def Combine(path, file):
    if (path == None or path == "") and (file == None or file == ""):
        return ""
    elif path == None or path == "":
        return file
    elif file == None or file == "":
        return path
    else:
        path_size = len(path)
        if file[0] == "\\":
            file[0] = "/" + file[1:]
        if path[path_size-1] == "\\":
            path = path[:-1] + "/"
        if file[0] != "/" and path[path_size-1] != "/":
                path += "/"
        return path + file

def GetElementData(element):
    resultData = None
    if element != None and element.nodeType == Node.ELEMENT_NODE:
        childNodeList = element.childNodes
        for childNode in childNodeList:
            if childNode.nodeType == Node.TEXT_NODE:
                resultData = childNode.data
                break
    return resultData
    
def GetChildNodeValue(node, nameSpace, nodeName, errorMessage):
    value = None
    prefix = None # prefix of the element node
    localName = None # local namd of the element node
    if nodeName != None and len(nodeName) > 1 and nodeName[0] == '@':
        # child is an attribute
        attributeName = nodeName[1:]
        nodeNameParts = nodeName.split(":", 1)
        if len(nodeNameParts) == 2 and nameSpace != None:
            prefix = nodeNameParts[0]
            localAttributeName = nodeNameParts[1]
            value = node.getAttributeNS(nameSpace, localAttributeName)
        else:
            value = node.getAttribute(attributeName)

    elif nodeName != None and len(nodeName) > 1:
        # child is an element
        # obtain prefix and localName
        nodeNameParts = nodeName.split(":", 1)
        if len(nodeNameParts) == 2:
            prefix = nodeNameParts[0]
            localName = nodeNameParts[1]
        else:
            localName = nodeName
        # find all nodes with the 'localName' that are children of the parent node
        childNodeList = node.getElementsByTagNameNS(nameSpace, localName)
        # if the list is not empty, we will take the first node only
        if childNodeList != None and len(childNodeList) > 0:
            childNode = childNodeList[0]
            # we accept the value only if the requested node does not have 
            if prefix == None or (childNode.prefix != None and len(childNode.prefix) > 0):
                value = GetElementData(childNode)
                # if value is not empty and it is a string, strip the value
                if value != None and isinstance(value, str):
                    value = value.strip()
            elif errorMessage != None:
                raise Exception(errorMessage)
        elif errorMessage != None:
            raise Exception(errorMessage)
    return value


def GetGlobalVariableValue(variableName):
    variableValue = None
    if os.environ.has_key(variableName):
        variableValue = os.environ[variableName]
    return variableValue

def CheckGlobalVariable(variableName):
    if os.environ.has_key(variableName):
        variableValue = os.environ[variableName]
        # Output("Found environment variable " + variableName + " = " + variableValue)
    else:
        raise Exception("Global variable '" + variableName + "' is not declared.")

def Quote(line):
    return "\"" + line + "\""

def EnsureFolder(folderPath):
    path = folderPath
    size = len(path)
    while path[size - 1] == "\\" or path[size - 1] == "/" :
       path = path[:-1]
       size = len(path)
    if not os.path.exists(path):
        slashIndex = max(path.rfind("\\"), path.rfind("/")) 
        if slashIndex > 0:
            EnsureFolder(path[:slashIndex])
        count = 0
        while not os.path.exists(path) and count < 10:
            try:
                # there is a possibility that the directory
                # was deleted recently. We need to give time to
                # the system to complet the delete transaction
                os.mkdir(path)
            except Exception or OSError:
                time.sleep(0.100)
                count += 1

## - enums for command line parser states
larg_end = -3
line_send = -2
line_end = -1 
line_start = 0 
arg_start = 1
arg_char = 2 
arg_end = 3
arg_space = 4
arg_qstart = 5
arg_qchar = 6
arg_qend = 7
arg_qpart = 8
arg_qslash = 9
arg_schar = 10
state_count = 11

## enums for command line parser lexems
char_mark = 0
space_mark = 1
back_slash_mark = 2
quote_mark = 3
line_end_mark = 4
lexem_count = 5

## Arguments class
class Arguments(object):
    """ This Arguments class is declared to support 
        JScript appoach of arguments parsing.
        It supports a command line with the following syntax:

          command_line := [spaces], ( unnamed_arguments | named_arguments )*.
          spases := space+.
          space := ' ' | '\t'.
          unnamed_arguments := unnamed_argument * spaces.
          unnamed_argument := word.
          named_arguments := named_argument * spaces.
          named_argument := '-', name, ['=', [value]].
          word := <sequence of any chars except spaces and not started from '-'>
          name := <sequence of any chars except spaces and '='>
          value := <sequence of any chars except spaces>.

        The class creats an 'unnamedList' array of words and
        a 'namedList' dictionary of names and values.
        The Arguments object can be initialized with either a command line string
        or with an 'argv' list of parameters. 
    """

    def __init__(self, arrayOrString = []):
        self.argumentList = None
        #                             | line_start  | arg_start | arg_char  | arg_end    | arg_space  | arg_qstart | arg_qchar  | arg_qend  | arg_qpart  | arg_qslash | arg_schar  |
        #                             +-------------+-----------+-----------+------------+------------+------------+------------+-----------+------------+------------+------------+
        self.char_mark_states =       [ arg_start,    arg_char,   arg_char,   arg_start,   arg_start,   arg_qchar,   arg_qchar,   arg_char,   arg_qchar,   arg_schar,   arg_qchar ]
        self.space_mark_staates =     [ line_start,   arg_end,    arg_end,    arg_space,   arg_space,   arg_qchar,   arg_qchar,   arg_end,    arg_qchar,   arg_schar,   arg_qchar ]
        self.back_slash_mark_states = [ arg_start,    arg_char,   arg_char,   arg_start,   arg_start,   arg_qslash,  arg_qslash,  arg_char,   arg_qslash,  arg_qchar,   arg_qslash]
        self.quote_mark_states =      [ arg_qstart,   arg_qpart,  arg_qpart,  arg_qstart,  arg_qstart,  arg_qend,    arg_qend,    arg_qpart,  arg_qend,    arg_qchar,   arg_qend  ]
        self.line_end_mark =          [ line_end ,    larg_end,   larg_end,   line_end,    line_end,    larg_end,    larg_end,    larg_end,   larg_end,    line_send,   larg_end  ]

        self.parse_states = [ self.char_mark_states, self.space_mark_staates, self.back_slash_mark_states, self.quote_mark_states, self.line_end_mark ]       

        if isinstance(arrayOrString, str):
            self.argumentList = self.parseCommandLine(arrayOrString)
        elif isinstance(arrayOrString, list):
            self.argumentList = arrayOrString
        else:
            self.argumentList = self.parseCommandLine(str(arrayOrString))
        self.namedList = {}
        self.unnamedList = []
        self.parse()
        self.length = len(self.argumentList)

    def show(self):
        print self.argumentList

    def parse(self):
        for argument in self.argumentList:
            if argument[0] == '-':
                parts = argument[1:].partition('=')
                self.namedList[parts[0]]=parts[2]
            else:
                self.unnamedList.append(argument)
        pass

    def showNamedList(self):
        print self.namedList
    
    def showUnnamedList(self):
        print self.unnamedList
        
    def parseCommandLine(self, commandLine):
        argv = []
        if commandLine != None and len(commandLine) > 0:
            commandLineSize = len(commandLine)
            index = 0
            charType = line_end_mark
            state = line_start
            parsingFinished = False
            argvCount = 0

            argValue = ""

            state = line_start

            while not parsingFinished:
                current = 0
                if index < commandLineSize:
                    current = commandLine[index]

                if current == 0:
                    charType = line_end_mark
                elif current == ' '  or current == '\t':
                    charType = space_mark
                elif current == '\\':
                    charType = back_slash_mark
                elif current == '\"':
                    charType = quote_mark
                else:
                    charType = char_mark

                state = self.parse_states[charType][state]

                if state == arg_start or state == arg_char or state == arg_qchar:
                    argValue += current
                elif state == arg_end or state == larg_end:
                    if len(argValue) > 0:
                        argv.append(argValue)
                        argValue = ""
                    parsingFinished = state == larg_end
                elif state == arg_qstart:
                    argValue = ""
                elif state == line_start or state == arg_space or state == arg_qend or state == arg_qpart or state == arg_qslash:
                    pass
                elif state == arg_schar:
                    argValue += '\\'
                    argValue += current
                elif state == line_send:
                    argValue += '\\'
                    argv.append(argValue)
                    argValue = ""
                    parsingFinished = True
                elif state == line_end:
                    parsingFinished = True

                index += 1
        return argv


class SubstitutionClass(object):
    """ Substitution class. 
		It supports substitution in a string in form of 
            %WORD%  (windows) or 
            ${WORD} (unix)
        from different sources like global environment variables, command line arguments,
		and predefined identifiers 'drive' and 'currentfolder'.
        Form of the substitution template depends on parameter type that can be equal to 
        'windows' or 'unix'.
	"""
    def __init__(self, argv, type):
        argObject = Arguments(argv)
        self._namedList = argObject.namedList.copy()
        self._template = '\$\{[^\}]*\}'
        self._leftIndent = 2
        self._rightIndent = 1
        if type == "windows":
            self._template = '%[^%]*%'
            self._leftIndent = 1
            self._rightIndent = 1
        elif type != "unix":
            raise Exception("Incorrect type of substitution format for SubstitutionClass - " + type + ". Expected 'windows' or 'unix'")

    def Substitute(self, line, delegate):
        # find environment/system variables in the string 
        # replace it with the variable value
        resultLine = line
        reObject = re.compile(self._template)
        
        while resultLine != None:
            entry = reObject.search(resultLine)
            if entry == None:
                break
            
            variableName = entry.string[entry.start()+self._leftIndent:entry.end()-self._rightIndent]
            
            substituteValue = None
            if len(self._namedList) > 0 and self._namedList.has_key(variableName):
                substituteValue = self._namedList[variableName]
                
            if os.environ.has_key(variableName):
                substituteValue = os.environ[variableName]
            
            if (substituteValue == None or len(substituteValue) == 0) and delegate != None and delegate._substitutionRootNode != None:
                substituteValue = self.Substitute(GetChildNodeValue(delegate._substitutionRootNode, delegate._nameSpace, variableName, None), delegate)
            
            if (substituteValue == None or len(substituteValue) == 0) and variableName.lower() == "drive":
                substituteValue = os.path.splitdrive(os.path.abspath(os.path.curdir))[0]
    
            if (substituteValue == None or len(substituteValue) == 0) and variableName.lower() == "currentfolder":
                substituteValue = os.path.abspath(os.path.curdir)
    
            if (substituteValue == None or len(substituteValue) == 0) and variableName.lower() == "timestart":
                substituteValue = delegate._timestart
    
            if substituteValue == None:
                substituteValue = ""
    
            resultLine = entry.string[0:entry.start()] + substituteValue + entry.string[entry.end():]
        
        return resultLine

class Chdir:
    def __init__(self, newPath):
        self.savedPath = os.getcwd()
        os.chdir(newPath)
    
    def __del__(self):
        # print "Changing back to ", self.savedPath
        os.chdir(self.savedPath)

def _test():
    testData = [
        ('a b c', ['a', 'b', 'c']), 
        ('a,b,c', ['a,b,c']),
        ('', []),
        (' a    b    c    ', ['a', 'b', 'c']),
        ('"a"   b    "c"  ', ['a', 'b', 'c']),
        ('"a  b c"', ['a  b c']),
        ('\ta', ['a']),
        ('a -b="c" -d="a b c" -e="\a \b \t"d', ['a', '-b=c', '-d=a b c', '-e=\a \b \td']),
        ('\\', ['\\']),
        ('\\a', ['\\a']),
        ('\\a\\', ['\\a\\']),
        ('"\\""a"\\""', ['"a"'])
    ]
    ## test Arguments with string parameter
    for data in testData:
        arguments = Arguments(data[0])
        line = str(arguments.argumentList)
        if data[1] == arguments.argumentList:
            line += "\tpassed"
        else:
            line += "\tfailed *** !!! ***"
        print line
    
    
if __name__ == '__main__':
    _test()
