##############################################################################
#                                                                            #
# Irdeto Canada Corporation                                                  #
#                                                                            #
# FILE: create-wmdrm-key.pl                                                  #
#                                                                            #
# The software and information contained in this package ("Software") is     #
# owned by Irdeto Canada Corporation, its affiliates or licensors	         #
# ("Software Owners").  The Software is protected by U.S., Canadian, and     #
# international intellectual property laws and treaties and may contain      #
# patents, trademarks, copyrights, or other intellectual property rights of  #
# the Software Owners.  If you acquired this package without an appropriate  #
# agreement, please contact Irdeto Canada Corporation at:		             #
#                                                                            #
# phone +1 613 271 9446, email: info@irdeto.com				                 #
#                                                                            #
# The furnishing of this package does not constitute a license, express or   #
# implied, by estoppel or otherwise, to any patents, trademarks, copyrights, #
# or other intellectual property rights of the Software Owners.  Use of the  #
# Software without an appropriate agreement may constitute an infringement   #
# of the Software Owners� intellectual property rights and could subject you #
# to legal action.						                                     #
#                                                                            #
##############################################################################


use strict;
use warnings;
use MIME::Base64;


#### Script Entry Point: Check arguments, ensure three parameters received ####
my $argnum;
my $numArgs = $#ARGV + 1;
print "Create-keys script invoked with $numArgs command-line arguments.\n";

if ($numArgs != 1)
{
    print "   Invocation error, expected: \n     perl create-keys.pl <playready_file> \n";
    exit;
}

print " WMDRM Private Key File: $ARGV[0]\n";
###############################################################################


###### WMDRM Private key: get the Private key information from file #######
my $PRD_PVK_FH;
my $buf1="";
my $prd_model_key_data="";
my $bytesRead;
my $keySize;

open( $PRD_PVK_FH, "<$ARGV[0]" ) or die "Can't open $ARGV[0] file: $!\n";
binmode($PRD_PVK_FH);
$bytesRead = read($PRD_PVK_FH, $buf1, 20, 0);
close($PRD_PVK_FH);

foreach (split(//, $buf1)) {
    $prd_model_key_data = $prd_model_key_data.sprintf("%02x", ord($_));
}
$keySize = sprintf("%02x",$bytesRead);

print "\n";
print "prd_model_key_data:\n";
print $prd_model_key_data;
print "\n";
print "model key length:\n";
print $keySize;
print "\n";


######## Construct WMDRM key based on msp160 template ##########
my $prd_model_key = <<EOS;
30 81 e5
      02 01 01
      04 $keySize
            $prd_model_key_data
      a0 81 9b 30
      8198020101302006072a8648ce3d0101
      02150089abcdef012345672718281831
      415926141424f7302c041437a5abccd2
      77bce87632ff3d4780c009ebe4149704
      140dd8dabf725e2f3228e85f1ad78fde
      df9328239e0429048723947fd6a3a1e5
      3510c07dba38daf0109fa12044574491
      1075522d8c3c5856d4ed7acda379936f
      02150089abcdef012345672716b26eec
      14904428c2a675020101a12c032a0004
      3c94c9d2039bbfef93a5bfa115aadf21
      8523511009ae36119b6d520143bc3b8e
      2436929908c81f07
EOS
###############################################################################


############## Subroutine to create the two required .pem files ###############
sub output_pem_key($$) {
  local $_ = $_[0];
  s/(\s)//g;
  s/(..)/chr(hex($1))/ge;
  open PEM, "> $_[1]";
  print PEM "-----BEGIN EC PRIVATE KEY-----\n";
  print PEM encode_base64($_);
  print PEM "-----END EC PRIVATE KEY-----\n";
  close PEM;
  return 0;
}

output_pem_key($prd_model_key, "key.pem");

###############################################################################
