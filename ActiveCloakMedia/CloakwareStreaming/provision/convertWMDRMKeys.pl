##############################################################################
#                                                                            #
# Irdeto Canada Corporation                                                  #
#                                                                            #
# FILE: convertWMDRMKeys.pl                                                  #
#                                                                            #
# The software and information contained in this package ("Software") is     #
# owned by Irdeto Canada Corporation, its affiliates or licensors	         #
# ("Software Owners").  The Software is protected by U.S., Canadian, and     #
# international intellectual property laws and treaties and may contain      #
# patents, trademarks, copyrights, or other intellectual property rights of  #
# the Software Owners.  If you acquired this package without an appropriate  #
# agreement, please contact Irdeto Canada Corporation at:		             #
#                                                                            #
# phone +1 613 271 9446, email: info@irdeto.com				                 #
#                                                                            #
# The furnishing of this package does not constitute a license, express or   #
# implied, by estoppel or otherwise, to any patents, trademarks, copyrights, #
# or other intellectual property rights of the Software Owners.  Use of the  #
# Software without an appropriate agreement may constitute an infringement   #
# of the Software Owners� intellectual property rights and could subject you #
# to legal action.						                                     #
#                                                                            #
##############################################################################

use strict;
use warnings;
use utf8;
use File::DosGlob 'glob';

if ($] < 5.008)
{
   require "unicore/lib/Word.pl";
   require "unicore/lib/Digit.pl";
   require "unicore/lib/SpacePer.pl";
   require "unicore/To/Lower.pl";
   require "unicore/To/Fold.pl";
}

#### Script Entry Point: Check arguments, ensure three parameters received ####
my $argnum;
my $numArgs = $#ARGV + 1;

if ($numArgs != 2)
{
    print "   Invocation error, expected: \n     perl convertWMDRMKeys.pl <priv.dat> <provision root> \n";
    exit;
}

print " WMDRM Private Key File: $ARGV[0]\n";
print " Provision root directory: $ARGV[1]\n";

my $key = $ARGV[0];
my $installDir = $ARGV[1];

###############################################################################

my $valuesDir = "$installDir/values";
my $keysDir = "$installDir/keys";
my $signDir = "$installDir/keys/sign";
my $signWmdrmDir = "$installDir/keys/sign/wmdrm";

my $pgktxtsign = "$installDir/keys/sign/wmdrm/ID_DRM_DKT_WMDRMPD_GROUP_SIGN";
my $pgktxtdecrypt = "$installDir/values/ID_DRM_DKT_WMDRMPD_GROUP_DECRYPT";
my $fbktxt = "$installDir/keys/sign/wmdrm/ID_DRM_DKT_WMDRMPD_FALLBACK_SIGN";

# Convert binary key file into two text files.
my $keysize = 20; #20 byte size for key
my $outfname_sign;
my $outfname_decrypt;
my $outfname;
my $i;
my $infsize;
my $bindata;
my @hexbytes;
create_textkeyfiles(); # creates pgk.txt and fallback.txt

sub check_directories
{
	if (! -d $installDir) {
		print("***Error: $installDir does not exist.\n");
		exit(1);
	}
	
	if (! -d $valuesDir) {
		print("***Error: $valuesDir does not exist.\n");
		exit(1);
	}
	
	if (! -d $keysDir) {
		print("***Error: $keysDir does not exist.\n");
		exit(1);
	}

	if (! -d $signDir) {
		print("***Error: $signDir does not exist.\n");
		exit(1);
	}
	
	if (! -d $signWmdrmDir) {
		print("***Error: $signWmdrmDir does not exist.\n");
		exit(1);
	}

}

sub create_textkeyfiles
{
    open(HBINFH, "$key") or die "***Error: Cannot open input file $key\n";
    binmode HBINFH, ":raw";
    $infsize = -s $key;
    if( $infsize ne ($keysize*2)) {
       print "***Error: Unexpected file size for $key.\n";
       close(HBINFH);
       exit(1);
    }
    read(HBINFH, $bindata, $keysize);
    
    $outfname_sign = $pgktxtsign;
    $outfname_decrypt = $pgktxtdecrypt;
    
    unlink $pgktxtsign;
    if (!open(NEWFHSIGN, ">$outfname_sign")) {
       unlink $pgktxtsign;
       die "***Error: Unable to create text file: $outfname_sign\n";
    }
    
    unlink $pgktxtdecrypt;
    if (!open(NEWFHDECRYPT, ">$outfname_decrypt")) {
       unlink $pgktxtdecrypt;
       die "***Error: Unable to create text file: $outfname_decrypt\n";
    }

	print NEWFHSIGN $bindata;
	print NEWFHDECRYPT $bindata;

    close(NEWFHSIGN);
    close(NEWFHDECRYPT);
    
    read(HBINFH, $bindata, $keysize);
    
    close(HBINFH);
    
    $outfname = $fbktxt;
    
    unlink $fbktxt;
    if (!open(NEWFH, ">$outfname")) {
       unlink $fbktxt;
       die "***Error: Unable to create text file: $outfname\n";
    }
    
	print NEWFH $bindata;
	
    close(NEWFH);
}