#! /bin/bash
#
#                                                                            
# Irdeto Canada Corporation                                                  
#                                                                            
# FILE: setenv.sh                                                            
#                                                                            
# The software and information contained in this package ("Software") is     
# owned by Irdeto Canada Corporation, its affiliates or licensors			 
# ("Software Owners").  The Software is protected by U.S., Canadian, and     
# international intellectual property laws and treaties and may contain      
# patents, trademarks, copyrights, or other intellectual property rights of  
# the Software Owners.  If you acquired this package without an appropriate  
# agreement, please contact Irdeto Canada Corporation at:					 
#                                                                            
# phone +1 613 271 9446, email: info@irdeto.com							     
#                                                                            
# The furnishing of this package does not constitute a license, express or   
# implied, by estoppel or otherwise, to any patents, trademarks, copyrights, 
# or other intellectual property rights of the Software Owners.  Use of the  
# Software without an appropriate agreement may constitute an infringement   
# of the Software Owners' intellectual property rights and could subject you 
# to legal action.															 
#                                                                            
#
# Syntax:  setenv.sh caller_script target_os target_arch
#
# This script is called by the createKeyStore.sh and createACV.sh 
# to setup environment variables need by these scripts. 
#
unset PERLLIB

SCRIPT_NAME=$1
TARGET_OS=$2
TARGET_ARCH=$3

# Less than 3 arguments? Print the help text:
if [ $# -lt 3 ] ; then
cat << HELP
  Error: Invalid input paramters!
  Usage: setenv.sh caller_script target_os target_arch
HELP
	exit 1
fi


if [ ${TARGET_OS} != "ios" -a ${TARGET_OS} != "ios-simulator" -a ${TARGET_OS} != "android" -a ${TARGET_OS} != "windows" -a ${TARGET_OS} != "linux" ]
then
	echo "Error: ${TARGET_OS} is not a valid TARGET_OS"
	exit 1
fi

if [ ${TARGET_OS} == "android" -a ${TARGET_ARCH} != "arm" ]
then
	echo "Error: ${TARGET_ARCH} is not a valid TARGET_ARCH for android"
	exit 1
fi

if [ ${TARGET_OS} == "ios-simulator" -a ${TARGET_ARCH} != "i386" ]
then
	echo "Error: TARGET_ARCH must be 'i386' for TARGET_OS=$TARGET_OS"
	exit 1
fi

if [ ${TARGET_OS} != "ios-simulator" -a ${TARGET_OS} != "windows" -a ${TARGET_ARCH} == "i386" ]
then
	echo "Error: TARGET_ARCH='i386' is invalid for TARGET_OS=$TARGET_OS"
	exit 1
fi
if [ ${TARGET_OS} == "windows" -a ${TARGET_ARCH} != "i386" ]
then
	echo "Error: TARGET_ARCH must be 'i386' for TARGET_OS=$TARGET_OS"
	exit 1
fi


# fileTest: Determine if a file exists and is readable
# $1 = name of variable containing filename
# $2 = filename
fileTest()
{
    if [ -z "${2}" ]
    then
        echo "${1} is not set."
        exit 1
    elif [ ! -r "${2}" ]
    then
        echo "${2} does not exist or is unreadable."
        exit 1
    fi
}


# test to see if a variable is set
# $1 = name of variable
# $2 = value of the variable
varTest()
{
    if [ -z $2 ]
    then
        echo "$1 is not set."
        exit 1
    fi
}

if [ -n $DEBUG ]
then
    export DEBUG_FLAG=-debug
fi

# Test for the ACM_DIST_DIR installation 
BIN_DIR=
DATA_DIR=

if [ -z "${ACM_DIST_DIR}" ]
then
    # in case of some customers still using the obsolete 'PLAYREADY_DIST_DIR' variable 
    if [ -n "${PLAYREADY_DIST_DIR}" ]
    then 
	  echo "WARNING: Using the obsolete PLAYREADY_DIST_DIR for ACM_DIST_DIR!"
      export ACM_DIST_DIR=${PLAYREADY_DIST_DIR}
    fi
fi

if [ -n "${ACM_DIST_DIR}" ]
then 
    echo "Using ACM_DIST_DIR"
    echo "TARGET_OS=${TARGET_OS}"
    BIN_DIR=${ACM_DIST_DIR}/CloakwareStreaming/bin
    CAT_LIB=${ACM_DIST_DIR}/CloakwareStreaming/lib
    echo "BIN_DIR=${BIN_DIR}"
    echo "CAT_LIB=${CAT_LIB}"
	export EXPORTED_RANDOMSEED=$(cat "${BIN_DIR}/internal/css/dat/RandomSeed")
elif [ -n "${AC_DIST}" ]
then
	echo "Using AC_DIST"
	BIN_DIR=${AC_DIST}/bin
	DATA_DIR=${AC_DIST}/demo
	CAT_LIB=${AC_DIST}/lib
	export EXPORTED_RANDOMSEED=$(cat "${AC_DIST}/demo/RandomSeed")
else
	BIN_DIR=${PWD}/../bin
	CAT_DIR=${PWD}/../lib
	echo "Warning: Unable to find ACM_DIST_DIR, or AC_DIST!"
	echo "         Attempting to use default location of $BIN_DIR"
fi

if [ -z ${DATA_DIR} ]
then
   DATA_DIR=${BIN_DIR}/internal/css/dat
fi


export INDIVIDUALIZATION_TOOL="${BIN_DIR}/ACIndividualization"
export ASSET_PROTECTION_TOOL="${BIN_DIR}/ACResourceProtection"

INDIVIDUALIZATION=("${INDIVIDUALIZATION_TOOL}" ${DEBUG_FLAG} -set_access_mode RW -compatible_agent "${CAT_LIB}/i${EXPORTED_RANDOMSEED}")
ASSET_PROTECTION=("${ASSET_PROTECTION_TOOL}" -randomseed ${EXPORTED_RANDOMSEED} ${DEBUG_FLAG} -target-arch ${TARGET_ARCH} -target-os $TARGET_OS -compatible_agent "${CAT_LIB}/i${EXPORTED_RANDOMSEED}")

if [ ${TARGET_OS} != "android" ]
then
	INDIVIDUALIZATION=("${INDIVIDUALIZATION[@]}" -set_nodelocking_mode Client)
fi

export INDIVIDUALIZATION
export ASSET_PROTECTION

export RPM_WORKING_FOLDER=../ProvisioningTemp/

fileTest "INDIVIDUALIZATION_TOOL" "${INDIVIDUALIZATION_TOOL}"
fileTest "ASSET_PROTECTION_TOOL" "${ASSET_PROTECTION_TOOL}"

LICENSE="${BIN_DIR}/internal/css/config/license.xml"
if [ ! -r "$LICENSE" ]
then
LICENSE="$XC_LICENSE"
fi
fileTest "LICENSE" "${LICENSE}"

echo "Using tools in ${BIN_DIR}"
