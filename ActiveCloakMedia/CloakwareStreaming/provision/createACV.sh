#! /bin/bash
#
#
# Irdeto Canada Corporation                                                  
#                                                                            
# FILE: createACV.sh                                                         
#                                                                            
# The software and information contained in this package ("Software") is     
# owned by Irdeto Canada Corporation, its affiliates or licensors			 
# ("Software Owners").  The Software is protected by U.S., Canadian, and     
# international intellectual property laws and treaties and may contain      
# patents, trademarks, copyrights, or other intellectual property rights of  
# the Software Owners. If you acquired this package without an appropriate  
# agreement, please contact Irdeto Canada Corporation at:					 
#                                                                            
# phone +1 613 271 9446, email: info@irdeto.com							     
#                                                                            
# The furnishing of this package does not constitute a license, express or   
# implied, by estoppel or otherwise, to any patents, trademarks, copyrights, 
# or other intellectual property rights of the Software Owners.  Use of the  
# Software without an appropriate agreement may constitute an infringement   
# of the Software Owners' intellectual property rights and could subject you 
# to legal action.															 
#                                                                            
#
# Syntax:  ./createACV.sh <APPLICATION_HOST_PATH> <APPLICATION_ROOT_MODULE> <TARGET_OS> <TARGET_ARCH>
#
# This script is used to sign a binary to be used with ActiveTrust Integrity 
# Verification Feature. 
#
# Prerequisite environment variables:
#   CAT_INSTALL_DIR - parent directory of the BIN folder containing 
#                     the ACResourceProtection and ACIndividualization
#
# Parameters:
#     - APPLICATION_HOST_PATH -- path on the host machine where you can 
#                                find the application
#     - APPLICATION_ROOT_MODULE -- the name of the application
#     - TARGET_OS -- OS running on target device e.g. android, ios_simulator, ios
#     - TARGET_ARCH -- Platform architecture e.g. arm, i386, armv7 etc.
#
# Output:
#   This scripts creates a file named 'acv.dat' in the subdir 'data' of 
#   the current directory. 
#
#

. ./setenv.sh ${0} ${3} ${4}
if [ $? -gt 0 ]; then
	exit 1
fi

# fileTest: Determine if a file exists and is readable
# $1 = name of variable containing filename
# $2 = filename
fileTest()
{
    if [ -z "${2}" ]
    then
        echo "${1} is not set"
        exit 1
    elif [ ! -r "${2}" ]
    then
        echo "${2} does not exist or is unreadable."
        exit 1
    fi
}

#Command check for each bash command, if not return 0, the script will exit and pop up error message
CommandCheck()
{
	if [ $(echo $?) != 0 ]; then
		echo "Error: \"${1}\""
		exit -1
	fi
}

APPLICATION_HOST_PATH=${1}
APPLICATION_ROOT_MODULE=${2}
TARGET_OS=${3}


fileTest "Application" "${APPLICATION_HOST_PATH}/${APPLICATION_ROOT_MODULE}"

APPLICATION_DEVICE_PATH=./
APPLICATION_CONTEXT_VOUCHER=acv.dat

# NOTE: tweak this number if performance is affected.
# in PFIV mode, the larger this number is, the faster each IV verification performs. 
IN_MEM_IV_PRECENTAGE=100


LIB_EXT=.so
EXE_EXT= 
LIB_PRE=lib 

# save the current path
OLDPWD=`pwd`

# set stripping command
STRIP=/usr/bin/strip

# delete the previous KEYSTORE in current folder
rm -f "${APPLICATION_CONTEXT_VOUCHER}"
CommandCheck "Failed to remove the previous KEYSTORE $APPLICATION_CONTEXT_VOUCHER in current folder"

rm -f "data/${APPLICATION_CONTEXT_VOUCHER}"
CommandCheck "Failed to remove the previous KEYSTORE data/${APPLICATION_CONTEXT_VOUCHER}"

# create the output directory and working directory
mkdir -p "${RPM_WORKING_FOLDER}"
if [ $? -gt 0 ]; then
    echo "Error: Unable to create ${RPM_WORKING_FOLDER} !"
	exit 1
fi
chmod a+w "${RPM_WORKING_FOLDER}"

# clean and copy files to working folder, then cd to that location
rm -rf "${RPM_WORKING_FOLDER}/*"
cp -rf * "${RPM_WORKING_FOLDER}/."
if [ $? -gt 0 ]; then
    echo "Error: Unable to copy provisioning files to ${RPM_WORKING_FOLDER} !"
    exit 1
fi

# do symbol stripping on binary files - only for iOS right now
if [ ${TARGET_OS} == "ios" ]; then
    fileTest "STRIP" "${STRIP}"

    $STRIP "${APPLICATION_HOST_PATH}/${APPLICATION_ROOT_MODULE}"
    if [ $? -gt 0 ]; then
        echo "Error: Unable to strip ${APPLICATION_HOST_PATH}/${APPLICATION_ROOT_MODULE} !"
        exit 1
    fi
fi


cp "${APPLICATION_HOST_PATH}/${APPLICATION_ROOT_MODULE}" "${RPM_WORKING_FOLDER}/${APPLICATION_ROOT_MODULE}"
if [ $? -gt 0 ]; then
    echo "Error: Unable to copy application module files to ${RPM_WORKING_FOLDER} !"
    exit 1
fi
cd "${RPM_WORKING_FOLDER}"
chmod -R +rwx *

# Create the keystore we'll be appending to
echo ""
echo "Creating ${APPLICATION_CONTEXT_VOUCHER} using ACResourceProtection"
echo "\"${ASSET_PROTECTION[@]}\" -create_secure_store ${APPLICATION_CONTEXT_VOUCHER} -algorithm AES -key_bits 128 -iv_bits 128"
"${ASSET_PROTECTION[@]}" -create_secure_store ${APPLICATION_CONTEXT_VOUCHER} -algorithm AES -key_bits 128 -iv_bits 128 

if [ $? -gt 0 ]; then
    echo "Error: Unable to create application keystore file!"
    exit 1
fi

ACV_SIGN_OPTION="-module_signed_in_store"
ACV_OPTIONS="${ACV_OPTIONS} -root_module $APPLICATION_ROOT_MODULE"
ACV_OPTIONS="${ACV_OPTIONS} -ac_agent $APPLICATION_ROOT_MODULE"
ACV_OPTIONS="${ACV_OPTIONS} ${ACV_SIGN_OPTION} ${IN_MEM_IV_PRECENTAGE} ${APPLICATION_ROOT_MODULE} ${APPLICATION_DEVICE_PATH}"

# Add the assets from externlibs directory
for file in externlibs/*
do
    if [ -e "${file}" ]
    then
        ACV_OPTIONS="${ACV_OPTIONS} ${ACV_SIGN_OPTION} ${IN_MEM_IV_PRECENTAGE} ${file} ${APPLICATION_DEVICE_PATH}"
	fi
done

echo ""
echo "**** Protecting application using ACResourceProtection ****"
echo "\"${ASSET_PROTECTION[@]}\" ${APPLICATION_CONTEXT_VOUCHER} -pfiv -protect_application ${ACV_OPTIONS}"
"${ASSET_PROTECTION[@]}" ${APPLICATION_CONTEXT_VOUCHER} -pfiv -protect_application ${ACV_OPTIONS}
if [ $? -gt 0 ]; then
    echo "Error: Unable to protect the above application module!"
    exit 1
fi

echo ""
echo "**** Calling ACIndividualization ****"
echo "\"${INDIVIDUALIZATION[@]}\" ${APPLICATION_CONTEXT_VOUCHER}"
"${INDIVIDUALIZATION[@]}" ${APPLICATION_CONTEXT_VOUCHER}
if [ $? -gt 0 ]; then
	echo "Error: Unable to perform application individualization!"
	exit 1
fi

cp -f "${APPLICATION_CONTEXT_VOUCHER}" "${OLDPWD}/data/${APPLICATION_CONTEXT_VOUCHER}"
if [ $? -gt 0 ]; then
	echo "Error: Unable to copy ${APPLICATION_CONTEXT_VOUCHER} to ${OLDPWD}/data !"
	exit 1
fi

cd "${OLDPWD}"

if [ "${TARGET_OS}" != "android" ]; then
	cp -f "data/${APPLICATION_CONTEXT_VOUCHER}" "${APPLICATION_HOST_PATH}/data/${APPLICATION_CONTEXT_VOUCHER}"
	if [ $? -gt 0 ]; then
		echo "Error: Unable to copy ${APPLICATION_CONTEXT_VOUCHER} to \"${APPLICATION_HOST_PATH}/data\" !"
		exit 1
	fi
fi


rm -rf "${RPM_WORKING_FOLDER}"
exit 0
