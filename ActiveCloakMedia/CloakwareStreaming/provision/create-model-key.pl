##############################################################################
#                                                                            #
# Irdeto Canada Corporation                                                  #
#                                                                            #
# FILE: create-model-key.pl                                                  #
#                                                                            #
# The software and information contained in this package ("Software") is     #
# owned by Irdeto Canada Corporation, its affiliates or licensors	         #
# ("Software Owners").  The Software is protected by U.S., Canadian, and     #
# international intellectual property laws and treaties and may contain      #
# patents, trademarks, copyrights, or other intellectual property rights of  #
# the Software Owners.  If you acquired this package without an appropriate  #
# agreement, please contact Irdeto Canada Corporation at:		             #
#                                                                            #
# phone +1 613 271 9446, email: info@irdeto.com				                 #
#                                                                            #
# The furnishing of this package does not constitute a license, express or   #
# implied, by estoppel or otherwise, to any patents, trademarks, copyrights, #
# or other intellectual property rights of the Software Owners.  Use of the  #
# Software without an appropriate agreement may constitute an infringement   #
# of the Software Owners� intellectual property rights and could subject you #
# to legal action.						                                     #
#                                                                            #
##############################################################################
# To test if the output file from this script is a valid ec key.pem run
# openssl ec -in key.pem -text


use strict;
use warnings;
use MIME::Base64;


#### Script Entry Point: Check arguments, ensure three parameters received ####
my $argnum;
my $numArgs = $#ARGV + 1;
print "Create-keys script invoked with $numArgs command-line arguments.\n";

if ($numArgs != 1)
{
    print "   Invocation error, expected: \n     perl create-keys.pl <playready_file> \n";
    exit;
}

print " Playready Private Key File: $ARGV[0]\n";
###############################################################################


###### Playready Private key: get the Private key information from file #######
my $PRD_PVK_FH;
my $buf1="";
my $prd_model_key_data="";
my $bytesRead;
my $keySize;

open( $PRD_PVK_FH, "<$ARGV[0]" ) or die "Can't open $ARGV[0] file: $!\n";
binmode($PRD_PVK_FH);
$bytesRead = read($PRD_PVK_FH, $buf1, 32, 0);
close($PRD_PVK_FH);

foreach (split(//, $buf1)) {
    $prd_model_key_data = $prd_model_key_data.sprintf("%02x", ord($_));
}
$keySize = sprintf("%02x",$bytesRead);

print "\n";
print "prd_model_key_data:\n";
print $prd_model_key_data;
print "\n";
print "model key length:\n";
print $keySize;
print "\n";


######## Construct dtcp device key based on dtcp private/public data ##########
# The last 66 bytes are bogus - they refer to a public key that is never used
my $prd_model_key = <<EOS;
30 77
      02 01 01
      04 $keySize
            $prd_model_key_data
      a0 0a
            06 08
                  2a 86 48 ce 3d 03 01 07
      a1 44
            03 42
                  00 04 fe 7b 41 b1 88 1d
                  50 ee 2e 04 62 53 89 54
                  49 78 3e d3 9a 52 f6 13
                  f9 59 a5 44 a9 94 9a ec
                  48 02 18 0b 02 d5 79 78
                  57 80 b3 f8 e2 3b 34 06
                  33 99 bc 4f aa 2f 69 2e
                  ea 35 74 9f d7 c9 92 7c
                  79 ef                    
EOS
###############################################################################


############## Subroutine to create the two required .pem files ###############
sub output_pem_key($$) {
  local $_ = $_[0];
  s/(\s)//g;
  s/(..)/chr(hex($1))/ge;
  open PEM, "> $_[1]";
  print PEM "-----BEGIN EC PRIVATE KEY-----\n";
  print PEM encode_base64($_);
  print PEM "-----END EC PRIVATE KEY-----\n";
  close PEM;
  return 0;
}

output_pem_key($prd_model_key, "key.pem");

###############################################################################
