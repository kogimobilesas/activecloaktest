#! /bin/bash
#
#
# Irdeto Canada Corporation                                                  
#                                                                            
# FILE: java_env.sh                                                          
#                                                                            
# The software and information contained in this package ("Software") is     
# owned by Irdeto Canada Corporation, its affiliates or licensors			 
# ("Software Owners").  The Software is protected by U.S., Canadian, and     
# international intellectual property laws and treaties and may contain      
# patents, trademarks, copyrights, or other intellectual property rights of  
# the Software Owners.  If you acquired this package without an appropriate  
# agreement, please contact Irdeto Canada Corporation at:					 
#                                                                            
# phone +1 613 271 9446, email: info@irdeto.com							     
#                                                                            
# The furnishing of this package does not constitute a license, express or   
# implied, by estoppel or otherwise, to any patents, trademarks, copyrights, 
# or other intellectual property rights of the Software Owners.  Use of the  
# Software without an appropriate agreement may constitute an infringement   
# of the Software Ownersí intellectual property rights and could subject you 
# to legal action.															 
#                                                                            
#                                                           
# Syntax:  source ./java_env.sh
#
# This script is used to force Java to use 32-bit. 
#
#

#!/bin/sh

echo "#!/bin/sh" > $PWD/java
echo "/usr/bin/java -d32 \$@" >> $PWD/java
chmod a+x $PWD/java
export PATH=$PWD:$PATH


