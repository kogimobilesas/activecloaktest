#!/usr/bin/env python

import sys
import os
import os.path
import platform
import re
import xml.dom
import xml.dom.minidom
import math
import datetime
import sys
import glob
import shutil
import time
import subprocess

from datetime import date, time
from xml.dom import xmlbuilder, minidom, Node
from stat import *
from scripttools import *
from subprocess import call

g_helpNameText = """
  [python] jacSigningPreparation.py (-name=value)*
  
  Arguments: -name=value
             'name' - is an identifier that can be substituted with.
             'value' - is the text to substitute to. \n
"""

def Output(line):
    g_trace.WriteLine(line)

class Trace(object):
    """Trace"""
    def __init__(self):
        self._logFileName = None
    
    def WriteLine(self, line):
        print line
        if self._logFileName != None:
            traceFile = open(self._logFileName, "a")
            # line = line.replace(/\n/g, "\r\n")
            print >>traceFile, line
            traceFile.close()
    
    def SetLogFileName(self, name):
        if self._logFileName != None and len(self._logFileName) > 0 and name != None and len(name) > 0: 
            shutil.move(self._logFileName, name)
        self._logFileName = name
        
class JacSigningPreparation(object):
    """ Description of the test automation class.
    """
    def __init__(self, argv):
        self._argv = argv
        self._argObject = Arguments(argv)
        self._subsObject = SubstitutionClass(argv, "unix")
        self._hostDirectory = os.path.abspath(os.path.dirname(sys.argv[0]))
        self._currentDirectory = os.getcwd()
        self._scriptName = os.path.basename(sys.argv[0])
        self._templateName = "irdeto_java_access_config_template.xml"
        self._configName = "irdeto_java_access_config.xml"
        self._defaultProperties = "default.properties"
        self._localProperties = "local.properties"
        self._defaultCopyProperties = "default (copy).properties"
        self._localCopyProperties = "local (copy).properties"
        self._jacSignToolLocation = "../javasecurity/tools/gcc_4.4_android_arm/commandLineSign"
        self._jacSignToolName = "commandLineSign"
        self._androidApplicationProject = self._subsObject.Substitute("${ANDROID_APP_PROJECT}", None)
        self._androidTargetOSImageID = 1
        self._androidTargetOSImageIDParameterName = "ANDROID_TARGET_OS_ID"
        print 'hostDirectory = ', self._hostDirectory
        print 'currentDirectory = ', self._currentDirectory
        print 'scriptName = ', self._scriptName
        
        print "platform = ", platform.system()
        
        if platform.system() == 'Windows':
            self._separator = "\\"
        elif platform.system() == 'Darwin' or platform.system() == "Linux":
            self._separator = "/"

        return
    
    def EnableLogFile(self):
        print "EnableLogFile: self._buildId = " + self._buildId
        if self._logfileName == None:
            self._logfileName = self._buildId + "-" + self._timestart + ".log"
        
        if self._logfilePath == None:
            if self._buildReportLocation != None:
                self._logfilePath = self._buildReportLocation
            else:
                self._logfilePath = os.path.abspath(os.path.curdir)
            self._logfilePath += self._separator + self._logfileName
        print "self._logfilePath = " + self._logfilePath
             
        if os.path.exists(self._logfilePath):
            os.remove(self._logfilePath)
        
        g_trace.SetLogFileName(self._logfilePath)
        return
        
    def DisableLogFile(self):
        g_trace.SetLogFileName(None)
    
    def ParseArguments(self):
        '''ParseArguments'''
            
        command = None
        unnamedArgs = self._argObject.unnamedList
        namedArgs = self._argObject.namedList
        
        if self._argObject.length == 0 or len(unnamedArgs) <= 0:
            command = "prepare"
        elif len(unnamedArgs) == 1:
            unnamed0 = unnamedArgs[0]
            lower0 = unnamed0.lower()
            if lower0 == "help":
                command = lower0
            else:
                raise Exception("Several command found in the command line. Expected only one - 'help'");
        elif len(unnamedArgs) > 2:
            raise Exception("Several command found in the command line. Expected only one - 'help'")
        
        if len(namedArgs) > 0 and namedArgs.has_key(self._androidTargetOSImageIDParameterName):
            self._androidTargetOSImageID=namedArgs[self._androidTargetOSImageIDParameterName]
        else:
            raise Exception("Android target OS image ID was not passed as a parameter with key=ANDROID_TARGET_OS_ID")
        return command
        
    def PrepareJavaAccessConfigFile(self):
        '''The function prepares irdeto_jave_access_config.xml file'''
        templatePath = Combine(self._hostDirectory, self._templateName)
        configPath = Combine(self._hostDirectory, self._configName)
        
        if not os.path.exists(templatePath):
            raise Exception("Could not find a config template file - " + templatePath)

        templateFile = open(templatePath, "r")
        if templateFile == None:
            raise Exception("Could not open a config template file to read - " + templatePath)
        configFile = open(configPath, "w")
        if configFile == None:
            raise Exception("Could not open a config file to write - " + configPath)
        for line in templateFile:
            outputLine = self._subsObject.Substitute(line, None)
            configFile.write(outputLine)
        configFile.flush()
        configFile.close()
        templateFile.close()
        
    def PreparePropertyFiles(self):
        '''This method is responsible for default.properties 
           and local.properties files preparation'''
        jacSignToolAbsLocation = Combine(self._hostDirectory, self._jacSignToolLocation)
        call(["android", "update", "project", "-p", jacSignToolAbsLocation, "-n", self._jacSignToolName, "-t",  self._androidTargetOSImageID])
        return
        
    def Prepare(self):
        '''main method of the JacSigningPreparation class'''
        print "perform JAC signing preparation"
        result = 0
        try:
            command = self.ParseArguments()
            CheckGlobalVariable("ANDROID_SDK_DIR")
            self.PrepareJavaAccessConfigFile()
            self.PreparePropertyFiles()
            
        except Exception, exception:
            Output("\nError: " + str(exception))
            Output("Script completed unsuccessfully")
            result = -3
        return result
            
    def OutputLogo(self):
        # output logo information
        print "Irdeto (R) JAC Signing Preparation, Version 0.1"
        print "Copyright (C) Irdeto Company, 2011. All rights reserved.\n"

    def OutputHelpInfo(self):
        self.OutputLogo()
        print g_helpNameText

def Main():
    result = 0
    jacSigningPreparation = JacSigningPreparation(sys.argv[1:])
    result = jacSigningPreparation.Prepare()
    return result

g_trace = Trace()
    
if __name__ == '__main__':
    sys.exit(Main())
