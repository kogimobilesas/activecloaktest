#!/bin/bash

# fileTest: Determine if a file exists and is readable
# $1 = name of variable containing filename
# $2 = filename
fileTest()
{
    if [ -z $2 ]
    then
        echo "$1 is not set."
        exit 1
    elif [ ! -r $2 ]
    then
        echo "$2 does not exist or is unreadable."
        exit 1
    fi
}

#Command check for each bash command, if not return 0, the script will exit and pop up error message
CommandCheck()
{
	if [ $(echo $?) != 0 ]; then
		echo "Error: \"${1}\""
		exit -1
	fi
}

# determine location of the bash script
if [ "${BASH_SOURCE}" = "" ]; then
    _SCRIPT_PATH=${0}
else
    _SCRIPT_PATH=${BASH_SOURCE}
fi
if [ "${_SCRIPT_PATH}" = "" ]; then
    echo "ERROR: Could not determine the bash script location."
    echo "       \${0} and \${BASH_SOURCE} values are empty."
    echo "       Abort the script."
    exit 1
fi
_SCRIPT_NAME=${_SCRIPT_PATH##*/}
_SCRIPT_DIR=${_SCRIPT_PATH%${_SCRIPT_NAME}}
# determine the full path of the bash script
if [ "${_SCRIPT_DIR}" = "" ]; then
    _MY_FULL_PATH=$(echo ${PWD}/${_SCRIPT_NAME})
else
    _MY_FULL_PATH=$(cd "${_SCRIPT_DIR}" && echo ${PWD}/${_SCRIPT_NAME})
fi 
_MY_HOME=`dirname "${_MY_FULL_PATH}"`
_INITIAL_DIR=$(pwd)

if [ "${DIST_DIR}" = "" ]
then
  DIST_DIR=${_MY_HOME}/..
  # verify that the script was called from inside of the distribution package
  if ! [[ -d "${DIST_DIR}/javasecurity" && -d "${DIST_DIR}/bin" ]]; then
    # if it was not, let's consider it as development environment
    DIST_DIR=${_MY_HOME}/../dist
  fi
fi
CAT_HOME=${DIST_DIR}

# JAC related settings
JAC_INSTALL_DIR=${DIST_DIR}/javasecurity
JAC_SHARED_LIB=libjavasign.so
JAC_SHARED_LIB_HOST_PATH=${JAC_INSTALL_DIR}/tools/gcc_4.4_android_arm/commandLineSign/libs/armeabi
JAC_COMMAND_LINE_SIGNING_FILE=${JAC_INSTALL_DIR}/tools/gcc_4.4_android_arm/commandLineSign/src/com/irdeto/commandline/signing/JacSigningActivity.java
JAC_SHARED_LIB_DEVICE_PATH=./

WORK_SPACE_HOME=${1}
WORK_SPACE_HOME_BIN=${2}
APK_FILE=${3}
EMULATOR_HOME=${4}
ANDROID_TARGET_OS_ID=${5}
if [ "${6}" = "-s" ]; then
    SPECIFIC_DEVICE=
    SIGN_OPERATION=${6}
elif [ "${6}" != "" ]; then 
    SPECIFIC_DEVICE=${6}
    SIGN_OPERATION=${7}
else
    SPECIFIC_DEVICE=
    SIGN_OPERATION=${7}
fi

if [ "${SPECIFIC_DEVICE}" = "" ]; then
    SPECIFIC_DEVICE_COMMENT=
    SPECIFIC_DEVICE_ADB_ARG=
    SPECIFIC_DEVICE_JAC_ARG=
else
    SPECIFIC_DEVICE_COMMENT="${SPECIFIC_DEVICE} "
    SPECIFIC_DEVICE_ADB_ARG=" -s ${SPECIFIC_DEVICE}"
    SPECIFIC_DEVICE_JAC_ARG=" -sn ${SPECIFIC_DEVICE}"
fi

MY_SIZE=""

SIGNING_TOOL_DEFAULT_LOCATION=/data/local

#this is a fixed name
JACVoucherHandle="PPOJUJ"
HMACSHA256_HANDLE="YYT76H"
HMACSHA256_KEY="87c7e446787a827f9aa0a247eb32f3a987c7e446787a827f9aa0a247eb32f3a9";
STORE_KEY="87c7e446787a827f9aa0a247eb32f3a9"
STORE_IV="4bd74865f96326d8dd891a4c1fad2162"
#this is a fixed name

MYSTORE="jac_store.dat"

usage(){
    echo ""
    echo "Command line: \$0 \$1 \$2 \$3 \$4 \$5 [\$6] -s"
    echo ""
    echo "where, first parameter is path to the application project" 
	echo "where, second parameter is path to the application project's apk output directory" 
    echo "where, third parameter is name of the apk in the above project" 
	echo "where, fourth parameter is the directory on the device where the signing will be done" 
	echo "where, fifth parameter is the Android target OS image ID"
    echo "where, sixth parameter is an optional Android device ID. This device will be used"
    echo "       to JAC-sign the target application"  
    echo "where, -s sign operation" 
    echo ""
    exit 1
}

changeSigningDir() {
    echo ""
    echo "Changing ${SIGNING_TOOL_DEFAULT_LOCATION} to ${EMULATOR_HOME} in ${JAC_COMMAND_LINE_SIGNING_FILE}."
    echo "sed -i 's_${SIGNING_TOOL_DEFAULT_LOCATION}/irdeto_java_access_config.xml_${EMULATOR_HOME}/irdeto_java_access_config.xml_g' ${JAC_COMMAND_LINE_SIGNING_FILE}"
    sed -i "s_${SIGNING_TOOL_DEFAULT_LOCATION}/irdeto\_java\_access\_config.xml_${EMULATOR_HOME}/irdeto\_java\_access\_config.xml_g" ${JAC_COMMAND_LINE_SIGNING_FILE}
    echo ""
}

initSecureStore(){

    echo ""
    echo "Initializing secure store"
    echo ""
    rm $MYSTORE  2>/dev/null
    
    EXPORTED_RANDOMSEED=$(cat ${DIST_DIR}/bin/internal/css/dat/RandomSeed)
    COMPATIBAL_AGENT_OPTION="-compatible_agent $CAT_HOME/lib/i${EXPORTED_RANDOMSEED}"
    
    #### IAC-BUG: NL disabled #### -set_nodelocking_mode Client
    INDIVIDUALIZATION="${CAT_HOME}/bin/ACIndividualization ${COMPATIBAL_AGENT_OPTION} -set_access_mode RW"
    RESOURCE_PROTECTION="${CAT_HOME}/bin/ACResourceProtection ${COMPATIBAL_AGENT_OPTION} -randomseed ${EXPORTED_RANDOMSEED} -target-arch arm -target-os android"

    echo "Creating Secure Store"
    $RESOURCE_PROTECTION -create_secure_store $MYSTORE -algorithm aes -key_data $STORE_KEY -iv_data $STORE_IV
	CommandCheck "Failed to create secure store with ${CAT_HOME}/bin/ACResourceProtection"
	
    MY_SIZE=`ls -al |grep $MYSTORE | awk '{print($5)}'`
    echo "OK. size: $MY_SIZE"

    echo "Inserting HAMC Key"
    $RESOURCE_PROTECTION -protect_data $MYSTORE -handle $HMACSHA256_HANDLE -data $HMACSHA256_KEY
	CommandCheck "Failed to insert HAMC Key with ${CAT_HOME}/bin/ACResourceProtection"
	
    MY_SIZE=`ls -al |grep $MYSTORE | awk '{print($5)}'`
    echo "OK. size: $MY_SIZE"

}

sign() {

    #change the signing dir in the JAC signing app
    changeSigningDir
	CommandCheck "Failed to change the signing directory in the JAC signing app."
    
    #create secure store for signing   
    initSecureStore
	CommandCheck "Failed to create secure store for signing"

	echo "check libjavasign.so"
	fileTest libjavasign.so $JAC_SHARED_LIB_HOST_PATH/$JAC_SHARED_LIB

    echo "Sign JAC shared library"
    $RESOURCE_PROTECTION -pfiv -protect_application $MYSTORE -module_signed_in_store 1 $JAC_SHARED_LIB_HOST_PATH/$JAC_SHARED_LIB $JAC_SHARED_LIB_DEVICE_PATH -root_module $JAC_SHARED_LIB_HOST_PATH/$JAC_SHARED_LIB -ac_agent $JAC_SHARED_LIB_HOST_PATH/$JAC_SHARED_LIB -sl_disable
	CommandCheck "Failed to sign JAC shared library with ${CAT_HOME}/bin/ACResourceProtection"

    echo "Run individualization tool"
    $INDIVIDUALIZATION $MYSTORE
	CommandCheck "Failed to individulize $MYSTORE with ${CAT_HOME}/bin/ACIndividualization"
        
    echo "Copying ${MYSTORE} to Android emulator ${SPECIFIC_DEVICE_COMMENT}at ${EMULATOR_HOME}/"
    adb${SPECIFIC_DEVICE_ADB_ARG} push ./${MYSTORE} ${EMULATOR_HOME}/${MYSTORE}
	CommandCheck "adb failed to copy ${MYSTORE} to Android emulator ${SPECIFIC_DEVICE_COMMENT}at $EMULATOR_HOME"

    echo "Copy runtime application: ${WORK_SPACE_HOME_BIN}/${APK_FILE} to ${SPECIFIC_DEVICE_COMMENT}device at ${EMULATOR_HOME}/ for signing"
    adb${SPECIFIC_DEVICE_ADB_ARG} push ${WORK_SPACE_HOME_BIN}/${APK_FILE} ${EMULATOR_HOME}/${APK_FILE}
	CommandCheck "adb failed to copy runtime application: ${WORK_SPACE_HOME_BIN}/${APK_FILE} to ${SPECIFIC_DEVICE_COMMENT}device at ${EMULATOR_HOME}/ for signing"

    #cd $CAT_HOME/bin
    $RESOURCE_PROTECTION -java_sign build -config_file ./irdeto_java_access_config.xml${SPECIFIC_DEVICE_JAC_ARG} 

    echo "Java sign completed"

    #the 30 seconds is not enough to finish the signing and the file copied is not complete. copy, check and wait if needed
    VOUCHER_CONTENT=""
    while [ "$VOUCHER_CONTENT" = "" ]
    do    
	    adb${SPECIFIC_DEVICE_ADB_ARG} pull ${EMULATOR_HOME}/irdeto_java_access.dat
		CommandCheck "adb failed to pull ${EMULATOR_HOME}/irdeto_java_access.dat"
	    VOUCHER_CONTENT=`cat irdeto_java_access.dat | grep EOF`
	    if [ "$VOUCHER_CONTENT" = "" ]; then
	    	echo wait 10 seconds for signing
		    sleep 10
        fi
    done 
   
    
# final delivery of secure store
    initSecureStore
	CommandCheck "Failed to delivery the secure store"

#   echo "Inserting JAC config items"
#   $RESOURCE_PROTECTION -protect_data $MYSTORE -handle JACConfigHandle -data_file ./JacConfigFile.txt MY_SIZE=`ls -al |grep $MYSTORE | awk '{print($5)}'`
#   echo "OK. size: $MY_SIZE"

    echo "Inserting JAVA voucher"
    $RESOURCE_PROTECTION -protect_data $MYSTORE -handle $JACVoucherHandle -data_file ./irdeto_java_access.dat 
	CommandCheck "Failed to insert JAVA voucher with ${CAT_HOME}/bin/ACResourceProtection"
    MY_SIZE=`ls -al |grep $MYSTORE | awk '{print($5)}'`
    echo "OK. size: $MY_SIZE"

#   echo "Inserting SecreteApps.apk"
#   $RESOURCE_PROTECTION -protect_data $MYSTORE -handle SecreteApps.apk -data_file /home/test/workspace/SecreteApps/bin/SecreteApps.apk
#    MY_SIZE=`ls -al |grep $MYSTORE | awk '{print($5)}'`
#    echo "OK. size: $MY_SIZE"

#   echo "Inserting ContactManager.apk"
#   $RESOURCE_PROTECTION -protect_data $MYSTORE -handle ContactManager.apk -data_file /home/test/jrong/workspace/ContactManager/bin/ContactManager.apk
#   MY_SIZE=`ls -al |grep $MYSTORE | awk '{print($5)}'`
#   echo "OK. size: $MY_SIZE"

    echo "Run individualization tool"
    $INDIVIDUALIZATION $MYSTORE 
	CommandCheck "Failed to individulize $MYSTORE"
    MY_SIZE=`ls -al |grep $MYSTORE | awk '{print($5)}'`
    echo "OK. size: $MY_SIZE"

    echo "Copying $MYSTORE to $WORK_SPACE_HOME/assets/"
    cp ./$MYSTORE $WORK_SPACE_HOME/assets/.
	CommandCheck "Failed to copy ./$MYSTORE to $WORK_SPACE_HOME/assets/., please check if this file is available or accessable"
}

./jacSigningPreparation.py -APK_FILE=${APK_FILE} -JAC_INSTALL_DIR=${JAC_INSTALL_DIR} -ANDROID_APP_PROJECT=${WORK_SPACE_HOME} -ANDROID_TARGET_OS_ID=${ANDROID_TARGET_OS_ID} -SIGNING_DIR=${EMULATOR_HOME}

if [ $# -lt 1 ]; then
    usage
	CommandCheck "Failed in calling function 'usage', please check if this function is available or accessable in jac_signing.sh."
elif [ "${SIGN_OPERATION}" = "-s" ]; then
    sign
	CommandCheck "Failed in calling function 'sign', please check if this function is available or accessable in jac_signing.sh. "
else
    usage
	CommandCheck "Failed in calling function 'usage', please check if this function is available or accessable in jac_signing.sh."
fi

