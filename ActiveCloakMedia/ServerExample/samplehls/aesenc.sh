#!/bin/bash 
openssl aes-128-cbc -e -in $1 -out $2 -p -nosalt -K $(cat $3) -iv $(cat $4)