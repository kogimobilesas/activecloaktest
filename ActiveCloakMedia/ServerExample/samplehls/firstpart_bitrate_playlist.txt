#!/bin/bash
clearfaststartkey=($(cat clearfaststartkey.txt))
contentkey=($(cat contentkey.txt))
fixedkeyfaststartkey=($(cat fixedkeyfaststartkey.txt))
# append X_IRDETO_KEY after the first #EXTINF
if [ $1 == 'enc_hls2' ] 
then
	sed -i "1,/#EXTINF*/ {/#EXTINF*/a $clearfaststartkey
	 }" $1\\\\\$2\\\\\$3
fi
if [ $1 == 'enc_hls2_fixedkey' ]
then
	sed -i "1,/#EXTINF*/ {/#EXTINF*/a $fixedkeyfaststartkey
	}" $1\\\\\$2\\\\\$3
fi
#add the reference of directory 
sed -i "s|^\\([^#]*.ts\\)|enc_hls2/\$2/\\\1|g" $1\\\\\$2\\\\\$3
# append content key to the place where input indicates
sed -i "1,/#EXTINF/! {
