#!/bin/bash
# copy the first part of bitrate playlist to here
while read line
do 
	echo "$line" >> create_bitrate_playlist.sh
done <firstpart_bitrate_playlist.txt
# append the key to the place where the input indicates
for (( i=1; i<=$1; i++ ))
do
	echo "1, /#EXTINF/! {" >> create_bitrate_playlist.sh
done
# append content key here
echo "1, 1 i \$contentkey" >> create_bitrate_playlist.sh
# append the end of }
for (( i=1; i<=$1; i++ ))
do
	echo "}" >> create_bitrate_playlist.sh
done
echo "}\" \$1\\\\\$2\\\\\$3" >> create_bitrate_playlist.sh
# in case of enc_hls2, replace enc_hls2/ with clear/ for the first 3 ts files 
echo "# in case of enc_hls2, replace enc_hls2/ with clear/ for the first 3 ts files " >> create_bitrate_playlist.sh
echo "if [ \$1 == 'enc_hls2' ]" >> create_bitrate_playlist.sh
echo "then" >> create_bitrate_playlist.sh
	for (( i=1; i<=$1; i++ ))
	do
		echo  "sed -i \"1,/enc_hls2/s/enc_hls2/clear/\" \$1\\\\\$2\\\\\$3" >> create_bitrate_playlist.sh
	done 
echo "fi" >> create_bitrate_playlist.sh
# in case of enc_hls2_fixedkey, replace enc_hls2/ with enc_hls2_fixed/ for the first 3 ts files 
echo "# in case of enc_hls2_fixedkey, replace enc_hls2/ with enc_hls2_fixed/ for the first 3 ts files " >> create_bitrate_playlist.sh
echo "if [ \$1 == 'enc_hls2_fixedkey' ]" >> create_bitrate_playlist.sh 
echo "then" >> create_bitrate_playlist.sh 
	for (( i=1; i<=$1; i++ ))
	do
		echo  "sed -i \"1,/enc_hls2\//s/enc_hls2\//enc_hls2_fixedkey\//\" \$1\\\\\$2\\\\\$3" >> create_bitrate_playlist.sh
	done 
echo "fi" >> create_bitrate_playlist.sh