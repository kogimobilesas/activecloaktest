#!/bin/bash 
perl hexdecode.pl < $1 > $1.tmp
openssl aes-128-cbc -e -in $1.tmp -out $2.tmp -p -nopad -nosalt -K $(cat $3) -iv 00000000000000000000000000000000
perl hexencode.pl < $2.tmp > $2
rm $1.tmp
rm $2.tmp

