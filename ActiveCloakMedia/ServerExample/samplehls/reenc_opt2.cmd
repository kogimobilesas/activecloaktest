REM This is the top-level command to encrypt a directory full of HLS transport stream files.
rmdir /Q /S enc_opt2

mkdir enc_opt2

perl hexdecode.pl < config\rawkey.txt > rawkey.bin
perl hexdecode.pl < config\rawiv.txt > rawiv.bin

REM generate top-level .m3u8 envelope
for %%f in (..\*.m3u8) do (
    bash makeenvelope.sh enc_opt2 ../%%~nxf ctrtemplate.txt
)        

REM encrypt individual .TS files from each bit-rate profile
for /D %%d in (..\*) do (
    if NOT "%%~nxd"=="HLSP" (
        mkdir enc_opt2\%%~nxd
        copy %%d\*.m3u8 enc_opt2\%%~nxd
    
        for %%f in (%%d\*.ts) do (
            crypt enc rawkey.bin rawiv.bin ..\%%~nxd\%%~nxf enc_opt2\%%~nxd\%%~nxf 
        )
    )
)

REM remove the temporary key/iv binaries
del /Q rawkey.bin
del /Q rawiv.bin