This folder contains sample scripts that convert normal unencrypted HLS into PlayReady-encrypted
HLS using the Irdeto scheme. (Please ensure this folder is named exactly as 'HLSP' in deployment.)

These scripts require the following additional tools:
- Envelope (requires PlayReady Server SDK 1.5.2 or later, and all prerequisites)
- bash (from cygwin)
- cat (from cygwin)
- basename (from cygwin)
- base64 (from cygwin)
- openssl 0.9.8h (or later)
- perl 5.10.1 (or later)
- .NET framework runtime (for Crypt.exe and BouncyCastle.dll - included in this folder)

To use it:
- place all of the above tools in your PATH environment variable
- copy this folder with original name "HLSP" into the raw media's root folder which contains the top-level m3u8 file
- open a command line and cd to this folder under the raw media folder 
(under the 'config' sub-folder: ) 
- customize rawkey.txt and rawiv.txt to the actual encryption key and IV to use for the TS files 
- customize kid.txt to be the kid to use for the playready license
- customize la.txt to be the license acquisition URL to use
- customize kid_fixedkey.txt, rawiv_fixedkey.txt, rawkey_fixedkey.txt to the actual parameters used for fixed key fast start.
(go back to the 'HLSP' folder: ) 

- perform the following operations: 
    1. For HLS 1.0, run 'reenc_opt2.cmd'. This will encrypt everything with AES CTR mode (so it can be decrypted with PlayReady). Encrypted files will be put in a subfolder of the current folder named 'enc_opt2'.

The process will take several minutes, depending on the number of files in the folder, and  it will be deleted and recreated.Top level manifest will have .prdy added to the end of it.


    2. For HLS 2.0, run 'reenc_hls2.cmd'. This will generate the following HLS 2.0 contents.  
	1). Three folders: clear, enc_hls2, enc_hls2_fixedkey. 
		Folder clear contains all the clear contents. 
		Folder enc_hls2 contains the protected  contents encrypted with AES_CTR mode and the parameters specified in rawkey.txt, kid.txt and rawiv.txt. 
  		Folder enc_hls_fixedkey contains the protected contents encrypted with AES_CTR mode and the parameters specified in rawkey_fixedkey.txt, kid_fixedkey.txt and rawiv_fixedkey.txt.
		Each folder has its own toplevel playlist which is a clear .m3u8 file. It can be used to play the contents in each individual folder.
	2). Bunch of clear_XXXX_faststart.m3u8 (depends on how many bitrates for the current contents) and a clear_faststart_index.m3u8 files. clear_faststart_index.m3u8 is the top level playlist for clear content fast start. It can be used to play the clear content fast start contents, which have the first 30 seconds clear contents for each bit rate. 
	3). Bunch of fixedkey_xxxx_faststart.m3u8 (depends on how many bitrates for the current contents) and a fixedkey_faststart_index.m3u8 files. fixedkey_faststart_index.m3u8 is the top level playlist for fixedkey content fast start. It can be used to play the fixekey content fast start contents, which has the first 30 seconds fixed key contents for each bit rate. Fixed key faststart needs a pre-acquired license to allow the decryption for fixed key encryped fast start contents.

The process will take several minuts, depending on the number of files in the folder.  
  
 



