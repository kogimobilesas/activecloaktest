#!/usr/bin/perl
use strict;
use warnings;
use MIME::Base64;
use Encode qw/encode/;
binmode STDOUT;

my $input = <STDIN>;

$input =~ m/(<.+>)/;
my $header = $1;

# Remove 00 inside
$header =~ s/\x0//g;

my $utf16  = encode('UTF-16LE', $header);
my $base64 = encode_base64($utf16);
$base64 =~ s/\n//g;
print STDOUT "$base64";