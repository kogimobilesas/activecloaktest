#!/bin/bash 
#concatenate the top-level m3u8, the raw key line, and the raw iv line.
cat $2 keytemplate.txt config/rawkey.txt ivtemplate.txt config/rawiv.txt $3 > rawenvelope.txt
  
#use the playready server SDK to create the envelope
Envelope Encrypt -kid $(cat config/kid.txt) -ck $(cat config/rawkey.txt | perl hexdecode.pl | base64) -la $(cat config/la.txt) rawenvelope.txt $1/$(basename $2).prdy

# get rid of temporary envelope file
rm rawenvelope.txt