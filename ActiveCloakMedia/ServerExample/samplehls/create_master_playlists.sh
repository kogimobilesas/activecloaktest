#!/bin/bash
# assumption: the structure of the master playlist is like this:
# #EXTM3U
# #EXT-X-STREAM-INF:PROGRAM-ID=1, BANDWIDTH=659000
# bitrate/prog_index.m3u8
# ....
#
# replace "bitrate/prog_index.m3u8" to "clear_bitrate_faststart.m3u8"
sed -i "s|^\([^#].*\)\/.*$|clear_\1_faststart.m3u8|g" $4
sed -i "s|^\([^#].*\)\/.*$|fixedkey_\1_faststart.m3u8|g" $5
# append playready header to master playlist for all 4 cases:
# clear fast start
# fixedkey fast start
# enc_hls2, content key
# enc_hls2_fixedkey, fixed key
cat "$6" >> $4
cat "$6" >> $5
cat "$6" >> $1\\$3
cat "$6" >> $2\\$3