﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Modes;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Crypto.Parameters;

namespace crypt
{
    class Program
    {
        static void PrintSyntax()
        {
            System.Console.WriteLine("Syntax: crypt <enc|dec|test> <rawkeyfile> <rawivfile> <infile> <outfile>");
        }
        static void Main(string[] args)
        {
            if (args.Length < 5)
            {
                PrintSyntax();
            }
            else
            {
                PerformOperation(args[0], args[1], args[2], args[3], args[4]);
            }

        }
        static void PerformOperation(string operation, string rawkeyfile, string rawivfile, string infile, string outfile)
        {
            if (operation.StartsWith("enc"))
            {
                Crypt(true, rawkeyfile, rawivfile, infile, outfile);
            }
            else if (operation.StartsWith("dec"))
            {
                Crypt(false, rawkeyfile, rawivfile, infile, outfile);
            }
            else if (operation.StartsWith("test"))
            {
                string test1 = infile + ".tmp";
                string test2 = infile + ".tmp2";
                Crypt(true, rawkeyfile, rawivfile, infile, test1);
                Crypt(false, rawkeyfile, rawivfile, test1, test2);

                byte[] file1 = File.ReadAllBytes(infile);
                byte[] file2 = File.ReadAllBytes(test2);

                int err = 0;
                if (file1.Length != file2.Length)
                {
                    System.Console.WriteLine("Files different sizes!");
                }
                else
                {
                    for (int i = 0; i < file1.Length; ++i)
                    {
                        if (file1[i] != file2[i])
                        {
                            ++err;
                        }
                    }
                }
                System.Console.WriteLine(err.ToString() + " errors.");

                File.Delete(test1);
                File.Delete(test2);

            }
            else
            {
                PrintSyntax();
            }
        }

        static byte[] CryptCTR(bool encrypt, byte[] key, byte[] iv, byte[] indata)
        {
            IBufferedCipher bc = CipherUtilities.GetCipher("AES/CTR/NoPadding");

            bc.Init(encrypt, new ParametersWithIV(new KeyParameter(key), iv));
            byte[] ret = bc.DoFinal(indata);

            return ret;
        }


        static void Crypt(bool encrypt, string rawkeyfile, string rawivfile, string infile, string outfile)
        {
            byte[] key = File.ReadAllBytes(rawkeyfile);
            byte[] iv = File.ReadAllBytes(rawivfile);
            byte[] inbytes = File.ReadAllBytes(infile);
            byte[] outbytes = CryptCTR(encrypt, key, iv, inbytes);

            File.WriteAllBytes(outfile, outbytes);
        }
    }
}
