package com.kogi.secondtry;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.irdeto.media.ActiveCloakAgent;
import com.irdeto.media.ActiveCloakDeviceIdChangedListener;
import com.irdeto.media.ActiveCloakEventListener;
import com.irdeto.media.ActiveCloakEventType;
import com.irdeto.media.ActiveCloakException;
import com.irdeto.media.ActiveCloakLocaleOption;
import com.irdeto.media.ActiveCloakMediaPlayer;
import com.irdeto.media.ActiveCloakOutputRestrictions;
import com.irdeto.media.ActiveCloakUrlType;
import com.kogi.secondtry.data.ACMS_ContentDescriptor;

public class MainActivity extends Activity {

	// Local handles on the agent and media player. These are instantiated
	// each time this activity is created, e.g. every time a video is played.
	private ActiveCloakAgent mActiveCloakAgent = null;
	private ActiveCloakMediaPlayer mActiveCloakMediaPlayer = null;


	public static final boolean DEBUG = true;
	
	// UI components specifying the location where the video will play. These
	// are defined externally in the layout XML file but we keep a local 
	// reference here in order to associate them with the media player.
	private SurfaceView mVideoSurface;
	private FrameLayout mVideoFrame;

	// Increment for each seek, decrement when POSITION_CHANGED event arrives.
	private int mSeeking = 0; 
    // Position to start playback from.
    private int mPositionToRestore = 0;

	// Handler for timer jobs.
	Handler mHandler = new Handler();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		init();
	}
	private void init() {

		mVideoSurface = (SurfaceView) findViewById(R.id.VideoViewOutput_Surface);
		mVideoFrame = (FrameLayout) findViewById(R.id.VideoViewOutput_Frame);
		// Create the agent.
		try {
			mActiveCloakAgent = new ActiveCloakAgent(this,
					new ActiveCloakDeviceIdChangedListener() {

						@Override
						public void deviceIdChanged(
								String previousDeviceId,
								String currentDeviceId) {
						}
					});

			// Create the player
			mActiveCloakMediaPlayer = new ActiveCloakMediaPlayer(
					mActiveCloakAgent);
	
			// Set up the display
			mActiveCloakMediaPlayer.setupDisplay(mVideoSurface,
					mVideoFrame);
			ACMS_ContentDescriptor mContentDescriptor = new ACMS_ContentDescriptor("label" , 
//					"http://www.nasa.gov/multimedia/nasatv/NTV-Public-IPS.m3u8",
					"http://p.ep226990.y.akamaientrypoint.net/ep/226990/grupoune_st30.isml",
//					"http://gusphls-i.akamaihd.net/hls/live/215103/irdeto_1@215103/1_index.m3u8",
//					"http://iis7test.entriq.net/ActiveCloak/Encrypted/Bunny.m3u8.prdy",
					ActiveCloakUrlType.HLS, false
					);

			long totalContentLength = 0;
			
			// Open the media player. 
			mActiveCloakMediaPlayer.open(
					new ACMS_SendUrlRequestListener(
							null, mActiveCloakAgent, this),
					new MyActiveCloakEventListener(mContentDescriptor, this), 
					mContentDescriptor.getType(), 
					mContentDescriptor.getUrl(),
					null,
					mPositionToRestore,
					totalContentLength
					);
			
		} catch (ActiveCloakException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void writeToLog(String log) {
		Log.d("this activity", log);
	}

	private void writeToErrorLog(String log) {
		Log.e("this activity", log);
	}
	// Common implementation of ActiveCloakEventListener 
	protected static class MyActiveCloakEventListener implements
			ActiveCloakEventListener {

		// Local handles on the content being played and the calling activity, for
		// logging purposes.
		private ACMS_ContentDescriptor content;
		private MainActivity activity;
		
		// The last event type received.
		private ActiveCloakEventType lastEventType;
		
		MyActiveCloakEventListener(ACMS_ContentDescriptor content, MainActivity activity) {
			this.content = content;
			this.activity = activity;
		}

		@Override
		public void onEvent(ActiveCloakEventType eventType, long result1,
				long result2, long result3, String resultString) {

			// When DRM decryption fails once it tends to fail over and over. To prevent 
			// flooding the log, ignore any consecutive event notifications of this type.
			if( lastEventType == ActiveCloakEventType.DRM_DECRYPT_FAILED
					&& eventType == ActiveCloakEventType.DRM_DECRYPT_FAILED) {
				return;
			}
	
			lastEventType = eventType;
			
			// Log the event.
			activity.writeToLog("Event triggered for "
					+ content.getLabel() + ": " + eventType.toString() + "\n");
						
			if (eventType == ActiveCloakEventType.LICENSE_UPDATE_FAILED) {
				try {
					if (activity.mActiveCloakMediaPlayer.isOpen()) {
						activity.mActiveCloakMediaPlayer.close();
					}
				} catch (ActiveCloakException e) {
					activity.writeToErrorLog(
							"An ActiveCloakException was thrown by the PlayerActivity.\n"
									+ "Result = " + e.getResult() + "\n"
									+ "Message = " + e.getMessage());
				}
			} else if (eventType == ActiveCloakEventType.POSITION_CHANGED) {
				activity.mSeeking = 0; // We are no longer seeking.
			} else if (eventType == ActiveCloakEventType.PROVISION_NEEDED) {
				// Nothing, for now.
			} else if( eventType == ActiveCloakEventType.MULTIPLE_AUDIO_STREAMS) {
				try {
					// If multiple audio options exist, add the Audio button to the 
					// viewable screen.
					List<ActiveCloakLocaleOption> audioOptions = activity.mActiveCloakMediaPlayer.getAvailableAudioOptions();
					activity.writeToLog( "Number of audio options = " + audioOptions.size() + "\n");
					for( ActiveCloakLocaleOption audioOption : audioOptions) {
						activity.writeToLog( "Audio Option: " + audioOption.getLanguageName() + "\n");
					}
					if( activity.mActiveCloakMediaPlayer.getAvailableAudioOptions().size() > 1 ) {
						// UI updates have to run on the UI thread.
						activity.runOnUiThread(new Runnable() 
						{
							public void run() 
							{ 
//								Button audioButton = (Button) activity.findViewById(R.id.controlsAudioButton);
//								audioButton.setVisibility(Button.VISIBLE);
							}
						}); 
					}
				} catch( ActiveCloakException e ) {
					activity.writeToErrorLog(
							"An ActiveCloakException was thrown by the PlayerActivity.\n"
									+ "Result = " + e.getResult() + "\n"
									+ "Message = " + e.getMessage());
				}
			} else if( eventType == ActiveCloakEventType.MULTIPLE_SUBTITLE_STREAMS) {
				try {
					// If multiple subtitle options exist, add the Subs button to the 
					// viewable screen.
					List<ActiveCloakLocaleOption> subtitleOptions = activity.mActiveCloakMediaPlayer.getAvailableSubtitleOptions();
					activity.writeToLog( "Number of subtitle options = " + subtitleOptions.size() + "\n");
					for( ActiveCloakLocaleOption subtitleOption : subtitleOptions) {
						activity.writeToLog( "Subtitle Option: " + subtitleOption.getLanguageName() + "\n");
					}
					if( activity.mActiveCloakMediaPlayer.getAvailableSubtitleOptions().size() > 1 ) {
						// UI updates have to run on the UI thread.
						activity.runOnUiThread(new Runnable() 
						{
							public void run() 
							{ 
//								Button subsButton = (Button) activity.findViewById(R.id.controlsSubtitleButton);
//								subsButton.setVisibility(Button.VISIBLE);
							}
						}); 
					}
				} catch( ActiveCloakException e ) {
					activity.writeToErrorLog(
							"An ActiveCloakException was thrown by the PlayerActivity.\n"
									+ "Result = " + e.getResult() + "\n"
									+ "Message = " + e.getMessage());
				}
			} else if( eventType == ActiveCloakEventType.PLAYBACK_COMPLETED ) {
				activity.exitWithMessage("Playback complete."); 			
			} else if( eventType == ActiveCloakEventType.EXTERNAL_DISPLAY_RESTRICTION_CHANGED) {
				if( DEBUG ) {
					try {
						ActiveCloakOutputRestrictions opl = activity.mActiveCloakMediaPlayer.getOutputRestrictions();
						activity.writeToLog("New OPL Levels: " +
						" AV = " + opl.getAnalogVideo() +
						" CDA = " + opl.getCompressedDigitalAudio() +
						" CDV = " + opl.getCompressedDigitalVideo() +
						" UDA = " + opl.getUncompressedDigitalAudio() +
						" UDV = " + opl.getUncompressedDigitalVideo() + "\n");
					} catch (ActiveCloakException e1) {
						activity.writeToLog("Error trying to get new OPL levels.\n");
					}
				}
			} else if( eventType == ActiveCloakEventType.HOST_NOT_FOUND_ERROR ) {
				// Handling of events that are fatal to playback.
				activity.exitWithMessage("Playback failed due to event: " + eventType.toString());
			}
		}
	}
	
	private void exitWithMessage(final String message) {
		final Activity activity = this;
		runOnUiThread(new Runnable() 
		{
			public void run() 
			{ 
				Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
			}
		}); 
		writeToLog("Player exit message: " + message);
		// Create a task that will finish the activity since playback is done.
		Runnable finishActivityTask = new Runnable() 
		{
			public void run() 
			{ 	
				finish();
			}
		}; 
		// Schedule the task to run in one second. This is less abrupt than closing right away.
		mHandler.postAtTime(finishActivityTask, SystemClock.uptimeMillis() + 1000);
	}

	
	/*
	 * This is called when the Back button is hit or when the minimized activity
	 * is destroyed by the Android OS.
	 * 
	 * (non-Javadoc)
	 * @see android.app.Activity#onDestroy()
	 */
	@Override
	public void onDestroy() {
		
		// Close the media player.
		closeCommon();
		
//        if( mBoundToLicenseManagerService) {
//        	unbindService(mLicenseManagerConnection);
//        	mBoundToLicenseManagerService = false;
//        }
//        if( mBoundToDownloaderService ) {
//        	unbindService(mDownloaderConnection);
//        	mBoundToDownloaderService = false;
//        }
        mHandler.removeCallbacksAndMessages(null);

		super.onDestroy();
	}
	
	/**
	 * Common code for closing the media player, and logging the performance information.
	 */
	private void closeCommon() {
		if (mActiveCloakMediaPlayer.isOpen()) {
			try {
				// Close the media player.
				mActiveCloakMediaPlayer.close();
				
				// Make the video frame and surface visible. If this is not done
				// the video is not visible.
				mVideoFrame.setVisibility(View.INVISIBLE);
				mVideoSurface.setVisibility(View.INVISIBLE);
			
//				if( PERFORMANCE_LOGGING ) {
//					// Write performance information to the log. This can be skipped in
//					// cases where the information is not useful.
//					mActiveCloakAgent.dumpPerfReport();
//					InputStream is = openFileInput("cwsperf.log");
//					byte[] buffer = new byte[is.available()];
//					is.read(buffer);
//					String bufferString = new String(buffer);
//					writeToLog("Performance report for: "
//							+ mContentDescriptor.getLabel() + "\n" + bufferString
//							+ "\n\n\n");
//				} 
			} catch (ActiveCloakException e) {
//				UIUtils.showDialog(this, 
						writeToErrorLog("ActiveCloakException thrown trying to close the media player.");
//			} catch (FileNotFoundException e) {
////				UIUtils.showDialog(this, 
//						writeToErrorLog("FileNotFoundException thrown trying to retrieve performance information." );
//			} catch (IOException e) {
////				UIUtils.showDialog(this, 
//						writeToErrorLog("IOException thrown trying to retrieve performance information.");
			}
		}
	}
}
