package com.kogi.secondtry;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.ByteArrayBuffer;

import android.content.Context;
import android.util.Log;

import com.irdeto.media.ActiveCloakAgent;
import com.irdeto.media.ActiveCloakException;
import com.irdeto.media.ActiveCloakSendUrlRequestListener;
import com.irdeto.media.ActiveCloakSendUrlRequestReason;

/**
 * Common implementation of ActiveCloakSendUrlRequestListener for multiple
 * cases. Performs HTTPS license acquisition request, with the ability to
 * override the license url. Use this as a sample for your own implementation.
 * 
 * @author irdetodev
 *
 */

public class ACMS_SendUrlRequestListener implements
		ActiveCloakSendUrlRequestListener {
	String licenseOverrideUrl = null;
	ActiveCloakAgent agent = null;
	Context context = null;
	
	public ACMS_SendUrlRequestListener(
			String licenseOverrideUrlInput,
			ActiveCloakAgent agent,
			Context context) {
		licenseOverrideUrl = licenseOverrideUrlInput;
		this.agent = agent;
		this.context = context;
	}

	@Override
	public boolean sendUrlRequest(ActiveCloakSendUrlRequestReason reason,
			String contentType, String url, byte[] challenge) {
		
		// Allow the test application to override the License URL
		if (licenseOverrideUrl != null) {
			url = licenseOverrideUrl;
		}

		// The response stored from the license server
		byte[] response = null;

		try {
			HttpPost post = new HttpPost(url);

			// Break out all extra HTTP header lines and add it to the HttpPost object
			for (String line : contentType.replace("\r", "\n").split("\n")) {
				if (line.length() > 0 && line.contains(":")) {
					String[] parts = line.split(":", 2);
					if (parts.length == 2) {
						post.addHeader(parts[0].trim(), parts[1].trim());
					}
				}
			}

			// Create a byte array entity for the POST data, the content
			// type here is only used for the postEntity object
			ByteArrayEntity postEntity = new ByteArrayEntity(challenge);
			postEntity.setContentType("application/octet-stream");
			post.setEntity(postEntity);

			// Create a HttpClient and execute the HttpPost filled out above
			HttpClient client = new DefaultHttpClient();
			HttpResponse httpResponse = client.execute(post);

			// Get the response entity out of the response
			HttpEntity entity = httpResponse.getEntity();
			
			// Stringbuilder for logging.
			StringBuilder sb = new StringBuilder();
			
			StatusLine statusLine = httpResponse.getStatusLine();
			boolean errorState = statusLine.getStatusCode() >= 400 && statusLine.getStatusCode() < 600;
			
			if( errorState ) {				
				sb.append("Received Http Status Code: ");
				sb.append(statusLine.getStatusCode());
				sb.append("\nFull Http Response: \n");
			}
			

			// If the entity is set, grab the data out of it and
			// add it to the response byte array
			if (entity != null) {
				
				InputStream stream = entity.getContent();

				ByteArrayBuffer baf = new ByteArrayBuffer(50);
				int current = 0;
				while ((current = stream.read()) != -1) {
					baf.append((byte) current);
					if( errorState ) {
						sb.append((char)current);
					}
				}
				if( errorState ) {
					Log.e("TAG", sb.toString());
//					ACMS_LogService.writeToLog(context, sb.toString());
				}

				response = baf.toByteArray();
				baf.toString();
				
			}
		} catch (IOException ex) {
			// If there's an exception, just reset the array to a known state
			response = null;
		}

		boolean ret = false;

		try {
			// Pass the response into processUrlResponse, even if it's null.
			// Please Note: This is required by the library, processUrlResponse must be called.
			agent.processUrlResponse(response);
			ret = true;
		} catch (ActiveCloakException e) {
			// Nothing to do
		}

		return ret;
	}
}
